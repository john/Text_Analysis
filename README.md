# Text Analysis using R

Instructor:  Mark Yacoub
Duke Population Research Institute
April 4, 2018

These materials accompany the DuPRI Text Analysis training. If you have any questions, contact Mark Yacoub at <mark.yacoub@duke.edu>.

* text_analysis.R - R script version of training code
* text_analysis.Rmd - Rmarkdown HTML version of training code
* text_analysis.html - HTML report of training for reference
* Text Analysis.Rproj - RStudio project file
* DailyKos_health.csv - Daily Kos blog post data
* NYT - Contains _New York Times_ health care articles
* topic_model.RData - RData file containing topic model
* topic_number_diagnostics.RData- RData file containing diagostics to determine number of topics for topic model
