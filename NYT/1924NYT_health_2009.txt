                   Copyright 2010 The New York Times Company


                             1924 of 2021 DOCUMENTS


                               The New York Times

                             April 23, 2010 Friday
                              Late Edition - Final

Elements of Style

BYLINE: By WILLIAM YARDLEY

SECTION: Section A; Column 0; National Desk; Pg. 14

LENGTH: 1001 words


JUNEAU, Alaska -- The fresh-faced governor of Alaska is jabbing at the federal
government over polar bears and health care, urging the Obama administration to
expand oil exploration and taking swipes on Facebook at fellow Republicans in
the Legislature.

While the script might feel familiar, why does virtually everyone here in the
state capital say that Gov. Sean Parnell, party affiliation and initials aside,
could not be less like the phenomenon he followed?

''A lot of people have described him as mush,'' said State Senator  John
Coghill, a Republican from Fairbanks. ''I think it's just that he's a
gentleman.''

Sarah Palin, Mr. Parnell's predecessor as governor, introduced herself to
America in 2008 as a pit bull in lipstick, and then she attacked. The same
summer, the mild-mannered Mr. Parnell was called ''Captain Zero'' by a campaign
opponent -- then he took more heat for not fighting back.

Now, even as he echoes many of Ms. Palin's positions and has tentatively tweaked
lawmakers (''they should spend less!'' he wrote on Facebook), it is clear that
Mr. Parnell's  political playbook  is different from Sarah Barracuda's.

''I've never been about standing up and just yelling for the sake of yelling,''
Mr. Parnell said in an interview in his Capitol office. ''That's not who I am.
I'm about getting the job done.''

The question now, as Mr. Parnell graduates from his first legislative session as
governor, is whether being a gentleman -- or mush, depending on the point of
view -- will get him elected to a term of his own in November.

''We pick risk-takers,'' Senator Lesil McGuire, a Republican from Anchorage,
said in describing the kind of governors Alaskans have elected since statehood
in 1959. ''We select strong people.''

Facing a crowded field of Republican challengers, Mr. Parnell, 47, is regarded
as the front-runner. Yet some see vulnerabilities.

In 2008, Mr. Parnell, then lieutenant governor, was thought to be well
positioned to unseat Representative Don Young, the state's sole member of the
House for more than 35 years. Mr. Young was mired in a federal investigation at
the time, and Ms. Palin, at the peak of her popularity, had openly criticized
him.

It was in the Republican primary that Mr. Young called Mr. Parnell Captain Zero.
Mr. Parnell ended up losing by 304 votes.

The perception of weakness is a favorite theme of opponents. Mr. Parnell's most
prominent Republican challenger, Ralph Samuels, a former state senator who has
strong allies in the oil industry, recently accused him of not doing enough to
fight a federal health care overhaul. That was before Alaska announced this week
that it would join other states in suing the federal government over the
changes.

''I'm a guy who does my homework,'' the governor said at a candidate forum.
''That's what distinguishes me from the rest of these folks.''

Mr. Parnell clearly benefited from Ms. Palin's popularity in Alaska and beyond;
he is in office now because she resigned last summer. Yet the stream of
attention and controversy that has surrounded Ms. Palin has also exhausted many
people in Alaska. Mr. Parnell, bland and by-the-book by comparison, stands to
benefit this time because he is different.

Alaska endured a wide-ranging federal investigation that sent several state
lawmakers to jail and, in 2008, led to the conviction of the state's longtime
political lion, Senator Ted Stevens, a Republican. Mr. Stevens lost his bid for
re-election that year, even though the guilty verdict was later thrown out.

Few expect Mr. Parnell to stir up much controversy.

''I don't think in his lifetime we'll ever see anything worthy of an inquiry,''
Ms. McGuire said of Mr. Parnell.

It helps Mr. Parnell that boom-and-bust Alaska is in remarkably strong shape
because of surging oil prices. Lawmakers here are arguing over how much to
spend, not cut. The state is projected to have more than $12 billion in savings
this year.

Still, as the legislative session came to a close Monday, Mr. Parnell could not
claim a resounding victory for his  agenda. Lawmakers  ignored his call for
restraint by passing more than $3 billion worth of capital spending projects.

''They're kicking sand in his face, seeing if he's going to muscle up,'' said
Senator Con Bunde, a Republican and the only one of the state's 20 senators to
vote against the capital budget.

Ms. Palin infuriated lawmakers by vetoing hundreds of millions of dollars in
local projects. During one prickly round of budget negotiations, she suggested
that she was the only ''adult in the house.''

Lawmakers often noted that Ms. Palin had no legislative experience. Mr. Parnell,
who became one of the top budget brokers in eight years in the State
Legislature, has made clear that he will also veto projects, but he has done so
without making things personal.

''I can make a phone call and walk upstairs and talk to the governor,'' said
Representative Mike Chenault, a Republican and the House speaker. ''That's the
way it should be. It's dramatically different.''

Mr. Parnell is careful not to criticize Ms. Palin or even compare himself with
her. He praised her efforts toward developing a natural gas pipeline from
Alaska's North Slope (it remains far from certain whether one will be built) and
said he learned how to communicate more effectively by watching her. Ms. Palin
did not respond to requests for comment.

Mr. Parnell said he talked with Ms. Palin periodically, but not about state
business. That is more contact than others in the Capitol have had with the
former governor.

''We watch her on TV,'' said Representative Carl Gatto, a Republican who
represents Wasilla, Ms. Palin's hometown. ''She's awfully good at those
microphones.''

Even as some lawmakers were skeptical of Mr. Parnell, none said they missed Ms.
Palin.

''The best thing she ever did was resign,'' said Senator Bert Stedman, a
Republican from Sitka and a major budget negotiator. ''We're a lot better off,
and she's better off. Everybody wins.''

URL: http://www.nytimes.com

LOAD-DATE: April 23, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: Sean Parnell: AGE: 47 PARTY: Republican STATE OF BIRTH:
California FAMILY: Married, two children FAMILY RESIDENCE WHILE GOVERNOR: Juneau

children home-schooled APPOINTEE REJECTED BY LEGISLATURE: Al Barrette for the
state Board of Game. Mr. Barrette, a fur tanner, was reportedly the first person
in the state to receive a permit to hunt wolves from the air LEGISLATIVE
ACCOMPLISHMENTS: Measures to reduce taxes on cruise ship passengers and expand
tax credits for oil and gas exploration
increased protections against domestic violence and sexual assault FACEBOOK:
People who say they like Mr. Parnell's page: 1,911 (PHOTOGRAPH BY MICHAEL PENN
FOR THE NEW YORK TIMES)
 Sarah Palin: AGE: 46 PARTY: Republican STATE OF BIRTH: Idaho FAMILY: Married,
five children, one grandchild FAMILY RESIDENCE WHILE GOVERNOR: Commuted between
Juneau and Wasilla APPOINTEE REJECTED BY LEGISLATURE: Wayne Anthony Ross as
state attorney general. Mr. Ross, a board member of the National Rifle
Association, had written that homosexuals were ''degenerates'' LEGISLATIVE
ACCOMPLISHMENT:S Raised taxes on oil production, and pushed through a bill to
expedite the construction of a natural gas pipeline, though it remains uncertain
that it will be built FACEBOOK: People who say they like Ms. Palin's page:
1,535,901 (PHOTOGRAPH BY BRIAN SNYDER/REUTERS)

PUBLICATION-TYPE: Newspaper


