                   Copyright 2009 The New York Times Company


                             798 of 2021 DOCUMENTS


                               The New York Times

                            October 11, 2009 Sunday
                              Late Edition - Final

Obama Cites G.O.P. Officials in Call to Action on Bill

BYLINE: By SHERYL GAY STOLBERG

SECTION: Section A; Column 0; National Desk; Pg. 28

LENGTH: 577 words

DATELINE: WASHINGTON


President Obama said Saturday that he saw an ''unprecedented consensus'' around
Congressional plans to overhaul the nation's health care system, citing the
recent statements of a string of Republicans -- most of them former elected
officials -- who have issued their own calls to action.

''The distinguished former Congressional leaders who urged us to act on health
insurance reform spoke of the historic moment at hand and reminded us that this
moment will not soon come again,'' Mr. Obama said in his weekly address to the
nation. ''They called on members of both parties to seize this opportunity to
finally confront a problem that has plagued us for far too long.''

The White House has been pressing Republicans who are in favor of passing health
legislation to issue the statements and circulating them to reporters. On
Saturday, a few more added their names to a list that already included Bill
Frist and Bob Dole, both former Senate Republican leaders; Tommy Thompson and
Louis Sullivan, both former health secretaries; and Gov. Arnold Schwarzenegger
of California.

Four former surgeons general -- two who served Democrats and two who served
Republicans -- issued a statement that stressed the importance of lowering
health costs and improving preventive care and said the ''approaches that
Congress is considering will help achieve these goals.''

And Chuck Hagel, the former senator from Nebraska, wrote: ''Congress and the
administration are working on bipartisan, practical solutions to improve our
health care system. I urge all members of Congress to put aside their narrow
partisan differences and seize this moment for health care reform.''

The White House effort has irked Republican leaders in Congress, who argue that
the statements are bland expressions of support issued by those who don't have
to vote on the particulars. A spokesman for Senator Mitch McConnell, the
Republican leader, said in an e-mail message on Saturday that Mr. McConnell
himself had given 44 speeches on the need for health reform; the spokesman, Don
Stewart, said that some of the Republicans whose remarks the White House had
been heralding had also said they would not vote for the bills currently pending
in Congress.

Even so, the tactic appears to be picking up steam. The Democratic National
Committee is capitalizing on it, and has put together a cable television
advertisement featuring the statements of Mr. Frist, Mr. Thompson and Mr. Dole.

While the bills moving through Congress contain some Republican ideas, they are
attracting little, if any, Republican support. In the Senate, the Finance
Committee will vote Tuesday on what is regarded as the most bipartisan of the
bills, yet only one Republican on the panel, Senator Olympia J. Snowe of Maine,
has said she is considering voting for it. And even Ms. Snowe has not made up
her mind.

In his address, Mr. Obama called the finance bill ''another milestone on what
has been a long, hard road toward health insurance reform.'' He said the
approach emerging ''includes the best ideas from Republicans and Democrats, and
people across the political spectrum.''

The bill does have one thing in its favor: the nonpartisan Congressional Budget
Office issued an analysis this week that found that it would reduce the federal
deficit by $81 billion over 10 years. Thus it meets a key criterion set forth by
Mr. Obama, who has said he will not sign any legislation that ''adds one dime''
to the deficit.

URL: http://www.nytimes.com

LOAD-DATE: October 11, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


