                   Copyright 2010 The New York Times Company


                             1704 of 2021 DOCUMENTS


                               The New York Times

                             March 21, 2010 Sunday
                              Late Edition - Final

Point of (Dis)order

BYLINE: By ADAM NAGOURNEY

SECTION: Section WK; Column 0; Week in Review Desk; Pg. 1

LENGTH: 1201 words


WASHINGTON -- You could forgive Americans for being a little confused. At a
moment when Congress is engaged in a crucial debate about overhauling the health
care system, the talk from Washington is about self-executing rules, deem and
pass, reconciliation, the Slaughter Rule, preliminary C.B.O. scores, final
C.B.O. scores -- not to mention filibusters, cloture votes, the Byrd Bath and
supermajorities.

At the end of the day, it is fair to wonder whether Americans even care about
these exhausting debates, much less follow them.

''I don't think procedural stuff really resonates with most Americans,'' said
Tom Daschle, the former Senate majority leader. ''It may add generally to their
cynicism, but it is accomplishment -- or lack of it -- that matters much more.''

Perhaps. Yet this yearlong debate  may test the proposition that no one outside
this city cares how the sausage is made. Indeed, as the midterm elections
approach, Republicans are betting that process matters. A central part of their
strategy has been to tangle the legislative works, resulting in both sides'
resorting to the most arcane legislative maneuvers, displaying sausage-making at
its grubbiest.

''It just seems to me that people are really in tune with what is happening
now,'' said Senator Scott Brown, the Massachusetts Republican whose election in
January suggested the depth of anti-Washington sentiment. ''People are informed,
and they are angry. They want this kind of political chicanery and the
parliamentary maneuvers to stop.''

Mr. Brown was referring to House Democrats who were moving to pass the Senate
health care bill over the weekend with a deem-and-pass maneuver, which means
they would be voting on fixes to the Senate bill after agreeing that the vote
would also serve to pass the Senate bill itself, something many Congressional
Democrats were loath to do. (Got that?) Indeed, Democrats on Saturday dropped
the deem-and-pass idea, presumably figuring that it might have been one
legislative maneuver too many.

But Democrats are hardly the only ones delving into the footnotes of the rule
book. Republicans -- who have managed to lock down the Senate for much of the
year with the threat of a filibuster -- are prepared to strike out core
provisions in the final legislation, by proposing an array of time-consuming
amendments, and employing parliamentary challenges.

Polls suggest that Americans are acutely interested in the health care bill
itself, but are not aware, and not really following, the arcane battling that
has framed it. A Wall Street Journal/NBC News poll found last week that 40
percent of respondents said they had heard ''not much'' or ''nothing at all''
about filibusters lately.

''The American public doesn't follow the ins and outs of political process,''
said Andrew Kohut, the president of the Pew Research Center. ''You don't find a
lot of people who know about this. And the ones that do, if you pushed them on
it, I bet you'd get a lot of misinformation.''

Does anyone really care if the bill is posted on the Internet 72 hours before
the vote? Or if Mr. Obama never fulfilled his pledge to conduct legislative
negotiations in public? Or if a bill is passed with a simple majority or the 60
votes required to overcome a filibuster?

''And reconciliation?'' said Senator Evan Bayh, the Indiana Democrat. ''Hah!
Only in Washington could the word 'reconciliation' be so divisive.''

Democrats are correct that historically, at least, voters have little interest
in process, particularly when the legislative stakes are this high. Mr. Obama,
in an interview with Fox News last week, made that point as his interviewer
pressed him on procedural maneuvers by Congressional Democrats.

''The reason that I think this conversation ends up being a little frustrating
is because the focus entirely is on Washington process,'' Mr. Obama said. ''And,
yes, I have said it, that is an ugly process. It was ugly when Republicans were
in charge, it was ugly when Democrats were in charge.'' Yet in many ways, times
are different.

For one thing, there has rarely been a legislative debate like this one --
stretching over more than a year, echoing from the Capitol and the White House
to town halls, radio shows and cable stations. In this new world, anyone who
wants to follow the debate can; there seems to be an infinite number of
platforms serving up detailed parliamentary motion-to-motion accounts of what's
taking place. In many ways, the health care battle has turned into a grand, if
not entirely inspiring, civics lessons on Congressional procedure.

The spotlight has not been particularly helpful. A deal struck by the Senate
majority leader, Harry Reid, last December with Senator Ben Nelson of Nebraska
-- an only-for-Nebraska increase in Medicaid money -- looked shrewd at the time,
helping the Democrats win the 60 votes needed to defeat a filibuster. But the
agreement became a symbol of a system gone wrong, a rallying cry for the Tea
Party movement. Not incidentally, as part of reconciliation, the money is likely
to be eliminated from the legislation.

For the midterm elections, the American voters who don't care about process are
less important than the base voters in both parties who do care. Those voters,
Mr. Kohut said, are the ones who go to the polls, and they don't like what
they've witnessed.

In the fall, Republicans may find that these voters respond to rallying cries
like ''reconciliation'' and ''deem and pass'' as they once did to ''gay
marriage'' and ''abortion.''

''When it comes to this election, Democrats are going to pay a price,'' said Tim
Pawlenty, the Republican governor of Minnesota and a probable candidate for
president. ''People's memories are not what we would like them to be. But the
intensity here is so great, I think it's going to last until the fall.''

On the other hand, Democrats are exercised by the Republican use of the
filibuster which, in the view of the left, helped produce a greatly watered-down
health care bill.

And from the Democrats' point of view, given these superaccelerated times, it's
an open question whether anyone will be talking about deem-and-pass come the
fall.

''Real people don't care about this stuff,'' Mr. Bayh said. ''It's the
glorification of form over substance.''

The president himself might find his own fortunes boosted by his persistence on
the health care bill, particularly among Democrats who had thought Mr. Obama
showed no taste for battle until now. Of course, while that boost might help Mr.
Obama in 2012, it does not do a lot for Democratic Congressional candidates
later this year.

One thing can be counted on: This gamesmanship is unlikely to produce a spike in
Congress's already historically low favorability ratings.

''I think the public can't stand Washington games,'' said Joe Trippi, a
Democratic consultant. ''All this procedural stuff is just making them more
convinced that they're right -- that Washington is a disaster.''

And as Congress prepared to slog through another weekend of convoluted
legislative maneuvering, Mr. Trippi was not alone in suggesting that a backlash
to Washington's business methods could splatter members of both parties this
November.

URL: http://www.nytimes.com

LOAD-DATE: March 21, 2010

LANGUAGE: ENGLISH

GRAPHIC: DRAWINGS (DRAWINGS BY HARRY CAMPBELL) (WK1
 WK4)

PUBLICATION-TYPE: Newspaper


