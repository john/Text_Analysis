                   Copyright 2010 The New York Times Company


                             1679 of 2021 DOCUMENTS


                               The New York Times

                            March 18, 2010 Thursday
                              Late Edition - Final

The Health Debate: Crunch Time

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 30

LENGTH: 555 words


To the Editor:

Re ''As Medicaid Payments Shrink, Patients Are Abandoned'' (front page, March
16):

The access woes of Michigan Medicaid patients illustrated so movingly in your
article are reverberating nationwide and are a real problem for physicians and
their patients. It's critical that Congress raise Medicaid physician payment
rates to at least Medicare levels, but Medicare payment rates are also a serious
concern.

On April 1, Medicare will automatically cut physician payment rates by 21
percent -- with more cuts in years to come. As practice costs continue to
increase, physicians tell the American Medical Association that this cut will
have a serious effect on their ability to care for the elderly who rely on
Medicare.

A new informal survey found that about two-thirds of physicians will limit the
number of Medicare patients they treat if the cut takes place. The Senate must
grab the bull by the horns and take decisive action to repeal the broken payment
formula that causes these steep cuts.

If it continues simply to delay the cut for a few more months -- or even a year
-- the ramifications of its inaction will be felt by Medicare patients as
physicians find that, just as with Medicaid, they can no longer afford to keep
the practice doors open to all patients.

J. James Rohack

Chicago, March 16, 2010

The writer, a doctor, is president of the American Medical Association.

To the Editor:

Regarding your articles about health care reform: Who's talking for me? I have
had enough of having my interests be misrepresented by political leaders,
industry leaders and the media.

I don't recognize myself, my life, my family or my values in the discussions
taking place in the public square.

So I write to offer the following: I want health care reform. I want my fellow
United States residents to have access to health care. I believe that providing
basic health coverage for us all is a defining element of a 21st-century fair
and just society.

I am prepared for the sacrifices required. To my political representatives: Do
you hear me? Jill Blair

Seattle, March 13, 2010

To the Editor:

If Democrats are damned if they do and damned if they don't pass the health care
bill, as many suggest is their choice now, they might as well provide health
care to millions of Americans who currently don't have it.

Matthew Roberts Clark

Portland, Ore., March 14, 2010

To the Editor:

Re ''In Ohio, Obama Makes a Working-Class Appeal for Health Care Votes'' (news
article, March 16):

President Obama is resorting to Natoma Canfield's sad story and saying that
''every argument has been made'' because he has not answered so many arguments
against his plan. How, for example, when an insurance mandate has failed
spectacularly in Massachusetts, will it work times 50?

I, too, feel sorry for Ms. Canfield, an uninsured woman with leukemia. But that
does not justify brushing aside the facts and increasing government intervention
in health care.

In fact, it is government intervention that places individually purchased
insurance out of Ms. Canfield's price range in the first place.

Feelings of pity, however strong, must not get in the way of a reasoned debate.
The only way to help Ms. Canfield and other Americans get health care is a fully
free market. Stella Zawistowski

New York, March 16, 2010

URL: http://www.nytimes.com

LOAD-DATE: March 18, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


