                   Copyright 2009 The New York Times Company


                              13 of 2021 DOCUMENTS


                               The New York Times

                              June 5, 2009 Friday
                              Late Edition - Final

Keeping Them Honest

BYLINE: By PAUL KRUGMAN

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 23

LENGTH: 808 words


''I appreciate your efforts, and look forward to working with you so that the
Congress can complete health care reform by October.'' So declared President
Obama in a letter this week to Senators Max Baucus and Edward Kennedy. The big
health care push is officially on.

But the devil is in the details. Health reform will fail unless we get serious
cost control  --  and we won't get that kind of control unless we fundamentally
change the way the insurance industry, in particular, behaves. So let me offer
Congress two pieces of advice:

1) Don't trust the insurance industry.

2) Don't trust the insurance industry.

The Democratic strategy for health reform is based on a political judgment: the
belief that the public will be more willing to accept reform, less easily
Harry-and-Louised, if those who already have health coverage from private
insurers are allowed to keep it.

But how can we have fundamental reform of what Mr. Obama calls a ''broken
system'' if the current players stay in place? The answer is supposed to lie in
a combination of regulation and competition.

It's a sign of the way the political winds are blowing that insurers aren't
opposing new regulations. Indeed, the president of America's Health Insurance
Plans, the industry lobby known as AHIP, has explicitly accepted the need for
''much more aggressive regulation of insurance.''

What's still not settled, however, is whether regulation will be supplemented by
competition, in the form of a public plan that Americans can buy into as an
alternative to private insurance.

Now nobody is proposing that Americans be forced to get their insurance from the
government. The ''public option,'' if it materializes, will be just that  -- an
option Americans can choose. And the reason for providing this option was
clearly laid out in Mr. Obama's letter: It will give Americans ''a better range
of choices, make the health care market more competitive, and keep the insurance
companies honest.''

Those last five words are crucial because history shows that the insurance
companies will do nothing to reform themselves unless forced to do so.

Consider the seemingly trivial matter of making it easier for doctors to deal
with multiple insurance companies.

Back in 1993, the political strategist (and former Times columnist) William
Kristol, in a now-famous memo, urged Republican members of Congress to oppose
any significant health care reform. But even he acknowledged that some things
needed fixing, calling for, among other things, ''a simplified, uniform
insurance form.''

Fast forward to the present. A few days ago, major players in the health
industry  laid out what they intend to do to slow the growth in health care
costs. Topping the list of AHIP's proposals was ''administrative
simplification.'' Providers, the lobby conceded, face ''administrative
challenges'' because of the fact that each insurer has its own distinct
telephone numbers, fax numbers, codes, claim forms and administrative
procedures. ''Standardizing administrative transactions,'' AHIP asserted, ''will
be a watershed event.''

Think about it. The insurance industry's idea of a cutting-edge, cost-saving
reform is to do what William Kristol  --  William Kristol!  --  thought it
should have done 15 years ago.

How could the industry spend 15 years failing to make even the most obvious
reforms? The answer is simple: Americans seeking health coverage had nowhere
else to go. And the purpose of the public option is to make sure that the
industry doesn't waste another 15 years --  by giving Americans an alternative
if private insurers fall down on the job.

Be warned, however. The insurance industry will do everything it can to avoid
being held accountable.

At first the insurance lobby's foot soldiers in Congress tried to shout down the
public option with the old slogans: private enterprise good, government bad.

At this point, however, they're trying to kill the public option in more subtle
ways. The most recent ruse is the proposal for a ''trigger''  --  the public
option will only become available if private insurers fail to meet certain
performance criteria. The idea, of course, is to choose those criteria to ensure
that the trigger is never pulled.

And here's the thing. Without an effective public option, the Obama health care
reform will be simply a national version of the health care reform in
Massachusetts:  a system that is a lot better than nothing but has done little
to address the fundamental problem of a fragmented system, and as a result has
done little to control rising health care costs.

Right now the health insurers are promising to deliver major cost savings. But
history shows that such promises can't be trusted. As President Obama said in
his letter, we need a serious, real public option to keep the insurance
companies honest.

URL: http://www.nytimes.com

LOAD-DATE: June 5, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


