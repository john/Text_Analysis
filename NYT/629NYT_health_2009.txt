                   Copyright 2009 The New York Times Company


                             629 of 2021 DOCUMENTS


                               The New York Times

                           September 18, 2009 Friday
                              Late Edition - Final

The Politics of Health Care Reform

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 30

LENGTH: 884 words


To the Editor:

Re ''Senator Unveils Bill to Overhaul U.S. Health Care'' (front page, Sept. 17):

When the health care reform debate began, liberal Democrats wanted to replace
private insurance companies with a single-payer system similar to those used
throughout the civilized world. Republicans, more concerned about insurance
companies' profits than Americans' health, said no.

Then the Democrats offered a compromise, the ''public option.'' They hoped this
would extend affordable coverage to the estimated 47 million uninsured in
America, and that some level of competition would drive down costs. Republicans,
more concerned about insurance companies' profits than Americans' health, said
no.

Now Senator Max Baucus is offering up a weak bill that would establish
''nonprofit insurance cooperatives.'' It is unclear what these cooperatives
would do, and the Congressional Budget Office says they ''seem unlikely to
establish a significant market presence in many areas of the country.'' But even
this tiny crumb offered to the American people has Republicans still saying no.

Politics is the art of compromise, but you cannot compromise with people whose
only goal is to make the sitting president look bad so they can score cheap
political points for the next election. Republicans obviously want no part of
health insurance reform, so why do the Democrats keep reaching out to them? The
same goes for the conservative Blue Dog Democrats.

The Democratic leadership (an oxymoron?) should present a bill that creates a
government-sponsored ''public option'' for an open vote in the Senate. If the
conservatives filibuster, then it will be clear to all Americans what their
priorities really are. After all, if the government is really so bad at running
things, what could private insurers possibly have to fear?

Branden Wolner  Auburn, Mass., Sept. 17, 2009

To the Editor:

Re ''Health Reform's Missing Ingredient,'' by Ron Wyden (Op-Ed, Sept. 17):

Finally someone has got it right. For all the fearful hysteria that the
government will take away our right to choose our own health care plan through a
free-market health insurance system, our current system does not offer much
individual choice.

My insurance company has always been chosen by my employer. What incentive does
''my'' insurance company have to provide high-quality and cost-effective service
to me and my family? Not much.

Senator  Wyden's Free Choice amendment, which would give employees more options,
deserves broad support.

Kathy Sack Solomon  Bronx, Sept. 17, 2009

The writer is a radiologist.

To the Editor:

Re ''The Fading Public Option'' (news analysis, front page, Sept. 13):

I believe that the House will pass a bill with a public option, which is
strongly supported by Speaker Nancy Pelosi. There will be a big media flurry
when it does, so the compromises of ''triggers'' and time delays are viable --
in other words, a public option is not dead by any means.

According to a CBS News poll, public support for the public option rose from 57
percent to 68 percent after President Obama's speech last week.

Americans want real reform to counter the insurance companies' stranglehold,
which makes people in the United States pay almost twice as much as the rest of
the world for medical care while we rank behind 44 other countries in infant
mortality and behind 49 countries in life expectancy.

Moreover, a recent study of private insurers in California showed denial of up
to 40 percent of claims.

The public option would result in true competition and would sharpen up the
insurance companies and the American health system.

Robert S. Weiner  Washington, Sept. 13, 2009

The writer is a former chief of staff for the House Select Committee on Aging
and its Health and Long-Term Care Subcommittee.

To the Editor:

You are certainly correct that the odds are against passage of a public option
in this session of Congress. Perhaps its chances of success would improve if
President Obama proposed to offer the public option to individual states on a
voluntary basis.

There are a number of states, like Michigan, Illinois and Massachusetts, that
might be interested in trying it out. The natural diversity across the country
might allow us to learn which public option models work to stimulate competition
(which is virtually nonexistent in many local markets).

Such a proposal would also avoid having the legislation get bogged down in
defining future events that could ''trigger'' a public option. Who knows, even a
few Republicans might accept this approach the same way they accepted the State
Children's Health Insurance Program, popularly known as S-chip.

Jeffrey Koshel  Washington, Sept. 13, 2009

To the Editor:

What a sorry spectacle it has become to see President Obama zigzagging from left
to right in order to find a center, a futile maneuver that will satisfy nobody.
His base is angry and disillusioned, his adversaries will continue to obstruct
him and his enemies will continue to revile him.

In backing away from the public option, Mr. Obama has changed his legacy from
that of a transformative visionary to that of just one more in the roster of
politicians who have thrown their principles by the wayside for the sake of
expediency.

Judith Zinn  Laurel Hollow, N.Y., Sept. 14, 2009

URL: http://www.nytimes.com

LOAD-DATE: September 18, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY ANDRE DA LOBA)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


