                   Copyright 2009 The New York Times Company


                             182 of 2021 DOCUMENTS


                               The New York Times

                              July 20, 2009 Monday
                              Late Edition - Final

Health Bill Might Direct Tax Money To Abortion

BYLINE: By ROBERT PEAR and ADAM LIPTAK

SECTION: Section A; Column 0; National Desk; Pg. 10

LENGTH: 626 words

DATELINE: WASHINGTON


An Obama administration official refused Sunday to rule out the possibility that
federal tax money might be used to pay for abortions under proposed health care
legislation.

Peter R. Orszag, the White House budget director, asked whether he was prepared
to say that ''no taxpayer money will go to pay for abortions,'' answered: ''I am
not prepared to say explicitly that right now. It's obviously a controversial
issue, and it's one of the questions that is playing out in this debate.''

Senator Judd Gregg, Republican of New Hampshire, who along with Mr. Orszag was
asked about the issue on ''Fox News Sunday,'' said it had the potential to
complicate the legislative battle over health care. ''I would hate to see the
health care debate go down over that issue,'' Mr. Gregg said.

Abortion has been simmering behind the scenes as an issue in legislation to
guarantee access to health insurance for all Americans. The debate affects not
only the public health insurance plan that Democrats want to create, but also
private insurers, who would receive tens of billions of dollars of federal
subsidies to cover people with low and moderate incomes.

Under the House bill, for example, most insurers would have to provide an
''essential benefits package'' specified by the health and human services
secretary, who would receive recommendations from a federal advisory committee.
Opponents of abortion want Congress to prohibit inclusion of abortion in that
benefits package, while advocates of abortion rights say the package should be
left to medical professionals to determine.

In an analysis of the House bill, the National Right to Life Committee said that
ordinary principles of administrative law could allow the Obama administration
to determine what would be included in the benefits package. ''There is no
doubt,'' the group said, ''that coverage of abortion will be mandated, unless
Congress explicitly excludes abortion from the scope of federal authority to
define 'essential benefits.' ''

Even if the health secretary did not require coverage of abortion, the group
said, ''federal courts would interpret the broadly worded mandatory categories
of coverage to include abortion.''

Susan M. Pisano, a spokeswoman for America's Health Insurance Plans, a trade
group, said that most insurance companies offered benefit packages that included
abortion coverage but that many employers decided not to buy such packages.

Douglas D. Johnson, legislative director of the National Right to Life
Committee, said the House bill, like one approved last week by the Senate health
committee, ''would result in the greatest expansion of abortion since Roe v.
Wade,'' the 1973 Supreme Court decision that established a constitutional right
to abortion.

In the three House committees that approved comprehensive health care bills last
week, Republicans tried unsuccessfully to restrict coverage of abortion.

Since 1976, Congress has imposed sweeping restrictions on the use of federal
money for abortions. The Hyde Amendment, for instance, prohibits the Medicaid
program from spending federal money on most abortions.

But opponents of abortion said the proposed federal subsidies to be provided for
coverage of the uninsured would not be subject to these restrictions.

Kathleen Sebelius, the health and human services secretary, was asked about the
issue in April when her nomination was being considered by the Senate Finance
Committee.

''Most private plans do not cover abortion services except in limited instances,
but do cover family planning,'' Ms. Sebelius said. ''And Congress has limited
the Federal Employee Health Benefit plan to covering abortion services only in
cases of rape or incest, or when the life of the mother is in danger.''

URL: http://www.nytimes.com

LOAD-DATE: July 20, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


