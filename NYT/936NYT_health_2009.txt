                   Copyright 2009 The New York Times Company


                             936 of 2021 DOCUMENTS


                               The New York Times

                           November 5, 2009 Thursday
                              Correction Appended
                              Late Edition - Final

To Their Own Devices

BYLINE: By BARRY MEIER

SECTION: Section B; Column 0; Business/Financial Desk; THE WORK-UP; Pg. 1

LENGTH: 1602 words


When makers of heart defibrillators wanted Medicare to vastly expand the types
of patients eligible to receive the devices, which can cost upward of $25,000,
agency officials were skeptical. It was not clear how many of those patients
would actually need a defibrillator, a device that can deliver a life-saving
shock to restore a faltering heart to normal rhythm.

So government and industry struck a deal back in 2004. Medicare agreed to expand
the device's use, nearly doubling the number of patients who qualified for one.
The companies, in return, agreed to pay for a study to see which patients really
benefited.

Five years later, Medicare underwrites more than half of the $4 billion the
nation now spends annually on defibrillators, but the agency is no closer to
knowing how many lives that big investment is saving. That is because the device
companies did not finance the study beyond their initial $4 million commitment,
and Medicare did not pick up the slack. As a result,  researchers  still cannot
gather data that would identify the types of patients who would most benefit
from a defibrillator.

And so, doctors keep implanting costly defibrillators in patients who may not
benefit from them. And doctors and patients have no way of knowing whether one
producer's model performs better than a competitor's.

The picture is no clearer for the many other types of medical devices that
taxpayers, through government-run programs like Medicare, underwrite. Every
year, for instance, doctors give patients tens of thousands of artificial hips
and knees, without having the data to indicate how long they will last or which
ones work best, and Medicare picks up the bills.

As Congress seeks to revamp the nation's health care system, medical devices
might seem an inviting target to better control Medicare spending. Outlays on
implanted devices stand at about $76 billion annually in this country and are
rising at a rate faster than the cost of drugs, according to a recent study by
the McKinsey Global Institute, a consulting group. With an aging population in
America, Medicare is picking up more of those costs.

But legislation pending in the House and Senate may not help, some experts say,
because the proposals do not require device makers to compete on the same ground
as  other manufacturers -- product performance and price.

Medical devices pose unique challenges for lawmakers. Medicare does not set or
negotiate prices for the implants. Instead, it pays a flat rate to a hospital
for  procedures to give a patient a defibrillator or a hip, leaving it  up to
the hospital to negotiate the price of the device with the maker -- a
negotiation in which hospitals may have little leverage.

Unlike other hospital products, implants are so-called physician preference
items, meaning that doctors -- not the hospitals -- often choose which
manufacturer's implant to use. It is a decision that can be skewed by a doctor's
relationship to a company and can also undercut a hospital's ability to
negotiate the best price, experts say.

Medicare's laissez-faire approach has big implications for taxpayers because
every year,  spending on device-related procedures soars ever higher.

''This is a dysfunctional market if you take the perspective of the consumer or
public programs like Medicare,'' said Jeffrey C. Lerner, the president and chief
executive of the ECRI Institute, a nonprofit organization in Plymouth Meeting,
Pa., that evaluates medical devices for clients that include hospitals.

No one questions that implants like defibrillators and artificial hips extend
and improve lives. But profit margins on medical devices are also among the
highest for any medical products -- over 20 percent, in the case of a
defibrillator or an artificial hip, according to analysts.

In an effort to slow federal spending, the bill passed by the Senate Finance
Committee would require the device industry to pay the government $4 billion a
year for five years, with the portions allocated among individual companies
based on their market shares. But device makers, including the ones that
initially financed the defibrillator study -- Medtronic, Boston Scientific and
St. Jude Medical -- have fiercely resisted the provision, calling it an unfair
''tax'' that will stifle  innovation and cause job losses.

The companies have the support of some elected officials from the device makers'
home states, including Al Franken, the freshman Democratic senator  of
Minnesota, where Medtronic, the nation's biggest device producer, and St. Jude
Medical and some other device companies are based.

Although a political compromise over the issue is expected, the type of revenue
claw-back contained in the Finance Committee bill would do little to address the
underlying scientific and economic challenges that devices pose, several experts
said.

The big problems, in such experts' view, is that there is little data available
to compare the benefits of competing makers' products or to determine how much
buyers, like hospitals, should be paying for them, said Eugene Schneller, a
business professor at Arizona State University in Tempe.

For example, even doctors acknowledge that they typically have little reason to
be concerned about a device's costs when it comes to deciding which one to use.
One doctor compared it to giving a car buyer a blank check and letting him
choose between a Maserati or Honda.

''You are going to walk out of a dealership with a really nice car, if you don't
have to pay,'' said William Maisel, a cardiologist at Beth Israel Deaconess
Medical Center in Boston.

However, unlike a car buyer, who can see a vast array of comparative information
about competing products, a cardiologist or an orthopedic surgeon has little if
any comparative data when choosing a device.

Also, many doctors are unlikely to shop around, because they tend to stick with
a single producer -- either because they have been trained on a particular
maker's devices or because they have financial ties to the company.

Meanwhile, hospitals are often hampered in their ability to negotiate prices
with device makers because the selling price of a defibrillator or hip joint is
not easy to determine. In selling products, device companies have required
hospitals to sign contracts that contain confidentiality clauses under which
facilities agree not to disclose what they paid for the product.

As a result, a big hospital that is a large-volume buyer of heart devices or
hips may pay higher prices than a smaller one that buys fewer units, said Dr.
Lerner of ECRI.

In 2007, Senator Charles E. Grassley, Republican of Iowa, helped introduce
legislation that would have required device makers to regularly disclose the
average selling price for their products. Current health care reform proposals
do not contain such a provision, said a spokeswoman for the Advanced Medical
Technology Association, the device industry's trade group.

In some other countries, medical device databases have been established to
provide both doctors and patients with more data about how competing products
differ. In such a database, or a registry, information about a product and the
surgical technique used by a doctor is recorded at the time of an implant. And
then by tracking whether and when the patients return for a replacement
procedure, or experience other problems, registries can show which producers'
models are failing faster than others.

The information can help doctors and insurers avoid less reliable devices, while
also avoiding the high additional medical costs of remedial treatments and
replacement procedures.

But while the United States is the world's largest user of orthopedic devices,
efforts to set up registries have largely failed. The result is that orthopedic
patients here are twice as likely to require an earlier-than-expected
replacement procedure for a hip or a knee than in countries, like Australia,
that have registries.

Doctors in such countries often are less eager to embrace newer, more costly
models than doctors in the United States because evidence shows that new models
are more prone to failure during their initial years of use.

Eliminating unnecessary replacement procedures could potentially save Medicare
hundreds of millions annually. But Medicare has not pushed the use of
registries, and the industry has also not embraced it.  To date, hip and knee
producers have contributed $500,000 to underwrite an effort by the American
Academy of Orthopaedic Surgeons, a professional group, to create  a national
artificial hip and knee registry. But that is a relative pittance -- about what
many individual companies pay to a few doctors each year to retain them as
consultants.

Some experts like Dr. Maisel, the cardiologist in Boston, said it was naive to
expect the industry to underwrite registries, because it was not in a company's
interest to see its products compared against those made by competitors.

The federal government could also play a more aggressive role in making sure it
is getting better value for its money, he added.  A case in point -- requiring
that makers of heart devices use batteries that last longer than five years,
the period of time when patients must now undergo an additional, potentially
dangerous operation to have a costly device replaced.

Dr. Maisel said, ''Why would you build a better light bulb that lasts longer if
it is going to reduce your profits?''

THE WORK UP: Articles in this series are analyzing the economic and financial
issues at the heart of the health care debate in Washington.

URL: http://www.nytimes.com

LOAD-DATE: November 5, 2009

LANGUAGE: ENGLISH

CORRECTION-DATE: November 16, 2009



CORRECTION: An article on Nov. 4 about a lack of data on the benefits and
relative performance of many medical devices described incorrectly the findings
of a McKinsey Global Institute report on the rising costs of such devices.
Current spending on the six highest-cost implanted devices alone is about $13
billion annually, not $76 billion. (While a chart in that report described
current annual spending on medical devices as about $76 billion, McKinsey said
that the figure reflected not just spending on implanted devices but also on
other hospital products like X-ray film and surgical gloves.)

GRAPHIC: PHOTOS: A researcher at the device maker Medtronic in Memphis examining
a spinal system. (PHOTOGRAPH BY LANCE MURPHEY/BLOOMBERG NEWS) (pg.B1)
A Medtronic heart pacemaker. Unlike other hospital products, implant brands are
often chosen by the doctors, not hospitals. (PHOTOGRAPH BY JODI HILTON FOR THE
NEW YORK TIMES) (pg.B6) CHART: OPERATING PROFIT MARGINS (Source: Credit Suisse
estimates based on company reports) (pg.B1)

DOCUMENT-TYPE: Series

PUBLICATION-TYPE: Newspaper


