                   Copyright 2009 The New York Times Company


                              46 of 2021 DOCUMENTS


                               The New York Times

                            June 17, 2009 Wednesday
                              Late Edition - Final

Inside the Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 878 words


International

NORTH KOREAN NEWS AGENCY SAYS REPORTERS CONFESSED

North Korea's official news agency reported that the two American reporters
sentenced last week to 12 years of hard labor admitted to crossing into North
Korea illegally. PAGE A5

ONLINE, CHINESE GAIN VOICE

A Chinese court freed Deng Yujiao,  who became a national hero as outrage over
her arrest spread online. The case is the latest example of the Internet's
potential as a catalyst for social change in China. PAGE A5

CARTER MEETS HAMAS IN GAZA

Former President Jimmy Carter urged Hamas leaders during a high-profile meeting
in Gaza to take steps to become accepted by the leading Western nations. PAGE A5

IRAQI PRISON ABUSE DETAILED

Iraq's interior minister said that more than 40 police officers would face
charges. An inquiry into prison abuse found inmates had been jailed without
warrants. PAGE A7

INDIA AND PAKISTAN MEET

On the sidelines of a meeting in Russia, India's prime minister and Pakistan's
president talked for the first time since the terrorist attacks in Mumbai last
year.  PAGE A8

BRITISH UNION TRIMS AID

One of Britain's biggest trade unions halted some financing for the governing
Labor Party and threatened to cut off election financing unless the party stops
privatizing public services.  PAGE A8

TENSIONS RISE IN TURKEY

Turkey's army chief met with the prime minister, officials said, as uneasiness
intensified between the military and the government over a reported military
plot to destabilize the government. PAGE A9

A FAKE HOMICIDE IN RUSSIA

The killing was a ruse. The St. Petersburg police said they had staged the death
of a school's director to ensnare her deputy. PAGE A9

National

F.D.A. WARNS AGAINST USE OF POPULAR COLD MEDICINE

Regulators warned consumers to stop using Zicam, a cold medicine, because it
could damage their sense of smell. PAGE A12

UNFINISHED GUN LAWS

A year ago, the Supreme Court, while establishing the right of gun ownership,
did not explain how the ruling would affect nonfederal gun laws. Now federal
appeals courts have begun weighing in, using very different reasoning. PAGE A12

DEMOCRATS HONE HEALTH BILL

Alarmed by cost estimates from the Congressional Budget Office, Senate Democrats
worked to pare the costs of legislation to overhaul the health care system. PAGE
A13

PENNSYLVANIA BUDGET WOES

Facing a projected $3.2 billion revenue shortfall this year, Gov. Edward G.
Rendell of Pennsylvania proposed temporarily raising the personal income tax
rate by 16 percent for the next three years.  PAGE A14

New York

IMMIGRATION FRAUD LURED VICTIMS IN A QUEENS CHURCH

About 120 Ecuadorean immigrants in the New York region paid $8,000 each to
pastors who claimed to have a special route to getting green cards, authorities
said. PAGE A17

DEFENDING AN EX-DETAINEE

A federal judge in Manhattan said he would probably allow two military lawyers
to help defend a former Guantanamo detainee who was ordered by President Obama
to face trial in a civilian court. PAGE A18

Obituaries

BOB BOGLE, 75

Mr. Bogle co-founded the Ventures, the long-running guitar band whose jaunty
1960 hit ''Walk -- Don't Run'' became an early standard of instrumental rock 'n'
roll. PAGE A19

Business

THE AGE OF AQUARIUS RETURNS TO MADISON AVENUE

Images and sounds from the 1960s are becoming increasingly popular in
advertising. Advertising, by Stuart Elliot. PAGE B1

EX-HEAD OF A.I.G. TESTIFIES

Maurice R. Greenberg, the former chief executive of the American International
Group, testified in a trial over whether he raided a trust set up to pay
retirement benefits. But he said very little. PAGE B1

MYSPACE FIRES 400 WORKERS

MySpace, the social networking site owned by News Corporation, said it was
laying off roughly 400 employees, or nearly 30 percent of its staff. The company
said the layoffs were an attempt to return to a ''start-up culture.''  PAGE B1

TANKER FIGHT LOOMS (AGAIN)

Bidding for a $35 billion contract to replace the Air Force's aging tanker fleet
is about to begin for the third time in a decade, and the two main contenders
for the contract are preparing for the fight.  PAGE B5

Sports

PARTY STARTS AT BETHPAGE AHEAD OF THE U.S. OPEN

Seven years after what many golfers still consider the loudest, rowdiest
tournament they have played, the party is just getting started for the second
United States Open at Bethpage's Black course. PAGE B11

Ten in Range to Win at Open B13

Arts

REVISITING WARTIME: 66 MILES OF CRUELTY

Michael Norman and Elizabeth M. Norman's ''Tears in the Darkness: The Story of
the Bataan Death March and Its Aftermath'' is authoritative history and a
narrative achievement. Books of The Times, by Dwight Garner. PAGE C1

Listeners at Le Poisson Rouge C1

Dining

WITH THIS BURGER, I THEE WED

This wedding season, many brides and bridegrooms are opting for Rice Krispie
treats and checkered tablecloths over more upscale fare. But authentic backyard
cooking comes at a price.  PAGE D1

A VINEYARD AND LONG ISLAND

Shinn Estate Vineyards is a relative newcomer to winemaking on the North Fork of
Long Island. Even so, in just nine years the vineyard has created a niche. The
Pour by Eric Asimov. PAGE D4

Op-ed

MAUREEN DOWD PAGE A21

THOMAS FRIEDMAN PAGE A21

URL: http://www.nytimes.com

LOAD-DATE: June 17, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


