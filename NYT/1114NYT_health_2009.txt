                   Copyright 2009 The New York Times Company


                             1114 of 2021 DOCUMENTS


                               The New York Times

                           December 3, 2009 Thursday
                              Late Edition - Final

Senate Breaks Health Stalemate; First Votes Today

BYLINE: By ROBERT PEAR and DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 32

LENGTH: 884 words

DATELINE: WASHINGTON


At the end of a third day of Senate debate over sweeping health care
legislation, Democrats and Republicans said Wednesday night that they had broken
an impasse over the seemingly simple question of how and when to vote on the
first amendments.

But even as lawmakers announced an agreement to begin voting Thursday, Democrats
accused Republicans of stalling debate and obstructing the legislation.

In a closed-door meeting of his caucus, the Senate majority leader, Harry Reid
of Nevada, told Democrats that Republicans were not interested in passing a
bill. In effect, he prepared them for trench warfare, saying the Democrats must
stick together and should be ready to work weekends to finish the bill before
Christmas.

For their part, Republicans said it was unrealistic to expect quick action on
such a big bill, and they denied they were stalling.

''That's an odd charge about a bill that would cost $2.5 trillion, when fully
implemented, and restructure one-sixth of the economy, affecting 300 million
people,'' said the Senate's No. 3 Republican, Lamar Alexander of Tennessee.

Noting that the Senate had spent one month on a farm bill, seven weeks on an
education bill and eight weeks on an energy bill, Mr. Alexander said, ''Surely
we can spend at least that much time on a comprehensive health care bill.''

Democrats need at least 60 votes to limit debate on the measure, whose
overarching purpose is to guarantee insurance for most Americans while curbing
the growth of health care costs and federal health spending.

Senate leaders of both parties said they would vote Thursday on four proposals
dealing with two issues: how to guarantee additional health benefits for women
and how to squeeze nearly a half-trillion dollars from Medicare over 10 years
without adversely affecting older Americans.

Democrats offered one proposal to cover a wide range of screenings and
preventive health services for women. Republicans drafted an alternative,
intended to outdo the Democrats.

Republicans offered a proposal to strike provisions of the bill that would save
money in Medicare. Democrats came back with an alternative that would protect
''guaranteed Medicare benefits'' -- not necessarily the extra benefits offered
by private Medicare Advantage plans, which cost the government more, on average,
than the traditional Medicare program.

The agreement to hold votes on Thursday followed some testy exchanges on the
floor.

''We are going to go into the fourth day of the debate on one of the most
important bills in the history of the United States Senate,'' said Senator
Richard J. Durbin of Illinois, the No. 2 Democrat. ''The fact is we've not had a
single vote this week on health care reform amendments, because of objections
from the other side.''

The Senate Republican leader, Mitch McConnell of Kentucky, protested that
''Republicans were prepared and fully ready and willing to vote.'' Mr. McConnell
said Democrats shared responsibility for the delay because they had rejected a
Republican offer to vote on two of the proposals Wednesday night rather than all
four at once. Democrats feared that such votes would lead the Republicans to
stretch out any voting on the two remaining proposals.

As the skirmishing continued on the floor, the American Medical Association
endorsed the thrust of the Senate bill. In a letter to Mr. Reid, the doctors'
group praised the measure's strict federal regulation of health insurance and
new tax credits to help low- and moderate-income people buy coverage.

But the medical association objected to some provisions, including a new tax on
''elective cosmetic medical procedures'' and creation of an Independent Medicare
Advisory Board that could ''mandate payment cuts for physicians.''

The association also objected to antifraud provisions that it said could
penalize doctors who make ''an honest mistake,'' with no intent to defraud
Medicare or Medicaid. And it complained about a section that would impose new
restrictions on doctor-owned hospitals.

Elsewhere on Capitol Hill, hundreds of supporters of abortion rights held a
rally Wednesday to insist that the legislation allow insurance coverage of
abortion. They denounced restrictions on abortion that were accepted by House
Democratic leaders as the price of passing their version of the legislation last
month.

Representative Judy Chu, Democrat of California, said ''women will lose
benefits'' under the House bill, which she described as ''the biggest rollback
of reproductive rights in decades.''

Marcia D. Greenberger, co-president of the National Women's Law Center, an
advocacy group, said the House bill went to ''ridiculous extremes'' in trying to
prevent the use of federal money to pay for abortions.

Senators Ben Nelson, Democrat of Nebraska, and Orrin G. Hatch, Republican of
Utah, said they would try to add similar restrictions to the Senate bill. Senate
supporters of abortion rights said that they were confident they could defeat
such proposals but that they needed a large majority vote to give the Senate a
strong hand in expected negotiations with the House.

The United States Conference of Catholic Bishops said the Senate bill, in its
current form, ''does not live up to President Obama's commitment of barring the
use of federal dollars for abortion.''

URL: http://www.nytimes.com

LOAD-DATE: December 3, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Senator Lamar Alexander, left, denied Democratic accusations
that Republicans were engaged in delay. He and a fellow Republican, Senator
Johnny Isakson, met with reporters on health care. (PHOTOGRAPH BY STEPHEN
CROWLEY/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


