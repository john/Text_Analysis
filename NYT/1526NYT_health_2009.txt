                   Copyright 2010 The New York Times Company


                             1526 of 2021 DOCUMENTS


                               The New York Times

                            February 19, 2010 Friday
                              Late Edition - Final

States Consider Medicaid Cuts As Use Grows

BYLINE: By KEVIN SACK and ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1184 words

DATELINE: WASHINGTON


Facing relentless fiscal pressure and exploding demand for government health
care, virtually every state is making or considering substantial cuts in
Medicaid, even as Democrats push to add 15 million people to the rolls.

Because they are temporarily barred from reducing eligibility, states have been
left to cut ''optional benefits,'' like dental and vision care, and reduce
payments to doctors and other health care providers.

In some states, governors are trying to avoid the deepest cuts by pushing for
increases in tobacco taxes or new levies on hospitals and doctors, but many of
those proposals are running into election-year trouble in conservative
legislatures.

In Nevada, which faces an $881 million budget gap, Gov. Jim Gibbons, a
Republican, proposed this month to end Medicaid coverage of adult day care,
eyeglasses, hearing aids and dentures, and, for a savings of $829,304, to reduce
the number of diapers provided monthly to incontinent adults (to 186 from 300).

''We are down to the ugly list of options,'' the state's director of health and
human services, Mike Willden, told a legislative committee last week.

The Medicaid program already pays doctors and hospitals at levels well below
those of Medicare and private insurance, and often below actual costs. Large
numbers of doctors, therefore, do not accept Medicaid patients, and  cuts may
further discourage participation in the program, which primarily serves
low-income children, disabled adults and nursing home residents.

In Kansas, a 10 percent cut in provider payments that took effect on Jan. 1 has
prompted such an outcry that Gov. Mark Parkinson, who imposed it, now wants to
restore the money by raising tobacco and sales taxes.

Even if Mr. Parkinson, a Democrat, overcomes resistance in his
Republican-controlled Legislature, it will be too late for Dr. C. Joseph Beck, a
Wichita ophthalmologist who informed his Medicaid patients last month that he
could no longer afford to treat them.

Dr. Beck said that over  eight months  last year, his practice wrote off $36,000
in losses from treating 17 Medicaid patients. The state-imposed payment cut, he
said, was ''the final straw.''

''I'm out, I'm done,'' Dr. Beck said in a telephone interview. ''I didn't want
to. I want to take care of people. But I also have three children and many
employees to take care of.''

Concerns about health care costs are likely to dominate the winter meeting of
the National Governors Association, which begins Saturday in Washington.

In advance of the gathering, administration officials have urged governors to
endorse President Obama's health care proposals, or at least to avoid
criticizing them. The Democratic plan, which is stalled in Congress, would
vastly expand eligibility for Medicaid as one means of reducing the number of
uninsured.

But many governors said they were more concerned about the growth of existing
health programs. The recession and high unemployment have driven up enrollment
in Medicaid while depleting state revenues that help pay for it.

A survey released Thursday by the Kaiser Family Foundation found a record
one-year increase in Medicaid enrollment of 3.3 million from June 2008 to June
2009, a period when the unemployment rate rose by 4 percentage points. Total
enrollment jumped 7.5 percent, to 46.9 million, and 13 states had double-digit
increases.

Because Medicaid enrollment often lags behind unemployment, this year's increase
could prove even greater.

The National Association of State Medicaid Directors estimates that state budget
shortfalls in the coming fiscal year, which begins in July in most states, will
total $140 billion. Because Medicaid is one of the largest expenditures in every
state budget, and one of the fastest-growing, it makes an unavoidable target.

''For most states, the fiscal situation is still dire, and the Medicaid cuts are
significant,'' said Scott D. Pattison, executive director of the National
Association of State Budget Officers.

Governors and legislators have managed to defer the deepest cuts because the
federal stimulus package provided $87 billion to states in Medicaid relief. The
cost of Medicaid is shared by the federal and state governments, with states
setting eligibility, benefit and reimbursement levels within broad federal
guidelines, and Washington covering the majority of the expense.

But the stimulus assistance is due to expire at the end of December, in the
middle of many states' fiscal years, leaving budget officials to peer over a
precipice. Congress and the White House are considering extending the enhanced
payments for six more months, at a cost of about $25 billion.

The House has passed such a measure and Mr. Obama included it in his budget
this month, but the Senate has not acted.

The extension would not come close to filling the Medicaid gap in many states.
In Georgia, for instance, Gov. Sonny Perdue assumed in his budget proposal that
the additional federal money would be provided, but that the state would still
face a Medicaid imbalance of $608 million, said Dr. Rhonda M. Medows, the
commissioner of community health.

Mr. Perdue, a Republican, decided it would be unwise to cut optional benefits
because that might drive Medicaid patients into  expensive  emergency rooms. He
proposed instead to levy a 1.6 percent tax on hospital and managed care revenues
and to cut payments to many providers by nearly 2 percent.

Without the tax increases, which face opposition in the General Assembly, the
state will have to cut provider payments by 16.5 percent, Dr. Medows said.

''I won't have any primary care doctors left, much less specialists,'' she said.
''Certainly down here nobody likes to talk about taxes, but sometimes you have
to bite the bullet and do what's right for a whole lot of people.''

In the Kaiser survey, almost every state reported that Medicaid enrollment  for
the current fiscal year was exceeding expectations, making midyear budget cuts
necessary.

The options are limited by several realities. To qualify for Medicaid dollars
provided in the stimulus package, states agreed not to tighten  eligibility  for
low-income people. And any time a state cuts spending on Medicaid, it loses at
least that much in federal matching money.

Despite the ban on restricting eligibility, hard-hit states like California and
Arizona are considering proposals by their governors that would remove hundreds
of thousands from the rolls once the federal financing ends. Gov. Jan Brewer of
Arizona, a Republican, has called for eliminating Medicaid coverage for 310,000
childless adults and ending the Children's Health Insurance Program to help
close a two-year budget gap of about $4.5 billion.

Gov. Phil Bredesen of Tennessee, a Democrat, is proposing the largest cuts in
the history of TennCare, his state's Medicaid program. To trim 9 percent of the
TennCare budget, he would establish a $10,000 cap on inpatient hospital services
for nonpregnant adults and would limit coverage of X-rays, laboratory services
and doctor's office visits.

''I have no choice,'' Mr. Bredesen said.

URL: http://www.nytimes.com

LOAD-DATE: February 19, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Dr. C. Joseph Beck has stopped treating Medicaid patients.
(PHOTOGRAPH BY CRAIG HACKER FOR THE NEW YORK TIMES) (A15)

PUBLICATION-TYPE: Newspaper


