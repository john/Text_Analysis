                   Copyright 2010 The New York Times Company


                             1581 of 2021 DOCUMENTS


                               The New York Times

                            February 28, 2010 Sunday
                              Late Edition - Final

Wary Centrists Pose Challenge In Health Vote

BYLINE: By SHERYL GAY STOLBERG and ROBERT PEAR; Carl Hulse contributed
reporting.

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1321 words


WASHINGTON -- The future of President Obama's health care overhaul now rests
largely with two blocs of swing Democrats in the House of Representatives --
abortion opponents and fiscal conservatives -- whose indecision signals the
difficulties Speaker Nancy Pelosi faces in securing the votes necessary to pass
the bill.

With Republicans unified in their opposition, Democrats are drafting plans to
try on their own to pass a bill based on one Mr. Obama unveiled before his
bipartisan health forum last week. His measure hews closely to the one passed by
the Senate in December, but differs markedly from the one passed by the House.

That leaves Ms. Pelosi in the tough spot of trying to keep wavering members of
her caucus on board, while persuading some who voted no  to switch their votes
to yes -- all at a time when Democrats are worried about their prospects for
re-election.

Representative Dennis Cardoza, Democrat of California, typifies the speaker's
challenge. The husband of a family practice doctor, he is intimately familiar
with the failings of the American health care system. His wife ''comes home
every night,'' he said, ''angry and frustrated at insurance companies denying
people coverage they have paid for.''

But as a member of the centrist Blue Dog Coalition, Mr. Cardoza is not convinced
that Mr. Obama's bill offers the right prescription. It lacks anti-abortion
language he favors, and he does not think it goes far enough in cutting costs.
So while he voted for the House version -- ''with serious reservations,'' he
said -- he is now on the fence.

''I think we can do better,'' Mr. Cardoza said of the president's proposal.

Representative Frank Kratovil Jr., Democrat of Maryland, is also unconvinced. He
voted against the House bill on the grounds that it is too big and too costly --
a view that some constituents in his Republican-leaning district share. In case
he did not get the message, one of them hanged him in effigy this past summer
outside his district office on the Eastern Shore of Maryland.

''This system is broken; we have to do something,'' Mr. Kratovil said. ''But my
preference would be to do smaller things.''

Under the Democrats' tentative plans, the House would pass the health care bill
approved in December by the Senate, and both chambers would approve a separate
package of changes using a parliamentary device known as budget reconciliation.

The tactic is intended to avoid a Republican filibuster, but in the Senate, the
majority leader, Harry Reid of Nevada, faces challenges if he tries to use it.
He is having trouble persuading a majority of his caucus to go along.

In the House, lawmakers like Mr. Kratovil, Mr. Cardoza and other swing Democrats
will come under increasing scrutiny from leadership as a vote draws near. Of the
219 Democrats who initially voted in favor of the House measure, roughly 40 did
so in part because it contained the so-called Stupak amendment, intended to
discourage insurers from covering abortion.

Some, notably Representative Bart Stupak, the Michigan Democrat for whom the
amendment is named, will almost certainly switch their yes votes to no because
the new version being pushed by Mr. Obama would strip out the House bill's
abortion restrictions in favor of Senate language that many of them consider
unacceptable.

An additional 39, like Mr. Kratovil, are fiscal conservatives who voted no the
first time around. Ms. Pelosi is hoping that she can get some to switch those no
votes to yes in favor of Mr. Obama's less expensive measure.

But persuading Democrats who are already on record as opposing a health overhaul
to do a turnabout will not be an easy task, especially during a midterm election
year in which Democrats' political prospects already look bleak. Of the 39
Democrats who voted against the House measure, 31, including Mr. Kratovil,
represent districts that were won in 2008 by Senator John McCain of Arizona, Mr.
Obama's Republican rival. Fourteen, including Mr. Kratovil, are freshmen, who
are generally considered more politically vulnerable than more senior lawmakers.

''The concern among Democrats right now is that there are more yes votes
reconsidering than no votes,'' said David Wasserman, who tracks House races for
the nonpartisan Cook Political Report. ''My sense is that for Democrats to pass
this bill, they would have to convince several members who are already in
serious jeopardy, even after voting no on the first health care bill, to put
passage of the bill ahead of their own chances of being competitive in the
fall.''

But politicians do not want to be martyrs. They want to hold onto their seats.

Ms. Pelosi is facing resistance from some of her most senior members, like
Representative Ike Skelton, Democrat of Missouri and chairman of the Armed
Services Committee. He has been in office since 1977, but is facing his toughest
re-election challenge in years.

Mr. Skelton says he does not see any improvements in the measure that would
cause him to vote in favor of it; like Mr. Kratovil, he favors a smaller, less
ambitious bill. ''It would be a lot easier,'' he said, ''if we cut this back to
basics -- take two or three or four issues on which everyone agrees and build on
it.''

Others, like Representative Jason Altmire, a Pennsylvania Democrat who also
voted against the House bill, seem to wonder aloud why Mr. Obama is bothering.
With so many Democrats feeling nervous about their past votes in favor of the
health bill, Mr. Altmire said, he can imagine vote-switching in only one
direction: from yes to no.

''I don't know of any no votes at this point that would switch unless the bill
is substantially changed, including me,'' he said. ''And I know of a handful of
yes votes who regret it and would relish the opportunity to put a no vote on the
board so they could go back home and talk about that.''

Analysts like Mr. Wasserman say Ms. Pelosi's best chances for no-to-yes
conversions rest with Democrats who are retiring, because they do not have to
worry about their political fortunes. So far, there are only three:
Representative John Tanner and Representative Bart Gordon, both of Tennessee,
and Representative Brian Baird of Washington.

Mr. Tanner has told colleagues he has no intention of switching his vote, said
one Democratic lawmaker who has spoken with him. And in interviews on Friday,
Mr. Gordon and Mr. Baird sounded decidedly noncommittal.

Mr. Gordon said his constituents were ''starting to get a little bit tired'' of
hearing about health care. He said he wanted to see ''at least a partially
bipartisan bill'' -- something that now seems impossible in the House, given
that the lone Republican who voted in favor last time, Representative Anh Cao of
Louisiana, has publicly changed his mind.

Mr. Baird said he was ''totally undecided'' about whether he could support the
new version taking shape in Congress, though he did say the bipartisan forum Mr.
Obama conducted at Blair House on Thursday would ''potentially'' make him more
likely to vote for the legislation, perhaps because Republicans seemed so dug in
against it.

''At several points,'' Mr. Baird said, ''President Obama tried to find common
ground, only to see the other side go back on message.''

Publicly, House Democratic leaders are trying to sound upbeat. The House
Democratic whip, Representative James E. Clyburn, Democrat of South Carolina,
said last week that he felt ''pretty good'' about the chances of passing Mr.
Obama's bill. But the leadership has not yet started counting votes, and a
senior House leadership aide, speaking on condition of anonymity, conceded that
rounding them up would not be easy.

''It's going to be a heavy lift,'' this aide said, ''but so have many other
votes. In the last health care vote we really didn't have the majority until the
afternoon, and this will probably be that way, too. That's how these votes come
together in the end.''

URL: http://www.nytimes.com

LOAD-DATE: March 2, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: Bart Stupak
 Nancy Pelosi, the House speaker, with Harry Reid, the Senate majority leader,
after President Obama's meeting on health care. (PHOTOGRAPH BY ALEX WONG/GETTY
IMAGES) (A25)

PUBLICATION-TYPE: Newspaper


