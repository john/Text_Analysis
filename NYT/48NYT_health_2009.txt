                   Copyright 2009 The New York Times Company


                              48 of 2021 DOCUMENTS


                               The New York Times

                            June 17, 2009 Wednesday
                              Late Edition - Final

Malpractice and Health Care Reform

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL; Pg. 26

LENGTH: 467 words


Hoping to enlist support for his campaign for health care reform, President
Obama told the American Medical Association this week that he would work with
doctors to limit their vulnerability to malpractice lawsuits. That was a
reasonable offer -- provided any malpractice reform is done carefully.

The current medical liability system, based heavily on litigation, has a spotty
record. It fails to compensate most victims of malpractice because most never
file suit. When cases reach the courts, some juries do a decent job of sorting
out whether there was negligence or preventable error; others are swayed to
grant large damage awards based more on the severity of a patient's injuries
than on clear evidence of negligence.

Mr. Obama did not specify which malpractice reforms he favors, but he wisely
rejected placing caps on malpractice awards, the preferred solution of
Republican tort reformers. Such caps would be unfair to people grievously harmed
by physician errors who need substantial compensation to live with their
injuries.

There is a variety of ideas worth exploring. Some analysts have called for
setting up tribunals of neutral experts to hear malpractice claims. Others
suggest requiring mediation, or granting doctors presumptive protection in
malpractice lawsuits if they have followed recommended clinical practice
guidelines, or encouraging doctors to confess error promptly, apologize to
patients forthrightly, and offer them fair compensation for their injuries.

Whether malpractice reform would save much money is unclear. Malpractice claims
do drive up insurance premiums paid by doctors in some high-risk specialties,
such as obstetrics and neurosurgery. Those costs are presumably passed on to
patients. There is also concern that doctors may overprescribe costly tests and
treatments to avoid possible lawsuits. But the evidence is inconclusive,
according to the Congressional Budget Office, that doctors engage in enough
''defensive medicine'' to have a significant impact on costs.

The office estimates that caps on damages would ultimately reduce malpractice
premiums for medical providers but would have a ''relatively small'' impact on
total health spending, reducing it by less than half a percent. Even that could
save billions of dollars a year, which is not trivial. But malpractice claims
are probably not a major cost driver.

Still, most doctors are convinced that malpractice suits are unfair and
burdensome, so it is worth exploring the issue, if only to gain their help in
reforming the health care system. Whatever the alternative -- tribunals,
mediation -- patients must retain the right to go to court and seek higher
damages than they have been offered. That is the only way to deter negligence by
doctors, hospitals and other health care providers.

URL: http://www.nytimes.com

LOAD-DATE: June 17, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


