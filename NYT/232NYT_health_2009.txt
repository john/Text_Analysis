                   Copyright 2009 The New York Times Company


                             232 of 2021 DOCUMENTS


                               The New York Times

                            July 29, 2009 Wednesday
                              Late Edition - Final

First, Make No Mistakes

BYLINE: By JIM HALL.

Jim Hall, the chairman of the National Transportation Safety Board from 1994 to
2001, is the managing partner of a health care and safety consulting firm.

SECTION: Section A; Column 0; Editorial Desk; OP-ED CONTRIBUTOR; Pg. 23

LENGTH: 585 words

DATELINE: Washington


IN the  health care debate, there is one thing we can all agree on: the
importance of reducing unnecessary deaths in medicine. Medical error causes tens
of thousands of deaths each year  that could be prevented by known techniques
and technologies. And all errors, even those that are not fatal, are costly: 10
years ago, the Institute of Medicine estimated that the effects of medical error
accounted for $17 billion to $29 billion in domestic health care spending, and
the error rate has not declined since then.

What makes the problem all the more frustrating is that we could address it with
little cost to the American taxpayer. Because  American medicine accepts error
as an inevitable consequence of treatment, our hospitals, insurers and
government do  little to respond  to unnecessary deaths. If we are to address
the problem in a serious manner, we must first change this culture.

As a former chairman of the National Transportation Safety Board, I am familiar
with the deadly consequences of human error. However, because that agency views
every transportation death as a preventable occurrence, our roads, rails and
skies enjoy an unparalleled level of safety. After any significant accident, the
board undertakes an extensive investigation, and makes recommendations to the
parties involved to ensure that such an accident never recurs. While the
transportation safety board has no regulatory authority, its recommendations are
viewed by the industry and the public as unbiased and therefore credible, and
federal regulators usually act with haste to address them.

Such an investigative body could substantially improve the safety of medicine in
the United States. While it surely could not investigate every individual
instance of error, it could address many well-known maladies. Hospital-acquired
infections, for instance, affect millions of Americans each year. A National
Medical Safety Board would collect regional data on the problem, paying
particular attention to hospitals with high incidences of infection. It would
then determine preventive measures and make recommendations to state and federal
regulators, hospitals and health care officials.

When such a board discovered new solutions to old problems, their advice would
have the credibility that comes with independent investigation. In some cases,
the board might recommend practices that doctors are already aware of, but for
some reason do not employ;  the  public awareness that would follow would
pressure hospitals to do better.

These benefits could come at a minuscule cost to taxpayers. The National
Transportation Safety Board costs each citizen approximately 25 cents per year.
This is a small price for an agency that has eliminated midair plane collisions,
persuaded Americans to put children in the back seats of cars instead of the
front and prevented deaths in every category of transportation. Given health
care's notorious struggles with rising prices, this is a cost-saving opportunity
the industry cannot afford to overlook.

An overhaul of our national health care system is at hand and with it a crucial
opportunity to improve medical safety. The Obama administration should take a
lesson from the transportation safety board's successes and establish an
independent agency charged with identifying and eliminating the causes of
medical error. Such a move would save money by saving lives and would ensure
that our nation's health care system is equipped to provide the safest medical
care possible.

URL: http://www.nytimes.com

LOAD-DATE: July 29, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


