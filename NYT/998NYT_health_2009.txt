                   Copyright 2009 The New York Times Company


                             998 of 2021 DOCUMENTS


                               The New York Times

                            November 13, 2009 Friday
                              Late Edition - Final

Organizing for Health Care

BYLINE: By KATHARINE Q. SEELYE

SECTION: Section A; Column 0; National Desk; Pg. 19

LENGTH: 286 words


Democrats are trying to build popular support for health care legislation by
enlisting people who voted in the presidential election last year for Barack
Obama.

Organizing for America, the Obama campaign arm of the Democratic National
Committee, sent e-mail messages on Thursday to Obama supporters in 32
Congressional districts represented by Republicans. They are districts that
voted for Mr. Obama in 2008 but whose representatives voted against health care
legislation in the House last Saturday.

In the e-mail message, Mitch Stewart, the director of Organizing for America,
encourages Obama supporters to tell these Republicans to get with the health
care program or they may be booted out of office.

Targets include Republican stalwarts like Mary Bono Mack of California, elected
over a decade ago, and Ileana Ros-Lehtinen, above right, of Florida, elected two
decades ago, as well as Charlie Dent of Pennsylvania,  elected in 2004 and
viewed as more vulnerable.

Democratic officials are hoping that the message  will help generate sufficient
foot traffic to overwhelm the Republican representatives into voting for a
health care bill when it comes back to the House after being merged with a
Senate version.

But it is not clear how effective this effort will be.

Organizing for America had an easier time galvanizing voters in last year's
campaign than it has this year. And virtually all Republican House members have
already vowed to vote against the final bill, just as all but one did last week.

So far, the organization has refrained from putting public pressure on the 39
Democrats who voted against the bill, but it has turned out supporters to thank
those who voted for it. KATHARINE Q. SEELYE

URL: http://www.nytimes.com

LOAD-DATE: November 13, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO (PHOTOGRAPH BY OSWALDO RIVAS/REUTERS)

PUBLICATION-TYPE: Newspaper


