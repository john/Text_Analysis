                   Copyright 2010 The New York Times Company


                             1451 of 2021 DOCUMENTS


                               The New York Times

                            January 29, 2010 Friday
                              Late Edition - Final

As Health Bills Stall, President Alters Agenda

BYLINE: By CARL HULSE and SHERYL GAY STOLBERG; Elisabeth Bumiller contributed
reporting.

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1192 words

DATELINE: WASHINGTON


The White House on Thursday signaled the outlines of its strategy for breaking
the partisan logjam holding up President Obama's agenda, saying Democrats would
move quickly to underline their commitment to fixing the broken economy and to
build an election-year case against Republicans if they do not cooperate.

With Mr. Obama's health care overhaul stalled on Capitol Hill, Rahm Emanuel, the
White House chief of staff, said in an interview that Democrats would try to act
first on job creation, reducing the deficit and imposing tighter regulation on
banks before returning to the health measure, the president's top priority from
last year.

But Mr. Obama quickly got a taste of how difficult it would be to bring the
opposition party on board.

One day after the president upbraided Congress in his State of the Union address
for excessive partisanship, Senate Republicans voted en masse against a plan to
require that new spending not add to the deficit (it passed anyway as all 60
members of the Democratic caucus hung together). And some Republicans
peremptorily dismissed Mr. Obama's main job-creating proposal, expressing no
interest in using $30 billion in bank bailout money for business tax credits.

''I think there is a right way and a wrong way to do it, and that is not the
right way,'' said Senator John Cornyn, the Texas Republican heading the effort
to elect more Republicans to the Senate.

On Friday, Mr. Obama will travel to Baltimore to announce specifics of his jobs
plan, including a proposed $5,000 tax credit for small businesses for each new
employee they hire in 2010. While there, he will address House Republicans at a
retreat they are holding.

The instant Republican resistance to the jobs plan -- coupled with a vote this
week to kill a deficit-reduction panel that had been initiated with high
bipartisan hopes -- illustrated the chasm between the two parties and the
difficulties Mr. Obama faces if he is serious about trying to work with an
energized opposition.

Increasingly confident of their prospects after the Massachusetts Senate
victory, Republicans are disinclined to give ground in policy debates and appear
willing to stick with their near-unanimous opposition to major initiatives
unless Democrats offer significant concessions.

''House Republicans will seize the opportunity in respectful terms, but candid
and frank terms, and make it clear to the president that we have better
solutions,'' said Representative Mike Pence of Indiana, the chairman of the
House Republican Conference.

The administration showed no signs of capitulating either, with officials saying
the White House will pursue a strategy of trying to shame Republicans whenever
they stand in lock-step against Mr. Obama. In an interview Thursday, Mr. Emanuel
warned that Republicans would suffer politically for their opposition to the
pay-as-you-go plan.

''One party was for fiscal discipline, the other party wasn't,'' he said,
previewing a message that Democrats could use in this year's midterm elections.

Officials said they were pressing ahead with one of the more controversial items
Mr. Obama laid out Wednesday night: repealing the policy barring gay men and
lesbians from serving openly in the military.

Senior Pentagon officials said Defense Secretary Robert M. Gates and Adm. Mike
Mullen, the chairman of the Joint Chiefs of Staff, had been in close discussions
with Mr. Obama on the issue and would present the Pentagon's initial plans for
carrying out the new policy at a hearing before the Senate Armed Services
Committee on Tuesday.

Changing the policy requires an act of Congress, and the officials signaled that
Mr. Gates would go slowly, and that repeal of the ban was not imminent. And it
could be a hard sell for the president, even among Democrats; Representative Ike
Skelton of Missouri, chairman of the House Armed Services Committee, on Thursday
restated his opposition to repealing the ban.

Mr. Emanuel, the chief of staff, said he hoped Congressional Democrats would
take up the jobs bill next week. Then, in his view, Congress would move to the
president's plan to impose a fee on banks to help offset losses to the Troubled
Asset Relief Program, the fund used to bail out banks and automakers.

Lawmakers would next deal with a financial regulatory overhaul, and then pick up
where they left off on health care. ''All these things start and lead to one
place: J-O-B-S,'' Mr. Emanuel said.

The execution, of course, will be much easier said than done. Democrats are
about to lose their 60-vote supermajority in the Senate, after the recent
Republican victory by Scott Brown in a special election to fill the seat held by
the late Edward M. Kennedy of Massachusetts. In the Senate, Republicans have
come under intense pressure from their colleagues to stay in the fold.

Even some of Mr. Obama's allies said that given united Republican opposition,
the goal of more cooperation might be out of reach. ''In order to dance, you
need a dance partner and there ain't no partner out there,'' Senator Bernard
Sanders, a Vermont independent, noted.

A vote this week on a proposal to create a bipartisan commission to recommend
ways to attack rising federal deficits was seen as illustrative of the
Republican strategy to thwart Democrats. Though the idea attracted 53 votes --
36 Democrats, one independent and 16 Republicans -- it failed because it did not
cross the 60-vote threshold.

At least six Republicans who had previously supported the plan voted against it,
as did others who have backed the idea in concept. Some of those who voted
against the plan suggested they did so because they did not want to give
Democrats political cover by joining with them in a deficit reduction effort.

''It was stacked,'' Senator John McCain, Republican of Arizona, told reporters
in explaining his rationale for switching from a supporter to an opponent of the
commission.

Some leading Republicans say they believe there is still an opportunity for the
administration, Congressional Democrats and Republicans to find ways to work
together. But they say it would require a concerted outreach effort and the
White House abandoning the idea of wooing a few individual Republican senators.

''I am astonished that the White House's idea of working in a bipartisan way is
this shooting gallery method, going around and seeing if you can pick off one or
two or three,'' said Senator Lamar Alexander of Tennessee, a member of the
Republican leadership.

Other Republicans say Mr. Obama should convene a summit of Congressional
Republican leaders.

''If the president reaches out to the Republican leadership in a genuine way,
the spotlight shifts from his overreaching to whether we can meet him in the
middle,'' said Senator Lindsey Graham, Republican of South Carolina.

Congressional Democrats say they are pessimistic about cooperation. Using their
power of 60 for one of the final times, Democrats did secure approval of the
antideficit legislation Thursday on a straight 60-40 party-line vote but
interpreted the united Republican opposition as a sign that Republicans were not
moved by the president's appeal.

URL: http://www.nytimes.com

LOAD-DATE: January 29, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Senator John McCain after voting on a debt-limit extension bill.
(PHOTOGRAPH BY JIM LO SCALZO FOR THE NEW YORK TIMES) (A12)

PUBLICATION-TYPE: Newspaper


