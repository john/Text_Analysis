                   Copyright 2010 The New York Times Company


                             1403 of 2021 DOCUMENTS


                               The New York Times

                            January 22, 2010 Friday
                              Late Edition - Final

Do The Right Thing

BYLINE: By PAUL KRUGMAN

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 31

LENGTH: 821 words


A message to House Democrats: This is your moment of truth. You can do the right
thing and pass the Senate health care bill. Or you can look for an easy way out,
make excuses and fail the test of history.

Tuesday's Republican victory in the Massachusetts special election means that
Democrats can't send a modified health care bill back to the Senate. That's a
shame because the bill that would have emerged from House-Senate negotiations
would have been better than the bill the Senate has already passed. But the
Senate bill is much, much better than nothing. And all that has to happen to
make it law is for the House to pass the same bill, and send it to President
Obama's desk.

Right now, Nancy Pelosi, the speaker of the House, says that she doesn't have
the votes to pass the Senate bill. But there is no good alternative.

Some are urging Democrats to scale back their proposals in the hope of gaining
Republican support. But anyone who thinks that would work must have spent the
past year living on another planet.

The fact is that the Senate bill is a centrist document, which moderate
Republicans should find entirely acceptable. In fact, it's very similar to the
plan Mitt Romney introduced in Massachusetts just a few years ago. Yet it has
faced lock-step opposition from the G.O.P., which is determined to prevent
Democrats from achieving any successes. Why would this change now that
Republicans think they're on a roll?

Alternatively, some call for breaking the health care plan into pieces so that
the Senate can vote the popular pieces into law. But anyone who thinks that
would work hasn't paid attention to the actual policy issues.

Think of health care reform as being like a three-legged stool. You would,
rightly, ridicule anyone who proposed saving money by leaving off one or two of
the legs. Well, those who propose doing only the popular pieces of health care
reform deserve the same kind of ridicule. Reform won't work unless all the
essential pieces are in place.

Suppose, for example, that Congress took the advice of those who want to ban
insurance discrimination on the basis of medical history, and stopped there.
What would happen next? The answer, as any health care economist will tell you,
is that if Congress didn't simultaneously require that healthy people buy
insurance, there would be a ''death spiral'': healthier Americans would choose
not to buy insurance, leading to high premiums for those who remain, driving out
more people, and so on.

And if Congress tried to avoid the death spiral by requiring that healthy
Americans buy insurance, it would have to offer financial aid to lower-income
families to make that insurance affordable  --  aid at least as generous as that
in the Senate bill. There just isn't any way to do reform on a smaller scale.

So reaching out to Republicans won't work, and neither will trying to pass only
the crowd-pleasing pieces of reform. What about the suggestion that Democrats
use reconciliation  --  the Senate procedure for finalizing budget legislation,
which bypasses the filibuster  --  to enact health reform?

That's a real option, which may become necessary (and could be used to improve
the Senate bill after the fact). But reconciliation, which is basically limited
to matters of taxing and spending, probably can't be used to enact many
important aspects of reform. In fact, it's not even clear if it could be used to
ban discrimination based on medical history.

Finally, some Democrats want to just give up on the whole thing.

That would be an act of utter political folly. It wouldn't protect Democrats
from charges that they voted for ''socialist'' health care  --  remember, both
houses of Congress have already passed reform. All it would do is solidify the
public perception of Democrats as hapless and ineffectual.

And anyway, politics is supposed to be about achieving something more than your
own re-election. America desperately needs health care reform; it would be a
betrayal of trust if Democrats fold simply because they hope (wrongly) that this
would slightly reduce their losses in the midterm elections.

Now, part of Democrats' problem since Tuesday's special election has been that
they have been waiting in vain for leadership from the White House, where Mr.
Obama has conspicuously failed to rise to the occasion.

But members of Congress, who were sent to Washington to serve the public, don't
have the right to hide behind the president's passivity.

Bear in mind that the horrors of health insurance  --  outrageous premiums,
coverage denied to those who need it most and dropped when you actually get sick
--  will get only worse if reform fails, and insurance companies know that
they're off the hook. And voters will blame politicians who, when they had a
chance to do something, made excuses instead.

Ladies and gentlemen, the nation is waiting. Stop whining, and do what needs to
be done.

URL: http://www.nytimes.com

LOAD-DATE: January 23, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


