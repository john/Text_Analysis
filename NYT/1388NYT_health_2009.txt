                   Copyright 2010 The New York Times Company


                             1388 of 2021 DOCUMENTS


                               The New York Times

                           January 21, 2010 Thursday
                              Late Edition - Final

The Messages in a Political Upset

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 38

LENGTH: 1091 words


To the Editor:

Re ''G.O.P. Surges to Senate Victory in Massachusetts'' (front page, Jan. 20):

While the Republican Party may be jumping for joy, let this historic upset be a
bellwether for all incumbent politicians that the people are fed up with
government at all levels. Main Street is going to be heard loud and clear in
2010 as our so-called representatives have done nothing for us for far too long.

Democrats were handed a majority and a mandate with the election of Barack Obama
and have failed to deliver. The insistence on bipartisanship, be it real or
fake, has cost the Democrats a supermajority.

If any politician should doubt the will of the people, the seat in Massachusetts
held for nearly half a century by Edward M. Kennedy was won by a Republican. If
this is not a jolt of reality, may we suggest preparing your concession speeches
now.

David Walker  North Dartmouth, Mass., Jan. 20, 2010

To the Editor:

Scott Brown certainly won the Senate seat from Massachusetts, and Martha Coakley
certainly lost. We can discuss the reasons at length, but the real losers will
be some 40 million Americans who came within a hair's-breadth of obtaining
access to health care and, if Republicans have their way, may well have to wait
several more decades before a new Congress can muster the courage to discuss
this again.

If we allow this to occur, it will be to our universal shame.

John Oliver  Watertown, Mass., Jan. 20, 2010

To the Editor:

Most Americans have not yet had an electoral opportunity to voice our alarm over
the growing role of the federal government, staggering budget deficits and
arrogant politicians in Washington. All of us owe a debt of gratitude to the
truly independent voters of the Commonwealth of Massachusetts. I say bravo and
well done! How appropriate that a taxpayer rebellion is again born in the Bay
State!

Perhaps now we can ask Congress and President Obama to craft sensible,
cost-effective health care reform legislation that can draw broad bipartisan
support.

Joseph P. Cunningham  Houston, Jan.   20, 2010

To the Editor:

The G.O.P. insists that the people's will in Massachusetts be given effect and
that Senator-elect Scott Brown be seated immediately. Fair enough, and with that
concession to the people's will -- since we claim to be a democracy -- let's get
rid of the supermajority, 60 votes, required in the Senate and allow the
soon-to-be 59 Democrats and independents in the Senate who represent
approximately two-thirds of the people in America to vote for or against health
care reform.

Every time these 40 (soon 41) united Republicans -- representing a third of
Americans, all of the insurance companies, all of big business, all of the
demagogued and easily led -- filibuster and prevent a vote, they violate the
people's will.  John E. Colbert  Chicago, Jan.  20, 2010

To the Editor:

So the Democrats have lost their filibuster-proof supermajority. But so what?
They caved in to the mere threat of Republican filibusters.

Why is it that President Obama, with a bigger majority than George W. Bush ever
had, has made so much less progress on his agenda than Mr. Bush managed? Because
the Democrats are cowards, and afraid to use their power.

During Mr. Bush's term, when the G.O.P. threatened the ''nuclear option'' to cut
off filibusters, the Democrats caved and did whatever the Republicans told them
to do. If the Democrats had the courage of their convictions, and really thought
that what they're doing is best for the country, they'd do now what the
Republicans did then.

I'd bet that a threat to use the ''nuclear option'' now would have a salutary
effect on Republican obstructionism, and get the Democrats' agenda moving again.

Nancy Harrison  Concord, Mass., Jan.  20, 2010

To the Editor:

Re ''Democrats Won't Rush to Pass Senate Bill'' (news article, Jan. 20):

Pulling back from health care reform won't improve the odds that Democrats
survive the midterm election in November. Why would voters elect Democrats who
back off from passing a health care bill if they won't or can't govern?

The best option open to the Democrats is for the House  to vote on the Senate
bill now. There really is no other option that offers as much health care reform
as the Senate bill does, or that stands any chance of being enacted now by the
Senate, given Republican opposition to doing anything.

I am afraid, however, that the Democrats will cave, and having come so close to
reforming health care, abandon the effort. Only President Obama can prevent this
outcome. The time has come for him to show some backbone.

Jack Archer   Ajijic, Mexico, Jan. 20, 2010

To the Editor:

Re ''A Year Later, Voters Send a Different Message'' (news analysis, front page,
Jan. 20):

No, it is the same message.

Independents and disaffected Republicans flocked to Barack Obama for his
declared interest in transparency, bipartisanship and getting things done. That
is still the message. Massachusetts made a second try at getting Washington to
hear it. It remains to be seen if Washington got it. Listening today to White
House spokesmen, I strongly doubt it.

I think that the people will just keep trying to drum it home until it is
finally heard and understood and acted upon. People voted for President Obama's
health care plan. They didn't vote for Nancy Pelosi's or Harry Reid's.

Barack Obama was so convincing during the campaign. One assumed that he would
listen to all sides and we could have health care reform that was more centrist
than extreme. But health care was just one point. And Scott Brown, in his
lengthy victory remarks on Tuesday night, listed almost all the points where the
Obama administration has demonstrated a tin ear.

Julie Finley  Washington, Jan. 20, 2010

To the Editor:

It seems appropriate to point out that President Obama's first anniversary in
office is Jan. 20. It took eight years for the Bush administration to get us
into the terrible situation we are in now, but the pundits are only too pleased
to blame President Obama for not turning the country around in a year!

You say many independent voters in Massachusetts were discouraged at the slow
pace of change, and so a blue state turned red. We expect everything to happen
overnight, and when it doesn't, we have no staying power.

We now have a president who is smart, efficient and eminently capable; let's
give him a chance to do what must be done and not expect eight years of
devastation to be overturned in a year.

Helen F. Gray  Washington Depot, Conn., Jan.  20, 2010

URL: http://www.nytimes.com

LOAD-DATE: January 21, 2010

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY POLLY BECKER)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


