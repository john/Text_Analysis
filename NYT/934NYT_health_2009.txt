                   Copyright 2009 The New York Times Company


                             934 of 2021 DOCUMENTS


                               The New York Times

                           November 5, 2009 Thursday
                              Late Edition - Final

An Invitation to Protesters

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 22

LENGTH: 289 words


House Democrats are bracing for a potential invasion of protesters in the
Capitol complex on Thursday after Representative Michele Bachmann, Republican of
Minnesota, urged opponents of the Democrats' health care legislation to come to
Washington to make their voices heard.

Ms. Bachmann, in an appearance on Fox television Tuesday morning, invited
opponents of the health care bill to join her for a news conference at noon
Thursday on the steps of the Capitol. And she urged them to visit the offices of
individual lawmakers to tell them to vote against the bill.

A protest by conservatives against the Democrats' health care legislation and
other Obama administration efforts on Sept. 12, a Saturday, drew thousands of
demonstrators to the West Front of the Capitol. It is hard to predict whether a
weekday event would draw such a large turnout.

Ms. Bachmann's effort comes as Democratic leaders are working to lock down the
votes needed to pass the bill.

Aides to Speaker Nancy Pelosi of California said they had alerted security
officials about the possibility of crowds on Thursday but did not expect
trouble.

In appealing to demonstrators, Ms. Bachmann called the health care vote,
tentatively scheduled for this Saturday, the ''Super Bowl of freedom.'' She has
repeatedly accused Democrats of pursuing ''socialized medicine'' and a
''government takeover'' of the health care system.

Some conservative groups said they were responding to Ms. Bachmann's request. In
a statement, Tony Pessaro, the founder and chairman of a ''tea party'' antitax
group in Bel Air, Md., said he had filled two buses and was working on a third.
Mr. Pessaro said he led a caravan of 12 buses to the September protest. DAVID M.
HERSZENHORN

URL: http://www.nytimes.com

LOAD-DATE: November 5, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO

PUBLICATION-TYPE: Newspaper


