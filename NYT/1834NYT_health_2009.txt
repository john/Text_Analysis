                   Copyright 2010 The New York Times Company


                             1834 of 2021 DOCUMENTS


                               The New York Times

                              April 2, 2010 Friday
                              Late Edition - Final

On the Front Line

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL; Pg. 22

LENGTH: 607 words


Passing health care reform was only half of the job. It is just as important --
likely just as politically difficult -- to quickly and smoothly carry out the
reforms. Republican critics have made clear that they are not going to stop
fighting just because comprehensive reform has become the law of the land.

One of the first things the Obama administration and Congress must do is put in
a strong chief for the Centers for Medicare and Medicaid Services, which runs
the public insurance programs that serve nearly a third of Americans. It has
been without an administrator since October 2006. That is well before President
Obama took office, but we are astonished he has left the job open for so long.

Officials say the president has finally settled on Dr. Donald Berwick. He is a
professor of pediatrics and health care policy at the Harvard Medical School,
renowned for his efforts to improve the quality and efficiency of health care --
two goals of effective reform.

You may have never heard of the agency that he would lead, but it is the largest
buyer of health care in the country and has a huge influence on medical care
through its interactions with hospitals, doctors and private health plans. It
will have a major role in getting reform right.

The Medicare program that insures older and disabled Americans will be testing
new payment mechanisms and delivery reforms that are supposed to slow the
relentless rise in health care costs while also improving quality. Pilot
programs in Medicare, for example, will reward doctors for the quality, not
quantity, of care they deliver and will encourage greater coordination among
doctors and institutions that typically work in isolation.

To help finance coverage of the uninsured, Medicare will also have to absorb
large cuts and find productivity savings. Those changes will need to be
carefully supervised to avoid harm to patients.

The agency also oversees the joint federal-state Medicaid program that insures
the poor. As part of the reform, it will need to draft regulations to guide a
big expansion in enrollment. Some states have already signaled their resistance
to taking on those added costs. That means the agency, and its new leader, will
have to come up with strategies to help states move vigorously on expansion --
and goad any laggards.

Dr. Berwick looks like a very solid choice for these tasks. In addition to his
clinical and professorial duties, he co-founded and serves as president of the
Institute for Healthcare Improvement, a consulting and analytical organization
that promotes measures to improve the quality and safety of health care while
reducing its costs.

He has a demonstrated ability to enlist hospitals, doctors and other health care
professionals in the cause of reforming their practices.

The agency will need a sustained infusion of money from Congress and talented
staff members to bring complicated reforms to fruition. Dr. Berwick's main
potential weakness is a lack of experience in running a very large organization,
especially one that will have to operate in a high-charged political
environment.

If Dr. Berwick is nominated, he would have to be confirmed by the Senate.
Senators from both sides of the debate will need to press him on how he might
backstop his lack of managerial experience and how he plans to deal with
political opposition to reform.

Democratic leaders will have to insist that any confirmation hearing stays
focused on the nominee's ideas and qualifications and not become another arena
for attacking health care reform. This job and those questions are too important
to get drowned by demagoguery.

URL: http://www.nytimes.com

LOAD-DATE: April 2, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


