                   Copyright 2009 The New York Times Company


                             589 of 2021 DOCUMENTS


                               The New York Times

                           September 14, 2009 Monday
                              Late Edition - Final

Inside The Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 975 words


International

AIDE TO OPPOSITION LEADER

In Iran Is Released

Alireza Hosseini-Beheshti, the top aide to Iran's main opposition leader who was
arrested last week because of his investigations into abuse of prisoners in the
post-election crackdown, has been released in what appears to be a sign of
retreat by the hard-core conservative authorities running Iran. PAGE A4

ISRAELI PILOT DIES IN CRASH

Lt. Assaf Ramon, the son of Israel's first astronaut, Col. Ilan Ramon, who died
in the space shuttle Columbia disaster in 2003, was killed when an F16-A plane
he was piloting crashed in the hills south of Hebron in the West Bank.  PAGE A7

JOB FAIRS FOR OLDER KOREANS

Silver job fairs, established to find jobs for people 60 and older, have
mushroomed across South Korea in the past year as part of a government effort to
assist a rapidly growing population of older Koreans adrift in a changing
society.  PAGE A6

National

EXCLUSION FROM PROPOSALS

On Health Upsets Groups

Nonprofit organizations say they are upset that Congress and the Obama
administration have not addressed their rising health care costs in the health
care proposals being floated on Capitol Hill. PAGE A12

WRESTLING WITH MEDICAID

While hot-button issues like the ''public option'' and illegal immigrants have
received far more attention, for many in Congress -- and for many governors and
state legislators -- there is a far more contentious issue: how to expand
Medicaid, the state-federal insurance program for the poorest Americans.
Prescriptions. PAGE A12

SLAIN PROTESTER REMEMBERED

James Pouillon's killing in Owosso, Mich., last week is believed to be the first
known slaying of a person protesting against abortion. The outspoken
antiabortion activist was a divisive figure in the small town where he regularly
protested with signs in public places. PAGE A11

New York

IN THE NETHERLANDS,

Lingering Love of New York

The 400th anniversary of Henry Hudson's arrival in New York Harbor was being
celebrated fiercely in the Netherlands, which backed the expedition. Dutch
claims to New York City ended nearly 350 years ago when control was formally
transferred to the British, yet the nation openly carries a lingering
attachment. PAGE A18

Business

SAME OLD STORY:

This Time Will Be Different

Bubbles are episodes of collective human madness -- euphoria over investments
whose skyrocketing values are unsustainable. But they continue to happen, as
investors insist that the next big thing will be different.  PAGE B1

SOCIAL NETWORKING BY PHONE

SayNow, a tiny Silicon Valley company whose low-key approach -- connecting stars
and their fans through voice mail -- is gaining traction, particularly among
teenage audiences. The free service says it averages 10 million fan calls a
month, or about one million voice minutes each day. PAGE B1

MAGAZINE'S FINANCIAL WOES

BusinessWeek is up for sale, but interest in buying the 83-year-old magazine has
waned as suitors have gotten a better chance to look at its books. PAGE B1

HEARING ON NEWSPAPERS

The fight for control of Philadelphia's two major newspapers could turn on a
crucial court hearing scheduled for Tuesday. Investors who bought The Inquirer
and The Daily News in 2006 have fought to remain in charge after the papers'
bankruptcy. PAGE B3

A TWITTERLIKE SENSIBILITY

After trying unsuccessfully last year to acquire Twitter, Facebook is remaking
itself to look a lot more like the microblogging site.  PAGE B4

WOMAN ARRESTED FOR BRIBERY

The police in Shanghai have detained a former employee of a Coca-Cola bottling
plant, whom they accused of corruption and bribery.   PAGE B3

Sports

WILLIAMS FINED FOR OUTBURST,

Faces Possible Suspension

Serena Williams was fined $10,000 for her tirade against a line judge at the
conclusion of her loss in the semifinals of the United States Open on Saturday,
and may face more penalties, including a possible suspension from next year's
Open. PAGE D2

GREAT PLAY BY FRESHMEN

Southern California and Michigan, two of college football's brand-name programs,
barely won big games Saturday, and each can attribute its scintillating comeback
to its freshman quarterback.  PAGE D2

N.F.L.'S DRAMA BEGINS

With the first Sunday of the N.F.L. season in the bag, some storylines -- anemic
offenses, quarterback controversies -- are already beginning to emerge.  PAGE
D10

Arts

AT CARNEGIE HALL,

A Lesson in Jamming

Trey Anastasio, the singer and guitarist for the rock band Phish, was
accompanied by an orchestra at Carnegie Hall in a concert benefiting the
foundation named for his late wife.  PAGE C1

INDIE VETERANS, UNEARTHED

All Tomorrow's Parties is a series of hardcore-enthusiast, indie-rock festivals
organized from a British production office since 1999. This year's unearthings
were the Jesus Lizard, from Chicago, which hasn't been on the road in 10 years,
as well as Suicide, from New York City. Review by Ben Ratliff.  PAGE C1

ANOTHER STAGE OF CANCER

Jenny Allen's new play, ''I Got Sick Then I Got Better,'' deals with her
experience with a diagnosis of, and treatment for, endometrial cancer and then
ovarian cancer.  PAGE C1

A LOOK AT RACE AND RADIO

''Race Music,'' a production at the Beckett Theater at Theater Row, examines the
pitch and pull between generations and among ethnicities as relations in a
changing United States evolve. Review by Daniel M. Gold.  PAGE C2

Obituaries

JACK KRAMER, 88

A Wimbledon and two-time United States singles champion, he promoted the
professional tennis tour in the 1950s, which led the way to the sport's open
era. PAGE A19

JIM CARROLL, 60

As a teenage basketball star in the 1960s at Trinity, an elite private school on
the Upper West Side of Manhattan, Mr. Carroll led a chaotic life that combined
sports, drugs and poetry. He chronicled his wild youth in ''The Basketball
Diaries.''  PAGE A19

URL: http://www.nytimes.com

LOAD-DATE: September 14, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


