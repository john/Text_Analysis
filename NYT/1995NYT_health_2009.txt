                   Copyright 2010 The New York Times Company


                             1995 of 2021 DOCUMENTS


                               The New York Times

                              May 21, 2010 Friday
                              Late Edition - Final

Senate, 59-39, Approves Vast Financial Overhaul

BYLINE: By DAVID M. HERSZENHORN; Edward Wyatt contributed reporting.

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 1

LENGTH: 1254 words


WASHINGTON -- The Senate on Thursday approved a far-reaching financial
regulatory bill, putting Congress on the brink of approving a broad expansion of
government oversight of the increasingly complex banking system and financial
markets.

The legislation is intended to prevent a repeat of the 2008 crisis, but also
reshapes the role of numerous federal agencies and vastly empowers the Federal
Reserve in an attempt to predict and contain future debacles.

The vote was 59 to 39, with four Republicans joining the Democratic majority in
favor of the bill. Two Democrats opposed the measure, saying it was still not
tough enough.

Democratic Congressional leaders and the Obama administration must now work to
combine the Senate measure with a version approved by the House in December, a
process that is expected to take several weeks.

While there are important differences -- notably a Senate provision that would
force big banks to spin off some of their most lucrative derivatives business
into separate subsidiaries -- the bills are broadly similar, and it is virtually
certain that Congress will adopt the most sweeping regulatory overhaul since the
aftermath of the Great Depression.

''It's a choice between learning from the mistakes of the past or letting it
happen again,'' the majority leader, Harry Reid of Nevada, said after the vote.
''For those who wanted to protect Wall Street, it didn't work.''

The bill seeks to curb abusive lending, particularly in the mortgage industry,
and to ensure that troubled companies, no matter how big or complex, can be
liquidated at no cost to taxpayers. And it would create a ''financial stability
oversight council'' to coordinate efforts to identify risks to the financial
system. It would also establish new rules on the trading of derivatives and
require hedge funds and most other private equity companies to register for
regulation with the Securities and Exchange Commission.

Passage of the bill would be a signature achievement for the White House, nearly
on par with the recently enacted health care law. President Obama, speaking in
the Rose Garden on Thursday afternoon, declared victory over the financial
industry and ''hordes of lobbyists'' that he said had tried to kill the
legislation.

''The recession we're emerging from was primarily caused by a lack of
responsibility and accountability from Wall Street to Washington,'' Mr. Obama
said, adding, ''That's why I made passage of Wall Street reform one of my top
priorities as president, so that a crisis like this does not happen again.''

The president also signaled that he would take a strong hand in developing the
final bill, which could mean changes to the restrictive derivatives provisions
the Senate measure includes and Wall Street opposes. It is also likely that the
administration will try to remove an exemption in the House bill that would
shield auto dealers from oversight by a new consumer protection agency. Earlier,
Mr. Obama had criticized the provision as a ''special loophole'' that would hurt
car buyers.

As the Senate neared a final vote, Senator Sam Brownback, Republican of Kansas,
withdrew an amendment to put a similar exemption for auto dealers into the
Senate bill.

Mr. Brownback's move had the effect of killing an amendment by Senators Jeff
Merkley, Democrat of Oregon, and Carl Levin, Democrat of Michigan, to tighten
language barring banks from proprietary trading, or playing the markets with
their own money -- a restriction generally known as the Volcker rule for the
former Fed chairman Paul A. Volcker, who proposed the idea. Congressional
Republican leaders, adopting an election-year strategy of opposing initiatives
supported by the Obama administration, voiced loud criticism of the legislation
while trying to insist that they still wanted tougher policing of Wall Street.

But while Republicans criticized the bill in mostly political terms, arguing
that it was an example of Democrats' trying to expand the scope of government,
some experts have warned that the bill, by focusing too much on the causes of a
past crisis, still leaves the financial system vulnerable to a major collapse.

The Senate bill, sponsored primarily by Senator Christopher J. Dodd, Democrat of
Connecticut and chairman of the banking committee, would  seek to curb abusive
lending by creating a powerful Bureau of Consumer Protection within the Federal
Reserve to oversee nearly all consumer financial products.

In response to the huge bailouts in 2008, the bill seeks to ensure that troubled
companies, no matter how big or complex, can be liquidated at no cost to
taxpayers. It would empower regulators to seize failing companies, break them
apart and sell off the assets, potentially wiping out shareholders and
creditors.

To coordinate efforts to identify risks to the financial system, the bill would
create a ''financial stability oversight council'' composed of the Treasury
secretary, the chairman of the Federal Reserve, the comptroller of the currency,
the director of the new consumer financial protection bureau, the heads of the
Securities and Exchange Commission and the Federal Deposit Insurance
Corporation, the director of the Federal Housing Finance Agency and an
independent appointee of the president.

The bill would touch virtually every aspect of the financial industry, imposing,
for instance, a thicket of rules for the trading of derivatives, the complex
instruments at the center of the 2008 crisis.

With limited exceptions, derivatives would have to be traded on a public
exchange and cleared through a third party.

And, under a provision written by Senator Blanche L. Lincoln, Democrat of
Arkansas, some of the biggest banks would be forced to spin off their trading in
swaps, the most lucrative part of the derivatives business, into separate
subsidiaries, or be denied access to the Fed's emergency lending window.

The banks oppose that provision, and the administration has also said that it
sees no benefit.

Concern about the derivatives provisions also led Senator Maria Cantwell,
Democrat of Washington, to vote against the bill, saying it still included a
dangerous loophole that would undermine efforts to regulate derivative trades.
Senator Russ Feingold of Wisconsin was the other Democrat to oppose the measure.

The four Republicans to support the bill were Senators Susan Collins and Olympia
J. Snowe of Maine; Scott Brown, the freshman from Massachusetts; and Charles E.
Grassley of Iowa, who is up for re-election this year.

Among the differences between the House and Senate bills is the inclusion in the
House measure of a $150 billion fund, to be financed by a fee on big banks, to
help pay for liquidation of failing financial companies.

The administration opposes the fund, which it says it believes could hamper its
ability to deal with a more costly collapse of a financial company. Republicans
demanded that a similar $50 billion fund be removed from the Senate bill because
they said it would encourage future bailouts of failed financial companies.

There are numerous other differences. For instance, the House bill addresses the
consumer protection goals by establishing a stand-alone agency that would be
subject to annual budget appropriations by Congress. The Senate bill establishes
its consumer protection bureau within the Federal Reserve, limiting future
Congressional oversight.

Lawmakers said that the bills would be reconciled in a formal conference
proceeding, possibly televised.

URL: http://www.nytimes.com

LOAD-DATE: May 21, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Senators Harry Reid, from left, Barbara Boxer, Blanche Lincoln
and Richard Durbin voted for the bill on Thursday. (PHOTOGRAPH BY MARK
WILSON/GETTY IMAGES) (A3)

PUBLICATION-TYPE: Newspaper


