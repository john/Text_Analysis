                   Copyright 2009 The New York Times Company


                             348 of 2021 DOCUMENTS


                               The New York Times

                            August 18, 2009 Tuesday
                         The New York Times on the Web

'Public Option' in Health Plan May Be Dropped

BYLINE: By SHERYL GAY STOLBERG; Matthew L. Wald and Brian Knowlton contributed
reporting from Washington.

SECTION: Section ; Column 0; National Desk; Pg.

LENGTH: 1389 words

DATELINE: PHOENIX


The White House, facing increasing skepticism over President Obama's call for a
public insurance plan to compete with the private sector, signaled Sunday that
it was willing to compromise and would consider a proposal for a nonprofit
health cooperative being developed in the Senate.

The ''public option,'' a new government insurance program akin to Medicare, has
been a central component of Mr. Obama's agenda for overhauling the health care
system, but it has also emerged as a flashpoint for anger and opposition.
Kathleen Sebelius, the health and human services secretary, said the public
option was ''not the essential element'' for reform and raised the idea of the
co-op during an interview on CNN.

Word that the public option might be dropped angered many liberals. One
prominent Democrat, former Governor Howard Dean of Vermont, said on Monday that
he saw a public plan as inextricably linked to a health overhaul. ''I don't
think it can pass without the public option,'' Mr. Dean, who is a physician and
a former chairman of the Democratic National Committee, said on  ''The Early
Show'' on CBS.

''There are too many people who understand, including the president himself, the
public option is absolutely linked to reform,'' he said. ''You can't have reform
without a public option. If you really want to fix the health-care system,
you've got to give the public the choice of having such an option.''

Dropping the public option might weaken support for a health care overhaul among
Democrats in Congress. ''I am not interested in passing health care reform in
name only,'' Senator Russ Feingold of Wisconsin said in a statement issued on
Monday. ''Without a public option, I don't see how we will bring real change to
a system that has made good health care a privilege for those who can afford
it.''

Mr. Obama himself sought to play down the significance of the public option at a
town-hall-style meeting on Saturday in Grand Junction, Colo., when a university
student challenged him on how private insurers could compete with the
government.

After strongly defending the public plan, the president suggested that he, too,
viewed it as only a small piece of a broader initiative intended to control
costs, expand coverage, protect consumers and make the delivery of health care
more efficient.

''The public option, whether we have it or we don't have it, is not the entirety
of health care reform,'' Mr. Obama said. ''This is just one sliver of it, one
aspect of it.''

For Mr. Obama, giving up on the public plan would have risks and rewards. The
reward is that he could punch a hole in Republican arguments that he wants a
''government takeover'' of health care and possibly win some Republican votes.
The risk is that he could alienate liberal Democrats, whose support he will also
need to pass a bill.

On Sunday, Senator John D. Rockefeller IV, Democrat of West Virginia, affirmed
his support for the public option. ''I believe the inclusion of a strong public
plan option in health reform legislation is a must,'' Mr. Rockefeller said in a
statement. ''It is the only proven way to guarantee that all consumers have
affordable, meaningful and accountable options available in the health insurance
marketplace.''

White House officials say the president has not abandoned the idea of a pure
government plan, a central feature of the legislation moving through the House.
But Ms. Sebelius's comments did seem to open the door, and at least one Democrat
close to the White House said the administration was well aware that, with
moderate Senate Democrats opposed to the idea of a public plan, Mr. Obama might
have to give up on the notion to get a bill through.

''The president is going to continue to try to persuade everyone of the great
value of having a true public plan,'' said this Democrat, who spoke on condition
of anonymity to avoid discussing strategy publicly. ''But at the end of the day,
I believe he recognizes that there are other, arguably less effective, ways to
achieve greater coverage, more choice, better quality and lower cost in our
system.''

In an interview on Sunday, Mr. Obama's senior adviser, David Axelrod, said the
president remained convinced that a public plan was ''the best way to go.'' But
Mr. Axelrod said the nuances of how to develop a nonprofit competitor to private
industry had never been ''carved in stone.''

On Capitol Hill, the Senate Finance Committee is expected to produce a bill that
features a nonprofit co-op. The author of the idea, Senator Kent Conrad,
Democrat of North Dakota and chairman of the Budget Committee, predicted Sunday
that Mr. Obama would have no choice but to drop the public option.

''The fact of the matter is, there are not the votes in the United States Senate
for the public option,'' Mr. Conrad said on ''Fox News Sunday.'' ''There never
have been. So to continue to chase that rabbit, I think, is just a wasted
effort.''

The co-op, modeled after rural electric and agricultural cooperatives in Mr.
Conrad's home state, would offer insurance through a nonprofit, nongovernmental
consumer entity run by its members. Mr. Axelrod said one downside of a co-op,
from Mr. Obama's point of view, was that it might be unable to ''scale up in
such a way that would create a robust'' competitor to private insurers.

And whether a co-operative would actually bring Republicans on board with Mr.
Obama is unclear. Senator Richard C. Shelby, the Alabama Republican who appeared
alongside Mr. Conrad on ''Fox News Sunday,'' called the co-op idea ''a step in
the right direction,'' adding: ''I don't know if it will do everything people
want, but we ought to look at it. I think it's a far cry from the original
proposals.''

As Mr. Obama envisions it, the public option would be a government-backed plan
available to consumers through a health exchange where people could buy
insurance, public or private, that best fits their needs. While a public plan
might require some government financing to start up, the idea is for it to be
financially self-sustaining and require no subsidies, Mr. Axelrod said.

Republicans argue that a public plan would invariably drive private insurers out
of business and prompt employers to drop private coverage, pushing people who
are already insured onto a plan run by the government. Mr. Obama counters that a
public option would keep insurers ''honest'' by forcing them to compete in the
marketplace, although he has said all along he would be open to other ideas.

In her interview Sunday on CNN, Ms. Sebelius was asked if it was time to come up
with an alternative to the public option. She replied that the president's main
concern was to promote competition with the private sector.

''What's important is choice and competition,'' she said. ''And I'm convinced at
the end of the day, the plan will have both of those.''

Here in Phoenix, where Mr. Obama is to address the Veterans of Foreign Wars on
Monday, conservative groups including Americans for Prosperity are planning to
protest the health plan. The same groups have turned up around the country at
Congressional town-hall-style meetings, which have sometimes turned into
shouting matches as opponents denounce him for promoting ''socialized
medicine.''

Mr. Obama is pushing back. As the nation heads into the last two weeks of
August, a time when the White House believes many Americans will tune out of the
health care debate to take their vacations, he has been waging an intense public
relations offensive to convince Americans that the health care system should be
overhauled. (He, too, is planning a vacation, to Martha's Vineyard the last week
of August.)

In the past week alone, Mr. Obama has held three town-hall-style meetings -- in
addition to the session on Saturday in Grand Junction, he traveled to
Portsmouth, N.H., and Belgrade, Mont. -- and devoted his weekly radio and
Internet address to health care. On Sunday, he published an opinion article in
The New York Times arguing, as he has in recent days, that overhauling the
system would result in protections for consumers.

''This is not about putting the government in charge of your health insurance,''
Mr. Obama wrote. ''I don't believe anyone should be in charge of your health
care decisions but you and your doctor -- not government bureaucrats, not
insurance companies.''

URL: http://www.nytimes.com

LOAD-DATE: August 18, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


