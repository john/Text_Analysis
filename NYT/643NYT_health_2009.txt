                   Copyright 2009 The New York Times Company


                             643 of 2021 DOCUMENTS


                               The New York Times

                           September 20, 2009 Sunday
                              Late Edition - Final

Taking the Fun Out of Popping Pain Pills

BYLINE: By NATASHA SINGER

SECTION: Section BU; Column 0; Money and Business/Financial Desk; SLIPSTREAM;
Pg. 3

LENGTH: 1085 words


HOW can you get a faster high from sustained-release pain pills like OxyContin?
Let me count some of the ways.

People have crushed them using bookends, hammers, mortars and pestles, and then
snorted the powder, according to doctors who study addiction. They've chewed and
swallowed fistfuls of pills. They've minced the pills in blenders, pulverized
them in coffee grinders, dissolved them in water and then injected the liquid.

Even for those of us who don't inhale, the misuse and abuse of prescription
painkillers called opioids should matter because, putting moral and ethics aside
for the moment, it's costing us billions of dollars.

In a 2008 federal survey, an estimated 4.7 million Americans were found to have
used prescription pain relievers for nonmedical reasons in the previous month.
The abuse of opioids now costs at least $11 billion annually in excess medical
care including overdoses by adults and accidental ingestion by children, said
Howard G. Birnbaum, a health economist with the Analysis Group in Boston.

Corporate America loves a void, and now some pharmaceutical companies are
developing innovative opioids intended to deter tampering and meet the market's
need.

Some pills under development are rubberlike and harder to crush.  Others contain
ingredients that cause unpleasant reactions in the body, like flushing or
itching, if the pill is adulterated. Taking a cue from exploding ink packets
that can render stolen money unusable, some pills have an outer opioid layer and
an inner core that, if tampered with, releases a drug that counters the high of
the pain reliever.

Embeda, made by King Pharmaceuticals in Bristol, Tenn., uses the last of these
strategies. Scheduled to arrive in drug stores this weekend, Embeda is the first
of the new so-called abuse-deterrent opioids to reach the market. But, the Food
and Drug Administration has approved Embeda only as a pain reliever, not as an
abuse-deterrent drug, an agency spokeswoman said.

In a clinical study of Embeda, a majority of volunteers experienced less
euphoria when they took a crushed form of the drug compared to immediate-release
morphine. But the company has not yet established the real-world significance of
the results.

The hope for abuser-unfriendly pills is that they might eventually decrease
abuse. The drawback is that the pills represent (bad pun alert!) a fix for only
one part of a very complex problem.

In a sense, the new opioids reflect the ups and downs of broader efforts to
overhaul health care: addressing cosmetic aspects of the problem without
thoroughly grappling with systemic causes.

While legislators are diligently trying to figure out how to get medical
coverage for millions of uninsured people, the insurance gap is just one symptom
of an ailing, inefficient medical system inclined toward reactive rather than
preventive care.

Likewise, the new pain relievers are intended to deter an important cause of
opioid abuse: tampering. But, for people who simply ingest too many whole,
intact pills, tamper-resistant opioids still don't reduce overdose risks, said
Dr. Nathaniel P. Katz, the chief executive of Analgesic Research, a research and
consulting firm in Boston that specializes in the development of pain
treatments.

(Dr. Katz and every other expert quoted here say they have worked as paid
consultants for King Pharmaceuticals or conducted research sponsored by the
company).

Drug companies have promoted novel opioids as nonaddictive on other occasions
and failed, said Dr. Katz, who is also a neurologist and adjunct faculty member
at Tufts University medical school. He cited heroin, introduced by Bayer as a
branded drug in 1898, and OxyContin, introduced by Purdue Pharma in 1995.
Moreover, according to Steven D. Passik, a clinical psychologist at Memorial
Sloan-Kettering Cancer Center in Manhattan, the advent of so-called
abuse-deterrent opioids also doesn't solve another, larger problem: uneven state
and physician monitoring of the legal circulation of opioids.

''It does not obviate the need to put together a strategy to teach every
prescriber to do Addiction Medicine 101,'' Dr. Passik said.

A more complete strategy to combat opioid abuse, he said, would involve asking
busy primary care doctors to routinely assess how vulnerable to addiction a
patient might be and to closely track patients taking opioids for chronic pain.
That, of course, means some health insurers would have to step up and cover more
frequent urine tests and doctor visits for those patients.

Meanwhile, on a regulatory level, some states should devote greater resources to
online prescription monitoring systems, Dr. Katz said. These are databases used
by pharmacists and doctors to check whether a patient already has multiple
opioid prescriptions, a phenomenon called ''doctor shopping'' that is a red flag
for drug abuse.

On a federal level, the F.D.A. is developing a comprehensive risk management
system for sustained-release opioids, an agency spokeswoman said. The program
may include a certification requirement for doctors who prescribe and pharmacies
that dispense those kinds of painkillers.

But the F.D.A. could solve another slice of the problem -- people who share
legally prescribed opioids with friends and family members -- if it required a
patient training program, Dr. Katz said.

If doctors verbally warned patients, '''Don't share, it's a felony, and you will
kill your neighbors, friends and family,' you'd have a lot less sharing,'' he
said.

Finally, opioid abuse might be diminished if there were national standards on
how to dispose of unused pills, said Sandra Comer, a professor of clinical
neurobiology in the psychiatry department at Columbia University medical school.

Often, Dr. Comer said, patients are prescribed an opioid for a month or more,
but their pain subsides after a week or two. Then they have leftover pills in
their medicine cabinets which are open to pilfering by, for example, college-age
children or visitors.

But nobody knows what to do with unused pills because pharmacies won't take them
back. Flushing them down the toilet is a bad idea because the chemicals can
pollute the water system. Some experts recommend mixing the pills with used
kitty litter or coffee grounds and throwing them in the garbage.

Perhaps, in addition to patient training, the F.D.A. might institute a give-back
program -- akin to government buy-back deals for guns --  for anybody housing an
inventory of unused prescription painkillers.

URL: http://www.nytimes.com

LOAD-DATE: September 20, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY OLIVER MUNDAY)

PUBLICATION-TYPE: Newspaper


