                   Copyright 2009 The New York Times Company


                             657 of 2021 DOCUMENTS


                               The New York Times

                           September 21, 2009 Monday
                              Late Edition - Final

Obama Insists That Insurance Will Be Affordable

BYLINE: By JEFF ZELENY and ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 16

LENGTH: 1076 words

DATELINE: WASHINGTON


President Obama on Sunday pushed back against the argument from liberal
Democrats that the leading health care bill in the Senate would place a new
financial burden on the middle class, saying in a series of television
appearances that insurance would be made affordable through a competitive
exchange and subsidies.

As the Senate Finance Committee prepares to take up the legislation this week,
the White House is trying to hold together fractious Democrats, particularly
those who believe that the plan is not generous enough for people required to
buy insurance. Mr. Obama argued that the proposal was not a bad deal for the
middle class and that it could not be called a tax hike.

''Right now, everybody in America, just about, has to get auto insurance,'' Mr.
Obama said on ''This Week with George Stephanopoulos'' on ABC. ''Nobody
considers that a tax increase.''

The interviews, conducted Friday in the Roosevelt Room of the White House and
broadcast on  news programs, represented the first  saturation of  the Sunday
morning airwaves to that  extent by a president. Mr. Obama said the health care
fight was tougher  than he had expected  and conceded that he had struggled in
''breaking through.'' He said he remained confident that he would sign a bill
and welcomed Republicans to the effort, but added, ''I don't count on them.''

The argument over the affordability of insurance is one of the most pressing
points of contention in imposing a mandate for people to obtain coverage. Since
the presidential race last year, many Democrats have braced for this treacherous
debate, which goes beyond whether the insurance mandate should be called a tax
increase to whether people might actually have to pay more for their coverage.

The president did not fully endorse a proposal by Senator Max Baucus, Democrat
of Montana and the chairman of the Finance Committee, but called it ''a serious,
strong effort to move an agenda forward.'' Under the measure, the government
would provide subsidies -- in the form of tax credits -- to help people buy
coverage through new insurance exchanges.

The Baucus plan, like the other health care bills in Congress, offers subsidies
to help low- and middle-income Americans. But eligibility is more limited, and
the subsidies appear to be less generous than in the other proposals, leading
some Democrats to question whether some people would be able to afford
insurance.

The subsidies are the biggest single cost in the health care proposals. In the
Baucus bill, it is $463 billion over 10 years; the cost is $773 billion in the
House bill. The House plan costs more, in part, because it provides more
generous subsidies.

One question stands out above most others in the debate: What is affordable?

People with incomes from 300 percent to 400 percent of the federal poverty line
-- $66,150 to $88,200 for a family of four -- might be expected to pay 11
percent to 13 percent of their income on insurance premiums, the legislation in
Congress suggests.

The Baucus plan would also require people to have insurance, with penalties as
high as $3,800 for a family that goes without coverage. Senator Charles E.
Schumer of New York and other Democrats have proposed waiving penalties for
people who cannot find plans that cost less than 7 percent of their income.

As the administration is seeking to reconcile competing versions of the health
care bills, Mr. Obama avoided specific figures and did not tip his hand about
what he believed was an appropriate level for subsidies. But he said rising
health care costs already amounted to a tax increase on families.

As he did in his health care speech to Congress on Sept. 9, the president spoke
favorably on Sunday about a public insurance option to inject competition into
the market, but he did not call it  essential. He urged ''folks in my own
party'' to move beyond that debate and focus on broader aspects of the effort.

''Everybody's going to have to give some in order to get something done,'' Mr.
Obama said on the NBC program ''Meet the Press.'' He added, ''We've got to get
past some of these ideological arguments to actually make something happen.''

Senator Mitch McConnell of Kentucky, the Republican leader, predicted that there
would be consequences for the Democrats  if they pushed a  measure through
Congress without bipartisan support. He said the current plan would raise taxes
on small businesses and individuals and would affect Medicare coverage.

''If they try to use this legislative loophole called reconciliation, what
they'll be doing, in effect, is jamming through a proposal to rewrite the
economy with about 24 hours of debate,'' Mr. McConnell said on ''State of the
Union'' on CNN. ''I think that will produce a very severe reaction among the
American people.''

Yet Mr. Obama's television appearances across the spectrum of channels on Sunday
underscored his decision to move forward on health care with -- or, more likely,
without -- Republicans.

''What I'm trying to do is explain the facts, which are if we don't do anything,
a lot of Americans are going to be much worse off,'' Mr. Obama said on ''Face
the Nation'' on CBS.

The only major news program that he did not appear on was ''Fox News Sunday,'' a
decision that administration officials said was made because the network did not
broadcast his speech to Congress this month.

Instead, Mr. Obama spoke to Univision, a Spanish-language channel. He reaffirmed
the administration's position that illegal immigrants should not have access to
the proposed insurance exchanges. He said he was sensitive to the problems of
''mixed families,'' in which the parents are illegal immigrants but the children
are American citizens born in this country.

''We're going to make sure that those children are covered,'' Mr. Obama said.

In his interview on ABC, Mr. Obama was asked whether he had a moment in the
early months of his presidency when he realized that he needed to do better. ''I
think there have been times where I have said, 'I've got to step up my game in
terms of talking to the American people about issues like health care,' '' Mr.
Obama said.

Mr. Stephanopoulos, the interviewer, said: ''You lost control?''

''Well, not so much lost control, but where I've said to myself, 'somehow I'm
not breaking through,' '' Mr. Obama said. He added: ''It's very hard for people
to get their whole arms around it. And that's been a case where I have been
humbled.''

URL: http://www.nytimes.com

LOAD-DATE: September 21, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: President Obama appeared on five television news programs Sunday
in a flurry of activity to promote passage of a health bill.(PHOTOGRAPH BY KARIN
COOPER/''FACE THE NATION,'' VIA ASSOCIATED PRESS)

PUBLICATION-TYPE: Newspaper


