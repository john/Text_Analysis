                   Copyright 2009 The New York Times Company


                             1176 of 2021 DOCUMENTS


                               The New York Times

                            December 13, 2009 Sunday
                              Late Edition - Final

Can We Afford It?

SECTION: Section WK; Column 0; Editorial Desk; EDITORIAL; Pg. 8

LENGTH: 896 words


Republican critics have a fiercely argued list of reasons to oppose health care
reform. One that is resonating is that the nation cannot afford in tough
economic times to add a new trillion-dollar health care entitlement.

We understand why Americans may be skittish, but the argument is at best
disingenuous and at worst a flat misrepresentation. Over the next two decades,
the pending bills would actually reduce deficits by a small amount and reforms
in how medical care is delivered and paid for -- begun now on a small scale --
could significantly reduce future deficits. Here is a closer look at the
benefits and costs of health care reform:

STATUS QUO IS UNSUSTAINABLE More than 46 million Americans have no insurance,
and millions more have such poor coverage that a severe illness threatens
bankruptcy. Small employers are dropping coverage because of the cost. Those
lucky enough to have insurance are struggling with higher premiums and
co-payments, and worry that if they are laid off they could lose coverage.

Without reform, that bad situation will only get worse. The Commonwealth Fund, a
respected research organization, warned that the average premium for family
coverage in employer-sponsored policies would almost double in the coming
decade, from about $12,300 in 2008 to $23,800 in 2020, with part paid by workers
and part by employers. Premiums are also soaring for individuals who buy their
own coverage directly.

BUT A TRILLION DOLLARS? Both the House and Senate bills would cover more than 30
million of the uninsured, and fully pay for it -- in part by raising taxes
(either on wealthy Americans or high-premium health plans and certain
manufacturers and insurers) and in part by cutting payments to health care
providers and private plans that serve Medicare patients.

A trillion dollars is still a lot of money, but it needs to be put in some
perspective. Extending Bush-era tax cuts for the wealthy would very likely cost
$4 trillion over the next decade. And the Medicare prescription drug benefit,
passed by a Republican-dominated Congress, is expected to cost at least $700
billion over the next decade. Unlike this health care reform, it became law with
no offsetting cuts and very little provision to pay for it.

YES, THEY OVER-PROMISED President Obama and his aides have, at times, made it
sound as if health care reform was the answer to runaway deficits and soaring
premiums. That is true in the long run, but not now.

The Congressional Budget Office projects that the vast majority of Americans,
those covered by employer-sponsored insurance, would see little change or a
modest decline in their average premiums under the Senate bill. It predicts that
the bills would reduce deficits in the first decade by a modest $130 billion or
so and perhaps $650 billion in the next decade -- a small share of the burden.

Critics scoff that Congress will never carry out the required cuts in payments
to Medicare providers. It is true that Congress has repeatedly deferred
draconian cuts in doctors' reimbursements. It has had no reluctance imposing
other savings. The Center on Budget and Policy Priorities, a liberal analytical
group, examined every major Medicare cut in deficit reduction bills over the
past two decades. Virtually all of the savings imposed in the 1990, 1993 and
2005 bills survived intact. So did 80 percent of the savings in the 1997
Balanced Budget Act.

There is an easy way to stiffen Congress's spine: it should adopt separate
pay-as-you-go rules that would require that any concession to providers be paid
for by tax increases or compensating cuts in other programs.

SHOULD WE GIVE UP ON SAVINGS? The House and Senate bills, and the stimulus
legislation, have a lot of ideas that could bring down costs over time.

Electronic medical records could eliminate redundant tests; standardized forms
and automated claims processing could save hundreds of billions of dollars;
''effectiveness'' research would help doctors avoid costly treatments that don't
work; various pilot projects devised to foster better coordination of care and a
shift away from fee-for-service toward fixed payments for a year's worth of a
patient's care all show some promise.

These reforms are mostly untested. And the C.B.O. is properly cautious when it
says that it does not see much if any savings for the government during the next
decade, in part because of upfront costs and in part because no one knows what
will work. These efforts are unlikely to be tried on any serious scale without
reform.

NO SINGLE FIX The debate is not over and sensible proposals are emerging in the
Senate to strengthen cost control. Various amendments would increase the
penalties for hospitals that infect patients, let Americans import cheaper drugs
from abroad and modestly increase the powers of a new commission that is
supposed to recommend ways to reduce Medicare costs. The House bill has
cost-cutting measures that could be incorporated into a final bill, including
authority for the government to negotiate lower drug prices for Medicare
beneficiaries.

Aggressive testing of promising ideas should increase the likelihood of ultimate
success. And millions of uninsured Americans should not be forced to wait until
all the answers are found.

This editorial is a part of a comprehensive examination of the debate over
health care reform.

URL: http://www.nytimes.com

LOAD-DATE: December 13, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


