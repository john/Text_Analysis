                   Copyright 2009 The New York Times Company


                             838 of 2021 DOCUMENTS


                               The New York Times

                            October 18, 2009 Sunday
                              Late Edition - Final

Quotations of the Week

SECTION: Section WK; Column 0; Week in Review Desk; Pg. 2

LENGTH: 86 words


''He's a semiliterate individual who has met with no more than a handful of
non-Muslims in his entire life. And he's staged one of the most remarkable
military comebacks in modern history.''

Bruce Riedel, a former C.I.A. officer, on Mullah Muhammad Omar, the reclusive
leader of the Taliban.

''I think the administration has put her in the driver's seat; it's very
disconcerting.''

Representative Raul M. Grijalva, Democrat of Arizona, on the Republican Senator
Olympia J. Snowe's influence in the health care debate.

URL: http://www.nytimes.com

LOAD-DATE: October 18, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


