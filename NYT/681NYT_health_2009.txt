                   Copyright 2009 The New York Times Company


                             681 of 2021 DOCUMENTS


                               The New York Times

                           September 25, 2009 Friday
                              Late Edition - Final

It's Easy Being Green

BYLINE: By PAUL KRUGMAN

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 29

LENGTH: 795 words


So, have you enjoyed the debate over health care reform? Have you been impressed
by the civility of the discussion and the intellectual honesty of reform
opponents?

If so, you'll love the next big debate: the fight over climate change.

The House has already passed a fairly strong cap-and-trade climate bill, the
Waxman-Markey act, which if it becomes law would eventually lead to sharp
reductions in greenhouse gas emissions. But on climate change, as on health
care, the sticking point will be the Senate. And the usual suspects are doing
their best to prevent action.

Some of them still claim that there's no such thing as global warming, or at
least that the evidence isn't yet conclusive. But that argument is wearing thin
--  as thin as the Arctic pack ice, which has now diminished to the point that
shipping companies are opening up new routes through the formerly impassable
seas north of Siberia.

Even corporations are losing patience with the deniers: earlier this week
Pacific Gas and Electric canceled its membership in the U.S. Chamber of Commerce
in protest over the chamber's ''disingenuous attempts to diminish or distort the
reality'' of climate change.

So the main argument against climate action probably won't be the claim that
global warming is a myth. It will, instead, be the argument that doing anything
to limit global warming would destroy the economy. As the blog Climate Progress
puts it, opponents of climate change legislation ''keep raising their estimated
cost of the clean energy and global warming pollution reduction programs like
some out of control auctioneer.''

It's important, then, to understand that claims of immense economic damage from
climate legislation are as bogus, in their own way, as climate-change denial.
Saving the planet won't come free (although the early stages of conservation
actually might). But it won't cost all that much either.

How do we know this? First, the evidence suggests that we're wasting a lot of
energy right now. That is, we're burning large amounts of coal, oil and gas in
ways that don't actually enhance our standard of living  --  a phenomenon known
in the research literature as the ''energy-efficiency gap.'' The existence of
this gap suggests that policies promoting energy conservation could, up to a
point, actually make consumers richer.

Second, the best available economic analyses suggest that even deep cuts in
greenhouse gas emissions would impose only modest costs on the average family.
Earlier this month, the Congressional Budget Office released an analysis of the
effects of Waxman-Markey, concluding that in 2020 the bill would cost the
average family only $160 a year, or 0.2 percent of income. That's roughly the
cost of a postage stamp a day.

By 2050, when the emissions limit would be much tighter, the burden would rise
to 1.2 percent of income. But the budget office also predicts that real G.D.P.
will be about two-and-a-half times larger in 2050 than it is today, so that
G.D.P. per person will rise by about 80 percent. The cost of climate protection
would barely make a dent in that growth. And all of this, of course, ignores the
benefits of limiting global warming.

So where do the apocalyptic warnings about the cost of climate-change policy
come from?

Are the opponents of cap-and-trade relying on different studies that reach
fundamentally different conclusions? No, not really. It's true that last spring
the Heritage Foundation put out a report claiming that Waxman-Markey would lead
to huge job losses, but the study seems to have been so obviously absurd that
I've hardly seen anyone cite it.

Instead, the campaign against saving the planet rests mainly on lies.

Thus, last week Glenn Beck  --  who seems to be challenging Rush Limbaugh for
the role of de facto leader of the G.O.P.  --  informed his audience of a
''buried'' Obama administration study showing that Waxman-Markey would actually
cost the average family $1,787 per year. Needless to say, no such study exists.

But we shouldn't be too hard on Mr. Beck. Similar  --  and similarly false  --
claims about the cost of Waxman-Markey have been circulated by many supposed
experts.

A year ago I would have been shocked by this behavior. But as we've already seen
in the health care debate, the polarization of our political discourse has
forced self-proclaimed ''centrists'' to choose sides  --  and many of them have
apparently decided that partisan opposition to President Obama trumps any
concerns about intellectual honesty.

So here's the bottom line: The claim that climate legislation will kill the
economy deserves the same disdain as the claim that global warming is a hoax.
The truth about the economics of climate change is that it's relatively easy
being green.

URL: http://www.nytimes.com

LOAD-DATE: September 25, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


