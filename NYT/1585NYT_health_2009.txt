                   Copyright 2010 The New York Times Company


                             1585 of 2021 DOCUMENTS


                               The New York Times

                              March 1, 2010 Monday
                              Late Edition - Final

Pelosi Says She'll Get Votes Needed for Health Bill

BYLINE: By ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 18

LENGTH: 527 words


WASHINGTON -- Speaker Nancy Pelosi says she is confident she will be able to get
the votes needed to pass sweeping health care legislation in the House, even if
it threatens the political careers of some members of her party.

In an interview carried Sunday on ABC's ''This Week,'' Ms. Pelosi said she was
working on changes to a Senate-passed bill that would make it acceptable to the
House.

Ms. Pelosi was asked what she would say to House Democrats who were ''in real
fear of losing their seats in November if they support you now.''

''Our members, every one of them, wants health care,'' Ms. Pelosi said. ''They
know that this will take courage. It took courage to pass Social Security. It
took courage to pass Medicare. And many of the same forces that were at work
decades ago are at work again against this bill.''

''But,'' Ms. Pelosi continued, ''the American people need it. Why are we here?
We're not here just to self-perpetuate our service in Congress. We're here to do
the job for the American people, to get them results that give them not only
health security, but economic security.''

On CNN's ''State of the Union,'' Ms. Pelosi was asked about opinion polls
showing opposition to the Democrats' health care bills.

''The point is that we have a responsibility here,'' Ms. Pelosi said. ''The
Republicans have had a field day going out there and misrepresenting what is in
the bill, but that's what they do.''

The House passed a bill to remake the health care system in early November. But
on Sunday, Ms. Pelosi said, ''There isn't a bill.''

The speaker said she would have another version, incorporating compromises
between the House and the Senate, ''in a matter of days.'' When the new package
is ready, Ms. Pelosi said, she will sell it to the public.

''Our bill, the House and the Senate bill, had some major differences, which
we're hoping now to reconcile,'' Ms. Pelosi said. ''Then when we have a bill --
as I say, you can bake the pie, you can sell the pie, but you have to have a pie
to sell. And when we do, we will take it out there.''

Republicans said Ms. Pelosi and President Obama were pursuing a high-risk
strategy.

Senator Lamar Alexander of Tennessee, the No. 3 Republican in the Senate, said,
''It would be a political kamikaze mission for the Democratic Party if they jam
this through after the American people have been saying, look, we're trying to
tell you in every way we know how, in elections, in surveys, in town hall
meetings, we don't want this bill.''

Democratic leaders hope to orchestrate passage of health legislation in a
three-step process intended to avoid the threat of a filibuster by Senate
Republicans. Under this plan, the House would pass the health bill approved in
December by the Senate, and both chambers would approve a separate package of
changes using a parliamentary device known as budget reconciliation.

But on NBC's ''Meet the Press'' on Sunday, the House Republican whip,
Representative Eric Cantor of Virginia, said: ''I'll tell you one thing: If
Speaker Pelosi rams this bill through the House using a reconciliation process,
they will lose their majority in Congress in November.''

URL: http://www.nytimes.com

LOAD-DATE: March 1, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


