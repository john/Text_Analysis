                   Copyright 2009 The New York Times Company


                             530 of 2021 DOCUMENTS


                               The New York Times

                          September 9, 2009 Wednesday
                              Late Edition - Final

Obama's Audience Speaks First

BYLINE: By ANNA DEAVERE SMITH

SECTION: Section A; Column 0; Editorial Desk; OP-ED CONTRIBUTOR; Pg. 29

LENGTH: 1702 words


Over the last few years, in preparation for a  new play,  I interviewed doctors,
patients and  healers about  the human body, its resilience and its
vulnerability. Although our conversations were not primarily about the health
care debate, they do reveal many of the feelings and thoughts of the people in
the audience President Obama will address tonight.

The unruliness that now animates the conversation stems from our passions, hopes
and discomforts -- about life, death, who should (or should not) take care of us
and whom we should take care of. The president's audience has  a million and one
perspectives, some of them clumping together like blood platelets under one
political roof or another. The following excerpts (not all of which are in my
play) reflect the range of views.

I personally think we need to go to a deal like when I broke my back and I went
to the military hospital ... Brooke Army Medical Center, in San Antonio, Tex.
Cost me a flat rate, $1,200  a day, and I spent the first six days in I.C.U.
Everyone, everyone pays a flat rate, no matter what it is. I got hung up by a
bull [that] weighed over a ton. ...When I hit the ground, I was on my side and
he stepped on my left side with two back feet, broke four ribs, L2, L3 [lumbar
vertebrae], and lost half my kidney. And them doctors, I felt, like  -- well,
they told me, if I'd have went to the other hospital in San Antone, they'd have
just taken my kidney out. They tried a new deal. They put a stent in there. But
them guys get paid a flat rate; it's not like they're trying to rape me to make
more money to pay their Mercedes-Benz bills or whatever they got.

Phil Pizzo,  the dean of the Stanford School of Medicine

We can't afford to have a system like this. The cultural expectation is that we
are in a community where the public -- every individual -- believes that she or
he should get the most advanced health care kind of on demand. And the notion
that it wouldn't be, that is sort of anathema and we've grown toward that over
the years.

I can remember as a child -- so this was   growing up in the 1950s when doctors
made  house calls -- when the doctor came to my house and did an examination, if
the doctor didn't do something, you know, give a shot or give the medication ...
it was as if they'd never come. Why did we call you if you didn't do something?
What are our expectations as individuals and as society about what constitutes
reasonable care?

Anonymous nurse, Western United States

When you come to the West, you have a different mentality. There's an
independence and an individuality here that you don't get anyplace else, because
when you're in the city, you're kind of like part of the hive. You know, people
that take the light rail in to work and come back and live in these big
apartment buildings and have restaurants and things and they're fine with that.

Here, people are really, really proud, and  they cherish their independence. And
they cherish the fact that we are all individuals. And that's what we're afraid
of, is that we're going to  lose our individuality and we're just going to be
part of the hive. If you're just part of the hive, then what are you going to
do? You're going to  cull out the weak links. You're going  to cull out the lady
that's on crutches and got diabetes, because she may be a good grandmother and
she may be a good person, she lives by herself, and her house is paid for, but
you know, her medicines cost a lot.

Adam, a ''patriot'' from Grand Junction, Colo.

Our founding fathers went to war to throw out tyranny, to overthrow a tyrannical
government without proper representation. We are about at that point now. We're
here to say we want our country back. Health care ... is socialism. And
socialism is not an American value. ... No, I do not have health insurance. I've
never had insurance. [If I need medical care] I should pay for it. I've been to
the doctor one time since I was 12 years old. I paid the full bill. ... If I
truly needed, had a medical need, I have a catastrophic plan that I bought. But
it just covers something that's truly catastrophic. Has a huge deductible. And
if that came about I would pay that. You know, you don't look for a handout.

Asghar Rastegar, a nephrologist at Yale-New Haven Hospital

We have become much better in diagnosis. We can look inside cells. We can look
inside genes. We could never do that 20 years ago. We can define diseases with
such power. But [we lack] the -- it's the realization that there is a human
being, fully developed, sitting across who has his or her own dreams, has his or
her own beliefs, wishes, desires. And they're being ravaged by disease. It's not
the disease. And so we often are caught [up] with technology and miss the fact
that there is an individual who has his own wishes, hopes, dreams, that plays
into their ability to defend themselves against the illness and recover.

Bill Tasche,a hiking guide in Big Sur, Calif.

We  don't go to doctors. I'm physically strong as an ox, but I'm spiritually
strong. We do a lot of  herbs and we put them in a Vita-Mix.  Everything I see
in the woods, horsetail, dandelion, we pull them up. We put them in a Vita-Mix!
And we juice them up and we drink it. Pure chlorophyll. We do a lot of that. I
do a lot of Vitamin C. I do 10,000 units of C. You know, Linus Pauling used to
live here and he's the one who got me into doing 10,000 units of Vitamin C a
day. I do a lot of minerals. You must do minerals. But that chlorophyll drink's
the best I take. Barley green, barley by the handfuls!

I walked 20 miles, what, last week? And I wasn't even fazed, not even fazed. And
these guys were, ''Bill, this is challenging!'' Listen, I'm 67. I can do it, you
can do it. I thought they were going  to turn back on me. I said, ''No, no, no,
we got to  go forward now! We can't go back!''

Peter Orszag, the director of the White House Office of Management and Budget

A key question is how do we move towards a different norm both for providers and
for beneficiaries so that when you go to the doctor you're not just asking for
more of everything, if that's not going to  make you better. It's not just
incentives. Norms sound fuzzy, but I think they're crucial.

How do you adjust norms? ... Take seat belts versus speeding. You get a ticket
if you don't use your seat belt or if you're speeding, and so the pure Econ 101
approach would suggest the penalty for not obeying is similar, and yet adherence
to seat belt laws seems much higher, in part because at least for those in the
front seat, the norm has changed. When you get in a car, if there's  a passenger
in a car, you sort of, at least I do, I kind of look over if a passenger in the
front seat doesn't put on their seat belt. You don't quite do the same thing if
you are in the front seat and the driver is speeding.

And how does that develop? There was a concerted effort to try to encourage seat
belt use. So similarly [in health care]  part of what needs to happen is not
just public policy ... but an emphasis on healthy living, and helping people do
what they say they want to do, which is eat better and exercise more and be
healthier. That's going to be gradual evolution too, but the norm setting I
think is important.

Jim Logue, a health care rally attendee from  Grand Junction, Colo.

My wife works at St. Mary's Hospital here 18 years. I got sicker than  a dog
last year about this time -- 104-degree temperature. I was freezing, go to the
hospital, talked to the triage, boy nurse.

I said: ''Look, buddy, your lobby is full of illegals and park bums. They're
bouncing off the wall. I'm dying here.  My wife's been working here 18 years. I
got the same insurance you got.'' He said, ''Well, you got to  wait behind the
rest of them.'' I said: ''Let me  get this straight -- not only am I buying all
their health care, and I got to stand behind them too? There's something wrong
with that.''

Bill Robinson,a doctor in  Bozeman, Mont.

American culture simply has never been based on caring about what happened to
your neighbor. It's been based on individual freedom and the spirit of, if I
work hard I'll get what I need and I don't have to worry about [the] fellow that
maybe can't work hard. It's a pretty cynical view of America.

But I honestly think that drives an awful lot of this debate -- the notion that
I've done my job, I've worked hard, I've gotten what I'm supposed to get. I have
what I need and if the other people don't, then that's sort of their problem.
And unfortunately the big picture -- that our nation can't thrive with such a
disparity between the rich and the poor, the access people and the
disenfranchised -- that hasn't seemed to really strike a chord with Americans.

So your average person actually has fairly good access. They're happy with their
physician and they're really frightened that something's going to happen to
that, on behalf of people that maybe they don't think it's their job to take
care of.

Fernando Mendoza,a professor of pediatrics at Stanford School of Medicine

Much of what I have done in my professional career is looking at health care in
Latino children. Clearly they don't have health care. Issues of obesity -- I had
been working with my colleagues on national health surveys. And actually we did
some of the surveys that were done in the '70s and the early '80s. It was clear
from back then that Latino kids had what we call a short plump syndrome, which
was they were a little shorter and they were ... heavier than the U.S.
population. But African-American kids were also larger. And it was not until
1990 when they sampled enough Mexicans, enough African-Americans to really get
numbers. It doesn't become an issue, unless it becomes an issue in the white
kids.

Stu Katz,a cardiologist at Yale-New Haven Hospital

When I was at Columbia we had a wealthy Turkish man come to the United States
seeking a heart transplant. ... He was just going to pay cash. And so he said,
''Well, how much does it cost?'' And we told him, you know, $250,000, and he
started laughing. And you know, going through a translator. So we said, ''Why
are you laughing?'' And he said, ''That's less than a Ferrari.''

URL: http://www.nytimes.com

LOAD-DATE: September 9, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY VIVIENNE FLESHER)

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


