                   Copyright 2009 The New York Times Company


                             243 of 2021 DOCUMENTS


                               The New York Times

                              July 31, 2009 Friday
                              Late Edition - Final

The Health Debate, at a Fever Pitch

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 22

LENGTH: 1329 words


To the Editor:

Re ''New Poll Finds Growing Unease on Health Plan'' (front page, July 30):

We should be ashamed of ourselves for allowing the message of those who are
driven by the powerful insurance lobby, and those who oppose the president's
plan for purely political reasons, to shape our opinions.

They have offered no alternatives to the president's health care reform plan,
and in fact have done nothing except muddy the waters of reasonable logic with
relentless, misleading and scary commentary and whisper campaigns, often verging
on outright nuttiness.

If we can't see through that for what it is, especially after the last eight
frightening years, we deserve whatever pathetic health care reform rises from
the ashes.

In November, we voted for change, and we got it, and it can be said that in many
ways, it is not the same old government of the last eight years. Unfortunately,
it is the electorate that remains the same -- susceptible to fear tactics and
misleading innuendo. When will we ever learn?

Patricia A. Weller   Westminster, Md., July 30, 2009

To the Editor:

The public sentiments that threaten health care reform may be traced to
President Obama's failure to deal with a 30-year legacy of market fundamentalism
that has dominated our public discourse since Ronald Reagan's first presidential
campaign.

In the context of this legacy, it comes as little surprise that Americans see
government as the problem rather than as the solution and that they want to
remain ''free to choose'' their health care options. These are core elements in
the antigovernment ideology ushered in by the Reagan Revolution.

Mr. Obama would have been better served by a series of small wins on energy, the
environment and education that would build confidence in the efficacy of
government before he tackled the largest social reform since the New Deal.

Mitchel Y. Abolafia   Albany, July 30, 2009

The writer is a professor at the Rockefeller College of Public Affairs and
Policy, University at Albany, SUNY.

To the Editor:

There is no wealth without health. We spend the most on health in the West, yet
have among the worst health outcomes.

What the poll reflects is the administration's failure on two levels: first, the
rapidity of presenting such a complex issue, and the lack of a lengthy
nationwide educational campaign that informs the American people as to its
content and costs while proactively addressing the sound bites adopted by
Republicans and their allied health care industry that have derailed past reform
attempts.

President Obama should deliver an Oval Office address admitting these
shortcomings, delaying health care reform to next year given his full plate,
regroup and undertake a national campaign to garner grass-roots support and make
health reform an election issue for Congress. He must insist on a public health
option that provides insurance to Americans whose lifeline depends on emergency
rooms, free clinics and drug samples from doctors.

Mohamed Khodr    Winchester, Va., July 30, 2009

The writer, a medical doctor, was public health director of the Lord Fairfax
Health district in Virginia from 1992 to 1999.

To the Editor:

David Leonhardt (''Health Care and the T-Word,'' Economic Scene, July 29) is
quite correct in identifying the growth in health care spending at a rate far
greater than the growth in the United States economy as the critical health care
problem. He is also correct in noting the waste and unnecessary expense in our
health system. But he is quite mistaken in believing that tax reform will
eliminate the perverse incentives in the system and deliver the kind of system
we need.

The ''root of the problem,'' to use his language, lies in the way doctors are
organized and the way they practice and are paid. Doctors are, and should be,
the most important factor in determining how clinical resources are used in the
diagnosis and treatment of illness and injury. Neither insurers nor government
administrators can make the medical care decisions in a given case. Only doctors
and their patients can do that.

The solution to our problem is to cap total national expenditures through an
earmarked, graduated health care tax, provide universal coverage on a prepaid
basis and encourage physicians to practice in private, not-for-profit
multispecialty groups, where they would work for salaries rather than
fee-for-service.

That kind of reform will not be easy to achieve, but it should be the ultimate
goal of our legislative efforts, because it would be the best -- maybe the only
-- way to make the medical care system work efficiently for the benefit of
patients, at a cost we can afford. Tinkering with taxes on private insurance
isn't going to do the job.

Arnold S. Relman   Boston, July 29, 2009

The writer is professor emeritus of medicine and of social medicine at Harvard
Medical School and a former editor in chief of The New England Journal of
Medicine.

To the Editor:

Re ''Health Policy Is Carved Out at Table for 6'' (front page, July 28):

Six moderate/conservative senators from the smallest states will dictate the
terms of health care reform, or whether there will be reform, to the rest of the
American populace.

These senators oppose single-payer, even a public option, and are obsessed with
the ''cost'' argument. Polls, however, consistently indicate that an
overwhelming majority of Americans want either single-payer or a public option
and are not so concerned with the cost if health care is delivered.

An even larger majority wants major, fundamental health care reform.

We tout our ''democracy'' throughout the world, and at every election cycle its
praises are sung, yet a handful of senators acting outside the mainstream,
apparently in line with their small constituencies, can thwart the will of the
vast majority of Americans, denying in the process, or limiting, a fundamental
right of all: a healthy life.

John E. Colbert   Taos, N.M., July 28, 2009

To the Editor:

You report that the small cohort of senators carving out the health care bill
have ''tossed aside'' the government option. In doing so they have cut out the
heart of any meaningful reform. To the majority of the American electorate this
is bound to prove a bitter disappointment, nothing less than a betrayal of its
trust in electing the Democratic Party to its majority role in government.

President Obama can restore that trust by threatening a veto unless this minimum
requirement for reform is included in the final bill.

Robert Brandfon   Schererville, Ind., July 28, 2009

To the Editor:

The diabetes-, hypertension- and heart attack-causing trans fat-laden and
sugared snack ''foods'' at the centrist table are the underlying issue.
Seventy-five percent of disease is lifestyle-induced. Thus, theoretically most
of that disease is preventable. And unless this is addressed, there will never
be any ''bending of the curve'' to decrease health care costs.

That's where the savings are. And that's where health care should lead.

Isn't good health for all Americans the goal? Senators Max Baucus, Charles E.
Grassley, Olympia J. Snowe et al. need to take care of their own health and need
to send a clear message to all of us by avoiding such fare. No excuses. They
display the same ''it won't happen to me'' attitude that too many of us
recklessly and irresponsibly live by.

''North Dakota Diet Food'' and ''we're too important and busy to be bothered
with what we eat'' aren't funny or cute. In the end they're deadly --
actuarially and literally.

Any health care insurance plan should include a substantial tax on trans fat and
sugared foods and tobacco products to account for the increased costs to society
of covering the disease and illness caused by their consumption. This tax is
fair and across the board. If someone chooses to use them, he should pay more
for health insurance.

Gordon Holahan   Santa Fe, N.M., July 28, 2009

The writer is a medical doctor.

URL: http://www.nytimes.com

LOAD-DATE: July 31, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY MORGAN ELLIOTT)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


