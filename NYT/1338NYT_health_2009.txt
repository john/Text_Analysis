                   Copyright 2010 The New York Times Company


                             1338 of 2021 DOCUMENTS


                               The New York Times

                            January 12, 2010 Tuesday
                              Late Edition - Final

Looking at Ways to Treat Depression

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 22

LENGTH: 1066 words


To the Editor:

Re ''The Wrong Story About Depression'' (Op-Ed, Jan. 9):

Judith Warner is correct that antidepressants are frequently prescribed by
primary-care doctors, often for the minor and moderate depressions referred to
in the study getting recent media attention.

Depressions respond to psychotherapy. Ms. Warner is also correct that the
effectiveness of this treatment very much depends on the individual
practitioner. The practitioner can be a psychiatrist, a psychologist or a social
worker.

The advantage of an evaluation by a well-trained psychiatrist is that the
medical and drug-induced causes of depression can be better ruled out, and a
judgment can be made whether the condition is of significant severity where the
benefits of the addition of medication outweigh the risks.

Unfortunately, as with other fields, attempts to determine who are the best
(performance measurements, published rankings, word of mouth) all have their
flaws. There are many people suffering from severe depression, and we have to do
a much better job ensuring that they get the best possible care.

Jeffrey B. Freedman New York, Jan. 9, 2010

The writer, a psychiatrist, is immediate past president of the New York County
district branch of the American Psychiatric Association.

To the Editor:

I am one of those people referred to by Judith Warner who have been prescribing
and studying antidepressants -- in my case, for 30 years. Every point made by
Ms. Warner is dead-on:

Antidepressants do work for people who are really depressed; they don't for
people who aren't. Depression is frequently diagnosed in people who don't have
it, and frequently not diagnosed in those who do.

Medications that work for depression are commonly misused, and types of
psychotherapy that work for depression are commonly not used at all. The reasons
for this state of affairs include mistrust of authority, stigma, big-stakes
health care economics, cross-discipline rivalries and simplistic thinking
(within the mental health care field as well as the general public).

The excesses of the media and the perverse incentives of our current health care
delivery system make things worse.

I am, sadly, less hopeful than Ms. Warner that health care reform will help
rectify things. I am more hopeful that advances in our understanding of
depression itself will enable us to more accurately diagnose it, so that
individuals in distress, whether from depression or other problems, get the help
they actually need.

Lawrence H. Price Providence, R.I., Jan. 9, 2010

The writer, a psychiatrist, is professor of psychiatry and human behavior at
Brown University and clinical director and director of research at Butler
Hospital.

To the Editor:

In her article about treatments for depression, Judith Warner cites a group of
psychologists who said that ''relatively few psychologists learn or practice''
interventions that these researchers deemed ''effective.''

Research since the 1970s has consistently established the effectiveness of
psychotherapy for treatment of conditions like depression when the therapy is
conducted by a highly trained person who holds to a philosophy of psychotherapy
(the philosophy does not much matter, as long as the therapist has one).

The more recent debate about ''evidence-based'' therapies is an old one in the
psychological world, pitting our competent researchers against clinicians.

While researchers can investigate treatments in laboratory conditions, few
clients fit into simple categories. Try applying an ''evidence-based treatment''
for a depressed, sexually abused child from a divorced family in which one
parent is alcoholic and the child has a learning disability. Such clients do not
tend to show up for laboratory studies and don't tend to respond to simple
bromides.

It is also untrue that psychologists do not learn about evidenced-based
treatments. Nearly all psychologists in all states must take continuing
education and are thereby exposed to the current research about psychological
conditions and treatments. The best clinicians practice flexibility in approach,
depending on the needs of their patients.

James Brush Cincinnati, Jan. 9, 2010

The writer, a child and adolescent psychologist, is a former president of the
Ohio Psychological Association.

To the Editor:

The article by Judith Warner as well as the original study on depression are
likely to do harm to many Americans with depression. For decades, it has been
recognized that the recognition and treatment of depression require that
primary-care physicians both screen for and, when appropriate, treat depression.
This treatment may take many forms, including the use of pharmacologic agents.

Many of those who need treatment have no access to psychiatrists because of
geographical distribution, restrictive insurance coverage, cost or a perceived
stigma to being cared for by a psychiatrist. Thus, distortion or
misinterpretation of the evidence about the treatment of depression will result
in those who desperately need help not obtaining it.

My colleagues and I spent many years in convincing family physicians and others
that chronic depression is an important and treatable disease. Yes, some of the
financing was from the pharmaceutical industry, but much of it came from federal
agencies and others whose sole motivation was the health of the public. Because
of this, we have made significant gains in the treatment of depression.

Neil Brooks Vernon, Conn., Jan. 10, 2010

The writer is a former president of the American Academy of Family Physicians.

To the Editor:

Judith Warner hits the nail on the head: the more accurate story about mental
health treatment is the lack of parity with the diagnosis and treatment of other
illnesses like cancer, diabetes and rheumatoid arthritis.

People with emotional problems suffer stigma and are vulnerable to fragmented
systems of care, and the pressure to prescribe as a quick fix for complex
problems. Cost-effective, evidenced-based treatments in which judicious use of
medication is combined with counseling reduce crippling symptoms and expand
quality of life.

Whether or not we enable those with mental illness to recover dreams damaged by
their disease remains an ethical challenge.

Sue Matorin  New York, Jan. 11, 2010

The writer is a social worker in the department of psychiatry, Weill Cornell
Medical College.

URL: http://www.nytimes.com

LOAD-DATE: January 12, 2010

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY LEIGH WELLS)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


