                   Copyright 2010 The New York Times Company


                             1628 of 2021 DOCUMENTS


                               The New York Times

                             March 9, 2010 Tuesday
                              Late Edition - Final

Hesitation on Wall Street Leaves Shares Flat

BYLINE: By JAVIER C. HERNANDEZ

SECTION: Section B; Column 0; Business/Financial Desk; STOCKS AND BONDS; Pg. 9

LENGTH: 674 words


Lacking a clear consensus about the strength of the recovery, Wall Street fell
into a seesaw pattern on Monday.

The major indexes ended mixed as traders saw promise in a round of corporate
deals but were concerned that a push by President Obama to overhaul the health
care system would hurt profits for insurance companies.

But analysts said the less visible cause of Monday's uncertainty was ambiguity
about the strength of the recovery. A better-than-expected report on the jobs
market helped spur a rally on Friday, but much of that enthusiasm had dissipated
by Monday as investors awaited further signs that the recovery was intact.

''The constant question is, 'What is the next shoe to fall?' '' said Uri D.
Landesman, head of global growth at ING. ''The possibility every morning of bad
news from somewhere is still very high, and that's going to restrict any
irrational exuberance.''

Trading has been light recently, raising concern that the market's movements do
not reflect prevailing views of the economy's health.

On Monday, the major indexes spent most of the day wandering, but drifted
downward slightly after the president stepped up his criticism of insurers. Mr.
Obama told a crowd of students in Pennsylvania that ''since there's so little
competition in the insurance industry, they're O.K. with people being priced out
of health insurance because they'll still make more by raising premiums on the
customers they have.''

In response, Wellpoint, the insurance giant, fell 0.47 percent. Health Net and
UnitedHealth Group were down nearly 2 percent, while HealthSpring, a Medicare
provider, declined 1.6 percent. The drug maker Pfizer fell 0.74 percent, and
Merck was down 0.37 percent.

''The whole health care industry is waiting for something to be finished,'' said
Dan A. Faretta, senior market strategist for LaSalle Futures Group. ''No market
really likes change initially.''

The concerns in health care were offset somewhat by signs that merger and
acquisition activity was picking up.

The American International Group, the insurance giant, completed a $15.5 billion
deal to sell a major insurance unit to MetLife. It was the second unit that
A.I.G. had sold in recent weeks, and its shares rose 3.63 percent.

Arrow Energy of Australia, a producer of natural gas, said it had received a
takeover offer of about $3 billion from a company owned by Royal Dutch Shell and
PetroChina, the state-run gas and oil giant.

Taken together, the deals rekindled hopes that businesses were gaining
confidence even in the face of a sputtering recovery.

''That tells investors that companies with decent balance sheets think it's time
to expand,'' Mr. Landesman said.

Energy shares declined as oil remained below $82 a barrel. Shares of
telecommunications companies surged, led by Cisco, which rose 3.65 percent as
expectations rose ahead of an expected product announcement.

At the close, the Dow Jones industrial average was down 0.13 percent, or 13.68
points, at 10,552.52. The Standard & Poor's 500-stock index fell 0.02 percent,
or 0.20 points, to 1,138.50. The Nasdaq rose 0.25 percent, or 5.86 points, to
2,332.21.

It was a soft opening to what is likely to be a quiet week for trading. Economic
data is light, and the flurry of fourth-quarter results from companies has
largely dissipated.

With no clear guidance on the economy, analysts say they believe shares will
spend much of the week in a holding pattern.

Investors said the employment data released Friday -- the unemployment rate held
steady at 9.7 percent in February, and job losses totaled 36,000 --  helped
quell fears of a double-dip recession.

''There is a sense of sustainability to this economic recovery that we think is
going to accelerate as we move into the second quarter,'' said Philip J.
Orlando, chief equity strategist at Federated Investors.

Still, he added, ''there's a lot of bearishness.''

Interest rates rose. The Treasury's 10-year note fell  10/32, to  99 7/32, and
the yield rose to 3.72 percent, from 3.68 percent late Friday.

URL: http://www.nytimes.com

LOAD-DATE: March 9, 2010

LANGUAGE: ENGLISH

GRAPHIC: CHARTS: The Dow Minute by Minute: Position of the Dow Jones industrial
average at 1-minute intervals yesterday. (Source: Bloomberg)
 3-Month Treasury Bills: High rate at weekly auction. (Source: The Bond Buyer)

PUBLICATION-TYPE: Newspaper


