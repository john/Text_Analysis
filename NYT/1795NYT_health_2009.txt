                   Copyright 2010 The New York Times Company


                             1795 of 2021 DOCUMENTS


                               The New York Times

                             March 29, 2010 Monday
                              Late Edition - Final

Six Districts, Two Sides, One Weekend: The Unabashed Liberal

BYLINE: By KATHARINE Q. SEELYE

SECTION: Section A; Column 0; National Desk; Pg. 12

LENGTH: 339 words


PORTLAND, Me. -- Even in Representative Chellie Pingree's liberal district in
southern Maine, the celebration was tempered.

At a reception and dinner here promoting gay rights on Saturday night, a
revolving door of well-wishers -- mostly liberals and mainstream Democrats --
shook Ms. Pingree's hand and thanked her for backing President Obama's health
care bill.

Adding to the luster was the news that Mr. Obama would be visiting her district
on Thursday, an extension of a fund-raising trip to Boston.

But these progressives had lived through a heart-wrenching period last year,
when the Maine Legislature voted for same-sex marriage only to see voters
unexpectedly repeal it.

So below the upbeat surface on Saturday was the question of whether Maine's
representatives in Congress, both Democrats who supported the health care bill,
might pay for it in November.

Pat Peard, a Portland lawyer, thanked Ms. Pingree, then leveled with her about
local reaction to the bill. ''It's mixed,'' Ms. Peard said. ''The biggest
problem with the bill is that a lot of people are frightened by it because it
seems so big and they don't understand it.''

The exchange suggested that even in a state that has already enacted its own
controls on the insurance industry, there is confusion and concern.

Dean Scontras, a Republican, hopes to highlight that in his race against Ms.
Pingree in November. He lost the Republican nomination in 2008 but hopes the
Senate victory by Scott Brown in nearby Massachusetts will put wind in his
sails.

None of that has stopped Ms. Pingree from unabashedly supporting the bill, which
she promises will be good for the state.

She said she had not encountered much resistance, at least not in person.

''People made phone calls to our office, and I'd occasionally get skepticism or
questions,'' she said. ''But I didn't have a lot of people storming me saying,
'How dare you?' The protests we had were people who wanted single payer, who
said maybe you shouldn't sign on because it doesn't go far enough.''

URL: http://www.nytimes.com

LOAD-DATE: March 29, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS (PHOTOGRAPH BY MAX WHITTAKER FOR THE NEW YORK TIMES) MAP: Maine
Democrat, 1st District: Voted YES on final health care bill Voted YES on
November health care bill Elected by a margin of 9.8% in 2008 District won by
Barack Obama in 2008

PUBLICATION-TYPE: Newspaper


