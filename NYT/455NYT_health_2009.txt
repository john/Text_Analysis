                   Copyright 2009 The New York Times Company


                             455 of 2021 DOCUMENTS


                               The New York Times

                             August 30, 2009 Sunday
                              Late Edition - Final

A Blame Game, Deficit-Style

BYLINE: By JACKIE CALMES

SECTION: Section WK; Column 0; Week in Review Desk; Pg. 4

LENGTH: 336 words


Last week, Congress and the White House released their summer budget updates,
touching off  a flurry of headlines and commentary suggesting President Obama's
agenda would produce deficits exceeding a total of $9 trillion over a decade.
Others said $2 trillion. Both calculations were misleading.

While Mr. Obama  has proposed nothing to reduce the nation's red ink, he also
has not deepened it -- yet.

The analyses of the Congressional Budget Office and the administration's Office
of Management and Budget are not exactly comparable for technical reasons.
Still, both  agree that much of the $9 trillion through fiscal year 2019 is the
government's so-called baseline deficit --  the shortfall that would result if
current law and policies don't change.

In effect, it is the deficit that Mr. Obama inherited -- about $6.3 trillion by
the administration's calculation, $7.1 trillion by the Congressional office's.

Some commentators apparently subtracted the Congressional office's $7.1 trillion
baseline deficit from the more than $9 trillion that both agencies say would
result under Mr. Obama's policies, and concluded the president would add at
least $2 trillion to the decade's debt. But so would any president, Democrat or
Republican, since the amount reflects  policy adjustments that are costly and
almost certain, and  that have bipartisan support.

Among the adjustments:

Extending Bush tax cuts past their scheduled 2010 expiration (Mr. Obama, by
letting those for the rich lapse, would reduce the revenue loss).

Adjusting the alternative minimum tax for inflation to spare  millions of
Americans  higher income taxes.

Blocking  cuts mandated for doctors' Medicare reimbursements.

Those fixes, in turn, would mean hundreds of billions more in interest on the
added debt.

As for Mr. Obama's big-ticket proposals, notably health care and energy policy
overhauls, those do not add to the deficits under the agencies' analyses because
he has proposed savings and tax increases to offset their costs.

URL: http://www.nytimes.com

LOAD-DATE: August 30, 2009

LANGUAGE: ENGLISH

GRAPHIC: CHARTS: TOTAL DEFICIT 2010-19
What makes up the deficit?
Current law: $6,259 billion Tax cuts and spending required under current law,
including Social Security, Medicare and Medicaid, in excess of revenues.
Extensions to Bush tax cuts: $2,102 billion Extends 2001 and 2003 tax cuts
except for top-earners.
 DEFICIT REDUCTION, 2010-19 (Source: Office of Management and Budget)

PUBLICATION-TYPE: Newspaper


