                   Copyright 2009 The New York Times Company


                             763 of 2021 DOCUMENTS


                               The New York Times

                            October 6, 2009 Tuesday
                              Late Edition - Final

Exchange, or Shortchange?

BYLINE: By REED ABELSON

SECTION: Section B; Column 0; Business/Financial Desk; Pg. 1

LENGTH: 1508 words


It is the sleeper issue  in the current health care debate.

Despite all the disagreement in Washington, every proposal now before Congress
to overhaul the nation's health care system includes creation of an insurance
''exchange'' -- a marketplace  that would operate something like a Travelocity
Web site for insurance policies.

In theory at least, the exchange would fix a fundamental flaw in the present
system by giving small businesses and individuals  a broad  choice of insurance
policies at competitive prices. Right now, such buyers typically have few
affordable options.

The idea of an exchange has support  from the White House and many in Congress
-- from people who also advocate including a government-run insurance plan in
the marketplace and  from those who oppose letting the government compete with
commercial insurers.

But policy experts say few lawmakers have  yet paid enough attention to what
that new marketplace should look like -- and whether it would actually work as
promised.

Without careful design and adequate rules of fair play, and without letting
enough buyers participate to create a robust market, the exchange might not
actually stimulate new competition among the nation's health insurers. In other
words: What happens if you build an exchange and insurers do not flock to it
with new, well-priced wares?

The risk is that many local markets could end up looking much as they do today
--  with small businesses and individuals at the mercy of too few insurers
wielding too much power in their regions.

''We may not be able to improve competition in the short run through the
exchange,'' said Len Nichols, a health economist at the New America Foundation,
a Washington research group that supports an overhaul of the insurance market.

So far, the House proposal calls for the creation of a national exchange where
someone in Iowa, for example, may have the same choice of health plans as buyers
in Maine and California. Because insurance is now a state-by-state patchwork,
such an approach would conceivably enhance competition by creating an interstate
market.

But critics worry that the result may be a few big providers of
one-size-fits-all plans,  still leaving many small businesses and consumers with
products that do not meet their needs or budgets.

Both Senate proposals, meanwhile, favor the creation of state-based exchanges,
where a state may have the choice of deciding whether its residents can also buy
from a national or regional exchange. The exchanges may have other wide
variations, depending on the model chosen by a state or outlined in the
legislation.

Some  may mirror the one in Massachusetts, which actively screens the policies
and negotiates with insurers on behalf of customers. Others may  operate more
like electronic yellow pages, where insurers list their offerings with little
government involvement or oversight.

Another big question is whether there will be enough new customers to prompt
insurers to actively compete for their business.

As currently outlined in the Congressional proposals, participation in the
exchanges would be limited in some way  -- to only the very smallest employers,
or  to residents of a single state or  to people who do not now have coverage
through their employers.

Another limiting factor, policy experts say, would be whether Congress provides
generous enough subsidies so that people who do not currently have insurance
could afford it.

For all their potential drawbacks, the  exchanges are meant to address a
fundamental issue: the current lack of competition in many parts of the country.

President Obama, for example, in his recent speech to  Congress, cited states
like Alabama where the  insurance market for individuals and small companies is
dominated by a handful of carriers. As a result, he said, ''the price of
insurance goes up and quality goes down.''

A recent analysis by the Government Accountability Office notes that in more
than a dozen states, including Delaware, Maine, North Dakota and Rhode Island,
the largest health insurer offering coverage for a small business commands more
than half of the overall market.

Some insurers argue that  the strongest need for competition is among the
hospitals and doctors, not the health plans serving as middlemen.

''There has to be competition within the delivery system,'' said David H. Klein,
the chief executive of the powerful Excellus BlueCross BlueShield in Rochester,
a nonprofit insurer that is one of the major sources of coverage in that market.
''In most of America, that just isn't going to happen.''

But small business owners frequently complain they when they have limited choice
among insurers, they have little bargaining clout.  ''It's a 'take it or leave
it' for some of the biggest carriers out there,'' said Amanda Austin, a lobbyist
for the National Federation of Independent Business, which represents small
businesses.

At Smithfield Diesel and Transmission Repair  in Rhode Island, for example, for
years the choice of insurers has been limited to the state's two biggest
carriers -- a Blue Cross plan and United Healthcare --  said Joan Frattarelli,
who runs Smithfield Diesel with her husband. This year, the company's costs went
up by 29 percent,  to about $34,000 for coverage of four employees under Blue
Cross.

Ms. Frattarelli said she worried that  some proposals in Congress would continue
to restrict her options to only those plans now available in Rhode Island, which
would leave her no better off. ''Competition is the name of the game,'' she
said,  ''I don't care what industry it is.''

The hope in Washington, of course, is that  an influx of tens of millions of
newly insured customers will lure insurance companies  to enter new markets.
That is what happened a few years ago when private insurers flocked to offer
prescription drug coverage after it was added to the Medicare program,  said Ken
Sperling, a health care consultant for Hewitt Associates.

But because of the restrictions on who can shop, and the question of whether the
shoppers will be able to afford insurance, no one knows how many new customers
there might be, especially in some in the least populous markets.

The big national insurance companies, like Humana, United Healthcare and Aetna,
would not speculate on how the exchanges might affect their business plans.  But
some of the regional insurers say they could be tempted to expand, at least into
nearby markets or states.

''It's not off the radar screen,'' said George C. Halvorson, the chief executive
of Kaiser Permanente, the big California health plan. ''We're going to sit down
and take a really hard look.''

Another deterrent facing new entrants is the fact that established insurers can
negotiate the deepest discounts with the local hospitals and doctors, making it
hard for a new player to compete on price.

''The big dominant insurer in any market has the providers pretty much sewn
up,'' said Timothy S. Jost, a professor at Washington and Lee University. ''I
think that's the big problem, and I don't know that the legislation addresses
that very well.''

Congress must also decide whether the exchanges would have any authority to
decide which plans are offered and at what price, said Paul Fronstin, a policy
analyst with the Employee Benefit Research Institute, a co-author of a recent
report on insurance exchanges. ''The exchange can have a more active role if it
negotiate rates,'' he said, ''but it is not clear what is going to happen.''

In Massachusetts, for example, the state's exchange, called the Connector,
negotiates directlywith the state's private insurance companies in offering a
small number of state-subsidized plans -- similar to what an employer does when
it screens the policies offered to its work force.

''We work as an aggressive employer, offering managed competition,'' said Jon
Kingsdale, the executive director of the Commonwealth Health Insurance Connector
Authority. He said the agency's ability to negotiate on behalf of 180,000
customers who required state subsidies was a reason it achieved a 6 percent
reduction in the cost of premiums this year.

But the Connector would be less effective if it had no say over which plans were
offered on the exchange, said Mr. Kingsdale, who criticized the Senate Finance
committee's proposal, for example, as potentially creating little more than ''an
automated yellow pages.''

Because formulating an effective exchange is so difficult, some policy analysts
are still arguing that only a new government-run competitor could create a
powerful enough force in many parts of the country to offset the home-court
advantage many insurers already wield.

''People don't realize how hard it is to crack these markets,'' said David
Balto, a former official with the Federal Trade Commission who is now a senior
fellow at the Center for American Progress, a liberal research group.  ''What
you need to have,'' he said,  ''is something that effectively disrupts the
market.''

URL: http://www.nytimes.com

LOAD-DATE: October 6, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY MINH UONG/THE NEW YORK TIMES) (pg.B1)
 CHART: DOMINATING THE HEALTH INSURANCE MARKET: In at least 17 states, the
largest insurance carrier offering health coverage to small businesses controls
at least half of the market. In an additional 14, the carrier has more than a
third. (Source: Government Accountability Office) (pg.B4)

PUBLICATION-TYPE: Newspaper


