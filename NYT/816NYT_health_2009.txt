                   Copyright 2009 The New York Times Company


                             816 of 2021 DOCUMENTS


                               The New York Times

                           October 15, 2009 Thursday
                              Late Edition - Final

Courting Young Voters

BYLINE: By JANIE LORBER

SECTION: Section A; Column 0; National Desk; A TARGET AUDIENCE; Pg. 24

LENGTH: 204 words


One of the biggest challenges facing lawmakers who favor universal health care
coverage is coming up with a plan for 20-somethings, the so-called young
invincibles.

A government-run insurance plan might be one way to bring this group into the
fold, but its future is uncertain, to say the least. Still, provisions tucked
into health care bills moving through Congress would make it easier for this
group to afford health coverage.

Speaker Nancy Pelosi announced on Tuesday that under the House's health care
legislation, young adults would be able to remain dependent on their parents'
insurance plans through age 26. The provision is intended to appeal to young
voters and their parents.

The Senate Health, Education, Labor and Pensions  Committee bill includes a
similar provision, though it would cover young adults only through age 25.

The bill approved by the Senate Finance Committee on Tuesday would offer a plan
providing relatively inexpensive, high-deductible catastrophic care to those
ages 25 or younger.

The plan would provide thinner coverage than other policies available under the
bill, but would cover some preventive care, like yearly checkups. It would not
pay for prescription drugs.

JANIE LORBER

URL: http://www.nytimes.com

LOAD-DATE: October 15, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO

PUBLICATION-TYPE: Newspaper


