                   Copyright 2009 The New York Times Company


                             301 of 2021 DOCUMENTS


                               The New York Times

                            August 13, 2009 Thursday
                              Late Edition - Final

Health Reform and Small Business

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL; Pg. 26

LENGTH: 992 words


The impact on small businesses has become a flashpoint in the increasingly
raucous debate over health care reform. Trade associations are charging that the
pending bills -- which would require all businesses to provide coverage to their
employees or pay a penalty -- would place a huge financial burden on their
members. Republican leaders are doing their best to inflame the fears and
opposition of small business owners.

These proprietors would be wise to ignore the rhetoric and take a closer look. A
vast majority of small businesses and their workers are likely to benefit
greatly. They should be supporting, not opposing, reform.

It is a little recognized fact that some 70 percent of uninsured Americans come
from families with one or two full-time workers. Most of those workers are
employed by small businesses that don't offer them health benefits or offer
coverage that they can't afford.

Small businesses would reap substantial benefits if their employees were
insured. Their work forces would likely become healthier, and they would have an
easier time attracting or holding talented employees. Even more striking, with
health care reform, small firms could buy insurance at substantially lower
rates. Lobbyists issue dire warnings that small businesses won't have the money
to pay for coverage or to pay the penalties and will have to eliminate a huge
number of jobs: more than one million under an early House bill, according to
the National Federation of Independent Business.

Such fears are grossly overblown.

A vast majority of the nation's small employers -- those who have 25 or fewer
workers in the Senate health bill or annual payrolls of $500,000 or less in the
House version -- would likely be exempted from the mandate.

An analysis by Jonathan Gruber, a respected health economist at the
Massachusetts Institute of Technology, concluded that those small businesses
that are not exempt would see little impact on employment or profits, although
employers would reduce wages to compensate for providing added benefits. The
nonpartisan Congressional Budget Office, the chief arbiter of the impact of
legislation, has come to similar conclusions.

What's been most lost in the furor is how much most small businesses would
benefit from provisions that should make insurance more affordable -- for
businesses that already provide coverage and for those that have been deterred
from providing coverage by cost.

Small businesses that currently offer coverage often pay significantly more per
worker than larger employers do for the same coverage. Under all of the current
bills, the smallest employers would gain quick access to new insurance exchanges
-- where plans would compete for their business with rates comparable to those
enjoyed by large employers. (In subsequent years, slightly bigger firms and
possibly even medium-size firms would likely gain access to the exchanges as
well.)

And many small businesses with low-wage workers would be eligible for
substantial tax credits to subsidize their coverage.

Still, not all small firms would benefit. One government estimate suggests that
39,000 firms (out of a total of six million small and large employers in the
country) would have to start providing benefits or pay a penalty, and another
240,000 that do provide benefits would have to increase their subsidy levels.
The penalties for not offering coverage could be relatively small ($750 per
worker after exempting the first 25 workers under the Senate bill) or quite
substantial (reaching an estimated $2,800 per worker for some firms under the
House bill).

There is no question that the cost of coverage -- which currently averages about
$5,000 per individual or $13,000 per family -- or paying fines could take a
substantial bite out of the profits of some firms, forcing them to accept lower
earnings, reduce wages, shed some jobs or raise prices.

Trade groups say the main reason small firms don't provide coverage is that they
can't afford to, and they complain that there is little in the reform bills that
would reduce medical costs any time soon. But in making that argument, they
conflate two issues. It is true that deep-seated reform of the health care
delivery system will take years to reduce medical costs. But the cost of health
insurance for small businesses could drop quickly once the exchanges are open.

While some small percentage of companies will suffer, there are good reasons for
requiring as many companies as possible to ''play'' -- by offering coverage --
or ''pay'' by paying a penalty. The most important is that the penalties would
help deter employers from dropping their own coverage. The number of companies
offering health insurance to their workers has been declining steadily, mostly
among small firms, and it is important not to accelerate that erosion.

The play-or-pay provisions could also raise significant money to help cover the
uninsured. The penalties alone could raise $52 billion over a decade under the
Senate health committee bill and probably much more under the House bill. We see
no easy way to ease the pain of the minority of firms that will face very
substantial new costs. We'd be inclined to suggest hardship exemptions were it
not for the likelihood that creative accountants might make every firm look like
a hardship case.

A bipartisan group within the Senate Finance Committee is considering dropping
the employer mandate and substituting a requirement that employers pay only for
those workers who end up with government-subsidized coverage. That seems a poor
approach because it could deter employers from hiring low-income workers that
could saddle them with high subsidy costs.

It makes good sense to us to require small businesses to contribute to solving a
problem that mostly affects their own workers. There also seems little doubt
that the small business community would be one of the biggest winners from
health care reform.

URL: http://www.nytimes.com

LOAD-DATE: August 13, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


