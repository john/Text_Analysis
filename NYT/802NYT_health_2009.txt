                   Copyright 2009 The New York Times Company


                             802 of 2021 DOCUMENTS


                               The New York Times

                            October 12, 2009 Monday
                              Late Edition - Final

Health Care Reform...for the N.F.L.

BYLINE: By STEFAN FATSIS.

Stefan Fatsis is the author of ''A Few Seconds of Panic: A Sportswriter Plays in
the N.F.L.''

SECTION: Section A; Column 0; Editorial Desk; OP-ED CONTRIBUTOR; Pg. 23

LENGTH: 483 words


MOUNTING evidence of early dementia among former National Football League
players has gotten a lot of press attention. But there are broader medical
issues that require simple reforms to protect current players, for whom a
career- or even life-threatening injury is just one play away.

N.F.L. players often get excellent medical treatment, but the primary goal is to
return them to the field as quickly as possible. Players are often complicit in
playing down the extent of their injuries.  Fearful of losing their jobs --
there are no guaranteed contracts in the N.F.L. -- they return to the huddle
still hurt.

I witnessed this play-first ethos when the Denver Broncos allowed me to join the
team as a place-kicker during training camp in 2006 and write a book on the
experience. One player told me that, the previous year, a team trainer had
dismissed his complaints of a knee injury; a few days later, he tore an anterior
cruciate ligament. Another Bronco, now retired, told me the team gave him a
diagnosis of a minor calf strain; an outside doctor found it was a severe muscle
tear.

Given the sport's violence, the N.F.L.'s collective-bargaining agreement with
players contains remarkably few medical provisions. Players are entitled to a
second opinion at club expense, they can choose their own surgeon, they can
examine their medical records twice a year. If a team doctor tells management
about a condition that ''adversely affects'' a player's health, he also has to
tell the player; if the condition could worsen with further play, he has to tell
the player in writing. That's about it.

The current labor deal expires after the 2010 season. Here are six  changes  the
players' union should insist on in the next contract:

Team doctors and trainers should no longer be employed by -- and beholden to --
individual teams. Rather, there should be a leaguewide medical system with
staffs selected jointly by the N.F.L. and the union, and paid for by the league.

Doctors should inform players about injuries before they tell any club
officials.

Players should be able to view their medical records whenever they want.

Teams should be required to report every injury, no matter how minor, to the
league and the union. Comprehensive data would enable the N.F.L. to take
preventive steps and study long-term effects.

Grievance procedures should be reformed. Teams aren't supposed to cut injured
players without pay, but it happens. Tougher rules would deter clubs from doing
so, and from delaying hearings to force lowball settlements.

Payments to injured players should be excluded from payrolls, which for this
season are capped at $127 million  per team. That would eliminate teams'
incentive to cut injured players to save salary-cap space.

These measures might not reduce injuries in an inherently brutal sport, but they
would help to protect the men who suffer the consequences.

URL: http://www.nytimes.com

LOAD-DATE: October 12, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY DAVID SANDLIN)

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


