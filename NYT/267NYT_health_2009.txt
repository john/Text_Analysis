                   Copyright 2009 The New York Times Company


                             267 of 2021 DOCUMENTS


                               The New York Times

                            August 6, 2009 Thursday
                              Late Edition - Final

White House Affirms Deal On Drug Cost

BYLINE: By DAVID D. KIRKPATRICK

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1138 words

DATELINE: WASHINGTON


Pressed by industry lobbyists, White House officials on Wednesday assured drug
makers that the administration stood by a behind-the-scenes deal to block any
Congressional effort to extract cost savings from them beyond an agreed-upon $80
billion.

Drug industry lobbyists reacted with alarm this week to a House health care
overhaul measure that would allow the government to negotiate drug prices and
demand additional rebates from drug manufacturers.

In response, the industry successfully demanded that the White House explicitly
acknowledge for the first time that it had committed to protect drug makers from
bearing further costs in the overhaul. The Obama administration had never
spelled out the details of the agreement.

''We were assured: 'We need somebody to come in first. If you come in first, you
will have a rock-solid deal,' '' Billy Tauzin, the former Republican House
member from Louisiana who now leads the pharmaceutical trade group, said
Wednesday. ''Who is ever going to go into a deal with the White House again if
they don't keep their word? You are just going to duke it out instead.''

A deputy White House chief of staff, Jim Messina, confirmed Mr. Tauzin's account
of the deal in an e-mail message on Wednesday night.

''The president encouraged this approach,'' Mr. Messina wrote. ''He wanted to
bring all the parties to the table to discuss health insurance reform.''

The new attention to the agreement could prove embarrassing to the White House,
which has sought to keep lobbyists at a distance, including by refusing to hire
them to work in the administration.

The White House commitment to the deal with the drug industry may also irk some
of the administration's Congressional allies who have an eye on drug companies'
profits as they search for ways to pay for the $1 trillion cost of the health
legislation.

But failing to publicly confirm Mr. Tauzin's descriptions of the deal risked
alienating a powerful industry ally currently helping to bankroll millions in
television commercials in favor of Mr. Obama's reforms.

The pressure from Mr. Tauzin to affirm the deal offers a window on the secretive
and potentially risky game the Obama administration has played as it tries to
line up support from industry groups typically hostile to government health care
initiatives, even as their lobbyists pushed to influence the health measure for
their benefit.

In an interview on Wednesday, Representative Raul M. Grijalva, the Arizona
Democrat who is co-chairman of the House progressive caucus, called Mr. Tauzin's
comments  ''disturbing.''

''We have all been focused on the debate in Congress, but perhaps the deal has
already been cut,'' Mr. Grijalva said. ''That would put us in the untenable
position of trying to scuttle it.''

He added: ''It is a pivotal issue not just about health care. Are industry
groups going to be the ones at the table who get the first big piece of the pie
and we just fight over the crust?''

The Obama administration has hailed its agreements with health care groups as
evidence of broad support for the overhaul among industry ''stakeholders,''
including doctors, hospitals and insurers as well as drug companies.

But as the debate has heated up over the last two weeks, Mr. Obama and
Congressional Democrats have signaled that they value  some of its industry
enemies-turned-friends more than others. Drug makers have been elevated to a
seat of honor at the negotiating table, while insurers have been pushed away.

''To their credit, the pharmaceutical companies have already agreed to put up
$80 billion'' in pledged cost reductions, Mr. Obama reminded his listeners at a
recent town-hall-style meeting in Bristol, Va. But the health insurance
companies ''need to be held accountable,'' he said.

''We have a system that works well for the insurance industry, but it doesn't
always work for its customers,'' he added, repeating a new refrain.

Administration officials and Democratic lawmakers say the growing divergence in
tone toward the two groups reflects a combination of policy priorities and
political calculus.

With  polls showing that public doubts about the overhaul are mounting,
Democrats are pointedly reminding voters what they may not like about their
existing health coverage to help convince skeptics that they have something to
gain.

''You don't need a poll to tell you that people are paying more and more out of
pocket and, if they have some serious illness, more than they can afford,'' said
David Axelrod, Mr. Obama's senior adviser.

The insurers, however, have also stopped short of the drug makers in their
willingness to cut a firm deal. The health insurers shook hands with Mr. Obama
at the White House in March over their own package of concessions, including
ending the exclusion of coverage for pre-existing ailments.

But unlike the drug companies, the insurers have not pledged specific cost cuts.
And insurers have also steadfastly vowed to block Mr. Obama's proposed
government-sponsored insurance plan -- the biggest sticking point in the
Congressional negotiations.

The drug industry trade group, the Pharmaceutical Research and Manufacturers of
America, also opposes a public insurance plan. But its lobbyists acknowledge
privately that they have no intention of fighting it, in part because their
agreement with the White House provides them other safeguards.

Mr. Tauzin said the administration had approached him to negotiate. ''They
wanted a big player to come in and set the bar for everybody else,'' he said. He
said the White House had directed him to negotiate with Senator Max Baucus, the
business-friendly Montana Democrat who leads the Senate Finance Committee.

Mr. Tauzin said the White House had tracked the negotiations throughout,
assenting to decisions to move away from ideas like the government negotiation
of prices or the importation of cheaper drugs from Canada. The $80 billion in
savings would be over a 10-year period. ''80 billion is the max, no more or
less,'' he said. ''Adding other stuff changes the deal.''

After reaching an agreement with Mr. Baucus, Mr. Tauzin said, he met twice at
the White House with Rahm Emanuel, the White House chief of staff; Mr. Messina,
his deputy; and Nancy-Ann DeParle, the aide overseeing the health care overhaul,
to confirm the administration's support for the terms.

''They blessed the deal,'' Mr. Tauzin said. Speaker Nancy Pelosi said the House
was not bound by any industry deals with the Senate or the White House.

But, Mr. Tauzin said, ''as far we are concerned, that is a done deal.'' He said,
''It's up to the White House and Senator Baucus to follow through.''

As for the administration's recent break with the insurance industry, Mr. Tauzin
said, ''The insurers never made any deal.''

Sheryl Gay Stolberg contributed reporting.

URL: http://www.nytimes.com

LOAD-DATE: August 6, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


