                   Copyright 2009 The New York Times Company


                             367 of 2021 DOCUMENTS


                               The New York Times

                            August 20, 2009 Thursday
                              Late Edition - Final

Obama Calls Health Plan a 'Moral Obligation'

BYLINE: By JEFF ZELENY and CARL HULSE

SECTION: Section A; Column 0; National Desk; Pg. 18

LENGTH: 728 words

DATELINE: WASHINGTON


President Obama sought Wednesday  to reframe the health care debate as ''a core
ethical and moral obligation,'' imploring a coalition of religious leaders to
help promote the plan to lower costs and expand insurance coverage for all
Americans.

''I know there's been a lot of misinformation in this debate, and there are some
folks out there who are frankly bearing false witness,'' Mr. Obama told a
multidenominational group of pastors, rabbis and other religious leaders who
support his goal to remake the nation's health care system.

As the president returned to the health care debate after two days of silence,
the administration encouraged Republicans to take part in the negotiations.
Still, Democratic leaders moved ahead with plans to advance a  measure next
month with or without Republican cooperation.

The White House acknowledged that its handling of the debate had been
inconsistent, with Robert Gibbs, the press secretary, saying, ''I don't think
anybody here believes we've pitched a no-hit game or a perfect game.''

Mr. Obama did not wade into the uproar among Democrats over whether the bill
would have a public insurance component,  a key point of contention,  but rather
tried to correct what he said were untruths about the plan.

''You've heard that there's a government takeover of health care. That's not
true,'' said Mr. Obama, who went on to call other assertions, like a death panel
for the elderly, ''an extraordinary lie.''

The members of Congress involved in bipartisan health care talks said they
remained determined to try to reach agreement, with Senator Max Baucus, Democrat
of Montana and chairman of the Finance Committee, saying that ''bipartisan
progress continues'' even though lawmakers are scattered for recess.

''The Finance Committee is on track to reach a bipartisan agreement on
comprehensive health care reform that can pass the Senate,'' Mr. Baucus said in
a statement.

The six negotiators are scheduled to consult via conference call on Thursday.
Plans for them to meet in person over the break have evidently been shelved.

Senator Charles E. Grassley of Iowa, the senior Republican on the panel, said in
a statement that no health care plan had yet found the kind of broad support he
thought necessary to move ahead.

''That doesn't mean we should quit,'' Mr. Grassley said.

Highly critical statements by Mr. Grassley of Democratic health care proposals
-- coupled with near blanket rejection by other Republicans -- led top Democrats
to suspect that no Republicans would be willing to vote for a health care plan.

A spokesman for the Senate majority leader, Harry Reid of Nevada, said the
leadership had not reached a decision that Democrats should try to force through
a health plan on their own.

The spokesman, Jim Manley, said Democrats would prefer to proceed in a
bipartisan way. But the Finance Committee has an informal Sept. 15 deadline for
reaching a deal, and Mr. Manley indicated that Democrats would not wait
indefinitely.

''Patience is not unlimited, and we are determined to get something done this
year by any legislative means necessary,'' said Mr. Manley, raising the prospect
of using a procedural shortcut that bars filibusters against a health measure.

With Democrats seeing their hopes for a bipartisan deal dim, Republicans sought
to make sure that they would not take the blame for any failure.

Representative John A. Boehner of Ohio, the House Republican leader, said
Democrats had been pursing health legislation on their own for months, ignoring
Republican offers to explore common ground.

Mr. Boehner joined other Republicans in continuing to criticize  current
proposals, saying they would lead to rationing of care and making other claims
that Democrats say are false -- an illustration of how far apart Republicans and
Democrats are on the issue.

In a late-afternoon telephone call with religious leaders on Wednesday, Mr.
Obama cast the difficulty of the health care debate in terms larger than his
presidency, comparing it to the creation of Social Security and Medicare.

''These struggles always boil down to a contest between hope and fear,'' he
said. ''That was true in the debate over Social Security, when F.D.R. was
accused of being a socialist. That was true when J.F.K. and Lyndon Johnson tried
to pass Medicare. And it's true in this debate today.''

URL: http://www.nytimes.com

LOAD-DATE: August 20, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


