                   Copyright 2009 The New York Times Company


                             135 of 2021 DOCUMENTS


                               The New York Times

                              July 12, 2009 Sunday
                              Late Edition - Final

Newswords

BYLINE: By WILLIAM SAFIRE

SECTION: Section MM; Column 0; Magazine Desk; ON LANGUAGE; Pg. 12

LENGTH: 933 words


Stop worrying about the ''dumbing down'' of our language by bloggers, tweeters,
cableheads and MSM thumbsuckers engaged in a ''race to the bottom'' of the page
by little minds confined to little words. The truth is that the media world is
seized as never before by a penchant for the polysyllabic.

RECONCILIATION

Previously known for its six syllables of sweetness and light, reconciliation
has become the political fighting word of the year.

Months ago, the Senate majority leader, Harry Reid, hinted that a procedural
step known as reconciliation might be used to turn the coming annual budget bill
-- which the Congress must pass -- into a vehicle for major legislation. If the
House and Senate, both in Democratic control, were to agree to ''reconcile''
differences in an overall budget bill, then the Republican minority in the
Senate might not be able to assemble the votes to filibuster it. The Times's
politics blog headlined Reid's veiled threat ''Senate Leader Wants Budget
Maneuver That Riles Republicans.''

Reconciliation became a word of political warfare in the post-Watergate
atmosphere. As president, Nixon cut Congressional spending by ''impounding''
appropriations; in 1974, Congress reclaimed its budgetary power from a weakened
presidency with the ''Impoundment Control Act,'' which introduced the
reconciliation device. With the next decade's rise of Reagan Republicans,
however, turnabout was seen as fair play: reconciliation's steamroller became
the G.O.P.'s method of avoiding the filibuster of widespread tax reduction. Now,
with Republicans in the minority denouncing ''fast-tracking a major legislative
overhaul such as health care reform'' as ''a disservice'' and ''purely
partisan,'' Democrats in control smile and say, ''Reagan did it.''

In nonpolitical use, the word means ''the process of restoring friendly
relations between persons or nations, promoting unity and peace.'' The O.E.D.
dates it to the 14th century, as ''the action of restoring humanity to God's
favor.'' Now, like the ''Janus verbs'' sanction, table and cleave, this gentle
noun has gained a second meaning the opposite of its first. I heard one
Republican strategist resurrect a sanguinary phrase made famous by Senator John
McCain in 2000, adapting it thus: ''If they try to use the reconciliation trick
to avoid debate on a trillion-dollar deal, there'll be 'blood all over the
floor.' ''

ANOMALIES

Another unfamiliar word that has found itself blinking unaccustomed in the
public eye is anomalies. In the aftermath of the deadliest crash in the 33-year
history of the Washington Metrorail system, The Washington Post headlined
''Probe Finds Metro Control 'Anomalies.' ''

CNN.com's headline was '' 'Anomalies' Discovered in Metro Track Control
Circuit.'' In both cases, the word properly appeared in quotes because it was
used by Debbie Hersman of the National Transportation Safety Board in pointing
to potential malfunction of the computer system. The early investigation found
no problems in five of the six circuits on the track near the crash, but the
sixth contained ''anomalies.'' Placing quotes around the word surely caused some
readers to wonder: ''What does that mean? Is it a bureaucratic euphemism for
something that they're not telling us?''

The Greek anomalos means ''uneven.'' An anomaly is something that deviates from
what we expect, which gives rise to our thinking it peculiar, strange or, more
informally, ''out of whack, funny-looking.'' It is an irregularity that piques
curiosity. In English grammar, with its plethora of irregular verbs, many of
which do not add an -ed to form a past tense, why is the plural of the noun
child not ''childs'' but children? Why is the plural of American ''Americans''
but our plural of Japanese not ''Japaneses''? (Explain that to your childs.)
Anomalies abound; I welcome the word in a headline.

EXPEDIENCY

As the English word is used in today's newswords being filed out of seething
Tehran, expediency does not offer us an easy way out.

Take the formal centers of power: the Guardian Council, which decided there was
no voting fraud in the re-election of President Ahmadinejad; the Islamic
Consultative Assembly, or Majlis, a parliament; and the Expediency Discernment
Council, which was set up in 1988 supposedly to settle disputes between the
elected Parliament and the unelected Guardian Council.

Nobody knows for sure if the head of the Expediency Council, an old
revolutionary named Rafsanjani, is a potential dealmaker or a Supreme Has-Been,
but language mavens know that his council has a problem with the English
translation of the key word in its name: in Farsi it is maslehat (does not rhyme
with mazel tov) as ''expediency.''

Expedient started out as meaning ''suitable, fit''; Shakespeare wrote that
''expedient manage must be made'' and ''with all expedient duty.'' Thomas
Jefferson wrote of George Washington in 1793 that ''the president thought it
expedient to remind our fellow citizens that we were in a state of peace.'' It
had speedy-managerial cousins in expedite and expedition.

But then the worm of meaning turned. A sense of shiftiness set in the central
sense of expediency, and instead of ''suitable,'' it became ''politic,'' more
concerned with utility than with morality, setting ends ahead of means, too
willing to compromise principles in a lust for power and pelf. Tehran's official
propagandists need a new translator. On the other hand, should the kakistocrats
in Iran stick with the name of expediency to describe their power brokers? Some
of us think it's eminently suitable.

URL: http://www.nytimes.com

LOAD-DATE: July 12, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY HEATHER HAN)

PUBLICATION-TYPE: Newspaper


