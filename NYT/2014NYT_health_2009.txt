                   Copyright 2010 The New York Times Company


                             2014 of 2021 DOCUMENTS


                               The New York Times

                              May 30, 2010 Sunday
                              Late Edition - Final

The Doctor Will See You Now. Please Log On.

BYLINE: By MILT FREUDENHEIM

SECTION: Section BU; Column 0; Money and Business/Financial Desk; Pg. 1

LENGTH: 2097 words


ONE day last summer, Charlie Martin felt a sharp pain in his lower back. But he
couldn't jump into his car and rush to the doctor's office or the emergency
room: Mr. Martin, a crane operator, was working on an oil rig in the South China
Sea off  Malaysia.

He could, though, get in touch with a doctor thousands of miles away, via
two-way video. Using an electronic stethoscope that a paramedic on the rig held
in place, Dr. Oscar W. Boultinghouse, an emergency medicine physician in
Houston, listened to Mr. Martin's heart.

''The extreme pain strongly suggested a kidney stone,'' Dr. Boultinghouse said
later. A urinalysis on the rig confirmed the diagnosis, and Mr. Martin flew to
his home in Mississippi for treatment.

Mr. Martin, 32, is now back at work on the same rig, the Courageous, leased by
Shell Oil. He says he is grateful he could discuss his pain by video with the
doctor. ''It's a lot better than trying to describe it on a phone,'' Mr. Martin
says.

Dr. Boultinghouse and two colleagues -- Michael J. Davis and Glenn G. Hammack--
run NuPhysicia, a start-up company they spun out from the University of Texas in
2007 that specializes in face-to-face telemedicine, connecting doctors and
patients by two-way video.

Spurred by health care trends and technological advances, telemedicine is
growing into a mainstream industry. A fifth of Americans live in places where
primary care physicians are scarce, according to  government statistics. That
need is converging with advances that include lower costs for video-conferencing
equipment, more high-speed communications links by satellite, and greater
ability to work securely and dependably over the Internet.

''The technology has improved to the point where the experience of both the
doctor and patient are close to the same as in-person visits, and in some cases
better,'' says Dr. Kaveh Safavi, head of global health care for Cisco Systems,
which is supporting trials of its own high-definition video version of
telemedicine in California, Colorado and New Mexico.

The interactive telemedicine business has been growing by almost 10 percent
annually, to more than $500 million in revenue in North America this year,
according to Datamonitor, the market research firm. It is part of the  $3.9
billion telemedicine category that includes monitoring devices in homes and
hundreds of health care applications for smartphones.

Christine Chang, a health care technology analyst at Datamonitor's  Ovum unit,
says telemedicine will allow doctors to take better care of larger numbers of
patients. ''Some patients will be seen by teleconferencing, some will send
questions by e-mail, others will be monitored'' using digitized data on symptoms
or indicators like glucose levels, she says.

Eventually, she predicts, ''one patient a day might come into a doctor's office,
in person.''

Although telemedicine has been around for years, it is gaining traction as never
before. Medicare, Medicaid and other government health programs have been
reimbursing doctors and hospitals that provide care remotely to rural and
underserved areas. Now a growing number of big insurance companies, like the
UnitedHealth Group and several Blue Cross plans, are starting to market
interactive video to large employers. The new federal health care law provides
$1 billion a year to study telemedicine and other innovations.

With the expansion of reimbursement, Americans are on the brink of ''a gold rush
of new investment in telemedicine,'' says Dr. Bernard A. Harris Jr., managing
partner at Vesalius Ventures, a venture capital firm based in Houston. He has
worked on telemedicine projects since he helped build medical systems for NASA
during his days as an astronaut in the 1990s.

Face-to-face telemedicine technology can be as elaborate as a high-definition
video system, like Cisco's, that can cost up to hundreds of thousands of
dollars. Or it can be as simple as the Webcams available on many  laptops.

NuPhysicia uses equipment in the middle of that range -- standard
videoconferencing hookups made by Polycom, a video conferencing company based in
Pleasanton, Calif. Analysts say the setup may cost $30,000 to $45,000 at the
patient's end -- with a suitcase or cart containing scopes and other special
equipment -- plus a setup for the doctor that costs far less.

Telemedicine has its skeptics. State regulators at the Texas Medical Board have
raised concerns that doctors might miss an opportunity to pick up subtle medical
indicators when they cannot touch a patient. And while it does not oppose
telemedicine, the American Academy of Family Physicians says patients should
keep in contact with a primary physician who can keep tabs on their health
needs, whether in the virtual or the real world.

''Telemedicine can improve access to care in remote sites and rural areas,''
says Dr. Lori J. Heim, the academy's president. ''But not all visits will take
place between a patient and their primary-care doctor.''

Dr. Boultinghouse dismisses such concerns. ''In today's world, the physical exam
plays less and less of a role,'' he says. ''We live in the age of imaging.''

ON the rig Courageous, Mr. Martin is part of a crew of 100. Travis G. Fitts Jr.,
vice president for human resources, health, safety and environment at Scorpion
Offshore, which owns the rig, says that examining a worker via two-way video can
be far cheaper in a remote location than flying him to a hospital by helicopter
at $10,000 a trip.

Some rigs have saved $500,000 or more a year, according to NuPhysicia, which has
contracts with 19 oil rigs around the world, including one off Iraq. Dr.
Boultinghouse says the Deepwater Horizon drilling disaster in the Gulf of Mexico
may slow or block new drilling in United States waters, driving the rigs to more
remote locations and adding to demand for telemedicine.

NuPhysicia also offers video medical services to land-based employers with 500
or more workers at a site. The camera connection is an alternative to an
employer's on-site clinics, typically staffed by a nurse or a physician
assistant.

Mustang Cat, a Houston-based distributor that sells and services Caterpillar
tractors and other earth-moving equipment, signed on with NuPhysicia last year.
''We've seen the benefit, '' says Kurt Hanson, general counsel at Mustang, a
family-owned company. Instead of taking a half-day or more off to consult a
doctor, workers can get medical advice on the company's premises.

NuPhysicia's business grew out of work that its founders did for the state of
Texas. Mr. Hammack, NuPhysicia's president, is a former assistant vice president
of the University of Texas Medical Branch at Galveston, where he led development
of the state's pioneering telemedicine program in state prisons from the
mid-1990s to 2007. Dr. Davis is a cardiologist.

Working with Dr. Boultinghouse, Dr. Davis and other university doctors conducted
more than 600,000 video visits with inmates. Significant improvement was seen in
inmates' health, including measures of blood pressure and cholesterol, according
to a 2004 report on the system in the Journal of the American Medical
Association.

In March, California officials released a report they had ordered from
NuPhysicia with a plan for making over their state's prison health care. The
makeover would build on the Texas example by expanding existing telemedicine and
electronic medical record systems and putting the University of California in
charge.

California spends more than $40 a day per inmate for health care, including
expenses for guards who accompany them on visits to outside doctors. NuPhysicia
says that this cost is more than four times the rate in Texas and Georgia, and
almost triple that of New Jersey, where telemedicine is used for mental health
care and some medical specialties.

''Telemedicine makes total sense in prisons,'' says Christopher Kosseff, a
senior vice president and head of correctional health care at the University of
Medicine and Dentistry of New Jersey. ''It's a wonderful way of providing ready
access to specialty health care while maintaining public safety.''

Georgia state prisons save an average of $500 in transportation costs and
officers' pay each time a prisoner can be treated by telemedicine, says Dr.
Edward Bailey, medical director of Georgia correctional health care.

With data supplied by the California Department of Corrections and
Rehabilitation, which commissioned the report, NuPhysicia says the
recommendations could save the state $1.2 billion a year in prisoners' health
care costs.

Gov. Arnold Schwarzenegger  wants the university regents and the State
Legislature to approve the prison health makeover. After lawsuits on behalf of
inmates, federal courts appointed a receiver in 2006 to run prison medical
services. (The state now runs dental and mental health services, with court
monitoring.) Officials hope that by putting university doctors in charge of
prison health, they can persuade the courts to return control to the state.

''We're going to use the best technology in the world to solve one of our worst
problems -- the key is telemedicine,'' the governor said.

WITHOUT the blessing of insurers, telemedicine could never gain traction in the
broader population. But many of the nation's biggest insurers are showing
growing interest in reimbursing doctors for face-to-face video consulting.

Starting in June, the UnitedHealth Group plans to reimburse doctors at Centura
Health, a Colorado hospital system, for using Cisco advanced video to serve
UnitedHealth's members at several clinics. And the insurer  plans a national
rollout of telemedicine programs, including video-equipped booths in retail
clinics in pharmacies and big-box stores, as well as in clinics at large
companies.

''The tide is turning on reimbursement,'' says Dr. James Woodburn, vice
president and medical director for telehealth at UnitedHealth.

Both UnitedHealth and WellPoint, which owns 14 Blue Cross plans, are trying
lower-cost Internet Webcam technology, available on many off-the-shelf laptops,
as well as advanced video.

UnitedHealth and Blue Cross plans in Hawaii, Minnesota and western New York are
using a Webcam service provided by American Well, a  company based in Boston.
And large self-insured employers like Delta Air Lines and Medtronic, a Blue
Cross Blue Shield customer in Minneapolis, are beginning to sign up.

Delta will offer Webcam consultations with UnitedHealth's doctor network to more
than 10,000 Minnesota plan members on July 1, says Lynn Zonakis, Delta's
managing director of health strategy and resources. Within 18 months, Webcam
access will be offered nationally to more than 100,000 Delta plan members.

Dr. Roy Schoenberg,  C.E.O. of American Well, says his Webcam service is ''in a
completely different domain'' than Cisco's or Polycom's.  ''Over the last two
years, we are beginning to see a side branch of telemedicine that some call
online care,'' he says. ''It connects doctors with patients at home or in their
workplace.''

Doctors ''are not going to pay hundreds of thousands of dollars for equipment,
so we have to rely on lower tech,'' he adds. The medical records are stored on
secure Web servers behind multiple firewalls, and the servers are audited twice
a year by I.B.M. and other outside computer security companies, Dr. Schoenberg
says.

In Hawaii, more than 2,000 Blue Cross plan members used Webcams to consult
doctors last year, says Laura Lott, a spokeswoman for the Hawaii Medical Service
Association. Minnesota Blue Cross and Blue Shield started a similar Webcam
service across the state last November.

Doctors who use the higher-tech video conferencing technology say that Webcam
images are less clear, and that Webcams cannot  accommodate electronic scopes or
provide the zoom-in features available in video conferencing.  ''If they are not
using commercial-grade video conferencing gear, the quality will be much
lower,'' says Vanessa L. McLaughlin, a telemedicine consultant in Vancouver,
Wash.

Last month, Charlie Martin, the crane operator, was back in the infirmary of the
Courageous for an eye checkup. In Houston, his face filled the big screen in
NuPhysicia's office.

After an exchange of greetings, Chris Derrick, the paramedic on the oil rig,
attached an ophthalmological scanner to a scope, pointed it at Mr. Martin's eye,
and zoomed in.

''Freeze that,'' Dr. Boultinghouse ordered, as a close-up of the eye loomed on
the screen.  ''His eyes have been bothering him. It may be from the wind up
there on the crane.''

URL: http://www.nytimes.com

LOAD-DATE: May 30, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: From thousands of miles away, Dr. Oscar Boultinghouse checks
the eye of a patient. (BU1)
 Dr. Jerry Jones uses two-way video at his home in Houston to consult with a
patient  across town.  Dr. Jones is under contract to NuPhysicia, one of the new
telemedicine companies. (PHOTOGRAPHS BY MICHAEL STRAVATO FOR THE NEW YORK TIMES)
(BU4)

PUBLICATION-TYPE: Newspaper


