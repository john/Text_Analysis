                   Copyright 2009 The New York Times Company


                             919 of 2021 DOCUMENTS


                               The New York Times

                            November 1, 2009 Sunday
                              Late Edition - Final

Top Democrats Are Pushing for New Strategies on Federal Deficit

BYLINE: By JACKIE CALMES and CARL HULSE

SECTION: Section A; Column 0; National Desk; Pg. 26

LENGTH: 1179 words

DATELINE: WASHINGTON


Faced with anxiety in financial markets about the huge federal deficit and the
potential for it to become an electoral liability for Democrats, the White House
and Congressional leaders are weighing options for narrowing the gap, including
a bipartisan commission that could force tax increases and spending cuts.

But even the idea of a panel to bridge the partisan divide has run into partisan
objections. Many Democrats, including in the White House, are loath to cede such
far-reaching decisions to a commission and doubt Republicans' willingness to
compromise. And most Republicans remain adamantly opposed to tax increases,
leaving the prospects for any bipartisan approach limited at best.

The proponents, however, are pressing for a Senate vote this month. ''If we have
the same process and the same people, we are going to get the same results,''
said Senator Evan Bayh, Democrat of Indiana, who recently met with Mr. Obama to
discuss the idea. ''The Democratic Party wants to spend more than we can afford;
the Republican Party tends to want to cut taxes more than we can afford. So we
are stuck.''

Concerns about the deficit are building even as the White House and Congress
continue to add to it with tax cuts and spending to stimulate a still-fragile
economy. Yet those one-time costs do not trouble most economists and market
analysts.

The main driver of long-term deficits is the chasm between the benefit programs
Medicare and Medicaid, which are growing faster than the economy, and federal
tax collections, which are at one of their lowest levels in many decades
relative to the size of the economy.

Mr. Obama's budget director, Peter R. Orszag, now at work on the president's
next budget, due in February for the 2011 fiscal year, declined to comment about
a bipartisan commission and instead promised that the coming budget would
propose additional ways to reduce the deficit beyond next year, when the economy
has  fully recovered.

''As the recovery strengthens and the economy begins to deliver job growth, we
will have to take the tough steps necessary to return our nation to a fiscally
disciplined and sustainable path,'' Mr. Orszag said. ''Even with a fiscally
responsible health insurance reform that will help to reduce long-term deficits,
we recognize that more is needed to put the budget back on a sustainable path.''

The administration continues to argue that Mr. Obama's proposed health care
overhaul is ''the most significant act we could take to tackle the deficit,'' as
Christina D. Romer, the chairman of his Council of Economic Advisers, said in a
speech last week. But it remains unclear how much the legislation working its
way through Congress would slow the growth of Medicare and Medicaid.

The administration has not signaled what specific proposals it might make, but
Mr. Orszag's challenges are formidable: the chief sources of significant deficit
reductions -- savings in government health programs and tax increases on the
rich -- will have been tapped to offset the cost of a health care bill if it is
enacted.

The military savings the administration projected this year are in question
given the buildup in Afghanistan, and it has been unable to close corporate tax
loopholes worth many billions of dollars. Interest costs have increased with the
bigger debt. And the economic forecast has worsened since Mr. Obama's first
budget.

Proposals for a bipartisan commission could get a vote in late November, when
Congress must raise the nation's nearly $12 trillion debt limit so the Treasury
Department can continue borrowing to keep the government running. Measures to
raise the legal debt limit have been magnets for anti-deficit amendments in the
past.

Mr. Bayh met with Mr. Obama at the White House several weeks ago as a leader of
a group of 10 senators -- nine moderate Democrats and an independent -- who
signed a letter supporting an amendment binding Congress and the White House to
''a special process'' that would force them ''to face up to our nation's
long-term fiscal imbalances.''

There is no indication that more liberal Democrats have any appetite for deep
spending cuts or further tax increases in the coming year as the party battles
to maintain its majorities in the House and Senate in the 2010 midterm
elections. But the party's more fiscally hawkish members are becoming more
outspoken about the need to address the issue in a serious way.

''It is imperative that we act,'' said Senator Kent Conrad, the North Dakota
Democrat who is chairman of the Senate Budget Committee and supports the
amendment, though he did not sign the letter. ''The default position now is to
take care of things by not paying for them. We are no longer in a position where
we can continue to do that.''

Mr. Conrad said he recently had a ''highly energetic'' meeting at the White
House with Mr. Obama about bringing a new urgency to the push for deficit
reduction. He also debated the pros and cons of a commission with a Democratic
opponent, Senator Charles E. Schumer of New York, in a meeting attended by Rahm
Emanuel, the White House chief of staff.

And Mr. Conrad has been reaching out to several Republicans, including Senator
Judd Gregg of New Hampshire, the senior Republican on the Budget Committee who
was Mr. Conrad's co-sponsor of a proposal for the bipartisan commission.

Mr. Conrad favors something like the successful commission to close military
bases, another decision that lawmakers found difficult to make because of
parochial and political considerations.

The commission's members would be all or mostly lawmakers, to give them a stake
in the outcome, and Congress would have to vote its recommendations up or down,
without amendment.

The Budget Committee will hold hearings in mid-November on the Conrad-Gregg
proposal and other ideas from lawmakers and budget analysts for attacking
deficits that are the largest since the end of World War II.

The deficit for the fiscal year that ended Sept. 30 was $1.4 trillion, and the
combined deficits projected over the next decade would add about $9 trillion to
the national debt.

Republicans are ''openly in support of it,'' Mr. Gregg said. ''It would be a
very significant step in the right direction,'' he added, and ''send a
definitive signal to the markets and the American people that we are finally
serious.''

But Mr. Gregg predicted that leading Congressional Democrats would resist,
saying, ''I have seen nothing that implies they would accept a real commission
that would accomplish what needs to be done and be bipartisan and fair.''

The speaker of the House, Representative Nancy Pelosi of California, is opposed
to creating a commission. Other Democrats say she does not want to further
reduce spending for Medicare and Medicaid, the entitlement programs whose growth
is driving the projections of unsustainable deficits, or give Republicans
near-equal power over the commission's work.

''She thinks we can accomplish the same goals through the work of the committees
we already have,'' said her spokesman, Brendan Daly.

URL: http://www.nytimes.com

LOAD-DATE: November 1, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Senator Kent Conrad, Democrat of North Dakota, is a co-sponsor
of a plan to create a bipartisan commission that would seek ways to cut the
federal deficit. ''It is imperative that we act,'' he says.(PHOTOGRAPH BY LUKE
SHARRETT/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


