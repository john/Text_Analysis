                   Copyright 2009 The New York Times Company


                             378 of 2021 DOCUMENTS


                               The New York Times

                            August 22, 2009 Saturday
                              Late Edition - Final

Public but Seeming Private

BYLINE: By ANNE UNDERWOOD

SECTION: Section A; Column 0; National Desk; MEDICARE; Pg. 11

LENGTH: 203 words


There is no question that older Americans are worried that any health care
overhaul may end up cutting their benefits, even if the worst of those fears are
being stoked by misinformation.

What may puzzle more is the confusion among Medicare recipients about what the
program is and how it works, as evident in the variations of ''Keep government
out of Medicare!'' shouted at town-hall-style meetings.

Why the confusion? Experts note that in many ways Medicare can seem like a
private program. Recipients pay part of the premiums, for example, through
deductions from their Social Security checks.

And they may pay private insurers for Medicare Part D drug coverage and Medigap
policies that cover things Medicare does not.

For anything Medicare does not fully cover, the patients can receive bills from
doctors.

''The perception is that it's between the doctor, the lab and the patient,''
said Dr. Cheryl Phillips, president of the American Geriatrics Society. ''It
feels like a private choice.''

And statements of Medicare benefits resemble traditional insurance statements.
The notices are printed on C.M.S. letterhead. The tiny type explains: the
Centers for Medicare & Medicaid Services. ANNE UNDERWOOD

URL: http://www.nytimes.com

LOAD-DATE: August 22, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


