                   Copyright 2010 The New York Times Company


                             2004 of 2021 DOCUMENTS


                               The New York Times

                              May 24, 2010 Monday
                              Late Edition - Final

Inside the Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 1353 words


International

STUDY FINDS DECLINES

In Childhood Mortality

According to a study by the Lancet, a medical journal, death rates in children
under 5 are dropping in many countries at a surprisingly fast pace. The report
is based on data from 187 countries from 1970 to 2010. PAGE A4

ANXIETY OVER ISRAELI DRILL

As Israel embarked on a large-scale civil defense exercise, Prime Minister
Benjamin Netanyahu sought to reassure Israelis and some jittery Arab neighbors
that the nationwide drill was not meant to signal a deterioration in security or
an imminent war. PAGE A10

ETHIOPIA HOLDS VOTE

Ethiopians went to the polls to choose a new Parliament, with Prime Minister
Meles Zenawi's ruling party expected to sweep the vote and extend its 19-year
rule, after a campaign marred by widespread complaints of harassment and
intimidation of the opposition.  PAGE A8

14 DIE IN SOMALI VIOLENCE

At least 14 people were killed and more than 25 were wounded in heavy fighting
between government troops and insurgents who attacked the presidential palace
with mortars, witnesses and officials said. PAGE A8

UNREST IN JAMAICAN CAPITAL

The Jamaican government declared a state of emergency in portions of Kingston,
the capital, after supporters of a gang leader who is wanted in the United
States on gun and drug charges attacked three police stations in an attempt to
pressure the government to let him remain free, officials said. PAGE A8

PRIEST ARRESTED IN BRAZIL

The authorities in Rio de Janeiro said that they had arrested Father Marcin
Michael Strachanowski, a Polish priest, and charged him with sexually abusing a
16-year-old former altar boy. It is the third case of sexual abuse involving a
priest in Brazil, which has the world's biggest Roman Catholic population, in
the last two months. PAGE A8

DUCHESS CAUGHT IN STING

The News of the World, a British tabloid, reported that Sarah Ferguson, the
Duchess of York and the queen's former daughter-in-law, had accepted $40,000 in
cash -- and a promise of about $717,000 more -- from a ''rich businessman''
(really a reporter in disguise) in return for pledging to introduce him to
Prince Andrew, her former husband and Queen Elizabeth's second son.  PAGE A10

CLERIC JUSTIFIES KILLINGS

In a newly released video, Anwar al-Awlaki, the Muslim cleric believed to be an
inspiration for a series of recent terrorism plots, justifies the mass killing
of American civilians and taunts the authorities to come find him in Yemen. PAGE
A10

National

STUDY POINTS TO PENALTIES

For Employer Health Plans

About one-third of employers subject to major requirements of the new health law
may face tax penalties because they offer health insurance that could be
considered unaffordable to some employees, a new study says. PAGE A15

FRUSTRATION SPREADS ON SPILL

Louisiana state and local officials continued to hammer BP and the federal
agencies reacting to the Gulf of Mexico oil spill on Sunday, repeatedly
threatening to ''take matters into our own hands'' if the response fell short.
PAGE A14

A TEA PARTY TARGET

Rahm Emanuel, the White House chief of staff, has a vision of how the foreboding
national political environment might evolve to his party's advantage: making
Rand Paul, who won the Kentucky Republican Senate primary, the face of the
opposition. John Harwood, The Caucus. PAGE A15

SHORTAGE OF TRAFFIC PAINT

A shortage of traffic striping paint has left road crews and transportation
departments across the country without the materials they need to separate
northbound and southbound traffic, and has threatened the completion of highway
projects.   PAGE A12

A HOME BASE IN WASILLA

Even as Sarah Palin travels the country stirring up Tea Party apostles, poking
at opponents and building a robust bank account through speaking fees, book
royalties and television contracts, she comes home to Wasilla, Alaska, one of
the more unlikely launching pads in politics -- and she apparently will not be
pulling up stakes anytime soon. PAGE A12

New York

LIVING-WAGE PROPOSAL

May Prompt City Hall Fight

In what could set off a major City Hall battle, two City Council members from
the Bronx plan to propose a bill on Tuesday that would guarantee wages of at
least $10 an hour, nearly $3 above the minimum wage, to all workers at
development projects receiving public subsidies. PAGE A20

OFFICIAL CRITICIZES CANDIDATE

A top Democratic Party official said that Richard Blumenthal, the attorney
general of Connecticut, had ''clearly overstated'' the extent of his military
service during the Vietnam War, a blunt assessment that came as Mr. Blumenthal
is trying to recover from reports that he misrepresented his war record. PAGE
A19

PARTY BOSSES WITH LESS CLOUT

Few New Yorkers know the names of the Republican and Democratic county chairmen,
and they are not the kingmakers they once were. But they may still play a
pivotal role this week and next as the two parties gather at their state
conventions to choose candidates for the elections this year. PAGE A19

Metropolitan Diary A20

Business

SOCIAL NETWORKS CAPITALIZE

On Facebook Privacy Fears

A handful of start-ups are eyeing the social networking industry with renewed
interest, due in part from the recent scrutiny focused on Facebook over
revisions to its platform and privacy policy that encourage its members to make
personal information accessible to anyone on the Internet. PAGE B1

A CROSSROADS FOR LIMEWIRE

Mark Gorton, a successful Wall Street trader with a varied academic background,
is not who comes to mind when the phrase ''Internet pirate'' is spoken. But two
weeks ago, a federal judge ruled that he and the popular file-sharing service he
created, LimeWire, were liable for copyright infringement and could be forced to
pay up to $450 million in damages. PAGE B1

A PRESS CORPS STAYS HOME

For decades it was a given that whenever the president traveled, a chartered
plane packed with members of the press would travel with him. But the press
flights have been sharply curtailed in recent months, a result of cost-cutting
by news organizations that are struggling to stay profitable. PAGE B1

WORLD CUP'S TV ECONOMICS

For television broadcasters preparing to cover the World Cup, by some measures
the world's biggest sports event, the odds will be challenging. The World Cup is
expected to provide them with a welcome increase in viewership and advertising.
But the soaring price of TV rights means that few will actually make money from
the event. PAGE B4

Sports

AS TOUR OF CALIFORNIA ENDS,

Doping Controversy Lingers

After four days of chaos in cycling, spurred by Floyd Landis's admission of
doping and his allegations against other top riders, HTC-Columbia's Michael
Rogers -- one of the few top riders that Landis did not implicate -- celebrated
his victory at the Tour of California.  PAGE D1

N.F.L. MULLS RULES CHANGES

Tuesday's N.F.L. owners' meeting in Dallas -- in which officials will decide
whether to award the first open-air, cold-weather Super Bowl in history to New
York and whether to erase the old sudden-death overtime rules from the game --
may provide the most obvious example yet that Commissioner Goodell doesn't want
the most popular professional league in the United States to slip into
complacency. PAGE D2

Arts

THAI FILMMAKER WINS

Palme d'Or at Cannes

The 63rd Cannes  Film Festival came to a close with the Palme d'Or going to the
fantastical ''Uncle Boonmee Who Can Recall His Past Lives,'' from the the
filmmaker Apichatpong Weerasethakul, the first Thai film to win the award. PAGE
C1

Obituaries

MARTIN GARDNER, 95

He teased brains by creating math puzzles in Scientific American for a
quarter-century, and indulged his own restless curiosity by writing more than 70
books on topics as diverse as magic, philosophy and the nuances of Alice in
Wonderland. PAGE A23

Op-ed

PAUL KRUGMAN PAGE A25

ROSS DOUTHAT PAGE A25

Online

ON THE BLOGS A recap of the finale of   ''Lost''   on the ArtsBeat blog, with
links to past coverage of the show and reaction to the end of the series from
around the Web.

nytimes.com/television

URL: http://www.nytimes.com

LOAD-DATE: May 24, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


