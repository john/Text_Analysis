                   Copyright 2009 The New York Times Company


                             1170 of 2021 DOCUMENTS


                               The New York Times

                           December 12, 2009 Saturday
                              Late Edition - Final

A Bill Evolving

SECTION: Section A; Column 0; National Desk; TRACKING CHANGES; Pg. 12

LENGTH: 113 words


What exactly is in the Senate health care bill? It's not an easy question to
answer. Though the original legislation is available online for anyone to read,
it has already been amended  to include mandates for preventive care for women,
for instance, and Medicare benefit guarantees for older Americans. In order to
help track the evolution of the Senate bill, The Times has started a new page
summarizing the status of major amendments and linking readers to articles that
explore the proposed changes in greater depth. Find it at the URL below, and
share your thoughts about the legislation with others on the blog.

http://politics.nytimes.com/congress/bills/111/hr3590/amendments

URL: http://www.nytimes.com

LOAD-DATE: December 12, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


