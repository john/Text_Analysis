                   Copyright 2009 The New York Times Company


                             464 of 2021 DOCUMENTS


                               The New York Times

                           September 1, 2009 Tuesday
                              Late Edition - Final

Massachusetts Cuts Back Immigrants' Health Care

BYLINE: By ABBY GOODNOUGH

SECTION: Section A; Column 0; National Desk; Pg. 17

LENGTH: 525 words

DATELINE: BOSTON


State-subsidized health insurance for 31,000 legal immigrants here will no
longer cover dental, hospice or skilled-nursing care under a scaled-back plan
that Gov. Deval Patrick announced Monday.

Mr. Patrick said his administration had struggled to find a solution ''that
preserves the promise of health care reform'' after the state legislature cut
most of the $130 million it had previously allotted immigrants, to help close a
budget deficit. Although their health benefits will be sharply curtailed in some
cases, Mr. Patrick portrayed the new program as a victory, saying the services
that the affected group tends to use the most will still be covered.

''It's an extraordinary accomplishment,'' he said in a conference call with
reporters, ''to offer virtually full coverage for the entire population that's
been impacted in the face of really extraordinary budget constraints.''

The new plan, which will cover permanent residents who have had green cards for
less than five years, will cost the state $40 million a year. Some of the
affected immigrants will be charged higher co-payments and will have to find new
doctors, said Leslie A. Kirwan, Mr. Patrick's finance director.

Still, Mr. Patrick described the new coverage as comprehensive and said it could
be a model for less expensive state-subsidized benefits as health care costs
continue to rise. Under the 1996 federal law that overhauled the nation's
welfare system, the 31,000 affected immigrants do not qualify for Medicaid or
other federal aid. Massachusetts is one of the few states -- others are
California, New York and Pennsylvania -- that provide at least some health
coverage for such immigrants.

Because of its three-year-old law requiring universal health coverage,
Massachusetts has the country's lowest percentage of uninsured residents: 2.6
percent, compared with a national average of 15 percent. The law requires that
almost every resident have insurance, and to meet that goal, the state
subsidizes coverage for those earning up to three times the federal poverty
level, or $66,150 for a family of four.

All of the affected immigrants will be covered under the new plan by Dec. 1, Mr.
Patrick said; in the meantime they will have to rely on hospitals that provide
free emergency care to the poor.

CeltiCare Health Plan of Massachusetts, a subsidiary of the Centene Corporation,
based in Missouri, won a yearlong state contract to provide the new coverage.

Eva Millona, executive director of the Massachusetts Immigrant and Refugee
Advocacy Coalition, said she was worried about immigrants' having to find new
primary care doctors at a time when the state is suffering from a shortage of
such providers. She also said that the new coverage would in some cases require
a much higher co-payment -- $50 instead of between $1 and $3 -- for non-generic
prescription drugs, and that enrollment would  be capped at the 31,000 current
enrollees.

''We see this as a temporary solution,'' Ms. Millona said, ''and we are still
working to get full restoration for this population that deserves the same level
of coverage as all other taxpaying residents of the state.''

URL: http://www.nytimes.com

LOAD-DATE: September 1, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Gov. Deval Patrick said 31,000 legal immigrants would no longer
be covered for dental, hospice or skilled-nursing care. (PHOTOGRAPH BY JOSH
REYNOLDS/ASSOCIATED PRESS)

PUBLICATION-TYPE: Newspaper


