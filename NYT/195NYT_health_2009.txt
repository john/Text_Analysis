                   Copyright 2009 The New York Times Company


                             195 of 2021 DOCUMENTS


                               The New York Times

                            July 22, 2009 Wednesday
                              Late Edition - Final

A Defining Moment Nears for President

BYLINE: By SHERYL GAY STOLBERG

SECTION: Section A; Column 0; National Desk; Pg. 14

LENGTH: 925 words

DATELINE: WASHINGTON


Six months into his administration, President Obama is at a pivotal moment. He
has pushed through a $787 billion economic stimulus package, bailed out Wall
Street and, on Tuesday, managed to beat the defense industry in the Senate,
which voted to kill a high-profile fighter jet program.

But the public, and lawmakers, are growing skittish over Mr. Obama's next big
plan, to remake the American health care system. How he handles the issue over
the next several weeks could shape the rest of his presidency, shedding light on
his political strength, his relationship with both parties in Congress and the
extent to which he is willing to bend in fighting for his agenda.

With some fellow Democrats balking over his insistence that both the House and
the Senate pass health legislation before the August recess, Mr. Obama has a
tough decision to make: Does he take a hard line, demanding that lawmakers stick
to his timetable -- and risk losing the support of Republicans and moderate
Democrats? Or does he signal flexibility, allowing lawmakers to take their time
-- and give opponents the chance to marshal their case against the bill?

''He's got to be careful that while he ratchets up the pressure, he doesn't bet
his whole presidency on whether this gets done before the August recess,'' said
Kenneth M. Duberstein, who orchestrated President Ronald Reagan's first-term
legislative strategy. ''He has a broad, broad agenda that he's in a rush to
enact, and if he's not careful he will be viewed as a steamroller who tries to
get things fast and not necessarily right.''

On Wednesday, Mr. Obama will address the nation in a prime-time news conference.
Rahm Emanuel, the White House chief of staff, said in an interview that the
president intended to use it as a ''six-month report card,'' to talk about ''how
we rescued the economy from the worst recession'' and the legislative agenda
moving forward, including health care and energy legislation, which squeaked
through the House and faces a tough road in the Senate.

Polls show that Mr. Obama is more popular than his own policies, a worrisome
sign for a president with such an ambitious agenda. Mickey Edwards, a former
Republican congressman who is now vice president of the Aspen Institute, said
Mr. Obama might be making a mistake in reading his election as a mandate for
dramatic change.

''A lot of people supported Obama because they wanted to repudiate the Bush
administration,'' said Mr. Edwards, who backed Mr. Obama for president. ''I was
one of those people who supported him for reasons other than the policies he is
proposing. He seemed more thoughtful, more contemplative -- I felt he had the
right temperament to be president. But I think his health care proposal goes
beyond what the public at the moment is ready to accept.''

Mr. Obama came into office promising a more bipartisan Washington tone, which he
has so far  been unable to achieve. His actions in the coming weeks on health
care may determine his long-term relationship not only with Republicans but also
with his fellow Democrats.

''I think this will be a major factor in defining his presidency,'' said Tom
Daschle, the former Senate Democratic leader, who remains a close adviser to the
White House on health issues. ''Because he's made it such an issue, and because
he has invested so much personal time and effort, this will, more than stimulus
and more than anything he has done so far, be a measure of his clout and of his
success early on. And because it is early on, it will define his subsequent
years.''

On the Republican side, one question is whether Mr. Obama will succumb to the
temptation to turn health care into a partisan fight, even as he tries to court
the opposing party. He is, after all, still a popular new president confronting
an unpopular Republican Party, and so it would be easy for him to demonize
Republicans as obstructionists who want to stand in the way of progress.

Senator Jim DeMint, Republican of South Carolina, gave Mr. Obama an opening to
do just that the other day, and the president took it. Mr. DeMint called health
care a ''Waterloo moment'' that could break Mr. Obama. The president struck
back, declaring, ''This isn't about me.'' But if Mr. Obama extends that line of
attack to Republicans more broadly, and rams a bill through without their
support, any claim he may have to bipartisanship will quickly evaporate.

As for Democrats, Mr. Obama faces a balance-of-power conundrum. He has said all
along that he will set out broad principles for a bill and leave the details to
Congress. But now House Democrats in the fiscally conservative Blue Dog
Coalition, including seven who hold decisive votes on the Energy and Commerce
Committee, say they will not support the House bill without big changes.

One question for Mr. Obama is whether to try to strong-arm them, and face a
rebellion from some of the very same conservative Democrats who helped put him
in office. If he forces them to vote for a bill their constituents do not like,
on a timetable that feels too rushed for them, it could hurt them at home. That
could mean a bigger political problem for the White House: a resulting loss of
Democratic seats in the 2010 midterm elections.

Another question is how hard Mr. Obama will push Congress as a whole to adopt
his progressive agenda, not only on health care but also on climate change and a
variety of other issues.

The next few weeks, as the president tries to broker a health care deal, may
well tell Americans just how far he is willing to go.

URL: http://www.nytimes.com

LOAD-DATE: July 22, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: President Obama will hold a prime-time news conference on
Wednesday that will in effect be a ''six-month report card.'' (PHOTOGRAPH BY
BRENDAN SMIALOWSKI FOR THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


