                   Copyright 2009 The New York Times Company


                             438 of 2021 DOCUMENTS


                               The New York Times

                            August 29, 2009 Saturday
                              Late Edition - Final

Bullish on Bipartisanship

BYLINE: By DUFF WILSON

SECTION: Section A; Column 0; National Desk; THE VIEW FROM WALL STREET; Pg. 11

LENGTH: 205 words


Which overhaul outcome would be best for the stock market?

A recent analysis by Goldman Sachs concludes that a bipartisan compromise in
Washington would be good news for investors in health care stocks.

And compromise is looking more and more likely, Goldman says. The report, issued
to the firm's brokerage clients, says it is ''more than 50 percent likely'' that
Congress will pass a ''sharply scaled back'' health care plan, one that covers
fewer than 20 million of the uninsured and that provides for neither a
government-run insurance plan nor an insurance cooperative.

The market winners if that happens, Goldman says, will be big drug makers and,
depending on what Congress does with insurance reform, possibly big insurers.
Hospital stocks might also do well, depending on the extent of Medicare cuts.

In a June report, Goldman had said health legislation that passed with only
Democratic support ''would be negative for most health care stocks.'' But a
Democratic attempt to end-run Republicans, in Goldman's view, would probably
cause the whole overhaul effort to collapse.

And then what? For the stock market, at least, Goldman says a health care
stalemate in Washington would be ''very positive'' news. DUFF WILSON

URL: http://www.nytimes.com

LOAD-DATE: August 29, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


