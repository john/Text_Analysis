                   Copyright 2009 The New York Times Company


                             531 of 2021 DOCUMENTS


                               The New York Times

                          September 9, 2009 Wednesday
                              Late Edition - Final

How to Watch the Speech

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 17

LENGTH: 707 words


In his speech to Congress on Wednesday night, President Obama will press his
case for major health care legislation,  not just with lawmakers in the
audience, but also with a deeply wary public watching at home.

And while Mr. Obama will probably talk in clear, plain-spoken terms, decoding
his remarks and the reaction he gets will require both a careful ear and a keen
eye. Here are some of the main things to listen and look for:

Will he reiterate his support for a government-run insurance plan -- or finally
state outright that he can accept a compromise that might disappoint liberals?

Will the president give a shout-out to influential Republicans like Senator
Charles E. Grassley of Iowa -- or threaten to barrel over opponents?

Will centrist lawmakers sit on their hands -- or give him a big ovation?

Will Mr. Obama reassure elderly voters worried about Medicare cuts? Can he do
that even while explaining how the country can afford to subsidize health
coverage for more than 40 million Americans who currently have none?

Beyond those big questions, here's an armchair guide to watching the president's
big speech at home.

Bipartisan,  or by Any Means Necessary?

Mr. Obama has long said he wants a bipartisan deal on health care. But after
getting pummeled by Republican critics during the Congressional recess,
Democrats now consider compromise a long shot.

So watch the president's speech closely for any sign that he is now willing to
let Democrats go it alone, maybe by using a controversial procedural tactic
known as budget reconciliation.

Aides have said he will draw lines in the sand. Will he? Or will he still be the
conciliator? One gauge will be whether Mr. Obama praises pivotal Republican
negotiators in the Senate, including Mr. Grassley, Michael B. Enzi of Wyoming
and Olympia J. Snowe of Maine.

In the past, the president has paid tribute to all three by name. But if Mr.
Obama directs his attention solely at Maine, it will suggest that he is giving
up on winning over Republicans beyond Ms. Snowe and the state's junior senator,
Susan Collins.

And any effort by Mr. Obama to set new deadlines for completion of a bill would
signal that he is losing patience, opening the door for more aggressive
procedural tactics. (See ''budget reconciliation'' above.)

Public Option,  or Healthy Competition?

Democrats are divided over whether to create a government-run health insurance
plan to compete with private insurers. Republicans categorically reject this
''public option.'' Some centrist Democrats also dislike it and believe they can
create healthy competition for private insurers by creating nonprofit insurance
cooperatives.

The president has expressed strong support for the public option, but also
hinted he could accept the cooperatives. Lawmakers in both chambers will be
parsing his words carefully on this point. Liberals will be furious if Mr. Obama
drops the public option, and chances are he will continue to walk a tight rope.

Applause Meter

It's not simply how much applause Mr. Obama gets. What matters is who applauds
and when.

If centrist Democrats like Senators Ben Nelson of Nebraska and Mary L. Landrieu
of Louisiana do not vote with their hands, Mr. Obama's health care agenda could
be in serious trouble.

A similar warning sign could be frowns by Senator John D. Rockefeller IV,
Democrat of West Virginia, who is a strong advocate for government-provided
health insurance.

At the same time, if Ms. Snowe and Ms. Collins cheer Mr. Obama, the White House
may start smelling victory.

Among House members, keep an applause-o-meter eye on fiscally conservative
Democrats, like Representatives Mike Ross of Arkansas, as well as liberals like
Raul M. Grijalva of Arizona.

Closing the Sale

Ultimately, lawmakers will support a health care bill only if their constituents
say they like what they hear. And that means Mr. Obama will have to win over two
crucial groups: the 180 million Americans who already have health insurance and
worry they have more to lose than to gain; and the more than 45 million
Americans age 65 and over on Medicare.

So the reaction in the House chamber on Wednesday night may be less important
than the reaction in your own living room.

URL: http://www.nytimes.com

LOAD-DATE: September 9, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS (PHOTOGRAPHS BY CHARLES DHARAPAK/ASSOCIATED PRESS
 DOUG MILLS/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


