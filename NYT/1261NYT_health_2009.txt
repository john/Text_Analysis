                   Copyright 2009 The New York Times Company


                             1261 of 2021 DOCUMENTS


                               The New York Times

                            December 25, 2009 Friday
                              Late Edition - Final

The Sidney Awards

BYLINE: By DAVID BROOKS

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 31

LENGTH: 796 words


Every year, I give out Sidney Awards to the best magazine essays of the year. In
an age of zipless, electronic media, the idea is to celebrate (and provide
online links to) long-form articles that have narrative drive and social impact.

The first rule of the Sidneys is that they cannot go to any article that
appeared in The Times. So David Rohde does not get a Sidney for his
unforgettable series on being held captive by the Taliban. But those pieces
possess exactly the virtues that the Sidneys are meant to honor, and they make
one proud to be a journalist.

This year, magazines had a powerful effect on the health care debate. Atul
Gawande's piece, ''The Cost Conundrum,'' in The New Yorker, was the most
influential essay of 2009, and David Goldhill's ''How American Health Care
Killed My Father,'' in The Atlantic, explained why the U.S. needs fundamental
health reform. But special recognition should also go to Jonathan Rauch's
delightful essay, ''Fasten Your Seat Belts -- It's Going to Be a Bumpy Flight,''
in The National Journal.

Rauch described what the airline industry would look like if it worked the way
the health care industry works. The piece takes the form of a customer trying to
book a flight with a customer service representative. The customer wants to fly
from Washington, D.C., to Oregon on Oct. 3, but the airline lady can squeeze him
in only in January or February. He can call each of two dozen other airlines if
he wants to check other availability.

When he finally gets on a flight, he finds that his airline will only take him
to Chicago, since it's an eastern-region specialist. He'll have to find a
western-region specialist to get to Eugene. In addition, he'll have to fax in a
30-page travel history questionnaire, make arrangements with a separate luggage
transport provider and see if he can find a fuelist who might be free to make
fuel arrangements on that date. That is, if the airline is in his insurance
company's provider network, which it isn't.

The most powerful essay I read this year was David Grann's ''Trial by Fire'' in
The New Yorker. Grann investigated the case of Cameron Todd Willingham, who was
executed in 2004 for murdering his three children by setting their house on
fire.

In the first part of the essay, Grann lays out the evidence that led to
Willingham's conviction: the marks on the floor and walls that suggested that a
fire accelerant had been splashed around; the distinct smoke patterns suggesting
arson; the fact that Willingham was able to flee the house barefoot without
burning his feet.

Then, in the rest of the essay, Grann raises grave doubts about that evidence.
He tells the story of a few people who looked into the matter, found a
miscarriage of justice and then had their arguments ignored as Willingham was
put to death. Grann painstakingly describes how bogus science may have swayed
the system to kill an innocent man, but at the core of the piece there are the
complex relationships that grew up around a man convicted of burning his
children. If you can still support the death penalty after reading this piece,
you have stronger convictions than I do.

I try not to give Sidneys to the same people year after year, but the fact is,
talent is not randomly distributed. Some people, like Matt Labash of The Weekly
Standard, just know how to write. His piece, ''A Rake's Progress'' was a
sympathetic and gripping profile of Marion Barry, the former Washington, D.C.,
mayor, crack-smoker and recent girlfriend-stalker.

At the start of his first interview, Labash, making small talk, asked Barry if
he still has a scar from an old bullet wound: '' 'Let's see,' he says, lifting
his shirt, so that within ten minutes of arriving, I'm eyeball to areola with
Barry's left nipple. It's a move that's very Barry. Most times, he reveals
nothing at all. Then he reveals too much.''

Labash delights in Barry's rascally nature, but also captures why the voters of
Barry's ward don't merely vote for him, they possess him and cherish him.

The region around Afghanistan is now regarded as a global backwater, but S.
Frederick Starr's ''Rediscovering Central Asia,'' in The Wilson Quarterly, is an
eye-opening look at what once was. A thousand years ago, those mountains were
the intellectual center of the world. Central Asians invented trigonometry, used
crystallization as a means of purification, estimated the Earth's diameter with
astonishing precision and anticipated Darwin's theory of evolution. Starr
describes glittering cities and a flowering of genius. He also describes the
long decline -- the Sunni-Shia split played a role -- and modern glimmers of
revival.

On Tuesday, we will publish another batch of Sidney winners, so turn off ''It's
a Wonderful Life.'' Read these today.

URL: http://www.nytimes.com

LOAD-DATE: December 25, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


