                   Copyright 2009 The New York Times Company


                             466 of 2021 DOCUMENTS


                               The New York Times

                           September 1, 2009 Tuesday
                              Late Edition - Final

The Obama Slide

BYLINE: By DAVID BROOKS

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 29

LENGTH: 821 words


Two tides swept over American politics last  winter. The first was the Obama
tide. Barack Obama came into office with an impressive 70 percent approval
rating. The second was the independent tide. Over the first months of this year,
the number of people who called themselves either Democrats or Republicans
declined, while the number who called themselves independents surged ahead.

Obama's challenge was to push his agenda through a Democratic-controlled
government while retaining the affection of the 39 percent of Americans in the
middle.

The administration hasn't been able to pull it off. From the stimulus to health
care, it has joined itself at the hip to the liberal leadership in Congress. The
White House has failed to veto measures, like the pork-laden omnibus spending
bill, that would have demonstrated independence and fiscal restraint. By force
of circumstances and by design, the president has promoted one policy after
another that increases spending and centralizes power in Washington.

The result is the Obama slide, the most important feature of the current moment.
The number of Americans who trust President Obama to make the right decisions
has fallen by roughly 17 percentage points. Obama's job approval is down to
about 50 percent. All presidents fall from their honeymoon highs, but in the
history of polling, no newly elected American president has fallen this far this
fast.

Anxiety is now pervasive. Trust in government rose when Obama took office. It
has fallen back to historic lows. Fifty-nine percent of Americans now think the
country is headed in the wrong direction.

The public's view of Congress, which ticked upward for a time, has plummeted.
Charlie Cook, who knows as much about Congressional elections as anyone in the
country, wrote recently that Democratic fortunes have ''slipped completely out
of control.'' He and the experts he surveyed believe there is just as much
chance that the Democrats could lose more than 20 House seats in the next
elections as less than 20.

There are also warning signs in the Senate. A recent poll shows Harry Reid, the
majority leader, trailing the Republican Danny Tarkanian, a possible 2010
opponent, by 49 percent to 38 percent. When your majority leader is down to a 38
percent base in his home state, that's not good.

The public has soured on Obama's policy proposals. Voters often have only a
fuzzy sense of what each individual proposal actually does, but more and more
have a growing conviction that if the president is proposing it, it must involve
big spending, big government and a fundamental departure from the traditional
American approach.

Driven by this general anxiety, and by specific concerns, public opposition to
health care reform is now steady and stable. Independents once solidly supported
reform. Now they have swung against it. As the veteran pollster Bill McInturff
has pointed out, public attitudes toward Obamacare exactly match public
attitudes toward Clintoncare when that reform effort collapsed in 1994.

Amazingly, some liberals are now lashing out at Obama because the entire country
doesn't agree with The Huffington Post. Some now argue that the administration
should just ignore the ignorant masses and ram health care through using
reconciliation, the legislative maneuver that would reduce the need for moderate
votes.

This would be suicidal. You can't pass the most important domestic reform in a
generation when the majority of voters think you are on the wrong path. To do so
would be a sign of unmitigated arrogance. If Obama agrees to use reconciliation,
he will permanently affix himself to the liberal wing of his party and
permanently alienate independents. He will be president of 35 percent of the
country -- and good luck getting anything done after that.

The second liberal response has been to attack the budget director, Peter
Orszag. It was a mistake to put cost control at the center of the health reform
sales job, many now argue. The president shouldn't worry about the deficit. Just
pass the spending parts.

But fiscal restraint is now the animating issue for moderate Americans. To take
the looming $9 trillion in debt and balloon it further would be to enrage a
giant part of the electorate.

This is a country that has always been suspicious of centralized government.
This is a country that has just lived through an economic trauma caused by
excessive spending and debt. Most Americans still admire Obama and want him to
succeed. But if he doesn't proceed in a manner consistent with the spirit of the
nation and the times, voters will find a way to stop him.

The president's challenge now is to halt the slide. That doesn't mean giving up
his goals. It means he has to align his proposals to the values of the political
center: fiscal responsibility, individual choice and decentralized authority.

Events have pushed Barack Obama off to the left. Time to rebalance.

URL: http://www.nytimes.com

LOAD-DATE: September 1, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


