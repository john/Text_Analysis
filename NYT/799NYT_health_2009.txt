                   Copyright 2009 The New York Times Company


                             799 of 2021 DOCUMENTS


                               The New York Times

                            October 11, 2009 Sunday
                              Correction Appended
                              Late Edition - Final

Another Fine Mess: Comics Whack Obama

BYLINE: By MARK LEIBOVICH

SECTION: Section WK; Column 0; Week in Review Desk; Pg. 1

LENGTH: 1085 words

DATELINE: WASHINGTON


-- Is President Obama in trouble with his late-night comedy base?

It's likely he hasn't noticed or doesn't care. He is, after all, in the midst of
his oft-invoked ''full plate'' of supposedly ''defining moments'' in his
presidency  --  a ''defining'' decision on Afghanistan, ''defining'' legislative
battle on health care, among other ''defining'' things.

But there is perhaps another more subtle set of ''defining'' episodes playing
out for Mr. Obama in the televised comedy salons that had previously, by and
large, been relatively gentle spaces for him. The bits about him are getting
harsher. They are no longer just gentle gibes about Bo the dog, big ears, bad
bowling and beer summits.

A conspicuous (if not ''defining'') episode occurred Oct. 3 on Saturday Night
Live in a skit set in the Oval Office. The president (played by Fred Armisen)
was defending his record against critics who had accused him of turning the
United States ''into something that resembles the Soviet Union or Nazi
Germany.'' Not so, protested the faux-Bama.

''When you look at my record, it's very clear what I've done so far,'' he said.
''And that is nothing.''

The sketch went on to show Mr. Obama/Armisen running through a checklist of
things he had vowed to do  --  closing Guantanamo Bay prison, overhauling health
care. All were marked, Not Done.

''Looking at this list I am seeing two big accomplishments,'' he said. ''Jack
and Squat.'' And ouch.

Mr. Obama has of course been a puzzle to comedians for some time. They agonized
during the campaign about how his low-key and confident manner did not lend
itself to edgy caricature. The challenge was made greater by the sensitivities
inherent to lampooning a black candidate.

And from the outset, Mr. Obama has been praised as someone who ''gets late
night,'' whose ironic and self-deprecating humor is well-suited to the genre's
sensibilities. He was the first sitting president to appear as a guest of Jay
Leno's and David Letterman's. ''You ignore their influence at your peril,'' said
Dan Pfeiffer, the White House's deputy communications director. ''They are often
leading indicators of where the narrative is headed.''

But recent indicators could be proving ominous. There has been a proliferation
of jokes that feed on  --   or are fed by  --  a resuscitated old narrative
against the president that goes back to last year's campaign when both John
McCain and Hillary Clinton tried to portray Mr. Obama as an All Talk/No Walk
showboat.

Last Tuesday, Jon Stewart advanced the Saturday Night Live ''do nothing'' theme
on ''The Daily Show.'' It began as a standard Stewart video-clip juxtaposition
of Mr. Obama (and surrogates) promising to end the military's ''Don't Ask, Don't
Tell'' policy. It continued with clips from the ensuing months of Defense
Secretary Robert Gates and National Security Adviser Jim Jones saying they had
not yet gotten around to reversing ''Don't Ask, Don't Tell,'' citing Mr. Obama's
''full plate'' of business. (''The president needs a metaphor czar,'' Mr.
Stewart said.)

What followed was Mr. Stewart, exasperated with a man he had supported, throwing
his hands up and essentially imploring the president to, you know, do something.

''All that stuff you've been putting on your plate?'' Mr. Stewart said. ''It's
[expletive] chow time, brother. That's how you get things off your plate.''

After a roar of laughter and applause from the audience, Mr. Stewart reminded
Mr. Obama that ''You are president of the United States.'' It sounded like
something between a liberal call to action or cry for help. As Mr. Stewart grew
more animated and the crowd grew louder, the routine took on the feel of a
televised catharsis.

''There have been some clear shots coming across the bow from the comic left,''
observed Ric Keller, a former Republican congressman from Florida who once wrote
jokes for Jeb Bush, the former governor.

Others have noted another creeping caricature of Mr. Obama as a ditherer. On
Thursday, for instance, Mr. Leno joked that no one should expect the president's
decision on sending more troops to Afghanistan anytime soon. ''Remember, it took
five months to decide on a puppy,'' he said.

Jeff Nussbaum, a Democratic speech and joke writer, disagrees that late-night
comedy is a leading indicator of a cultural zeitgeist. ''To use an economic
term, it is more of a lagging indicator,'' he said, something that responds to
perceptions that are already entrenched. In practical terms, President Obama has
now been in office almost nine months, Mr. Nussbaum said, and ''comedians now
have a greater body of work to go after, for better or worse.''

By and large, the bulk of late-night barbs directed at the president remain
glancing at best. ''The jokes are still largely about things like how the media
lionizes Obama, or what the opposition is saying about him,'' said Bob Lichter,
of George Mason University's Center for Media and Public Affairs, who has been
tracking themes in late-night humor since 1988.

Mr. Lichter said it was too soon to tell whether the Saturday Night Live skit is
a ''harbinger or outlier'' in how comics will treat Mr. Obama. At the very
least, it provided a comic articulation of a potentially devastating message:
''The danger is that Mr. Obama is going to be defined by inaction and not living
up to expectations,'' he said.

Mr. Lichter posits Mr. Obama's Nobel Prize as a kind of test case for how people
are perceiving him, and whether a caricature has taken hold of a man more
celebrated than accomplished. ''It will be telling to see how the comedians
treat this,'' he said.

As if trying to strike pre-emptively against inevitable ridiculers, Mr. Obama
seemed eager to embrace the ''I haven't done anything yet'' conceit in his Rose
Garden remarks Friday morning. ''Let me be clear,'' he said. ''I do not view it
as a recognition of my own accomplishments.'' But it was also striking how so
many people seemed to greet the Nobel news with shock followed by laughter, as
if truth and caricature has achieved a newly seamless blend in the Obama
imprint.

Really, the words rang distinctively comic: ''Did you hear that Barack Obama won
the Nobel Peace Prize?'' It sounded like the set up to a joke -- one of those
jokes where the set-up itself is the punch-line.

''That's pretty amazing, winning the Nobel Peace Prize,'' Jay Leno said, first
out of the box Friday night. ''Ironically, his biggest accomplishment as
president so far ... winning the Nobel Peace Prize.''

URL: http://www.nytimes.com

LOAD-DATE: October 11, 2009

LANGUAGE: ENGLISH

CORRECTION-DATE: October 18, 2009



CORRECTION: An article last week about comedians getting tougher on President
Obama misspelled the given name of one of the comics. He is Jon Stewart, not
John.

GRAPHIC: PHOTOS

PUBLICATION-TYPE: Newspaper


