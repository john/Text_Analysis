                   Copyright 2009 The New York Times Company


                             353 of 2021 DOCUMENTS


                               The New York Times

                           August 19, 2009 Wednesday
                              Late Edition - Final

Inside the Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 700 words


International

IRAN ROCKETS SEIZED

In Iraq, Officials Say

Iraqi and American troops seized a rocket launcher loaded with about a dozen
Iranian-made rockets aimed at an American base in the southern city of Basra,
Iraqi officials said.  PAGE A9

CHINESE LAWYER IS DETAINED

Prosecutors have charged one of China's leading public-interest lawyers, Xu
Zhiyong, with tax evasion, his lawyer said, continuing a government crackdown on
the nation's small band of activist lawyers and scholars that has lasted months.
PAGE A6

QUESTIONS ON MISSING SHIP

The Russian Defense Ministry said the cargo ship that seemed to vanish off of
Portugal last month had been hijacked. But the government's statement left many
other questions unanswered. PAGE A4

National

MORE FAKE LETTERS

On Climate Change

Congressional investigators have uncovered five more letters sent to members of
Congress that falsely claimed to be from charities expressing opposition to
climate change legislation. PAGE A18

COURT'S TILT IS UNCHANGED

Justice Sonia Sotomayor cast her first vote in a Supreme Court death penalty
case, dissenting from a decision that allowed the execution of a death row
inmate to proceed, and giving an indication that the ideological fault line at
the court has not changed.  PAGE A13

New York

STATE TO REQUIRE FLU SHOTS

For Health Care Workers

The  Health Department is requiring tens of thousands of health care workers
across the state to be vaccinated for flu, amid fears that swine flu will return
in the fall. PAGE A20

Business

AS DEMAND SURGES,

G.M. Will Add Jobs

Just a month after emerging from bankruptcy reorganization, General Motors said
it would add shifts and run some plants on overtime so it can increase
production by 60,000 vehicles by year's end in response to the demand created
largely by the cash for clunkers program. PAGE B1

TAKING ISSUE WITH GOOGLE

A growing chorus of authors, academics and other book industry players are
voicing objections to the settlement of a class-action suit that would allow
Google to profit from digital versions of millions of books it has scanned from
libraries.  PAGE B2

ROCK-BOTTOM HOTEL RATES

To attract business, Manhattan hotel operators have slashed room rates by nearly
one-third since last year, to an average of just under $200 a night. Manhattan's
discounted rates are proving a great deal for visitors but are taking a huge
bite out of hotel revenue. PAGE B6

EXAMINING IPHONE REPORTS

The European Commission said that it was examining the safety of Apple iPhones
and iPods, after news reports said that several of the devices had exploded.
PAGE B4

Sports

AN UNFAMILIAR, IF PLEASANT,

Position for the Phillies

No recent team has made late-season surges more famous, fashionable or fun than
the Philadelphia Phillies, who each time zoomed into the playoffs on the
strength of strong Septembers. And yet with a quarter of the season left, they
hold a six-game lead over second-place Florida in the National League East.
PAGE B14

A PHYSICAL U.S. TEAM

Speed, finesse and creativity are the hallmarks of the international hockey
game. But Brian Burke, the general manager of the United States team, promises
that the American squad at the 2010 Vancouver Games will also be a physically
imposing one. PAGE B10

Arts

A NEW TAKE

On Landmark Works

John Adams is the artist in residence at this year's Mostly Mozart festival. At
Alice Tully Hall on Monday he presided, as a conductor, over a program devoted
to three of his seminal chamber works, in vigorous, richly detailed
performances. Review by Allan Kozinn.  PAGE C1

A CONTENTIOUS '70S STEW

Eagerness and zeal can't overcome an uneven script and flawed execution, but
they do occasionally add spark to ''... Another Man's Poison,'' a spirited if
underdeveloped play that touches on racism, the Vietnam War and sex. Review by
Ken Jaworowski.  PAGE C2

Arts, Briefly C2

Dining

THEY SCREAM

Against Ice Cream

Across message boards and playgrounds, soccer fields and day camp exits, parents
have been raging. In a greener, more health-conscious, unsafe world, the ice
cream man has lost some of his mojo. PAGE D1

Op-ed

MAUREEN DOWD PAGE A27

TIMOTHY EGAN PAGE A27

URL: http://www.nytimes.com

LOAD-DATE: August 19, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


