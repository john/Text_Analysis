                   Copyright 2010 The New York Times Company


                             1672 of 2021 DOCUMENTS


                               The New York Times

                            March 17, 2010 Wednesday
                              Late Edition - Final

Democrats Consider New Maneuvers for Health Bill

BYLINE: By DAVID M. HERSZENHORN and ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 18

LENGTH: 1223 words


WASHINGTON -- As lawmakers clashed fiercely over major health care legislation
on the House floor, Democrats struggled Tuesday to defend procedural shortcuts
they might use to win approval for their proposals in the next few days.

House Democrats are so skittish about the piece of legislation that is now the
vehicle for overhauling the health care system -- the bill passed by the Senate
in December -- that they are considering a maneuver that would allow them to
pass it without explicitly voting for it.

Under that approach, House Democrats would approve a package of changes to the
Senate bill in a budget reconciliation bill. The Senate bill would be ''deemed
passed'' if and when the House adopts rules for debate on the reconciliation
bill -- or perhaps when the House passes that reconciliation bill.

The idea is to package the changes and the underlying bill together in a way
that amounts to an amended bill in a single vote. Many House Democrats dislike
some provisions of the Senate bill, including special treatment for a handful of
states, like Medicaid money for Nebraska, and therefore want to avoid a direct
vote on it.

Republicans paraded to the House floor on Tuesday to denounce the maneuver as a
parliamentary trick. Representative Ted Poe, Republican of Texas, said Democrats
were using ''a sneaky snake oil gimmick'' to pass their bill. ''Let's have an
up-or-down vote on this bill and not hide behind some procedural mumbo jumbo,''
Mr. Poe said.

At the White House on Tuesday, the debate over procedural tactics proved
uncomfortable for President Obama's press secretary, Robert Gibbs. He
sidestepped numerous questions about whether Mr. Obama wanted an explicit,
separate vote on the Senate bill and deferred to Speaker Nancy Pelosi of
California.

''The final decision is the speaker's,'' Mr. Gibbs said.

Representative Chris Van Hollen, Democrat of Maryland and assistant to the
speaker, said Republicans were trying to deceive the public about the
legislation that Democrats were working on.

''They want to send a signal to the American people that the product that is
going to come out of the House is the Senate bill, but the fact of the matter is
we are amending the Senate bill,'' Mr. Van Hollen said. ''We are going to get
rid of the Nebraska deal. We are going to get rid of other provisions in the
Senate bill that shouldn't have been there.''

The House Democratic leader, Representative Steny H. Hoyer, also defended the
maneuver on Tuesday. ''It is consistent with the rules,'' Mr. Hoyer said. ''It
is consistent with former practice.''

An analysis of the procedural device prepared by Ms. Pelosi's office says,
''Some opponents of reform are objecting to the process in an attempt to kill
the bill.''

But some Democrats who support the bill have expressed reservations about the
maneuver and said House leaders might rethink their plans if the chorus of
criticism continued to grow.

House Democratic leaders said they still expected the full House to vote on
health care by this weekend, even though they are still tinkering with the text
of the legislation and do not have a final cost estimate from the Congressional
Budget Office.

Democrats are trying to hold the cost of the new insurance coverage provisions
in the bill to roughly $950 billion over 10 years, in keeping with a limit
suggested by  the president. Also, under budget reconciliation rules, they must
meet goals for reducing future deficits.

To make the numbers come out right, Democrats said, they are considering bigger
cuts in payments to private Medicare Advantage plans, which cover about
one-fourth of the 45 million Medicare beneficiaries. And they may ask
pharmaceutical companies to pay more to help close a gap in Medicare coverage of
prescription drugs.

Ms. Pelosi said she had ''a massive whip operation'' trying to round up votes
for the bill.

The ferocious floor fight on Tuesday was the opening clash in what is shaping up
to be the decisive week in the yearlong battle over health care legislation.

Democrats are aiming for a vote by the weekend that would approve both the bill
that the Senate adopted on Dec. 24 and a package of changes in an expedited
budget reconciliation bill.

In a fusillade of one-minute speeches, the two sides argued over both substance
and procedural issues.

Republicans on Tuesday accused Democrats of resorting to legislative sleight of
hand by planning to approve the Senate-passed health care bill tucked into the
rule that sets the parameters of debate rather than holding a stand-alone vote
on the Senate measure and a separate vote on a package of revisions to be
included in an expedited budget bill.

The Democrats, in turn, said the Republicans were arguing about arcane rules and
procedures because they could not make a cogent, substantive case against the
legislation.

Democrats said the procedures they wanted to focus on were insurance industry
practices that the legislation would ban, including denying coverage based on
pre-existing medical conditions, imposing annual and lifetime limits on
benefits, and revoking coverage on technical grounds even when premiums have
been paid in full.

''Either you want to stand up for the American people, or you want to stand up
for the insurance companies,'' said Representative Ben Ray Lujan, Democrat of
New Mexico.

Democratic leaders said they were confident that they could muster the 216 votes
needed to pass the legislation, though they stopped short of claiming to have
firm commitments in hand.

''I believe that we have the votes, and I am confident about our victory,'' said
Representative John B. Larson of Connecticut, the Democratic conference
chairman.

If Mr. Larson is right, however the House approves the Senate bill -- either
deeming it approved as part of the rules of debate, or with a stand-alone roll
call vote -- President Obama would be able to declare victory on his top
domestic priority.

House Republicans said Tuesday that they were intent on never letting that
happen.

''The American people are appalled by what they have seen in this health care
debate, but the worst is still ahead,'' the Republican leader, Representative
John A. Boehner of Ohio, thundered in his speech.

Mr. Boehner added: ''The majority plans to force the toxic Senate bill through
the House under some controversial trick. There is no way to hide from this
vote. It will be the biggest vote most members ever cast.''

His comments made clear that Republicans would hold Democrats responsible for
the health care legislation regardless of the procedures used to advance it.

The campaign committee for House Republicans, the National Republican
Congressional Committee, unveiled a television advertisement on Tuesday that
warns wavering Democrats they will be held accountable if they vote for the
health care bill.

The ad describes the legislation as ''a corrupt bill'' filled with pork-barrel
spending and special deals for Florida, Louisiana and Nebraska.

Republicans on Tuesday also attacked a plan by Democrats to overhaul federal
student loan programs that will be included in the budget reconciliation bill
along with the health care revisions.

Democrats say the existing program is a giveaway to private banks, which are
paid fees to make risk-free loans using taxpayer money.

URL: http://www.nytimes.com

LOAD-DATE: March 17, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: Speaker Nancy Pelosi said Tuesday that she had put ''a massive
whip operation'' in place to round up votes for the health care bill.
(PHOTOGRAPH BY STEPHEN CROWLEY/THE NEW YORK TIMES)
 Representative Louie Gohmert, Republican of Texas, at a rally Tuesday in
Washington opposing health care overhaul. (PHOTOGRAPH BY CHIP SOMODEVILLA/GETTY
IMAGES)

PUBLICATION-TYPE: Newspaper


