                   Copyright 2009 The New York Times Company


                             653 of 2021 DOCUMENTS


                               The New York Times

                           September 21, 2009 Monday
                              Late Edition - Final

A Proposed Tax on the Cadillac Health Insurance Plans May Also Hit the Chevys

BYLINE: By REED ABELSON; Geraldine Fabrikant contributed reporting.

SECTION: Section A; Column 0; National Desk; Pg. 17

LENGTH: 1351 words


Although cast as a tax on gold-plated insurance policies for the well-heeled, it
has prompted anxiety among the  middle class.

The idea, proposed last Wednesday by Senator Max Baucus, is to help raise money
for the nation's health care overhaul by placing a new excise tax on the most
expensive health insurance policies, like the ones offered to partners at
Goldman Sachs and other affluent professionals.

The tax is meant to  raise more than a quarter of the $774 billion needed to pay
for the Baucus plan. But just as much, the tax is intended to discourage the
overly generous coverage that many experts say has helped propel the country's
reckless spending on medical care.

As it turns out, though, many smaller fish would get caught in Mr. Baucus's tax
net. The supposedly Cadillac insurance policies include ones that cover many of
the nation's firefighters and coal miners, older employees at small businesses
-- a whole gamut that runs from union shops to Main Street entrepreneurs.

Under the Baucus plan, insurers selling a plan costing more than $8,000 for an
individual and $21,000 for a family would have to pay a 35 percent excise tax on
the excess amount.

Although the national average premium is currently  $13,375 for a family policy,
according to the Kaiser Family Foundation, many are much higher than that --
particularly in high-cost parts of the country.

Nationwide, about one in 10 family insurance plans would be subject to the new
excise tax, according to the Center on Budget and Policy Priorities, a
liberal-leaning policy and research group.

The tax would be levied on insurers  -- or on employers that act as their own
insurers. Either way, the tax  would very likely be passed along to workers in
even higher premiums than they pay now. But  if insurance premiums continue to
rise faster than inflation, as they have for years, many more people's policies
could end up setting off the luxury tax in coming years.

''It puts a bigger tax on middle-income Americans who are already paying
enough,'' said Harold A. Schaitberger, the general president of the
International Association of Fire Fighters. The union says some of its members
around the country are in plans that would be subject to the tax.

People who live in high-cost areas, like the Northeast or California, would also
have a greater risk that their insurance plans would set off the excise tax  --
not because the coverage is particularly generous, but because the price of
their policies reflects the higher medical costs  where they live.
Massachusetts residents, for example, tend to pay  more than a quarter more in
premiums, on average, than people living in Idaho.

Bruce Hodson is a state employee in Maine and the president of Local 1989 of the
Service Employees International Union, where the cost of a family plan is now
running at about $20,500 a year. At the typical pace of health premium
inflation, his policy could very likely set off the tax if it goes into effect
in 2013, as the Baucus plan proposes.

The plan has a $400 annual deductible per family, and Mr. Hodson has a
co-payment whenever he goes to the doctor.

''We really worked hard to keep the cost down,'' Mr. Hodson said of the union
plan. And that coverage came at the expense of higher salaries, he said. ''We've
given up pay raises for this.''

The tax  proposal has also drawn fire from some of Mr. Baucus's Democratic
colleagues, including John D. Rockefeller IV, who been an outspoken critic of
the overall Baucus plan. Mr. Rockefeller says he is particularly worried about
the effect of the tax on coverage for workers like coal miners, including those
in his home state of West Virginia, and firefighters.

''I am concerned that the current excise tax proposal could prevent workers in
high-risk professions from getting the health benefits they need,'' he said.

Mr. Baucus acknowledges  some of the concerns  and plans to work with the other
members of the Finance Committee to address them, said Erin Shields, a
spokeswoman for the senator. The proposal, she said, already sets the thresholds
somewhat higher in those states where coverage is the most expensive.

But Ms. Shields defined the tax as vital to discouraging insurance coverage that
is overly  generous. ''High-cost health plans are a major contributor to
skyrocketing health care costs,'' she said. Employers, she said,  would still be
able to find meaningful coverage below the threshold.

On Sunday, President Obama said he saw the need to protect union members, but he
also defended the tax. ''I do think that giving a disincentive to insurance
companies to offer Cadillac plans that don't make people healthier is part of
the way that we're going to bring down health care costs for everybody over the
long term.''

Proponents say the tax is squarely aimed at the very richest plans -- like the
family coverage offered to the 400 or so managing directors at Goldman Sachs and
its top executive officers. It carries a premium of around $40,000 a year. The
Goldman plan would be subject to nearly $7,000 in taxes. Goldman Sachs declined
to comment.

Even if most employees around the nation might initially escape, experts say
more people's insurance plans  would cross into the taxable range in future
years. If the inflation rate in premiums continues its pace of   the last 10
years, even the average cost of family coverage would probably cross the
threshold within a few years of the tax's going into effect.

''That number is going to grow and grow and grow,'' said Marissa Milton, a
health care policy analyst for the HR Policy Association, which represents large
employers' benefit managers.

Ms. Milton drew a comparison to the alternative minimum tax -- a  tax modified
in 1986,  supposedly as a way to keep affluent people from using deductions to
avoid paying taxes. In reality, it has ended up capturing more and more
middle-class taxpayers, Ms. Milton said. Likewise, the insurance excise tax ''is
going to affect a lot of families,'' she said.

By his own calculation, Robert G. Hansen, a business professor at Dartmouth, is
one of the Americans who might be considered to have luxurious coverage, because
the cost of his benefits could easily top $25,000.

But that supposedly ''gold-plated coverage,'' he said, includes the cost of
related benefits, like dental coverage and the money in his flexible spending
account -- all of which would be subject to the excise tax.

And who will pay the $1,450 in additional taxes Mr. Hansen would incur? ''In the
end, look in the mirror,'' he said.

''It's the worst kind of economic engineering,'' he said. ''You end up with
something that looks like the I.R.S. code.''

Small employers would also probably be hit by the taxes -- and, again, not
because they offer overly generous coverage. Instead, small businesses tend to
pay more for their insurance than bigger employers that can negotiate better
premiums. And because they do not have large pools of workers to help spread the
risk, small employers tend to pay even  higher amounts if they have older or
sicker workers.

About 14 percent of small employers, counted as those with fewer than 500
workers, now offer policies that would be subject to the excise tax, said Beth
Umland, director of research for Mercer, a consulting firm that conducts an
annual survey of employee benefits. That compares with just 5 percent of large
employers with 500 or more workers.

''That is a very heavy hammer on the cost of your premiums,'' said Donna
Marshall, the executive director of theColorado Business Group on Health, which
represents employers of all sizes in that state. ''You don't want to cause a
chilling effect on the employers who are trying to do the right thing.''

Even union plans, which tend to offer the most generous coverage, are expensive
because  they frequently cover a number of older workers or some who retired
early and have not yet reached Medicare age. Jim Hoffa, president of the
Teamsters union, warned that the middle class, not the rich, would bear the
brunt.  ''It taxes the wrong people.''

URL: http://www.nytimes.com

LOAD-DATE: September 21, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Senator Max Baucus has proposed a tax on expensive health plans,
but as written, the tax could affect more than the rich.(PHOTOGRAPH BY ALEX
WONG/GETTY IMAGES) CHART: When Average Is a Luxury: Senator Max Baucus has
proposed a 35 percent excise tax on insurance policies that cost more than
$21,000 a year for a family. The tax would go into effect in 2013, and the
threshold would rise with the general inflation rate. But if insurance costs
continue to rise at the faster pace of premium inflation, even the average-price
family policy would eventually trigger the excise tax.(Source: The Henry J.
Kaiser Family Foundation)

PUBLICATION-TYPE: Newspaper


