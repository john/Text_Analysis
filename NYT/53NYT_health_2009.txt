                   Copyright 2009 The New York Times Company


                              53 of 2021 DOCUMENTS


                               The New York Times

                              June 19, 2009 Friday
                              Late Edition - Final

Inside the Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 694 words


International

20 YEARS AFTER WALL FELL, GERMANY'S EAST IS LAGGING

This November will mark 20 years since the Berlin Wall fell. Germany has planned
a series of commemorations celebrating the revival of what was East Germany. But
beyond big cities, little has changed. PAGE A6

MURDER CASES RIVET FRANCE

The cases, although separate, have both captivated France, and both defendants
were sentenced to relatively lenient prison terms. In one, a mother murdered
three of her babies. In the second, a businessman was shot dead by his lover.
PAGE A6

13 KILLED AT TALIBAN CAMP

The camp of a local Taliban leader in South Waziristan was hit by four missiles,
most likely fired from an American drone, a Pakistani intelligence official
said. PAGE A8

BROWN REVERSES ON PANEL

British Prime Minister Gordon Brown reversed course and said a panel formed to
examine Britain's role in the Iraq war could hold some public hearings and take
some testimony under oath. PAGE A8

ROYAL TASTE IN LONDON

A British architect has accused Prince Charles, who has a well-known dislike of
modern architecture, of abusing his power by using his influence to scuttle a
multimillion dollar project. PAGE A14

China Persists With Censorship A10

Clinton to Have Arm Surgery A10

Peru Repeals Divisive Decrees A13

Rights Groups Praise Russia A13

National

HEALTH BILLS HAVE SUPPORT, BUT DETAILS STIR RESISTANCE

Politics and economics have converged to spur momentum on health care
legislation. And yet, the riskiest phase is just starting, when real details
bring real opposition. PAGE A17

WOLVERINE IN COLORADO

Researchers tracked a wolverine into Colorado, 90 years after they were last
known to live in the state. PAGE A19

Iranians in California Feel Bond A15

Bush on Guantanamo Closing A19

Swine Flu in Summer Camps A19

New York

TAXI-SHARING'S PRECURSOR ON THE UPPER EAST SIDE

There is a little-known antecedent to the city's share-a-cab program, which
begins this fall: a taxi stand where it is four to a car, and $6 apiece for a
quick, squashed trip to Wall Street. PAGE A20

JOBLESS RATE CLIMBS TO 9%

New York City's unemployment rate jumped to 9 percent in May, its highest level
in more than a decade, according to data from the state's Labor Department. PAGE
A21

'Rye' Dispute Recalls City's Past A20

Obituaries

DUSTY RHODES, 82

His pinch-hitting heroics led the New York Giants to win the 1954 World Series,
a championship the franchise -- now in San Francisco -- has yet to repeat. PAGE
A25

Business

SMALL BUSINESSES ARE HIT BY CRACKDOWN ON CREDIT

As credit card companies cut limits and avoid risk, the nation's small
businesses are being squeezed, exacerbating the problems brought on by a
stagnant economy. PAGE B1

IN AIRLINE'S STRIFE, A LESSON

With soaring fuel prices and too-rapid expansion, the financial difficulties of
Kingfisher Airlines of India offer a cautionary tale for investors and suppliers
eager to do business in the country. PAGE B1

READER'S DIGEST SEEKS NICHE

After years of trying to expand the appeal of Reader's Digest, the publishers
are aiming more to the right, adding more material about religion and the
military. PAGE B1

Labor Sees Green in Solar Plants B1

Music Labels Win Internet Case B2

Judge Lets G.M. Dispose of Jets B3

Sports

RAIN, MUD AND UMBRELLAS, BUT NOT MUCH GOLF AT OPEN

Golf fans know that on any given day, the United States Open can be washed out,
as it was on Thursday. But they sloshed away in amazingly even spirits. Sports
of The Times by George Vecsey. PAGE B13

N.F.L. SUSPENDS STALLWORTH

Two days after he pleaded guilty to manslaughter while driving drunk, Cleveland
Browns wide receiver Donte' Stallworth was suspended indefinitely by the N.F.L.
PAGE B13

Egypt Upsets Italy B11

Weekend

IN THE BEGINNING, GOD CREATED YUKS

Harold Ramis's ''Year One'' is a sly conceptual laugh-in laced with jokes. It
stars two men dressed in animal skins -- Jack Black and Michael Cera as
Paleolithic tribesmen -- and neck deep in shtick. A Film Review. PAGE C1

Music's Shifting Role on Stage C1

Free Shakespeare in New York C2

Op-ed

PAUL KRUGMAN PAGE A27

DAVID BROOKS PAGE A27

URL: http://www.nytimes.com

LOAD-DATE: June 19, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


