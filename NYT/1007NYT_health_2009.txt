                   Copyright 2009 The New York Times Company


                             1007 of 2021 DOCUMENTS


                               The New York Times

                            November 16, 2009 Monday
                              Late Edition - Final

Law Seeks to Ban Misuse of Genetic Testing

BYLINE: By STEVEN GREENHOUSE

SECTION: Section B; Column 0; Business/Financial Desk; Pg. 5

LENGTH: 1196 words


The most important new antidiscrimination law in two decades -- the Genetic
Information Nondiscrimination Act -- will take effect in the nation's workplaces
next weekend, prohibiting employers from requesting genetic testing or
considering someone's genetic background in hiring, firing or promotions.

The act also prohibits health insurers and group plans from requiring such
testing or using genetic information -- like a family history of heart disease
-- to deny coverage or set premiums or deductibles.

''It doesn't matter who's asking for genetic information, if it's the employer
or the insurer, the point is you can't ask for it,'' said John C. Stivarius Jr.,
a trial lawyer based in Atlanta who advises businesses about the new law.

The biggest change resulting from the law is that it will -- except in a few
circumstances -- prohibit employers and health insurers from asking employees to
give their family medical histories. The law also bans group health plans from
the common practice of rewarding workers, often with lower premiums or one-time
payments, if they give their family medical histories when completing health
risk questionnaires.

''Genetic information is very broad,'' said J. D. Piro, a principal in the
Health Care Law Group at Hewitt Associates. ''It doesn't simply include my own
genetic information, such as do I have a risk for cancer. It also includes my
family medical history -- do I have any relatives who have had cancer or
leukemia.''

Genetic tests help determine whether someone is at risk of developing an
inherited disease or medical condition. These tests identify variations in
genes, like whether a woman has a predisposition for ovarian cancer.

Such testing can help determine which course of treatment might work best for
fighting a specific cancer or for helping a patient process a specific drug.

The new law  (called GINA) was passed by Congress last year because many
Americans feared that if they had a genetic test, their employers or health
insurers would discriminate against them, perhaps by firing them or denying
coverage. In a nationwide survey, 63 percent of respondents said they would not
have genetic testing if employers could see the results.

''The message to employees is they should now be able to get whatever genetic
counseling or testing they need and be less fearful about doing so,'' said Peggy
R. Mastroianni, associate legal counsel for the Equal Employment Opportunity
Commission.

The act takes effect Nov. 21 for all  employers with 15 or more employees. It
applies to group health insurers whose plan years begin on or after Dec.  7, and
it took effect for individual health insurance plans last May. The act does not
apply to life insurers.

The act would ban a company from not promoting a 49-year-old to chief executive
because it knew his father and grandfather died of heart attacks at age 50.

''There's an absolute ban on the use of genetic information to make any kind of
decision about employment,'' said Christopher Kuczynski, assistant legal counsel
with the commission.

Sharon F. Terry, chairwoman of the Coalition for Genetic Fairness, a group that
backed the legislation, told of a woman who had informed her office she was
having a genetic test to learn whether she was predisposed to breast cancer. She
was soon fired, with her boss saying the company could not afford to keep her if
the results were positive.

One episode that created momentum for the legislation involved the Burlington
Northern and Santa Fe Railway Company, which conducted genetic testing on
several employees without their permission.

David Escher, a track maintenance worker in Nebraska who developed carpal tunnel
syndrome on both wrists, said he was mystified why Burlington Northern ordered
him to see a doctor who drew seven vials of his blood. Mr. Escher later learned
that the railroad wanted to do genetic testing to determine whether he had a
predisposition for carpal tunnel -- in an effort to reduce its medical and
workers' compensation costs.

''I was really mad,'' Mr. Escher said. ''They were trying to do this through the
back door. With genetic testing, they can learn lots of things about you that
you don't even know. If they can do genetic testing and find some problems, you
might end up getting fired.''

Burlington Northern reached a $2.2 million settlement in 2002 that was
distributed to 36 workers who were either tested or asked to submit to blood
tests.

While the act makes it illegal for employers to intentionally acquire genetic
information, it includes a ''water cooler'' exception, as in a case where a
manager overhears one employee telling another that his father had a stroke.

Under the act, it is legal for a manager to garner genetic information from an
obituary, for instance that an employee's mother died of breast cancer. And if a
manager asks why a worker took off a week to care for his father under the
Family Medical Leave Act, it generally will not be considered illegal if the
employer learns that the worker's father has pancreatic cancer.

The act, however, prohibits use of such inadvertent knowledge to alter the
terms, conditions or privileges of  employment.

''The challenge becomes what if down the road, the employee has a lot of
absences or his performance is off, and you discipline the employee,'' said
Michael P. Aitken, director of governmental affairs for the Society of Human
Resource Management. ''The employee could come back and say, 'That's because you
knew I had a genetic marker.' ''

The act and its accompanying regulations allow group health plans to request
family medical histories to help determine whether an employee should be placed
in a disease management or wellness program to combat, for instance, high blood
pressure. The rules say employees must give that information voluntarily and the
group plan cannot request such information before health plan enrollment or use
it in any way for underwriting.

Under the rules, group health plans, in seeking information for wellness
programs, cannot attach a request for family medical history to any penalty or,
as is far more common, any benefit. But wellness programs can request family
medical history if there is no financial benefit attached.

''This can be a big deal,'' said Mr. Stivarius, the Atlanta lawyer. ''A lot of
people incentivize employees to provide this family medical information. They
give them some extra paid time off if they participate in surveys. Now they
can't do that.''

Mr. Piro of Hewitt said many employers were only now realizing that their health
risk questionnaires might violate the law.

''Employers have to scramble to scrub this information out,'' he said. ''The
alternative is to modify their reward structures so that they're not considered
to be purchasing or requiring the genetic information.''

Susan Pisano, a spokeswoman for America's Health Insurance Plans, said the rules
were a challenge for insurers because they take effect during the open
enrollment period. She said her group disagreed with the federal agencies'
interpretation that the law bans incentives to encourage employees to fill out
family medical histories.

URL: http://www.nytimes.com

LOAD-DATE: November 16, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: David Escher was ordered to take a blood test by his former
company, to determine a predisposition to carpal tunnel syndrome. (PHOTOGRAPH BY
MARILYN NEWTON FOR THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


