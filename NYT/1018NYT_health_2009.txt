                   Copyright 2009 The New York Times Company


                             1018 of 2021 DOCUMENTS


                               The New York Times

                           November 17, 2009 Tuesday
                              Late Edition - Final

Criticism of U.S. Attorney Nominee Arises Late in Process

BYLINE: By JULIA PRESTON

SECTION: Section A; Column 0; National Desk; Pg. 20

LENGTH: 687 words


Eleventh-hour criticism is arising over President Obama's nomination for United
States attorney in northern Iowa of a prosecutor who had a leading role in the
criminal cases against hundreds of illegal immigrants arrested in a May 2008
raid at a meatpacking plant in Postville, Iowa.

Those cases, the broadest use to date of tough criminal charges against
immigrants caught working without authorization, were emblems of a crackdown on
illegal immigration by the Bush administration.

In supporting the prosecutor, Stephanie Rose, Mr. Obama is following the
recommendation of Senator Tom Harkin, the Democrat from Iowa who is an important
ally -- especially in the health care debate because he is chairman of the
Health, Education, Labor and Pensions Committee.

Ms. Rose, a senior assistant United States attorney in the office she has been
chosen to run, has also garnered support from criminal defense lawyers in Iowa,
including at least 11 lawyers who defended immigrants from Postville. In those
proceedings, ''she exhibited a level of competence and ability that would be
hard to overstate,'' the lawyers wrote in a letter in April.

But some defense and immigration lawyers have said that felony identity-theft
charges against the immigrants were excessively harsh, that immigration lawyers
were not given adequate access to their clients, and that improper contact took
place between prosecutors and one judge. They contend that possible civil rights
and ethical violations by prosecutors should have been investigated.

''Does she stand by those tactics?'' asked David Leopold, the president-elect of
the American Immigration Lawyers Association, the national immigration bar.
''Would she engage again in this type of prosecution of scores of undocumented
workers guilty of nothing more than civil immigration violations?''

The immigration lawyers' association has not taken an official position on the
nomination.

In May, the Supreme Court ruled unanimously that the identity-theft law could
not be applied to prosecute immigrants only because they used false Social
Security or visa numbers, as it was in many Postville cases.

Ms. Rose's nomination was unanimously approved by the Judiciary Committee on
Nov. 5 and is awaiting a vote by the full Senate.

Ms. Rose declined through a spokesman to comment at this point in the nomination
process.

Katherine Bedingfield, a spokeswoman for the White House, said: ''As U.S.
attorney, Stephanie Rose will be a great advocate for the people of Iowa. The
president strongly supports her nomination.''

During 12 years in the northern district, Ms. Rose was the lead prosecutor in
more than 200 criminal cases and argued 34 appeals, according to a fact sheet
provided by Mr. Harkin.

After the raid at the Agriprocessors kosher meatpacking plant in Postville, 270
immigrants entered guilty pleas and were sentenced in four days of fast-track
hearings, in temporary courtrooms in a cattle fairground. According to lawyers
who participated, Ms. Rose distributed prepackaged plea agreements and was the
principal case manager for the prosecutors.

In an interview, Mr. Harkin vigorously defended Ms. Rose, saying she is
''extremely bright and well versed with the law, has a lot of self assurance and
a good demeanor for a U.S. attorney.''

In the Postville cases, Mr. Harkin said, officials in Washington made the
strategic decisions about what charges to bring and what pleas to offer.
''Within the powers she had, she bent over backwards to make sure justice was
done,'' he said.

But at a hearing before the House Judiciary immigration subcommittee in July
2008, Deborah J. Rhodes, then senior associate deputy attorney general,
testified that ''all of the charging decisions were made by career prosecutors
in the local office.''

James Benzoni, an immigration lawyer in Des Moines whose office has secured
visas for two dozen Postville immigrants as victims of exploitation, said,
''There was a general failure of due process and common decency.''

''You can't go forward, you have to clean it up, and she's not going to do
that,'' Mr. Benzoni said.

URL: http://www.nytimes.com

LOAD-DATE: November 17, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Stephanie Rose's nomination for United States attorney in
northern Iowa awaits a full vote in the Senate.(PHOTOGRAPH BY LIZ MARTI/THE
GAZETTE)

PUBLICATION-TYPE: Newspaper


