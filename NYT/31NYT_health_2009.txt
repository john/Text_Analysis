                   Copyright 2009 The New York Times Company


                              31 of 2021 DOCUMENTS


                               The New York Times

                             June 11, 2009 Thursday
                              Late Edition - Final

Doctors' Group Opposes Public Health Insurance Plan

BYLINE: By ROBERT PEAR; Ron Nixon contributed reporting.

SECTION: Section A; Column 0; National Desk; Pg. 19

LENGTH: 1018 words

DATELINE: WASHINGTON


As the health care debate heats up, the American Medical Association is letting
Congress know that it will oppose creation of a government-sponsored insurance
plan, which President Obama and many other Democrats see as an essential element
of legislation to remake the health care system.

The opposition, which comes as Mr. Obama prepares to address the powerful
doctors' group on Monday in Chicago, could be a major hurdle for advocates of a
public insurance plan. The A.M.A., with about 250,000 members, is America's
largest physician organization.

While committed to the goal of affordable health insurance for all, the
association had  said in a general statement of principles that health services
should be ''provided through private markets, as they are currently.'' It is now
reacting, for the first time, to specific legislative proposals being drafted by
Congress.

In the presidential campaign last year and in a letter to Congress last week,
Mr. Obama called for a new ''public health insurance option,'' which he said
would compete with private insurers and keep them honest.

Speaker Nancy Pelosi of California said Wednesday that she supported that goal.
''A bill will not come out of the House without a public option,'' she said
Wednesday  on MSNBC.

But in comments submitted to the Senate Finance Committee, the American Medical
Association said: ''The A.M.A. does not believe that creating a public health
insurance option for non-disabled individuals under age 65 is the best way to
expand health insurance coverage and lower costs. The introduction of a new
public plan threatens to restrict patient choice by driving out private
insurers, which currently provide coverage for nearly 70 percent of Americans.''

If private insurers are pushed out of the market, the group said, ''the
corresponding surge in public plan participation would likely lead to an
explosion of costs that would need to be absorbed by taxpayers.''

While not the political behemoth it once was, the association probably has more
influence than any other group in the health care industry. Lawmakers seek its
opinion and support whenever possible. It has repeatedly persuaded Congress to
cancel or postpone cuts in Medicare payments to doctors, though it has not
secured a ''permanent fix.''

If the doctors are too aggressive in fighting the public plan, they risk
alienating Democrats whose support they need for legislation to increase their
Medicare fees.

The group has historically had a strong lobbying operation, supplemented by
generous campaign donations. Since the 2000 election cycle, its political action
committee has contributed $9.8 million to Congressional candidates, according to
data from the Federal Election Commission and the Center for Responsive
Politics. Republicans got more than Democrats in the four election cycles before
2008, when 56 percent went to Democrats.

Robert Gibbs, the White House press secretary, said that in his address to the
group next week, Mr. Obama would ''outline the case for health care reform and
make clear why we can't afford to wait another year, or another administration,
to bring down costs that are crushing families, businesses and government.''

Mr. Gibbs did not say whether Mr. Obama would discuss a public insurance plan,
the most contentious issue in the debate.

The A.M.A., an umbrella group for 180 medical societies, does not speak for all
doctors. One group, Physicians for a National Health Program, supports a
single-payer system of insurance, in which a single public agency would pay for
health services, but most care would still be delivered by private doctors and
hospitals. In recent years, some doctors have become so fed up with the
administrative hassles of private insurance that they are looking for
alternatives.

Until now, stakeholders in the health care industry have generally muted their
criticism of Democratic proposals. But as details of the legislation have
emerged, the criticism has become more pointed.

America's Health Insurance Plans, a lobby for insurers, said Tuesday that the
government plan proposed by some Senate Democrats could ''dismantle
employer-based coverage and significantly increase costs for those who remain in
private coverage.''

Under a proposal favored by many Democrats, doctors who take Medicare patients
would also have to participate in the new public plan. Democrats say that
requirement is needed to make sure the public plan can go into business right
away with a large network of doctors.

The medical association said it ''cannot support any plan design that mandates
physician participation.'' For one thing, it said, ''many physicians and
providers may not have the capability to accept the influx of new patients that
could result from such a mandate.''

''In addition,'' the A.M.A. said, ''federal programs traditionally have never
required physician or other provider participation, but rather such
participation has been on a voluntary basis.''

In an interview, Dr. Nancy H. Nielsen, president of the American Medical
Association, said she was delighted by Mr. Obama's plan to address the doctors.

''Health care reform is as important to us as it is to him,'' Dr. Nielsen said.
''We will be engaged in discussions in a constructive way. But we absolutely
oppose government control of health care decisions or mandatory physician
participation in any insurance plan.''

Mr. Obama's trip recalls a speech to the A.M.A. in Chicago on June 13, 1993, by
Hillary Rodham Clinton. She proposed ''a new bargain'' in which the White House
would limit malpractice lawsuits and free doctors from onerous rules if doctors
supported her effort to overhaul the health care system.

The association agrees with Mr. Obama on some points. It says that individuals
and families who can afford coverage should be required to obtain it.

Like Mr. Obama, the association wants Congress to cut payments to private
Medicare Advantage plans. The White House says Medicare pays the private plans
14 percent more than it would cost the government to care for the same people in
traditional Medicare.

URL: http://www.nytimes.com

LOAD-DATE: June 11, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Senators Christopher J. Dodd, left, and Tom Harkin met reporters
Wednesday on Capitol Hill. (PHOTOGRAPH BY DOUG MILLS/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


