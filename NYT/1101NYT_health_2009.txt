                   Copyright 2009 The New York Times Company


                             1101 of 2021 DOCUMENTS


                               The New York Times

                            November 30, 2009 Monday
                              Late Edition - Final

Rough Race to Finish For Senate Democrats

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; PRESCRIPTIONS MAKING SENSE OF THE
HEALTH CARE DEBATE; Pg. 16

LENGTH: 992 words

DATELINE: WASHINGTON


Even if lawmakers do not take a day off until Christmas, the Senate majority
leader, Harry Reid of Nevada, has just 25 days to meet his own goal of finishing
the Senate version of the bill by the holiday. And then he must iron out
differences with the House bill.

Those differences, including some major disagreements over how to pay for the
legislation, will almost certainly have to be resolved after New Year's.

But just getting the Senate bill done before the holiday break will not be easy,
especially with fiscal matters and war issues crowding the agenda.

Veteran policy makers disagree about the importance of Mr. Reid's deadline. Some
White House officials, mindful of a customary January lull on Capitol Hill, are
increasingly worried about letting the health bill slip too far into a midterm
election year.

And aside from the politics, all the cost projections for the health care bill
will have to be updated by the spring, to reflect new budget figures.

Pushing and Pulling

All of this means that Mr. Reid, whose amateur sport was boxing not running,
will be looking to sprint once the Senate reconvenes at 2 p.m. Monday. And the
Senate is hardly known for its fast-twitch muscles.

But the White House said lawmakers had already made historic progress. ''We're
closer than ever before to passing meaningful health insurance reform,'' said
Reid Cherlin, a spokesman. ''We're confident that the Senate and House will
continue their steady progress to work quickly and will produce a bill.''

Mr. Reid has implored each of the 59 other senators in the Democratic caucus to
identify only one top priority amendment because the nonpartisan Congressional
Budget Office has limited capacity to provide cost analysis. Nothing prevents
senators from offering more.

''I've got about 10 separate amendments,'' Senator Bernard Sanders, independent
of Vermont and a supporter of the measure, said on ''This Week with George
Stephanopoulos'' on ABC.

Any amendment lacking the support of 60 senators can be filibustered, although
uncontroversial ones can be passed without extended debate by unanimous consent.

If the challenge for Mr. Reid will be deciding how fast to push, the challenge
for his Republican counterpart, Senator Mitch McConnell of Kentucky, will be
figuring out how hard to hit the brakes. Mr. Reid will be accused of rushing;
Mr. McConnell of obstructing.

Republicans will seek to stop some of the Democrats' proposed Medicare changes,
including cuts to privately run Medicare Advantage plans that provide enhanced
benefits mostly at higher cost.

Republicans are likely to try to eliminate or sharply reduce some of the
Democrats' proposed new taxes, including an increase in the Medicare payroll tax
for high earners.

There will also be amendments on the politically volatile topics of abortion and
immigration.

Most of the Republican amendments will fail because the Democrats have the votes
to set them aside.

And Republicans will face the challenge of explaining why they need to offer so
many amendments when their party leaders have made clear that they simply want
to kill the measure. ''We would like to start over,'' Senator Jon Kyl of
Arizona, the No. 2 Republican, said on ''Fox News Sunday.'' ''There's no way to
fix this bill.''

Democratic senators, led by Charles E. Schumer of New York, want to increase
federal financing of new medical residency slots, with an eye to increasing the
ranks of primary care doctors.

Others, including Senators John D. Rockefeller IV of West Virginia and Bob Casey
of Pennsylvania, are eager to propose amendments intended to protect  health
benefits for children. Some lawmakers want to add benefits for women, others for
rural Americans.

Some fiscal hawks are intent on amendments that will strengthen provisions in
the legislation intended to slow the seemingly uncontrollable spiral in
long-term health care costs, and constrain government spending, particularly on
Medicare.

Senator Evan Bayh, Democrat of Indiana, also appearing on Fox News, said
Congress should be forced to follow through on steps to slow spending. ''I
think,''  Mr. Bayh said, ''we need to have an enforcement mechanism in there, as
best we can, to ensure that future Congresses will have the backbone to put some
of these efficiencies into place.''

The trickiest part of the debate, however, will be over provisions on which
Democrats disagree sharply among themselves, like the proposal for a
government-run health insurance plan, the public option, to compete with private
insurers.

Counting to 60

At some point, Mr. Reid will have to push for a vote to end debate. And to do
that, he will once again need the support of 60 senators -- either the entire
Democratic caucus or some Republicans to make up for any defections.

At least four senators -- three Democrats and one independent -- who voted yes
on sending the measure to the floor for debate have already publicly threatened
to block any effort to get a vote on final passage.

Mr. Reid succeeded in getting the bill to the Senate floor with no clear path to
a final vote to get it off the floor.

In recent days, however, the mood has brightened in the majority leader's
office, as a group of centrist senators has stepped up efforts at a deal on the
public option.

One idea is a trigger mechanism first proposed by Senator Olympia J. Snowe,
Republican of Maine, which would allow a public insurance plan to be created
only in states where the legislation fails to meet a target for increased
insurance coverage. That trigger might be coupled with an option for states to
enact laws to offer a public plan.

Another idea is a national nonprofit insurance plan, which would be a variation
of state-based, private nonprofit health care cooperatives originally proposed
by Senator Kent Conrad, Democrat of North Dakota.

Opening speeches in the Senate floor debate are expected to begin at about 3
p.m. Monday.

URL: http://www.nytimes.com

LOAD-DATE: November 30, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Senator Harry Reid faces a number of obstacles in reaching his
goal of completing a Senate bill before the holiday break.  (PHOTOGRAPH BY LUKE
SHARRETT/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


