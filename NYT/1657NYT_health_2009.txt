                   Copyright 2010 The New York Times Company


                             1657 of 2021 DOCUMENTS


                               The New York Times

                             March 14, 2010 Sunday
                              Late Edition - Final

QUOTATIONS OF THE WEEK

SECTION: Section WK; Column 0; Week in Review Desk; Pg. 2

LENGTH: 67 words


''My question to them is: When is the right time? If not now, when? If not us,
who?'' --President Obama, on those seeking to delay a health care overhaul.

''Weighing the risks against the rewards, I thought this was an opportunity to
make the best of a bad mess.'' --Gerald L. Shargel, the lawyer representing
Robert Joel Halderman, who pleaded guilty to trying to extort $2 million from
David Letterman.

URL: http://www.nytimes.com

LOAD-DATE: March 14, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


