                   Copyright 2009 The New York Times Company


                              96 of 2021 DOCUMENTS


                               The New York Times

                             June 30, 2009 Tuesday
                              Late Edition - Final

Obama Steers Health Debate Out of Capital

BYLINE: By SHERYL GAY STOLBERG

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1129 words

DATELINE: WASHINGTON


With Democrats deeply divided over health legislation, President Obama is trying
to enlist the nation's governors and his own army of grass-roots supporters in a
bid to increase pressure on lawmakers without getting himself mired in the messy
battle playing out on Capitol Hill.

In a meeting last week with five governors -- including Republicans who may be
more sympathetic to health legislation than those on Capitol Hill -- Mr. Obama
privately urged them to serve as his emissaries to Congress. He even coached
them on the language they should use with lawmakers, two of the governors said,
advising them to avoid terms like ''rationing'' and ''managed care,'' which
evoke bitter memories of the Clintons' ill-fated health initiative.

The hourlong session in the Roosevelt Room was part of an intensifying but
potentially risky White House strategy to shift the health care debate away from
Washington and to the states. On Wednesday, Mr. Obama will travel to Virginia to
hold a town-hall-style meeting on health care -- his second in two weeks -- that
will include questions from online communities like Facebook and Twitter.

With members of Congress back in their districts for the Fourth of July recess,
Mr. Obama's political group, Organizing for America, has recruited thousands of
supporters to participate in blood drives, raise money for medical research and
volunteer at community health clinics this week, all with the intent of sending
reminders to lawmakers that the public wants action on health care.

''The main thing,'' David Axelrod, Mr. Obama's senior adviser, said, ''is to
involve as many people as possible and demonstrate in a variety of ways the
level and degree of intensity of support that this has.'' Of Mr. Obama's
supporters, Mr. Axelrod said, ''There's no issue that motivates them more than
health care.''

While this outside-the-Beltway strategy lets Mr. Obama stay out of Democrats'
internal fights -- for now at least -- there are  risks. If Mr. Obama waits too
long to exert his presidential muscle to forge consensus on Capitol Hill, his
moment of opportunity could pass. He could also lose control of the final
outcome if lawmakers cut backroom deals he dislikes, for example, by deciding to
pay for the expansion by taxing employee health benefits,  a move that worries
Mr. Obama's political advisers because it could cause the president to break a
campaign promise.

Some Democrats are privately pushing the president to do more to bring his party
in line. When Rahm Emanuel, the White House chief of staff, went to Capitol Hill
last week, the majority leader, Senator Harry Reid of Nevada, pressed for the
president to intervene more directly to settle Democrats' disputes over Mr.
Obama's call for a government-run insurance plan to compete with the private
sector, two people familiar with the session said.

Mr. Emanuel, in an e-mail message, acknowledged that some Democrats ''wanted
more direct and specific involvement,'' but said others were happy with the
president's level of engagement, adding, ''We received a lot of advice.''

Over the last several weeks, Mr. Obama has steadily increased his contact with
lawmakers on health care, even as he steers clear of specific policy disputes.
He met privately with Senator Ron Wyden, Democrat of Oregon, and telephoned
Democrats on the Senate Finance Committee to check on their progress and urge
them to stick to his timetable for a final bill to reach his desk in October.

John D. Podesta, who ran Mr. Obama's transition to the presidency and consults
closely with the administration on the health bill, predicted that the White
House would resist the urge to ''knock heads and hammer consensus'' at least
until after the Finance Committee produced a bill, sometime after the Fourth of
July  holiday.

But if the panel, widely regarded as Mr. Obama's best hope for a bipartisan
measure, gets stuck or further delayed, Mr. Podesta and other Democrats say, Mr.
Obama will have to step in to broker a deal.

''He's the president of the United States; he does have to lead and he will,''
said Senator Kent Conrad, Democrat of North Dakota. ''But he's got to pick his
spots.''

As lawmakers struggle to work out their differences, Mr. Obama is courting the
governors, an effort that one White House official, speaking anonymously to
discuss strategy, said began with last week's meeting. In interviews, two
governors -- a Democrat, Christine Gregoire of Washington, and a Republican,
Michael Rounds of South Dakota -- both said Mr. Obama asked them to talk to
members of Congress about their own innovative approaches to health reform. Both
said he urged them to be careful about their language.

''I think he said what we have to do is not call it rationing, because clearly
there is from H.M.O. days a concern about rationing,'' Mr. Rounds said, adding
that he sensed Mr. Obama was hoping to have ''a bipartisan group of governors
working directly with lawmakers to perhaps break a stalemate.''

Ms. Gregoire said the president reminded the governors that ''Congress has a bad
taste in its mouth from previous experience with managed care,'' and suggested
they avoid the term.  Instead, he spoke of  ''evidence-based care,'' the
practice of using research to guide medical decisions. She said the president
told them, ''I need you to stick with me.''

Governors are deeply concerned about the rising price of Medicaid, the
government insurance program for the poor, which makes them natural allies of
the president, who has made driving down health costs a centerpiece of his
effort.

But some of the proposals under consideration in Washington would expand
Medicaid,  a prospect that the governors find worrisome. They also fear that any
legislation passed by Congress would undermine their own efforts at health
reform, and said they used last week's meeting to tell Mr. Obama so.

In reaching out to the governors, Mr. Obama is reprising a strategy that worked
for him during the debate over his economic stimulus package, when he found far
more support among Republican governors than among Republican lawmakers.

Whether Mr. Obama can have a more bipartisan  outcome with health care remains
unclear. He has invested so much political capital in a health care bill that
not to have legislation would be politically disastrous for him. If that means
passing a bill without Republican support, some Democrats say, Mr. Obama will do
it.

''His instinct is to try, if he can, to find an honorable compromise with
Republicans,'' Mr. Podesta said. ''But ultimately what he cares about at the end
of the day is sitting there, pen in hand, signing a bill that's a good bill, and
that he believes in. When all is said and done, that's what I think they will
drive toward.''

URL: http://www.nytimes.com

LOAD-DATE: June 30, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: Taking part in meetings on health care were Senator Kent
Conrad, Democrat of North Dakota, at left, and Govs. Christine Gregoire of
Washington and Michael Rounds of South Dakota. (PHOTOGRAPHS BY STEPHEN
CROWLEY/THE NEW YORK TIMES
 POOL PHOTO BY SEAN THEW) (pg.A6)

PUBLICATION-TYPE: Newspaper


