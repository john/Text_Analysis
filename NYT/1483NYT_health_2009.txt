                   Copyright 2010 The New York Times Company


                             1483 of 2021 DOCUMENTS


                               The New York Times

                            February 7, 2010 Sunday
                              Late Edition - Final

Obama Pledges to Press Ahead on Goals

BYLINE: By JEFF ZELENY

SECTION: Section A; Column 0; National Desk; Pg. 23

LENGTH: 656 words

DATELINE: WASHINGTON


President Obama said Saturday that the Democratic Party should not simply
''regroup, lick our wounds and try to hang on'' during a challenging political
season, pledging to find a way for health care and job-creation measures to be
passed this year.

''I know we've gone through a tough year,'' Mr. Obama told party advocates,
''but we've gone through tougher years.''

The Democratic National Committee convened its annual winter meeting here as the
capital was buried by a storm, which had dumped about two feet of snow on the
region. The city was shuttered, but the gathering proceeded as scheduled, with
Mr. Obama traveling two blocks from the White House to the Capital Hilton hotel.

''It is good to be among friends who are so committed to the future of this
party that you braved a blizzard,'' Mr. Obama said. ''Snowmageddon!''

The president's appearance was the latest in a series of party events as he
tries to boost the spirits of Democrats who are bracing for the midterm
elections. He acknowledged the difficult headwinds facing the party, but warned
against moving away from initiatives on energy, education and financial
regulation.

''We can't return to the dereliction of duty that helped deliver this
recession,'' Mr. Obama said. ''America can't afford to wait, and we can't look
backwards.''

Democrats leapt to their feet when Mr. Obama took the stage, and his 20-minute
speech was interrupted several times by applause. But his remarks offered no new
strategies for how the administration intends to revive health care overhaul and
advance the rest of its agenda.

The president said he was not surprised that his approval ratings had fallen
from where they were a year ago, when the election and inauguration were fresh
in the minds of voters.  ''Of course people are frustrated, they have every
right to be,'' he said.

Mr. Obama renewed his call to not abandon health care legislation, which hit a
setback when Democrats lost a special election in Massachusetts last month that
cost the party its supermajority in the Senate.

''Just in case there's any confusion out there, I am not going to walk away from
health care reform,'' Mr. Obama said. ''I'm not going to walk away on this
challenge. I'm not going to walk away on any challenge. We're moving forward.''

The accolades for the president on Saturday belied a nervous mood among
Democratic officials who gathered here for the two-day meeting. Several party
advocates acknowledged being worried about the year ahead for Democrats, but
hoped the defeat in Massachusetts had served as a warning siren.

''There has to be some positive news on the economy and jobs. That's what leads
people to vote,'' said Don Fowler of South Carolina, a former Democratic
national chairman. ''Massachusetts was a viper. We collectively have to do
something to tamp down the anger. We've got to produce.''

Tim Kaine, a former Virginia governor and current chairman of the Democratic
National Committee, said the party needed to do a ''better job telling the
story'' of how the Obama administration saved the country from economic
collapse. He said Democrats should not dwell on losing the Senate seat long held
by Senator Edward M. Kennedy to Scott Brown, a Republican who was sworn in to
office last week.

''The ghost of Harry Truman would kill us if he heard us complaining about 59
seats in the United States Senate,'' Mr. Kaine said. ''But we have to learn some
lessons.''

Raymond Buckley, the New Hampshire party leader who also leads the Association
of State Democratic Chairs, said too many party advocates were complacent last
year as voter anger was growing. He said the Democrats needed to do a better job
of fighting their critics and proclaiming their accomplishments.

''Massachusetts was a giant wakeup call,'' said Mr. Buckley, who added that many
Democrats had not taken the Tea Party very seriously. ''We have to push back.
It's really on our shoulders.''

URL: http://www.nytimes.com

LOAD-DATE: February 7, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Tim Kaine, the leader of the Democratic National Committee, with
the president on Saturday. (PHOTOGRAPH BY SUSAN WALSH/ASSOCIATED PRESS)

PUBLICATION-TYPE: Newspaper


