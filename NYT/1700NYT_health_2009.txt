                   Copyright 2010 The New York Times Company


                             1700 of 2021 DOCUMENTS


                               The New York Times

                            March 20, 2010 Saturday
                              Late Edition - Final

Coverage for Adult Children

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; INSURANCE; Pg. 10

LENGTH: 237 words


At a rally on Friday at George Mason University in Fairfax, Va., President Obama
got some of his loudest applause when he talked about a provision in the House
reconciliation bill that would allow adult children to remain on their parents'
health insurance policies until the age of 27.

The provision cited by the president is one of the more significant changes for
consumers in the Democrats' health care legislation. Currently, states determine
the maximum age until which policies must offer dependent coverage, and the
limits vary.

The Senate-passed health care bill would have required insurers to offer
dependent coverage for adult children -- but only with new insurance policies.

The reconciliation bill, which is scheduled for a House vote on Sunday, would
allow parents to keep children on their insurance plans through age 26 and would
require all insurers to make this coverage available with existing policies.

But the dependent coverage has to be made available only if the adult child has
no option to enroll in group coverage through an employer. And, of course, the
coverage might not be cheap.

The provision, which would take effect six months after adoption of the bill, is
among the most popular aspects of the legislation because of its potential
benefit to Americans who already have health insurance through their employers,
which is to say a majority of the population.

DAVID M. HERSZENHORN

URL: http://www.nytimes.com

LOAD-DATE: March 20, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO (PHOTOGRAPH BY LUKE SHARRETT/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


