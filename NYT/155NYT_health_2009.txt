                   Copyright 2009 The New York Times Company


                             155 of 2021 DOCUMENTS


                               The New York Times

                             July 16, 2009 Thursday
                              Late Edition - Final

Massachusetts Faces Suit Over Cost of Universal Care

BYLINE: By ABBY GOODNOUGH; Katie Zezima contributed reporting

SECTION: Section A; Column 0; National Desk; Pg. 16

LENGTH: 752 words

DATELINE: BOSTON


A hospital that serves thousands of indigent Massachusetts residents sued the
state on Wednesday, charging that its costly universal health care law is
forcing the hospital to cover too much of the expense of caring for the poor.

The hospital, Boston Medical Center, faces a $38 million deficit for the fiscal
year ending in September, its first loss in five years. The suit says the
hospital will lose more than $100 million next year because the state has
lowered Medicaid reimbursement rates and stopped paying Boston Medical
''reasonable costs'' for treating other poor patients.

''We filed this suit more in sorrow than in anger,'' said Elaine Ullian, the
hospital's chief executive. ''We believe in health care reform to the bottom of
our toes, but it was never, ever supposed to be financed on the backs of the
poor, and that's what has happened in Massachusetts.''

The central charge in the suit is that the state has siphoned money away from
Boston Medical to help pay the considerable cost of insuring all but a small
percentage of residents. Three years after the law's passage, Massachusetts has
the country's lowest percentage of uninsured residents: 2.6 percent, compared
with a national average of 15 percent.

Low-income residents, who have benefited most from expanded access to health
care, receive state-subsidized insurance, one of the most expensive aspects of
the state plan. But rapidly rising costs and the battered economy have caused
more problems than the state and supporters of the 2006 law -- including Boston
Medical -- anticipated.

According to the suit, Massachusetts is now reimbursing Boston Medical only 64
cents for every dollar it spends treating the poor. About 10 percent of the
hospital's patients are uninsured -- down from about 20 percent before the law's
passage in 2006. But many more are on Medicaid or Commonwealth Care, the
state-subsidized insurance program for low-income residents.

One of the state's reimbursement rates to Boston Medical,  dropped from $12, 476
in 2008 to $9,323 by 2009, the suit says.

Wendy E. Parmet, a professor at the Northeastern University School of Law, said
the suit was ''a step in a wider minuet''  as state lawmakers, health care
providers and other stakeholders try to figure out how to make the new law work
in the long term.

''I think it's going to be a very hard lawsuit for them to prevail on,''
Professor Parmet said of the hospital. ''I think they're trying to bring another
weapon into what is essentially, in many ways, a political and economic battle
going on in the state about how to pay for health care, and making sure their
voice gets heard.''

The suit comes as Congress looks to Massachusetts as a potential model for
overhauling the nation's health care system. Even before the suit, the state's
fiscal crisis had cast doubts on the law's sustainability.

To help close a growing deficit, the Democratic-controlled Legislature
eliminated  coverage for some 30,000 legal immigrants in the new state budget.
Gov. Deval Patrick, a Democrat, is seeking to restore about half of the $130
million cut, but lawmakers have expressed reluctance, saying that doing so would
require cuts to other important programs.

State officials expressed surprise at the lawsuit, saying that Boston Medical
received $1.5 billion in state funds in the past year and should not be seeking
more in the midst of a fiscal crisis.

''At a time when everyone funded and served by state government is being asked
to do more with less, B.M.C. has been treated no differently,'' said Dr. JudyAnn
Bigby, the state secretary of health and human services, in a prepared
statement. ''We are confident that the administration's actions in this area
comply with all applicable law and will be upheld.''

State officials have suggested that Boston Medical could reduce costs by
operating more efficiently. The state has also pointed out that the hospital has
reserves of about $190 million, but Tom Traylor, the hospital's vice president
of federal and state programs, said the reserves could only sustain the hospital
for about a year.

''The magnitude of the loss here can't be solved on the program-cutting or
expense-cutting side,'' Mr. Traylor said. Professor Parmet said the hospital's
dissatisfaction with the new law should be a warning to Congress that
''insurance alone doesn't solve the problems'' of the health care system. In
fact, she said, it might exacerbate the financial problems of safety-net
hospitals in the short term.

URL: http://www.nytimes.com

LOAD-DATE: July 16, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


