                   Copyright 2009 The New York Times Company


                             920 of 2021 DOCUMENTS


                               The New York Times

                            November 2, 2009 Monday
                              Late Edition - Final

Obama Strategy on Health Care Legislation Appears to Be Paying Off

BYLINE: By ROBERT PEAR and SHERYL GAY STOLBERG

SECTION: Section A; Column 0; National Desk; Pg. 10

LENGTH: 1179 words

DATELINE: WASHINGTON


After months of plodding work by five Congressional committees and weeks of
back-room bargaining by Democratic leaders, President Obama's arms-length
strategy on health care appears to be paying dividends, with the House and the
Senate poised to take up legislation to insure nearly all Americans.

Debate in the House is expected to begin this week, and the Senate will soon
take up its version. Democratic leaders and senior White House officials are
sounding increasingly confident that Mr. Obama will sign legislation overhauling
the nation's health care system -- a goal that has eluded American presidents
for decades.

The Senate Finance Committee chairman, Max Baucus of Montana, described ''a
sense of inevitability, the sense that, yes, we're going to pass health
reform.'' In interviews, senior advisers to the president said the progress on
Capitol Hill vindicated Mr. Obama's strategy of leaving the details up to
lawmakers, though they are wary of sounding overconfident.

''You don't see any shimmying in the end zone,'' said Rahm Emanuel, the White
House chief of staff. ''No spiking the ball on the 20-yard line here.''

The bills have advanced further than many lawmakers expected. Five separate
measures are now pared down to two. But the legislative progress has come at a
price. In the absence of specific guidance from the White House, it has moved
ahead in fits and starts. From here on, the challenges will only grow more
difficult.

In the House, where leaders have vowed to pass a bill by Nov. 11, a fight over
abortion coverage could still imperil the legislation, and Mr. Obama could lose
some votes from liberals upset that the bill includes a weakened ''public
option,'' a government insurance plan to compete with the private sector. Mr.
Obama, trying to keep progressives in line, met with them Thursday night in the
White House Roosevelt Room.

''He is making the case to them that this isn't the exact bill you'd write,
however, let's take a step back and look at what we're about to do here, and
what a historic moment this will be,'' said a senior administration official,
speaking on condition of anonymity to discuss a private meeting.

In the Senate, where Democrats will need support from every member of their
caucus to reach a critical 60-vote threshold to avoid a potential filibuster,
Mr. Obama's hands-off strategy carries particular risks. Without clear direction
from the president on the public option, the Democratic leader, Senator Harry
Reid of Nevada, moved ahead last week on his own, unveiling a bill that includes
a government-run plan, but allows states to opt out.

Within hours, the proposal was being questioned by centrist Democrats whose
concerns Mr. Obama must now address. As Senator Benjamin L. Cardin, Democrat of
Maryland, said, ''When you are seeking 60 votes, every person is a kingmaker.''

Last week's back-and-forth in the Senate was emblematic of a process that has at
times seemed on the brink of anarchy. Lawmakers have missed many deadlines,
including the one Mr. Obama set for all five Congressional committees to wrap up
work by August. (Only four did.) Even close allies of the White House sometimes
questioned its approach.

''It felt like it was getting out of control at the end of July and in the
beginning of August,'' said John D. Podesta, a former chief of staff to
President Bill Clinton who informally advises the Obama White House. ''People
were getting nervous that it was going every which way.'' Mr. Podesta said the
president risked ''giving too much rope to a Congress that is liked a lot less
than he is.''

Mr. Obama said early on that he would not repeat the mistakes of Mr. Clinton,
who wrote his own detailed plan, only to see it fall flat on Capitol Hill.
Instead, the president set out broad principles -- an approach that the House
speaker, Nancy Pelosi of California, acknowledged at a rally last week, when she
thanked Mr. Obama for ''the intellectual contributions'' he had made to the
legislation.

The president's distance caught Congressional Democrats by surprise. It took
them months to realize that Mr. Obama would not weigh in on some issues, like
the precise shape of a government insurance plan. One House Democrat called it a
''a laissez-faire strategy.''

But in an interview in his West Wing office on Friday evening, Mr. Emanuel --
joined by other top members of Mr. Obama's health care team -- disputed that
characterization. He said the White House had given ''leeway to legislators to
legislate,'' but ''not leeway to take a policy off track.''

Mr. Emanuel also said it was no accident that ''the basic bones'' of the House
and Senate legislation were, despite some big differences, quite similar.
''While there was a cry for more presidential direction,'' he said, ''that
direction was being provided by the president's staff.''

Some veterans of the Clinton effort -- including Mr. Podesta; Tom Daschle, the
former South Dakota senator who was Mr. Obama's first choice for health
secretary; and Donna E. Shalala, who was Mr. Clinton's health secretary -- said
it was wise for the president to keep out of the details of legislative work.
And Ms. Shalala said Mr. Obama was deeply involved in another way -- as lead
spokesman for the effort.

''He's been carrying a lot of water,'' she said. ''I would suggest that he
really has put his first term on the line for health reform.''

White House officials approached their work like a political campaign, and they
said they had learned as much from the 2008 presidential race as from the health
care fiasco of 1993-94. They said they learned the importance of pressing on and
keeping up momentum, even when cable television commentators -- and some fellow
Democrats -- declared their initiatives dead.

Congressional Democrats said it often seemed as if the top priority for the
White House was simply to advance health care bills to the next step in the
legislative process.

Indeed, that is exactly what White House officials were trying to do. They
described their legislative strategy as a very step-by-step process, in which
they kept intensely focused on the next specific goal: passing a bill out of
this or that committee, resolving the doubts of particular lawmakers, like the
liberals who met with Mr. Obama on Thursday.

Yet White House officials have shown little interest in Republicans, with the
exception of Senator Olympia J. Snowe of Maine, whom they have wooed
assiduously, and one or two others. Mr. Obama did meet with some Republicans
early on, when his aides still believed it was possible to get the support of
Senator Charles E. Grassley of Iowa, the senior Republican on the Finance
Committee.

The No. 3 Republican in the Senate, Lamar Alexander of Tennessee, who attended
one session with the president, recalled that in the 1960s, when he was a
Congressional aide, Democrats and Republicans worked together on civil rights.
He said he saw no possibility of a bipartisan health bill.

''White House officials don't want one or don't know how to do one,'' Mr.
Alexander said.

URL: http://www.nytimes.com

LOAD-DATE: November 2, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Speaker Nancy Pelosi, second from right, at an event last week
for the House health care bill.(PHOTOGRAPH BY LUKE SHARRETT/THE NEW YORK TIMES)
(pg.A11)

PUBLICATION-TYPE: Newspaper


