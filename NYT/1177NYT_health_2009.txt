                   Copyright 2009 The New York Times Company


                             1177 of 2021 DOCUMENTS


                               The New York Times

                            December 14, 2009 Monday
                              Late Edition - Final

Coverage for Abortions

SECTION: Section A; Column 0; Editorial Desk; LETTER; Pg. 30

LENGTH: 302 words


To the Editor:

I agree with Representative Bart Stupak that there is a lot of misinformation
about abortion coverage in the health reform bill (''What My Amendment Won't
Do,'' Op-Ed, Dec. 9). Unfortunately, I believe that his article only adds to the
confusion.

For example, my amendment continued the prohibition of federal funding of
abortion services, but did so without restricting insurance coverage of this
legal medical procedure when it is paid for with private funds. Reputable third
parties, like a recent study from George Washington University, have found that
the practical effect of the Stupak amendment would be to restrict coverage of
abortion services even when paid for entirely with private funds.

Representative Stupak likens his amendment's burdensome requirement -- that
insurance companies in the health exchange that offer a plan covering abortion
services must offer another plan without abortion services -- to my amendment's
requirement guaranteeing choice in the marketplace by ensuring that the
exchange, rather than individual companies, include at least one plan that
provides abortion services and one that doesn't.

Again, reputable third parties have stated that under the Stupak amendment
insurance companies won't bother to offer plans with abortion services because
it wouldn't make any business sense to offer a plan that would be available only
to a small number of potential customers (the 15 to 20 percent who aren't
eligible for any federal assistance).

Finally, a recent poll conducted by the Mellman Group showed that the majority
of Americans find my amendment to be a reasonable compromise: no federal funds
for abortion and no government interference with privately funded procedures.

Lois Capps Member of Congress 23rd District, Calif.  Washington, Dec. 9, 2009

URL: http://www.nytimes.com

LOAD-DATE: December 14, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


