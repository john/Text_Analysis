                   Copyright 2009 The New York Times Company


                             1281 of 2021 DOCUMENTS


                               The New York Times

                           December 29, 2009 Tuesday
                              Late Edition - Final

Expanding Health Coverage and Shoring Up Medicare: Is It Double-Counting?

BYLINE: By ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 20

LENGTH: 952 words

DATELINE: WASHINGTON


At the heart of the fight over health care legislation is a paradox that
befuddles lawmakers of both parties.

Separate bills passed by the Senate and the House would squeeze nearly a
half-trillion dollars from projected spending on Medicare over the next 10
years. These savings would help offset the cost of providing coverage to people
who are uninsured.

At the same time, federal accountants say the money would shore up the Medicare
trust fund, so the program could continue paying hospitals to treat older
Americans in the future.

In other words, Medicare savings mean both more money available to spend now and
the appearance of more money to spend later on Medicare.

How is this possible?

The Congressional Budget Office tried to answer the question last week. In
effect, it said, the same money cannot be used for both purposes without
double-counting.

''To describe the full amount of hospital insurance trust fund savings as both
improving the government's ability to pay future Medicare benefits and financing
new spending outside of Medicare would essentially double-count a large share of
those savings, and thus overstate the improvement in the government's fiscal
position,'' the budget office said.

But the clarification came too late to affect the outcome of debate over the
legislation, passed Thursday in the Senate by a party-line vote of 60 to 39.

For weeks, Republicans had been saying that Democrats would plunder Medicare,
raid it, use it as a ''piggy bank'' to pay for coverage of the uninsured under a
new entitlement program.

Such fusillades frightened some older voters and prompted defensive maneuvers by
Democrats, who said their bill would ''save lives, save money and save
Medicare,'' while providing additional benefits to older Americans.

Senator Michael Bennet, Democrat of Colorado, offered an amendment that said
nothing in the bill would result in a reduction of ''guaranteed benefits'' under
Medicare. The amendment was approved, 100 to 0, on Dec. 3.

Richard S. Foster, the chief Medicare actuary, agrees with the Congressional
Budget Office. He traces the confusion to different accounting rules used for
the federal budget and for the Medicare trust fund.

The Senate bill would reduce the growth of Medicare spending and increase the
Medicare payroll tax on high-income people. The combination of less spending and
more revenue would lower the deficit, based on budget accounting rules, and
extend the life of the Medicare trust fund.

However, Mr. Foster said, the same money ''cannot be simultaneously used'' to
cover the uninsured and to extend the Medicare trust fund, ''despite the
appearance of this result from the respective accounting conventions.''

Senator Jon Kyl of Arizona, the No. 2 Republican in the Senate, summarized the
situation with a pithy metaphor. ''You can't sell the same pony twice,'' he
said.

After issuing its clarification, the budget office reaffirmed its earlier
estimate that the Senate bill would reduce the deficit by $132 billion in the
next 10 years, compared with the deficits expected under current law.

The issue involves not only technical accounting matters, but also a huge
political issue: the impact of a health care overhaul on Medicare and its
beneficiaries, whose numbers are about to explode -- to 60 million in 2019, from
46 million now.

On a purely technical level, the federal budget deficit -- $1.4 trillion in the
last year -- is the difference between federal receipts and federal spending in
a given year. It measures cash flows into and out of the Treasury. If Congress
cuts Medicare spending, it reduces the deficit, assuming everything else stays
the same.

By contrast, Medicare's hospital insurance trust fund serves, in the words of
the Congressional Budget Office, ''primarily as an accounting mechanism.''
Payroll taxes paid by workers and employers are credited to the trust fund.
Medicare draws on this account to pay hospitals, nursing homes and certain other
health care providers.

Under federal law, the Medicare hospital trust fund exists ''on the books of the
Treasury.'' It may have a positive balance -- enough money to pay expected
claims for a decade or more -- even though the government as a whole runs a
deficit every year and borrows immense sums to pay its bills.

In one sense, money that ''goes into'' the Medicare trust fund cannot be used
for other purposes. But it is part of a unified federal budget, which includes
spending for dozens of other federal programs. So if Congress reduces Medicare
payments to hospitals or private Medicare Advantage plans -- and if everything
else stays the same -- the federal budget deficit will be lower and the balance
in the Medicare trust fund higher than they otherwise would be.

The Congressional Budget Office says the Senate bill would cover 31 million
uninsured people -- about 10 percent of the population -- while the House bill
would cover 36 million. The budget office has not estimated the effects on total
national health spending, but Mr. Foster, the Medicare actuary, has done so.

Mr. Foster estimates that the Senate bill would increase national health
spending by a total of $234 billion, or 0.7 percent, in the decade from 2010
through 2019, while the House bill would increase it by $289 billion, or 0.8
percent.

If the savings in the Senate bill are achieved, Mr. Foster said, they would add
nine years to the life of Medicare's hospital trust fund, so it would be
exhausted in 2026, rather than 2017.

But, Mr. Foster said, some of the estimated savings ''may be unrealistic''
because they assume increases in productivity that can probably not be attained
by hospitals, nursing homes and home health agencies.

URL: http://www.nytimes.com

LOAD-DATE: December 29, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


