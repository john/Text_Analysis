                   Copyright 2009 The New York Times Company


                             1286 of 2021 DOCUMENTS


                               The New York Times

                           December 29, 2009 Tuesday
                              Late Edition - Final

An Underground Campaign

BYLINE: By ELIZABETH OLSON

SECTION: Section B; Column 0; Business/Financial Desk; ADVERTISING; Pg. 6

LENGTH: 983 words

DATELINE: WASHINGTON


LAWMAKERS debating health care reform in recent weeks haven't been reticent
about blaming trial lawyers for driving up the nation's medical costs by
pursuing large malpractice awards.

Trying to fend off any limits  to patient lawsuits, the lawyers decided to press
their arguments in a new  location -- the subway system here. Lawmakers and
their aides arriving on Capitol Hill by Metro, as the subway is known, pass
through a blizzard of brightly colored ads on the platform and the walls and
hanging from the ceiling.

They bear the lawyers' message that nearly 100,000 people die each year from
medical errors, and that tort reform won't fix the health care system.

''We wanted a prominent space to educate key people about the number of people
who are killed annually by medical errors,'' said Anthony Tarricone, president
of the American Association for Justice, formerly known as the Association of
Trial Lawyers of America.

The lawyers, who introduced their subway campaign this month, anticipated that
they would be a target for senators looking to control health care costs, said
Mr. Tarricone, a Boston plaintiffs' lawyer. During the debate, several senators
introduced amendments, including one called ''loser pays''  -- meaning the
person who loses the lawsuit must pay all the legal costs  -- to clamp down on
medical liability lawsuits. None was adopted.

The trial lawyers argue in the ads that patients need legal recourse because
preventable medical errors are the sixth-leading cause of death in America,
killing at least 98,000 people a year. (The ads' tag line is: ''Tell Congress to
Put Patients First. There Are 98,000 Reasons Why You Should.'') The campaign Web
site, 98000reasons.org, calls that number equivalent to two 737s crashing every
day for a year -- and the ads include two small images of planes.

The figure comes from  a 1999 report called ''To Err Is Human,'' from the
federal Institute of Medicine, part of the National Academy of Sciences. This is
the most recent nationwide figure, said Mr. Tarricone, who added that the number
of deaths and injuries could be higher because the ''problem has only gotten
worse.''

Business groups, including the Chamber of Commerce, say litigation creates cost
beyond settlements and awards and the malpractice insurance to cover them. They
say it also encourages doctors to practice ''defensive medicine'' -- practices
like ordering more tests than needed in order to avoid being called negligent.
But the lawsuits are the flashpoint.

''The threat of these 'jackpot justice' suits against doctors is one of the
reasons health insurance premiums are rising faster than the rate of
inflation,'' said Senator Jon Kyl, Republican of Arizona.

Senators introduced a number of amendments in recent weeks to limit awards in
medical malpractice lawsuits and lawyers' fees  -- typically about one-third of
a jury award  --  and to restrict patients' ability to bring lawsuits against
hospitals and other health care providers. None has passed, but plaintiffs'
lawyers say they are very much in Congress's crosshairs.

''A lot of folks have been asked to sacrifice under this legislation,'' Mr. Kyl
said on the floor of the Senate. ''The one constituency that hasn't been asked
to sacrifice anything is the trial lawyers.''

And it's not just Congress: Trial lawyers were alarmed in September when the
White House said it would support state-run programs to test alternatives to
litigation in malpractice claims.

The lawyers' ads started in print, on the radio and online in September.
Advertising began this month at Union Station, a transit hub on Capitol Hill
near the Senate buildings, to coincide with the Senate's debate.

The bright blue ads in the subway feature yellow sans-serif type urging Congress
''to put patients first.'' Large banners at the Union Station Metro station tell
the stories of two plaintiffs, including Blake Fought, 19, of Blacksburg, Va.,
who died when an intravenous tube was removed incorrectly.

Their stories and six other cases are included on the ''98,000 reasons'' Web
site. The campaign also includes print ads once a week in the newspapers
covering Congress, including Politico, The Hill and Roll Call. In addition, the
group advertises online with The Huffington Post, The Washington Post and
Politico, among other sites, and sponsors a public radio station's Capitol news
roundup during morning drive time.

But the subway blitz is drawing the most notice. Blackbarn Media,  a  Web
development and design firm in Alexandria, Va., created the campaign. Sarah Fox,
the creative director, said she used ''vibrant, almost shocking colors'' to
emphasize ''the effects of medical mistakes on real people. And we chose clean
typography to focus on the staggering number of deaths.''

The American Association for Justice solicited its members to support its
campaign, which included buying a monthlong ''station saturation'' package that
CBS Outdoor sells for the Washington Metropolitan Area Transit Authority.

Union Station has 33 positions, including billboardlike posters, canvas banners
and freestanding backlit displays  -- along with vinyl applications on floors
and pillars. ''Our most popular stations are Union Station and Capitol South, on
the other side of the Capitol, as well as the Pentagon station, which is a
favorite of defense contractors,'' said Ron Rydstrom, the transit authority's
marketing director.

Trial lawyers  are not the only advocacy group to try to influence decision
makers this way. The National Association of Broadcasters, the Cancer Action
Network and the Pew Charitable Trust have also bought Metro ad space, he said.
The cost varies. The trial lawyers paid about $100,000 for the month.

''We're easy to demonize,'' Mr. Tarricone said, ''but the price was well worth
it to make sure the rights of patients aren't bargaining chips in this
process.''

URL: http://www.nytimes.com

LOAD-DATE: December 29, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: A lawyers' association is aiming to fend off any limits to
patient lawsuits by putting ads at the subway station on Capitol Hill.

PUBLICATION-TYPE: Newspaper


