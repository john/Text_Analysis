                   Copyright 2009 The New York Times Company


                             397 of 2021 DOCUMENTS


                               The New York Times

                            August 25, 2009 Tuesday
                              Late Edition - Final

Count Them Among the Uninsured

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 20

LENGTH: 1159 words


To the Editor:

Re ''The Uninsured'' (editorial, Aug. 23):

I was disappointed that your itemization of who the uninsured are did not
mention the large number of Americans whom the private, for-profit insurance
industry deems ''uninsurable.''

If you have a pre-existing condition, you often can't get private insurance for
that condition at an affordable price, or sometimes at any price. This is one of
the gravest failures of our free-market system: sick people are deliberately and
legally excluded from the insurance system. The only way they can get coverage
is through an employer group policy or by being poor enough to qualify for
public programs.

This pool of people continues to grow as people are laid off, as jobs with group
health become more scarce generally and as the population grows older and gets
''uninsurable'' chronic diseases.

David Tallman Atlanta, Aug. 24, 2009

To the Editor:

Your editorial pondered who, exactly, are the uninsured. Where I live in rural
Northern California, it seems that at least half of the working people I know
have no insurance.

These are average folks of all ages and backgrounds who work for local small
businesses. Their employers can't afford to insure them, and they can't afford
to buy insurance on the open market.

These uninsured small-business employees include the very people who work at my
doctor's office. My internist isn't able to offer reasonably priced insurance to
his own staff. When the nurses who are taking my blood pressure and the nice
front-office people who are booking my medical appointments can't themselves
have affordable access to medical care, something is disastrously wrong with the
system.

Ellen Golla Bayside, Calif., Aug. 24, 2009

To the Editor:

Inequalities in access to health care do not have a negative effect only on the
health and well-being of the uninsured. There is a serious body of evidence to
suggest that middle-income groups in less equal societies have worse health than
comparable population groups in more equal societies.

These findings, reported by Norman Daniels, Bruce Kennedy and Ichiro Kawachi in
their essay ''Justice Is Good for Our Health,'' deserve scrutiny.

If they can be substantiated, then it is in the interests of the already insured
middle classes to promote universal coverage and to work toward reducing other
social inequalities. Simply put, making sure that everyone has equal access to
high-quality medical care could make us all healthier.

Darian Meacham Brussels, Aug. 23, 2009

To the Editor:

Many Americans get health insurance through their employer. So if they become
chronically ill to the extent that they can't work, they lose both income and
access to health care. That fact alone ought to be enough to scrap our current
system and come up with a universal plan for everyone.

The rich probably can't even imagine the plight of being seriously ill and
having no help available. But how dare anyone condemn plans that would assure
coverage for everyone. Who among us believes that we ourselves don't deserve
access to health care when needed? We don't know in advance what will befall us,
what care we'll need or when. It's not a choice.

To allow human suffering and death because of lack of health insurance is beyond
a moral outrage. It is primitive, uncivilized, barbaric and unforgivable in a
country as rich as the United States.

Nancy Bennett O'Hagan Swatragh, Northern Ireland Aug. 24, 2009

To the Editor:

I suggest that if Congress does not adopt the ''public option'' provision as
part of health care reform, all members of Congress be required to obtain their
health care insurance through private carriers exclusively.

Paul G. Bursiek Boulder, Colo., Aug. 24, 2009

To the Editor:

''The Uninsured'' speaks to the concerns that every thinking American should
have about the backlash against health insurance reform. Except for the very
rich, everyone in this country is one illness or one layoff away from having no
health insurance.

More and more companies are offering insurance only to the employee and not to
his or her family. Many are no longer offering health insurance. With the
economic downturn and lower-paying jobs replacing those that have been lost,
this is likely to continue.

As things stand now, the uninsured cost all of us money when they go to the
emergency room. If they can't afford to pay their bills, either the government
pays for them with taxpayer dollars or those of us with insurance pay for them
with higher premiums.

How would the millions without any or adequate insurance affect our society
during an epidemic? I hope we don't have to find out if an even more extensive
swine flu outbreak occurs, as is being predicted.

I've heard it said that a nation's greatness is measured by how it treats its
weakest members. If we are truly as great a nation as we believe, let's prove it
by passing health insurance reform now.

Jacqueline Parrish  Philadelphia, Aug. 24, 2009

To the Editor:

Re ''A Public Option That Works,'' by William H. Dow, Arindrajit Dube and Carrie
Hoverman Colla (Op-Ed, Aug. 22):

It is gratifying to see San Francisco's universal health plan, Healthy San
Francisco, receive glowing marks from economists. But a few salient points might
limit this program's applicability elsewhere.

San Francisco has long preserved a relatively extensive safety net of public
health clinics and other services, including a large public hospital -- a sadly
endangered species. We also have one of the highest ratios of physicians per
resident anywhere.

Nationally, a broader ''public option'' is probably necessary for true reform.
Locally, San Francisco has been called ''49 square miles surrounded by
reality,'' and this program is one reason many of us here are proud of that.

Steve Heilig  San Francisco, Aug. 22, 2009

The writer was a member of the Universal Health Council, which designed Healthy
San Francisco, and is co-editor of Cambridge Quarterly of Healthcare Ethics.

To the Editor:

San Francisco boasts that it gives its citizens health benefits under the
Healthy San Francisco program, but in reality local employers do not bear the
ultimate cost -- the workers do.

Studies show that corporate profits have soared during the last 30 years  while
hourly workers' earnings have been stagnant. To say that San Francisco provides
health benefits paid for by employers under the notion of ''shared
responsibility'' is misleading and leads to nowhere regarding health care
reform.

Spyros Andreopoulos Stanford, Calif., Aug. 22, 2009

The writer is director emeritus, Office of Communication and Public Affairs,
Stanford University School of Medicine.

To the Editor:

How encouraging to learn that a problem as challenging as health care reform can
be resolved when people behave in a responsible manner that builds community and
defines our humanity.

Bill Lavine West Hartford, Conn., Aug. 22, 2009

The writer is a periodontist.

URL: http://www.nytimes.com

LOAD-DATE: August 25, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY ANTHONY RUSSO)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


