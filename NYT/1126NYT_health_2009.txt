                   Copyright 2009 The New York Times Company


                             1126 of 2021 DOCUMENTS


                               The New York Times

                           December 5, 2009 Saturday
                              Late Edition - Final

Efforts to Strip Health Care Provisions Fall Short

BYLINE: By ROBERT PEAR and DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 13

LENGTH: 627 words

DATELINE: WASHINGTON


Senate Democrats on Friday beat back Republican efforts to strip out two major
provisions of their health care bill that would make deep cuts in payments to
private Medicare Advantage plans and create a new long-term-care insurance
program.

In different ways, the two provisions reflect Democratic priorities. For years,
Democrats have asserted that the private Medicare plans, operated by insurance
companies under contract with the government, are getting unwarranted subsidies.
Many Democrats say the long-term-care program would fill a gap in the social
safety net, though critics assert that the program would be financially
unsustainable.

The votes capped a bitterly partisan first week of debate on the legislation.
The Senate majority leader, Harry Reid, Democrat of Nevada, said the Senate
would work through the weekend.

By a vote of 57 to 41, the Senate on Friday rejected a Republican motion to
eliminate the cuts affecting private Medicare Advantage plans, which provide
comprehensive care to nearly one-fourth of the 45 million Medicare
beneficiaries. The bill would cut $118 billion from projected spending on such
plans over the next 10 years.

Senator Orrin G. Hatch, Republican of Utah, said some older Americans would
inevitably lose some benefits, like vision and dental care, which they now
receive from the Medicare Advantage plans.

The No. 2 Republican in the Senate, Jon Kyl of Arizona, said Democrats were
breaking Mr. Obama's promise that ''if you like your health care plan, you'll be
able to keep it.''

But Democrats said the Republicans were just trying to protect the insurance
companies that offer Medicare Advantage plans.

Senator Sheldon Whitehouse, Democrat of Rhode Island, said he had yet to see any
instance in which the Republican argument ''does not happen to coincide with the
interest of the insurance industry.''

In the long-term-care insurance program, people could qualify for cash benefits
if they became severely disabled after voluntarily paying premiums for at least
five years. The money could be used for things that help people maintain their
independence, like housing modifications, special telephone and computer
equipment or the services of a caregiver.

Senator Tom Harkin, Democrat of Iowa, said: ''This is the next logical step
after the Americans with Disabilities Act. It will provide so many people in
this country with the security and peace of mind, knowing they won't have to go
to a nursing home or an institution if they become disabled.''

Under the bill, premiums would be set to cover the cost of the new program. But
Senator John Thune, Republican of South Dakota, said the government could incur
''a huge new liability down the road.''

The Senate voted, 51 to 47, to strip the new program from the bill, but the
Senate had previously agreed to set a 60-vote threshold, so the effort failed,
and the program remains in the bill.

Mr. Thune led the effort to eliminate the program. He won support from 11
Democrats, including the chairman of the Finance Committee, Max Baucus of
Montana, and the chairman of the Budget Committee, Kent Conrad of North Dakota.

The program is supported by President Obama. The House bill includes a similar
program.

Away from the floor debate, Democrats continued searching for a compromise on
the divisive question of whether to create a new government-run health insurance
plan.

Two pivotal centrists, Senators Joseph I. Lieberman, independent of Connecticut,
and Susan Collins, Republican of Maine, said they could not support any of these
proposals being floated by Democrats.

''The public option is really a government-created and government-run insurance
company,'' Mr. Lieberman said. ''It won't help a single poor person get
insurance.''

URL: http://www.nytimes.com

LOAD-DATE: December 5, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


