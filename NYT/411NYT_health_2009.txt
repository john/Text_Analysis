                   Copyright 2009 The New York Times Company


                             411 of 2021 DOCUMENTS


                               The New York Times

                           August 26, 2009 Wednesday
                              Correction Appended
                              Late Edition - Final

Waxman Takes On Drug Makers Over Medicare

BYLINE: By DUFF WILSON

SECTION: Section A; Column 0; National Desk; Pg. 14

LENGTH: 1250 words


As the health care debate focuses on whether  cost cuts are looming in Medicare
coverage,  Representative Henry A.  Waxman is on a crusade to save Medicare
billions of dollars -- in a way that he says would end up helping  the elderly.

That is because the money would come from the drug industry, which is why Mr.
Waxman may have a fight on his hands.

Drug makers contend they have already worked out a 10-year, $80 billion
cost-savings deal with the White House and crucial Senate gatekeepers on the
trillion-dollar health care overhaul. The industry says that trying to add Mr.
Waxman's provision could scuttle that agreement.

''You not only break the deal, but you break the bank for us,'' said Billy
Tauzin,  chief executive  of the drug industry's trade group, the Pharmaceutical
Research and Manufacturers of America,  known as PhRMA.

At issue is a  multibillion-dollar ''windfall'' that Mr. Waxman contends the
drug industry received when drug benefits were added to Medicare coverage in
2006. Mr. Waxman, Democrat of California, is chairman of the House  Energy and
Commerce Committee and is  central to the House's legislative efforts on health
care.

When drug coverage was added to Medicare, under the so-called Part D program,
about 6.4 million low-income elderly or disabled people were shifted into the
program from the government's Medicaid program for the poor. Such people,
entitled to Medicare and Medicaid, are known as dual eligibles in health
policy-speak.

Before that shift, because Medicaid administrators have the legal authority to
negotiate prices with drug makers, those 6.4 million dual eligibles had their
drugs paid for by the government at deeply discounted prices.

But under the Part D program, by law, Medicare is not allowed to negotiate drug
prices. And so, when the dual eligibles were added to Medicare's drug rolls, the
government suddenly started paying higher prices for their drugs -- 30 percent
higher, on average, according to an analysis by Mr. Waxman's committee.

As a result, Mr. Waxman contends, the drug industry got a $3.7 billion windfall
in the first two years of the Medicare drug program, and he says pharmaceutical
makers continue to reap more from Medicaid-eligible patients in the program than
the companies  would get if their drugs were provided through Medicaid.

''We want it back,'' Mr. Waxman said in an recent interview.  ''We want to make
sure the windfall for the drug companies does not continue, and we want to
recover the money that has been a windfall.''

Dual eligibles now total some eight million people -- or about one-fourth of
Medicare drug beneficiaries. Mr. Waxman wants the drug makers to  pay rebates on
medicines sold to the dual eligibles, to put the prices more in line with what
Medicaid pays for the same drugs.

The drug industry, says it has not received a windfall, and that Mr. Waxman's
analysis oversimplifies the issue.

It says that the government and patients  in the Medicare Part D drug program
can save more money in other ways that the industry has already worked out in
the $80 billion agreement with the White House. That deal, which PhRMA says
specifically precludes rebates for the dual eligibles,  reportedly has the
backing of Max Baucus,  the Montana Democrat who is chairman of the Senate
Finance Committee, which is  a leader in the Senate's health care effort.

Some details of the deal, including what political assurances the industry
received for its offer, have not been made public by the White House, or Mr.
Baucus. But PhRMA has agreed to support  health care reform   with a $150
million television advertising campaign -- as long as the deal is what the
industry says it agreed to.

Ron Pollack,  executive director  of Families USA, a nonpartisan group in
Washington that works for  affordable health care, said Mr. Waxman faced ''an
uphill climb'' on the issue. Even if the House legislation includes the rebates
Mr. Waxman seeks, Mr. Pollack predicted, the Senate will not go along.

But Mr. Waxman vowed to fight for the rebate plan in a conference committee with
the Senate.

In 2003, when the Part D drug program was being planned as part of a
Congressional overhaul of Medicare, Republicans insisted that the program be
administered by private insurers and that the government be precluded from
negotiating prices.

Stephen Schondelmeyer, a professor of pharmaceutical economics at the University
of Minnesota who had conducted research for the government and industry, said
that in 2003 many in Congress argued that private insurers would be more than
capable of negotiating discounts with  drug makers.

''That didn't pan out,''  Dr. Schondelmeyer said. The dozens of insurers
involved, competing among themselves, simply do not have the government's
negotiating clout.   While Medicaid is able to obtain rebates of up to 35
percent from drug makers, the Medicare drug rebates have been less than 15
percent, he said.

Dr. Schondelmeyer said it would be easy for the government to impose rebates on
the dual eligible part of  Medicare. But  groups that promote free enterprise
oppose that idea.

''This is best described not as a rebate but as a price control,'' said Michael
F. Cannon, director of health policy studies at the Cato Institute, a research
group.

The nonpartisan Congressional Budget Office  has studied the  dual eligibles. A
report it issued in mid-July  showed that   the federal government could save a
net $30 billion over 10 years if drug prices were set at Medicaid levels for
dual eligibles.

It was partly to head off such talk that PhRMA made an offer to the White House.
As part of its $80 billion savings plan, it would give all people in the
Medicare drug program a 50 percent discount if they entered an annual no man's
land in the coverage known as the doughnut hole.

The doughnut hole is a coverage gap that occurs after the person's drug costs
for the year reach $2,700; the coverage does not resume until the person's costs
exceed $6,154 for the year.

While in the hole, Medicare beneficiaries must pay for their own drugs or buy
supplemental insurance. The PhRMA deal would give people drugs at half price
during the doughnut hole period. PhRMA estimates this part of its proposal would
save Medicare patients $34 billion over 10 years.

According to Ken Johnson, a PhRMA vice president, the industry placed a
condition on its offer:  the White House and Mr. Baucus would not support Mr.
Waxman's proposal for rebates.

''Over the long run,'' Mr. Johnson said, ''we felt seniors would be better
served by reducing out-of-pocket costs in the doughnut hole, as opposed to a
rebate in the so-called dual eligibles.''

He said that a study paid for by PhRMA had concluded that imposing rebates to
benefit the low-income dual eligibles would  make drug coverage 25 percent to 50
percent more expensive for the rest of the Medicare population.

But Mr. Waxman is not convinced by PhRMA's arguments -- or its agreement  with
the White House and Mr. Baucus.

''We don't have any deal with them, and the whole enterprise of doing health
insurance for all Americans isn't to make the drug companies happy, or
wealthier,'' Mr. Waxman said. ''They're going to make a lot of money when we
insure all Americans. There's no argument for them to get a windfall.''

THE WORK-UP: Medicare Spending: Articles in this series are analyzing the
economic and financial issues at the heart of the health care debate in
Washington.

URL: http://www.nytimes.com

LOAD-DATE: August 26, 2009

LANGUAGE: ENGLISH

CORRECTION-DATE: September 2, 2009



CORRECTION: An article last Wednesday about Representative Henry A. Waxman's
effort to force drug makers to give back money he says was ''a windfall'' when
drug benefits were added to Medicare coverage misstated the size of the drug
discounts the federal-state Medicaid program receives. It receives an average
discount of 35 percent -- not discounts of ''up to 35 percent.'' The article
also referred imprecisely to Medicaid administrators who have the legal
authority to negotiate discounts with drug makers. State Medicaid officials --
not all administrators -- can seek the discounts. (Their federal counterparts
are bound by a set formula.)

GRAPHIC: PHOTO: Representative Henry A. Waxman's plan to save Medicare billions
could cost the drug industry. (PHOTOGRAPH BY STEPHEN CROWLEY/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


