                   Copyright 2009 The New York Times Company


                             649 of 2021 DOCUMENTS


                               The New York Times

                           September 21, 2009 Monday
                              Late Edition - Final

Considering a Tax on That Can of Soda

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 22

LENGTH: 600 words


To the Editor:

Your Sept. 14 editorial ''The Eeuww! Ads'' is correct to assert that a tax on
soft drinks is needed to curb their consumption, no matter how effective those
yuck ads might be. New York is to be commended for at least recognizing the
connection between soft drink consumption and overweight people.

Even better than a new tax, however, would be to have the federal government
withdraw all subsidies for corn because most of those soft drinks are sweetened
now with high-fructose corn syrup, not sugar.

We are using taxpayer money to underwrite the fattening of America, which is
absurd.

As Michael Pollan noted on your Op-Ed page on Sept. 10, ''the government is
putting itself in the uncomfortable position of subsidizing both the costs of
treating Type 2 diabetes and the consumption of high-fructose corn syrup.''

Rational people need to take over the government, wrest it from special
interests, and put it on a course back toward being of, by and for the people.
In the meantime, I wish New York success.

Gary Peters Paso Robles, Calif., Sept. 14, 2009

To the Editor:

You opine that to combat obesity ''the best move when it comes to soft drinks
[is] a tax on sodas and other sugary beverages,'' likening it to the tax on
cigarettes. This is fallacious reasoning.

Even one cigarette can be harmful and can lead to an addiction to nicotine,
whereas drinks containing sugar, when imbibed in moderation, have no such
effects.

A soft-drink tax would be unnecessarily punitive and a stunning example of the
government acting in loco parentis.

Ann J. Kirschner Brooklyn, Sept. 14, 2009

To the Editor:

Re ''Tempest in a Soda Bottle'' (Business Day, Sept. 17):

It is to be expected that the chief executive of Coca-Cola finds the idea of
taxation on soft drinks to be outrageous. As a mother and grandmother, I say it
is an idea whose time is overdue.

I recently toured the Coca-Cola center in Atlanta. The marketing glitz was
beyond belief. It is no wonder that half the world is now addicted to sugary
beverages. If this same marketing genius had been used to promote healthier
foods and beverages, we wouldn't have to deal with the current obesity epidemic
or its ensuing health care demands.

This is a no-brainer and would serve to ease the minds of those who protest the
potential costs of current health care reform proposals. I would have happily
paid such a tax on the one six-ounce glass of soda I allowed my children on
Sundays as they were growing up. Happily, all of them are healthy, not obese,
and have passed this practice on to their own children.

Mary Louise Hartman Princeton, N.J., Sept. 17, 2009

To the Editor:

I remember sitting in my high school biology class in 1993 and learning about
the evils of soda. Unlike today's crusade against sugar, however, the health
risks that were highlighted for me included the high phosphorus and acid content
of all sodas.

The phosphorus in carbonated beverages leaches calcium from our bones,
increasing risk for osteoporosis, and the acid in soda can erode tooth enamel.

Sodas also typically contain sodium, which can increase blood pressure.

It seems to me that in our country's war against obesity, we have forgotten to
evaluate everything else that affects our health. Despite the growing
recognition of the Health at Every Size approach (covered by your paper),
obesity remains the great bogyman to the neglect of other measures of health.

The suggested exemption of sugar-free, ''diet'' sodas from proposed tax schemes
is just the latest manifestation of this.

Beth Kelly Brooklyn, Sept. 17, 2009

URL: http://www.nytimes.com

LOAD-DATE: September 21, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY KATE O'CONNOR)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


