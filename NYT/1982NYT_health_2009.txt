                   Copyright 2010 The New York Times Company


                             1982 of 2021 DOCUMENTS


                               The New York Times

                              May 16, 2010 Sunday
                              Late Edition - Final

Pennsylvania Race May Show Democrats Which Way Midterm Winds Blow

BYLINE: By MICHAEL LUO

SECTION: Section A; Column 0; National Desk; Pg. 15

LENGTH: 1222 words


WAYNESBURG, Pa. -- Sam Boyd has been a Democrat his entire adult life, just like
many here in this mostly rural, economically impoverished southwestern corner of
the state, where the party's roots run as deep as the coal underfoot.

But in Tuesday's closely watched special election to succeed the late
Representative John P. Murtha in the state's 12th Congressional District, Mr.
Boyd, 65, is leaning toward casting his vote for the Republican candidate, Tim
Burns, a millionaire former software entrepreneur who got involved in politics
through the Tea Party movement.

''I'm for Burns for the reason I was for Obama,'' said Mr. Boyd, a retired
general contractor who served as an unpaid campaign liaison for Mr. Murtha in
his county. ''I want change.''

Whether or not Mr. Burns pulls off a victory over his Democratic opponent, Mark
Critz, in what polls suggest is a competitive race, voters like Mr. Boyd embody
the nightmare scenario for Democrats nationally: that even committed Democrats
will turn on their party.

Both parties have poured money and political star power into the contest, hoping
to shape the political narrative heading into the fall.

Senator Scott Brown, Republican of Massachusetts, headlined a rally for Mr.
Burns in Washington, Pa., on Friday. (Mr. Boyd got to meet Mr. Brown afterward
and shake his hand.) Former President Bill Clinton was scheduled to stump for
Mr. Critz in Johnstown on Sunday.

Democratic leaders hope that improved economic news will help Mr. Critz, as well
as their party nationwide. But that may not be enough to convince voters like
Mr. Boyd, who only a year and a half ago was putting up Murtha and Obama signs
across Greene County, the southwestern-most part of this sprawling district.

Mr. Boyd's path to discontent since then traces the bumpy legislative path in
Washington, from the auto bailouts to the stimulus plan to the passage of the
health care overhaul.

His decision on Tuesday, as well as that of other voters like him in this
heavily Democratic district, represents a test of Republicans' ability to make
the midterm elections a referendum on President Obama and the Democratic-led
Congress.

Mr. Boyd, who first joined his local Young Democrats club as a 14-year-old, says
he now regrets voting for Mr. Obama, even though he hastened to add that he
still found the president personally appealing.

''I just think I bought the sizzle, not the steak,'' he said.

Voters here are grappling with the end of the 36-year reign of Mr. Murtha, who
died in February. Mr. Murtha, a legendary master of the earmark process, used
his powerful position as the ranking Democrat on the Appropriations Committee's
military subcommittee to channel hundreds of millions of dollars to his sagging
district. That bounty helped him maintain a stranglehold on power over a region
pocked with shuttered steel mills and factories.

Even here in Greene County, a two-hour drive from Johnstown, where Mr. Murtha
used to live and a place he treated as the hub of his district, the signs of his
munificence are everywhere, from Murtha Road, where the local Wal-Mart is
located, to the defense contractor that anchors the county's technology park.

But now, there is clearly an opening for Republicans. Democratic voters
outnumber Republicans in this district by more than 2 to 1, but the Democrats
who populate the area tend to be conservative, like Mr. Boyd, especially when it
comes to social issues. With mostly white, blue-collar voters, it is also the
kind of district that gave the Obama campaign fits. It is the only district in
the country that voted for the Democratic presidential nominee, John Kerry, in
2004 and for the Republican nominee, John McCain, in 2008.

Congressional committees on both sides are on pace to spend about a million
dollars each on the race to replace Mr. Murtha. Outside groups have also poured
hundreds of thousands of dollars into the race. Mr. Burns has lent his campaign
$380,000 out of his own pocket.

Advertisements by Mr. Burns, as well as the National Republican Campaign
Committee, have almost invariably sought to tie Mr. Critz, who was Mr. Murtha's
district director, to Speaker Nancy Pelosi, who is extremely unpopular in the
district, and to a lesser extent President Obama, whose approval ratings here
are similarly abysmal.

''It's going to come down to, do you think country is on the right track under
this administration or the wrong track?'' Mr. Burns said in an interview at his
campaign headquarters in Washington, Pa. ''I know the majority of the people in
this district are not happy with Washington.''

Meanwhile, Mr. Critz has sought to draw a bright line between him and national
Democratic leaders, saying he would have opposed the health care bill, as well
as cap-and-trade climate legislation that is viewed coolly in this area where
coal mining remains a way of life. He has tried to focus on local issues,
describing his job with Mr. Murtha as that of economic development director and
arguing that he can bring jobs to the area.

''This campaign is not about Washington, D.C.,'' he said during a debate this
month with Mr. Burns. ''It's about Washington, Pa. It's about Washington
Township, Cambria County.''

Some voters have decided that Mr. Critz, with his knowledge of the district and
the Byzantine art of securing federal money, would be a better champion, even if
the earmarking process that has benefited them so much is now roundly vilified.

''Politics is not a clean game, but you better know how to play the game,'' Buzz
Walters, a friend of Mr. Boyd who runs a tire shop in nearby Rogersville, said
on a recent morning as the political talk among several friends grew heated.

Interviews with some two dozen voters in the district, most of them Democrats,
found varying degrees of approval or disenchantment with Washington. Some
resented efforts to turn the race into a broader referendum, saying they would
make up their minds as they always have, based on the experience or character of
the candidates. Others said they were so disgusted at politics in general that
they were planning to stay away from the polls.

(The state's primary is also being held on Tuesday, forcing the candidates to
battle on two fronts: winning the special election and fending off challengers
from their own party so they can run for a full term in November).

It is the angry talk among longtime Democrats, albeit ones who often sounded
decidedly like Republicans, that is potentially most worrisome to party leaders.

''I just think we need a better balance of power in Washington,'' Jim
Stephenson, 62, a retired electrician, said at the Airport Restaurant here,
where both he and Mr. Boyd often spend their mornings.

With Mr. Boyd, the Obama administration's communications challenges are clearly
evident. He said he was not necessarily opposed to the health care law but would
''like to know what's in the thing,'' calling it ''smoke and mirrors.'' As for
the stimulus plan, he said he only knew what he could see. And, he said, he had
not seen the economy improve.

It is the growing deficit that riles him the most, he said. Rumors of a
potential second stimulus package last year caused him to sink into a depression
for several days. With four grandchildren, he said he was worried for their
future.

URL: http://www.nytimes.com

LOAD-DATE: May 16, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: John P. Murtha represented Pennsylvania's 12th Congressional
District for 36 years. (A15)
Senator Scott Brown of Massachusetts stumped for Tim Burns, a fellow Republican,
in Washington, Pa., on Friday. Polls show Mr. Burns in a competitive race with
his Democratic opponent, Mark Critz, to succeed the late Representative John P.
Murtha. (PHOTOGRAPHS BY JEFF SWENSEN FOR THE NEW YORK TIMES) (A16) MAP (A15)
 CHARTS: How the District Voted in the Past: Representative John P. Murtha, a
Democrat, held Pennsylvania's 12th Congressional District for 36 years until his
death in February. (Source: Dave Leip's Atlas of U.S. Presidential Elections)
(A16)

PUBLICATION-TYPE: Newspaper


