                   Copyright 2010 The New York Times Company


                             1402 of 2021 DOCUMENTS


                               The New York Times

                            January 22, 2010 Friday
                              Late Edition - Final

The Party of Nope

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL; Pg. 30

LENGTH: 384 words


Here is the latest from the Grand Old Party: After years of eagerly enabling the
Bush administration's deficit spending and record debt, Republicans in Congress
are dusting off their budget hawk costumes and suddenly demanding fiscal
responsibility for the nation. In this scenery-chewing role, they are not just
denying their own profligate history, but they hardly seem to be serious. Take
their reaction to President Obama's proposal for a bipartisan commission to
tackle debt and budget threats.

''It's a nothing-burger,'' said Senator Judd Gregg, the ranking Republican
member of the Senate Budget Committee. He said he finds the proposal for a
commission of 10 Democratic and eight Republican experts de facto partisan, and
prefers any panel be created and controlled in Congress (where Senate
Republicans feast on serial filibustering). Representative John Boehner, the
House minority leader,  suggested Republicans would simply boycott posts on the
commission, asking why provide political cover for panicky Democrats.

Unabashedly feckless, the Republican Party aims to rise from the Congressional
minority by brandishing a political agenda of just saying no. As in no to health
care reform. (Their new senator-elect from Massachusetts, Scott Brown, ran
outright on a promise to be the latest to shoot down health care.) No to global
warming repairs; no to real reform of Wall Street; no, in fact, to any act of
creative opposition.

The proposed commission would make recommendations for facing the most
politically painful decisions about how to rein in programs and raise revenues
as the national debt rises. The proposal is occasioned by an election-year fight
that's heating up over the current deficit, a good part of which is because of
Bush administration spending on such wasteful exercises as the Iraq war. Don't
expect the No Party to own up to their past red ink.

Sooner or later constituents must wonder how far Republicans can take pure
opposition as a political philosophy without offering believable program
alternatives. The party should remember how Speaker Newt Gingrich got into a
fight with the Clinton White House over the budget and debt limit, and then
played a game of political chicken in which he precipitated a government
shutdown in 1995. He did not fare well.

URL: http://www.nytimes.com

LOAD-DATE: January 22, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


