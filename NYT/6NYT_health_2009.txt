                   Copyright 2009 The New York Times Company


                              6 of 2021 DOCUMENTS


                               The New York Times

                              June 2, 2009 Tuesday
                              Late Edition - Final

Democratic Senators Set to Visit White House to Discuss Health Care Overhaul

BYLINE: By SHERYL GAY STOLBERG and ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 14

LENGTH: 708 words

DATELINE: WASHINGTON


President Obama will meet with influential Senate Democrats on Tuesday to
discuss overhauling health care, as the White House releases a report asserting
that revamping the system would increase the income of a typical family of four
by $2,600 in 2020, and by $10,000 in 2030.

The Democrats on two Senate committees that are drafting health legislation have
been invited to the White House to meet with Mr. Obama, hours before he leaves
for the Middle East and Europe. As part of a push to secure Congressional
passage of a bill this year, the administration will also make the case on
Tuesday that reforming health care is critical to fixing the economy.

''If we don't do this we're going to be facing a real mess 30 years from now,''
Christina Romer, the chairwoman of the White House Council of Economic Advisers,
told reporters Monday on a conference call to discuss her new report, ''The
Economic Case for Health Care Reform.''

Also, six health care organizations followed up Monday on a commitment they made
last month to Mr. Obama to trim $2 trillion in health care costs over 10 years.
The groups, representing doctors, hospitals, drug companies and a labor union,
proposed eliminating unnecessary medical tests and procedures, slashing red tape
and better managing chronic diseases.

They said the potential savings could be $1 trillion to $1.7 trillion over 10
years.

Health care spending in the United States accounts for 18 percent of the gross
domestic product, according to the White House report, and is expected to rise
sharply, to as much as 28 percent in 2030 and 34 percent in 2040. The
administration says it can slow the growth of health spending even as it expands
coverage to the more than 45 million people who are now uninsured.

Republicans are doubtful. Referring to the health care groups' proposals,
Senator Charles E. Grassley of Iowa, the senior Republican on the Finance
Committee, said Monday, ''I'm skeptical that these proposals will add up to
anywhere near $2 trillion.''

The House Republican leader, Representative John A. Boehner of Ohio, called the
White House report ''nothing more than smoke and mirrors,'' and said the
administration had not offered a credible plan to expand coverage ''without
raising taxes or rationing care.''

The White House report -- 51 pages, with charts, graphs and algebraic formulas
-- estimated that slowing the growth rate of health care spending by 1.5 percent
a year would increase economic output by more than 2 percent in 2020 and nearly
8 percent in 2030. The report also states that revamping the health care system
would ''prevent disastrous increases in the federal budget deficit.''

The 1.5 percent figure was cited as a goal by the six organizations that
proposed cost savings to the president. Each submitted recommendations, though
it was not clear how much each was willing to sacrifice.

The Pharmaceutical Research and Manufacturers of America, representing drug
companies, advocated greater use of certain prescription drugs, like medicines
for high blood pressure -- a move it said could save lives and money by keeping
people healthier and reducing hospital admissions.

Doctors, represented by the American Medical Association, promised to try to
curb the overuse of imaging services, like magnetic resonance imaging of the
knee and the shoulder, and to reduce surgeries that might not be necessary, like
Caesarean section deliveries and angioplasties in patients with stable coronary
artery disease.

The Service Employees International Union said Medicaid and Medicare could save
money by encouraging the use of home care services, instead of nursing homes.
The union recommended that the federal government temporarily increase Medicaid
payments to the states for home- and community-based services.

America's Health Insurance Plans, representing insurers, vowed to establish
standard claim forms and Web sites that allowed doctors to communicate more
easily with insurers.

Congress may try to put some proposals into legislation, to help offset the
costs of providing coverage for millions of the uninsured. This week, the
chairmen of the two relevant Senate committees are finishing legislation to be
considered by their panels this month.

URL: http://www.nytimes.com

LOAD-DATE: June 2, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


