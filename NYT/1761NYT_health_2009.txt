                   Copyright 2010 The New York Times Company


                             1761 of 2021 DOCUMENTS


                               The New York Times

                             March 26, 2010 Friday
                              Late Edition - Final

In Possible Retirement, the Likelihood of an Election-Year Confrontation

BYLINE: By PETER BAKER

SECTION: Section A; Column 0; National Desk; Pg. 12

LENGTH: 1144 words


WASHINGTON -- No announcement has been made, but the widely anticipated
retirement of Justice John Paul Stevens in coming weeks has the White House,
Senate and lobbying groups bracing for an election-year confrontation over the
future of the Supreme Court.

Although Justice Stevens has not disclosed his intentions, he has suggested he
may announce as soon as next month plans to step down after 35 years on the
bench, providing President Obama his second opportunity to shape the nation's
highest court. A new nomination could set off another charged ideological battle
heading into the fall midterm campaign.

Wary of appearing presumptuous, the White House has avoided overt moves to
prepare, but it already has long dossiers on a host of candidates after last
year's nomination of Sonia Sotomayor. If Justice Stevens retires, Democrats
close to the White House said, the leading contenders will be three runners-up
from last year: Elena Kagan, the solicitor general; Diane P. Wood, an appeals
court judge in Chicago; and Merrick B. Garland, an appeals court judge in
Washington.

The choice would depend in part on what kind of fight Mr. Obama is willing to
wage amid other tough legislative battles. Energized if bruised from his
campaign to overhaul the nation's health system, Mr. Obama this year wants to
push through energy, education and financial regulation measures, ratify an arms
control treaty and make progress on immigration legislation.

A confirmation battle could not only provoke fresh skirmishing on longstanding
issues like guns, abortion, race and terrorism; it might also generate new
divisions stemming from constitutional challenges to Mr. Obama's new health care
program and a recent Supreme Court ruling guaranteeing the right of corporations
and unions to spend unlimited amounts of money in candidate elections.

Mr. Obama may feel empowered to take on a fight. ''The president now has health
care behind him,'' said Walter E. Dellinger III, acting solicitor general in the
Clinton administration. ''Though there are other major initiatives, there's
nothing comparable with health care to compete with this for expenditure of the
president's political capital.''

But he may still want to avoid conflict. ''The wise way to do this would be to
find someone who would be hard to be defeated,'' said Senator Charles E.
Schumer, a New York Democrat who shepherded Justice Sotomayor's confirmation.

Justice Stevens, appointed by President Gerald R. Ford in 1975, turns 90 next
month and is already the fourth-longest-serving justice in history. Leader of
the liberal wing, he signaled his possible retirement last fall by hiring only
one clerk for the term that starts this October, instead of the usual four.

He told The New Yorker on March 8 that he would decide within a month. He said
three current clerks had volunteered to stay if needed. ''So I have my options
still,'' he said. ''And then I'll have to decide soon.'' Either way, he made it
clear he wanted Mr. Obama to choose his successor: ''You can say I will retire
within the next three years. I'm sure of that.''

While a replacement for Justice Stevens most likely would not shift the
ideological balance on the court, it could secure the seat for the liberal
faction for years. But not all liberals are alike. The president's base hopes he
will name a full-throated champion to counter Justice Antonin Scalia, the most
forceful conservative on the bench.

''In the Sotomayor case, they weren't willing to take it on,'' said Geoffrey R.
Stone, a University of Chicago law school professor and former colleague of Mr.
Obama's who joined a group letter to the president last month urging more
assertive judicial appointments. ''I hope they're willing to take it on now. In
light of the health care vote, they have some momentum now.''

Other activists said Mr. Obama should not be deterred by the coming
Congressional elections. ''No matter who he chooses, the Republicans will use
the issue to mobilize their base for the midterms,'' said Nan Aron, president of
the Alliance for Justice, a liberal advocacy group.

The candidates who would most excite the left include the constitutional
scholars Harold Hongju Koh, Cass R. Sunstein and Pamela S. Karlan. Mr. Koh and
Mr. Sunstein now work in the Obama administration while Ms. Karlan teaches at
Stanford Law School. But none were finalists last year, and insiders doubt Mr.
Obama would pick any of them now.

''If it were a Sunstein or a Koh, you would have all-out war,'' said Curt Levey,
executive director of the Committee for Justice, a conservative advocacy group.

The front-runner with the most support among liberals would be Judge Wood, who
has opposed some abortion restrictions and is respected for standing firm
against strong, conservative judges on the Court of Appeals for the Seventh
Circuit. She and Mr. Obama were colleagues at the University of Chicago.

With no judicial record, Ms. Kagan is less known. As dean at Harvard Law School,
she hired conservative professors to expand academic diversity and has supported
assertions of executive power. But she stirred a furor by barring military
recruiters because of the ban on gays and lesbians serving openly.

Judge Garland might be the safest choice. A former federal prosecutor now on the
Court of Appeals for the District of Columbia Circuit, he is well regarded by
Democrats as well as influential Republican senators like Orrin G. Hatch of
Utah. But his careful jurisprudence stirs less enthusiasm among liberal
activists.

All three were vetted last year, and Judge Wood and Ms. Kagan were interviewed
by the president along with Justice Sotomayor. The fourth candidate interviewed
was Janet Napolitano, the homeland security secretary, but her prospects may be
marred by her comment after the attempted Christmas bombing of an American
airliner that ''the system worked.'' She later explained she meant the system's
response after the attack, and she remains a favorite of Mr. Obama's who has not
been ruled out, a top official said.

The search, being led by the new White House counsel, Robert F. Bauer, may reach
beyond the typical pool of appellate judges for a politician, Democrats said.
Gov. Jennifer M. Granholm of Michigan was vetted last year, and Gov. Deval
Patrick of Massachusetts is a political ally. On Capitol Hill, there is talk of
Senators Richard J. Durbin of Illinois and Claire McCaskill of Missouri.

Thomas C. Goldstein, a Supreme Court litigator at Akin Gump Strauss Hauer & Feld
and founder of the Scotusblog Web site, said the White House wanted to duplicate
the success of the Sotomayor confirmation.

''There's no diversity imperative here,'' Mr. Goldstein said. ''They can push
whoever they want. But that doesn't mean there aren't diversity advantages.
Appointing two women in a row I think they would view as a plus.''

URL: http://www.nytimes.com

LOAD-DATE: March 26, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: Justice John Paul Stevens, between Justice Anthony M. Kennedy,
left, and Chief Justice John G. Roberts Jr. Justice Stevens has said that he
plans to make a decision on retirement soon. (PHOTOGRAPH BY OFFICE OF GABRIELLE
GIFFORDS, VIA A.P.)
Among the leading contenders to succeed Justice Stevens would be, from left,
Elena Kagan, the solicitor general
and two federal appeals court judges, Diane P. Wood and Merrick B. Garland.
(PHOTOGRAPHS BY JOSE LUIS MAGANA/ASSOCIATED PRESS
UNIVERSITY OF CHICAGO
 SMITHSONIAN INSTITUTION)

PUBLICATION-TYPE: Newspaper


