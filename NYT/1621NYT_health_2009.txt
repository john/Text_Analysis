                   Copyright 2010 The New York Times Company


                             1621 of 2021 DOCUMENTS


                               The New York Times

                              March 8, 2010 Monday
                              Late Edition - Final

Sure-Fire Crowd Pleaser: Reining In Wall Street

BYLINE: By JOHN HARWOOD

SECTION: Section A; Column 0; National Desk; THE CAUCUS; Pg. 12

LENGTH: 912 words


For President Obama and Congressional Democrats, public opinion this past year
has mostly gone in the wrong direction -- on his job performance, on health care
and economic stimulus, on midterm elections.

But there's one conspicuous exception: the president's call for reining in Wall
Street. The nation's continuing angst over bailouts, bonuses and bad mortgages
has made sure of that.

Three-fifths of Americans supported tougher regulation of Wall Street in April
2009, according to Pew Research Center polling. Despite rising disaffection with
government, three fifths still supported it last month.

That explains why Senate Republicans, after throwing up a wall of opposition on
health care, continue negotiating with their Democratic counterparts on
financial regulation. It also explains why the White House, Democratic leaders
and liberal activists believe they hold the upper hand -- and an issue that
could limit their expected November losses.

''The anger is much greater at the financial industry than it is at the health
care industry,'' said Steve Hildebrand, a Democratic consultant who helped run
Mr. Obama's 2008 campaign. ''Once health care is over, this will be given a lot
of attention by the president and by Democrats.''

Big Finance in Action

Regulatory details on derivative investments, systemic risk and capital
requirements can be so arcane as to make the sprawling health care system appear
simple by comparison. That's normally a circumstance in which affected
industries exercise maximum influence on lawmaking.

Big Finance is trying. Banks, insurance, securities and other financial firms
spent some $382 million on lobbying in 2009, according to the Center for
Responsive Politics. The finance, insurance and real estate sectors have given
$102 million to 2010 election campaigns, a majority to the Democrats who control
Congress.

So far industry lobbyists have wielded their influence quietly, as health care
dominates the public dialogue. Republicans believe the issue's obscurity has
minimized Democrats' ability to turn the politics of Wall Street against them,
even as the minority party resists tax and regulatory elements of the
administration's agenda.

''I have been most struck by how invisible the issue has been as part of the
public debate,'' said Bill McInturff, a Republican pollster. Unless voters
''understand what it is and why it matters,'' he added, ''it's unlikely to have
much consequence in the campaign.''

But the issue may soon assume a higher profile. Senator Christopher J. Dodd, the
Connecticut Democrat who is chairman of the Senate Banking Committee, plans to
send a bill to the floor for debate later this month.

If health care has commanded winter political headlines, Democratic leaders see
financial regulation as a chance for springtime renewal. Their goal is Senate
passage, and a compromise with the already-passed House version, by July 4.

G.O.P. Uncertainty

Frank Luntz, the Republican political wordsmith, assured opponents of financial
regulation legislation in a recent strategy memo that ''Washington's
incompetence'' represents ''your critical advantage.'' By linking Mr. Obama's
effort to financial industry bailouts, he argued, opponents can leverage public
anger against Democrats. In that effort, the left may prove to have been an
unwitting ally of Republicans with its repeated complaints that Timothy F.
Geithner, the Treasury secretary, and Lawrence H. Summers, the White House
economic adviser, are themselves too close to Wall Street.

Alex Castellanos, a Republican consultant, pointed to another edge: with
Americans most anxious about unemployment, calling for stricter regulation of
Wall Street is ''not a growth argument, it's a punishment argument.''

But the actions of Senate Republicans suggest they are not so sure. The ranking
Banking Committee Republican, Senator Richard C. Shelby of Alabama, has returned
to discussions  with Mr. Dodd, after earlier reaching an impasse and watching
his junior colleague, Senator Bob Corker of Tennessee, step up as Mr. Dodd's
Republican interlocutor.

And the reaction of key Democrats to those talks suggests that they agree with a
Democratic pollster, Celinda Lake, that ''the fight over financial reform is one
that progressives absolutely want to have.''

Negotiations have largely revolved around the location and authority of a new
consumer protection unit. After the industry objected to Mr. Obama's idea for a
new agency, Mr. Dodd entertained Republican suggestions that it be housed in the
Treasury Department, the Federal Deposit Insurance Corporation and, most
recently, the Federal Reserve.

Democratic activists and influential legislators like Senator Charles E. Schumer
of New York balked at the last idea, citing the Fed's weak reputation as a
regulator. Some industry lobbyists now expect that bipartisan negotiations will
break down -- but that a  bill drafted by Democrats will be enacted anyway with
a few Republican defections.

If so, that may be because financial regulation gives Democrats a rare
opportunity to tap the Tea Party populism that Republicans have exploited on
health care. In an otherwise polarized environment, two-thirds of Democrats,
Republicans and independents share an unfavorable view of major banks and
financial institutions, the Pew Research Center found last month.

''In this environment,'' concluded Andrew Kohut, the center's director, ''Wall
Street is pretty inflammatory.''

URL: http://www.nytimes.com

LOAD-DATE: March 8, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Senator Christopher J. Dodd is working on a finance-regulation
bill.  (PHOTOGRAPH BY BRENDAN HOFFMAN/GETTY IMAGES)

PUBLICATION-TYPE: Newspaper


