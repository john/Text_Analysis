                   Copyright 2009 The New York Times Company


                             384 of 2021 DOCUMENTS


                               The New York Times

                            August 22, 2009 Saturday
                              Late Edition - Final

Voices Of Anxiety

BYLINE: By BOB HERBERT

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 17

LENGTH: 763 words


Barack Obama is a guy who is easy to underestimate. Six years ago hardly anyone
outside of Illinois had ever heard of him. Now he's America's first Zen-master
president.

On Thursday, he made a jocular reference to the tendency of pundits and others
to declare periodically that he's off his game and headed for disaster. He noted
that in August 2008, with his poll numbers wavering and the Republicans
energized by the addition of Sarah Palin to the national ticket, a lot of people
were convinced that, as he put it, ''Obama's lost his mojo.''

''You remember all that?'' he asked, smiling. ''There is something about August
going into September where everybody in Washington gets all wee-weed up.''

Wee-weed up? I don't know what that means, but the president seemed pretty
relaxed when he said it. This was the same day that he went on a radio program
and did his Joe Namath routine, guaranteeing that health care reform would get
done.

The president may be sanguine, but the same cannot be said of the general
public, including some of Mr. Obama's most ardent supporters. The American
people are worried sick over the economy, which may be sprouting green shoots
from Ben Bernanke's lofty perspective but not from the humble standpoint of the
many millions who are unemployed, or those who are still working but barely able
to pay their bills and hold onto their homes.

This is the reality that underlies the anxiety over the president's ragged
effort to achieve health care reform. Forget the certifiables who are scrawling
Hitler mustaches on pictures of the president. Many sane and intelligent people
who voted for Mr. Obama and sincerely want him to succeed have legitimate
concerns about the timing of this health reform initiative and the way it is
unfolding.

The president has not made it clear to the general public why health care reform
is his top domestic priority when the biggest issue on the minds of most
Americans is the economy. Men and women who once felt themselves to be securely
rooted in the middle or upper middle classes are now struggling with pay cuts,
job losses and home foreclosures -- and they don't feel, despite the rhetoric
about the recession winding down, that their prospects are good.

People worried about holding on to their standard of living need to be assured,
unambiguously, that an expensive new government program is in their -- and the
country's -- best interest. They need to know exactly how the program will work,
and they need to be confident that it's affordable.

Mr. Obama, who has a command of the English language like few others, has been
remarkably opaque about his intentions regarding health care. He left it up to
Congress to draft a plan and he has not gotten behind any specific legislation.
He has seemed to waffle on the public option and has not been at all clear about
how the reform that is coming will rein in runaway costs. At times it has seemed
as though any old ''reform'' would be all right with him.

It's still early, but people are starting to lose faith in the president. I hear
almost daily from men and women who voted enthusiastically for Mr. Obama but are
feeling disappointed. They feel that the banks made out like bandits in the
bailouts, and that the health care initiative could become a boondoggle. Their
biggest worry is that Mr. Obama is soft, that he is unwilling or incapable of
fighting hard enough to counter the forces responsible for the sorry state the
country is in.

More and more the president is being seen by his own supporters as someone who
would like to please everybody, who is naive about the prospects for
bipartisanship, who believes that his strongest supporters will stay with him
because they have nowhere else to go, and who will retreat whenever the
Republicans and the corporate crowd come after him.

People want more from Mr. Obama. They want him to be their champion. But they
don't feel that he is speaking to them in a language that they understand. He is
seen as more comfortable speaking the Wall Street lingo. People don't feel that
the voices of anxiety are being heard.

Maybe they're wrong. Maybe the economy really is turning around. Maybe Mr. Obama
is working on a bipartisan deal that will take us a few small steps down the
road to real health care reform.

It's possible that we've been without mature leadership for so long that it's
difficult to recognize it when we see it. Mr. Obama has proved the naysayers
wrong time and again. But if it turns out that this time  he's wrong, hold onto
your hats. Because right now there is no Plan B.

URL: http://www.nytimes.com

LOAD-DATE: August 22, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


