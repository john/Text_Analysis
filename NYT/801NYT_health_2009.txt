                   Copyright 2009 The New York Times Company


                             801 of 2021 DOCUMENTS


                               The New York Times

                            October 12, 2009 Monday
                              Late Edition - Final

Insurance Industry Assails Health Care Legislation

BYLINE: By ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 16

LENGTH: 562 words

DATELINE: WASHINGTON


In a blistering new attack, the health insurance industry said Sunday that
health care legislation drafted by Senate Democrats would drive up premiums,
rather than making coverage more affordable, as the White House contends.

A lobby for the industry, America's Health Insurance Plans, focused its
criticism on a bill likely to be approved Tuesday by the Senate Finance
Committee.

''The overall impact will be to increase the cost of private insurance coverage
for individuals, families and businesses above what these costs would be in the
absence of reform,'' said Karen M. Ignagni, president of the trade association.

Democratic aides on the Finance Committee disputed the conclusion. They said the
bill would provide tax credits to millions of people to help them afford
coverage. Moreover, they said, people could keep the coverage they now have if
they wanted. In addition, they said, some provisions of the bill would reduce
the administrative costs of insurance.

Ms. Ignagni cited a report done last week for her organization by
PricewaterhouseCoopers, the accounting firm.

The report says that the cost of the average family coverage, now $12,300, will
rise to $18,400 in 2016 under current law and to $21,300 if the Senate bill is
adopted. Likewise, it said, the cost of individual coverage, now $4,600, will
average $6,900 in 2016 under current law and $7,900 under the bill.

The study provides ammunition to Republicans attacking the legislation and might
intensify the concerns of some Democrats who worry that the bill does not
provide enough help to low- and middle-income people to enable them to buy
insurance.

Scott Mulhauser, a spokesman for Democrats on the Finance Committee, said:
''This report is untrue, disingenuous and bought and paid for by the same health
insurance companies that have been gouging consumers for too long. Now that
health care reform grows ever closer, these health insurers are breaking out the
same tired playbook of deception. It's a health insurance company hatchet job.''

Ms. Ignagni and PricewaterhouseCoopers said several provisions of the Senate
bill would drive up insurance premiums.

First, they said, the bill would require insurance companies to sell coverage to
all applicants and would prohibit them from considering health status in setting
rates. But, they said, the penalties for going without coverage are modest, so
the ''individual mandate'' is weak.

This creates ''a powerful incentive for people to wait until they are sick to
purchase coverage,'' Ms. Ignagni said. Sick people with high medical expenses
are likely to join the insurance pool, while healthier people may defer buying
insurance, secure in the knowledge they can get it when they need it, the study
says.

In addition, the study says, the bill would impose a new excise tax on high-cost
insurance policies and new fees on insurance companies. The study, like the
Congressional Budget Office, predicts that insurers will pass these costs on to
their customers, in the form of higher premiums.

Finally, the study says, the bill would cut hundreds of billions of dollars from
the projected growth of Medicare. To make up for these cutbacks, it says,
hospitals and other health care providers are likely to increase charges to
private insurers, which in turn would increase premiums charged to businesses,
families and individuals.

URL: http://www.nytimes.com

LOAD-DATE: October 12, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


