                   Copyright 2009 The New York Times Company


                             975 of 2021 DOCUMENTS


                               The New York Times

                           November 10, 2009 Tuesday
                              Late Edition - Final

Obama Seeks Revision of Plan's Abortion Limits

BYLINE: By ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 18

LENGTH: 820 words

DATELINE: WASHINGTON


President Obama suggested Monday that he was not comfortable with abortion
restrictions inserted into the House version of major health care legislation,
and he prodded Congress to revise them.

''There needs to be some more work before we get to the point where we're not
changing the status quo'' on abortion, Mr. Obama said in an interview with ABC
News. ''And that's the goal.''

On the one hand, Mr. Obama said, ''we're not looking to change what is the
principle that has been in place for a very long time, which is federal dollars
are not used to subsidize abortions.''

On the other hand, he said, he wanted to make sure ''we're not restricting
women's insurance choices,'' because he had promised that ''if you're happy and
satisfied with the insurance that you have, it's not going to change.''

Before passing its health bill on Saturday, the House adopted an amendment that
would block the use of federal money for ''any health plan that includes
coverage of abortion,'' except in the case of rape or incest or if the life of a
pregnant woman is in danger.

Some private insurance now covers abortion. Under the bill, most private
insurers would receive federal subsidies on behalf of low- and middle-income
people.

The Senate is working on its own version of health legislation.

Senator Susan Collins, a Maine Republican and pivotal centrist courted by the
White House, delivered a blistering critique of the Senate bill on Monday,
saying she could not support it because it would increase insurance costs for
many middle-income families and small businesses.

''We should rewrite the whole bill,'' Ms. Collins said. ''There is considerable
unease on both sides of the aisle about the impact of this bill, and as more
analysis is done, I believe those concerns will only grow.''

Ms. Collins and the other Maine senator, Olympia J. Snowe, are among the few
Republicans who had been considered potential supporters of the bill, drafted
mainly by Senate Democrats with help from the White House.

Ms. Collins appeared to dash those hopes on Monday, even as she affirmed her
belief that Democrats and Republicans could still find significant areas of
agreement.

Summarizing her study of the bill over the past 10 weeks, Ms. Collins said it
was ''too timid'' in revamping the health care system to reward high-quality
care. She said the bill included ''billions of dollars in new taxes and fees
that will drive up the cost of health insurance premiums.''

And she noted that many of the taxes would take effect before the government
started providing subsidies to low- and middle-income people to help them buy
insurance.

Thus, Ms. Collins said, ''there will be a gap for even low-income people where
the effect of these fees will be passed on to consumers and increase premiums
before any subsidies are available to offset those costs.''

The bill sets standards for the value of insurance policies, stipulating that
they must cover at least 65 percent of medical costs, on average.

Most policies sold in the individual insurance market in Maine do not meet those
standards, Ms. Collins said, so many insurers would have to raise premiums to
comply with the requirements. As a result, she said, the premium for a
40-year-old buying the most popular individual insurance policy in Maine would
more than double, to $455 a month.

The chairman of the Senate Finance Committee, Max Baucus, Democrat of Montana,
has tried to answer such criticism, saying that many of the current policies
provided meager bare-bones coverage.

The Senate Democratic leader, Harry Reid of Nevada, has drafted a health bill
that he hopes to take to the Senate floor within weeks. He is waiting for a cost
analysis by the Congressional Budget Office.

Supporters of the Senate bill said they believed that their efforts would gain
momentum from the approval of a broadly similar bill in the House on Saturday by
a vote of 220 to 215. But Representative Robert E. Andrews, Democrat of New
Jersey and an architect of the House bill, said, ''The hardest part is still
ahead of us.''

The House and Senate bills differ in at least five ways: how to configure a new
government insurance plan; whether to require employers to provide coverage to
employees; whether to finance the legislation with a tax on high-income people
or a tax on high-cost insurance plans; how strictly to limit coverage of
abortion; and whether illegal immigrants should have access to new insurance
marts, or exchanges.

The House bill imposes more stringent restrictions on the coverage of abortion.

Ms. Collins said the Senate Finance Committee ''did a good job of putting up a
firewall that would prevent federal funds from being used to finance
abortions.'' But she added, ''If Congress makes the mistake of establishing a
new government-owned insurance company, it would need to extend the prohibition
to that company because it is using federal funds.''

URL: http://www.nytimes.com

LOAD-DATE: November 10, 2009

LANGUAGE: ENGLISH

GRAPHIC: CHARTS: The Dividing Lines: The House on Saturday night narrowly
approved sweeping legislation to overhaul tthe nation's health care system.
Democratic leaders in the Senate have merged two bills -- one passed by the
Finance Committee in October and the other approved by the health committee in
July -- and are waiting for a cost analysis from the Congressional Budget Office
before bringing it to the floor for debate. The proposals from the two chambers
are broadly similar but differ on some major issues. Here's a look: House
 Senate

PUBLICATION-TYPE: Newspaper


