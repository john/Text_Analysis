                   Copyright 2010 The New York Times Company


                             1945 of 2021 DOCUMENTS


                               The New York Times

                             April 30, 2010 Friday
                         The New York Times on the Web

Fueling the Anger of Doctors

BYLINE: By PAULINE W. CHEN, M.D.

SECTION: Section ; Column 0; Science Desk; DOCTOR AND PATIENT; Pg.

LENGTH: 1243 words


At a recent social gathering, a doctor friend who has been in private practice
for almost 15 years revealed something that caused one physician to nearly choke
on her drink, another to gasp in disbelief and the rest of us to stop what we
were doing and gawk as if he had committed some grave social faux pas.

''I love what I do,'' he announced to all of us. ''I really love being a
doctor.''

His wife, suddenly aware of the silence that fell upon the room, inched closer
to her husband. ''He really does,'' she said nodding to confirm what some of the
rest of us couldn't quite believe.

While I had often heard older, usually retired, physicians speak about their
love of doctoring, hearing a doctor currently in practice talk about the job
with such fondness was so rare that it left us stunned. More frequently,
doctors' conversations about work reflect a sense of disenchantment, frustration
and even anger -- not toward patient care or doctoring per se, but toward the
increasingly intrusive role of insurance companies and government agencies.

One colleague told me late one night, just a few months before she left medicine
altogether, that she was proud of being a conscientious doctor who spent time
with patients and thus could often avoid costly tests and referrals. ''But look
at me,'' she said, the anger in her voice nearly palpable over the phone. ''I'm
at the office late every night taking care of mindless paperwork, just so the
insurance companies can deny payment.''

For nearly three decades, editorials, online posts and surveys have noted this
rising frustration and anger among practicing physicians. But over the last two
years, the pot of emotions seems to have boiled over. In all the recent
discussions about health care reform, what had heretofore played out only beyond
earshot of the exam room suddenly was very public: the tangled, uneasy and often
antagonistic relationship between practicing doctors and the insurance companies
who pay for the services they deliver.

As a primary care doctor posted recently on Sermo, the nation's largest online
community of physicians: ''We are our own worst enemies, as we have allowed
insurance companies and Medicare to set the value of our services. Clearly those
values they impose have nothing to do with our contribution to the health of our
patients or the cost savings we bring about.''

The fraught nature of the relationship became clear again last month when Sermo
and athenahealth, a provider of Internet-based business services for medical
offices, released the results of a national survey of 1,000 physicians. Nearly
two-thirds of doctors felt that the current health care environment was
detrimental to the delivery of care, and more than half believed that the care
quality would only decline over the next five years. Less than a fifth of
doctors felt they could make clinical decisions based on what was best for the
patient rather than on what payers were willing to cover. And an overwhelming
majority believed that getting reimbursed was becoming increasingly complex and
burdensome.

''Physicians have concerns about the power and undue influence of third parties
and insurance companies,'' said Dr. Daniel Palestrant, founder and chief
executive of Sermo. ''When it comes to medical practice, they are saying this
just doesn't work. They are acting in effect like the canaries in a coal mine.''

These canaries may be right. Last year, a study published in the health policy
journal Health Affairs found that physicians in private practice on average
spent nearly three weeks in time and $68,000 in staffing per year dealing with
the particular administrative constraints of third-party payers. Doctors who
were specialists could better afford to support these costs; but primary care
physicians devoted as much as a third of their average yearly income (including
benefits) to these interactions with the various health plans.

No wonder a lot of doctors are unhappy.

''The complexity is mind-boggling,'' said Dr. Lawrence P. Casalino, chief of the
division of outcomes and effectiveness research at Weill Cornell Medical College
and lead author on the study. Patients in a single practice are covered by a
multitude of health plans, each of which offers different services and
limitations to subscribers. Complicating matters further, large companies and
their employees will often have their own versions of a plan.

With each plan permutation, it becomes more and more difficult for a doctor to
know how to provide care that will work with a patient's particular coverage.
One of the doctors who was surveyed in the Health Affairs study wrote: ''It's
like going to the gas station to gas up your car and having to change the nozzle
on the gas pump because you have a Toyota and the pump was made to fit Fords.''

Even with an act as fundamental, and as frequent, as writing a prescription,
doctors face tremendous variability between health care plans. Because of
specific financial agreements between plans and different pharmaceutical
companies, only certain drugs are available on a plan's formulary, or listing,
of covered drugs. Doctors must consult these formularies each time they write
out a prescription if they want their patients to obtain the medications
prescribed. ''I literally had a whole drawer full of formulary books,'' said Dr.
Casalino, who was in primary care private practice for 20 years. And those
drawers, he noted, did not always include the countless notifications his office
received each week of formulary changes and modifications.

Nonetheless, Dr. Casalino pointed out that some of the interactions between
doctors and insurers can be helpful in controlling costs and ensuring quality
care. The process of obtaining prior authorization from a health plan for a
procedure, test or referral can eliminate inappropriate requests. But the
necessity of asking for such permission over and over again can seem senseless
and inefficient to physicians and their staff, especially if they have an
excellent prior track record of delivering high quality and efficient care.

Dr. Casalino and his colleagues estimate that the overall cost in the United
States of these administrative tasks is at least $31 billion, or about 7 percent
of all expenditures for physician and clinical services. But they also admit
that this figure is, at best, an underestimate; it fails to include the costs
incurred by health care providers who work in settings other than private
practice. While several health plans, government agencies and at least one broad
coalition group have made efforts to simplify parts of the current health care
administrative system, that change, as the most recent survey of physician
sentiment shows, cannot come fast enough for some physicians in practice.

''The administrative costs certainly add to physicians' feelings of being hard
done by,'' Dr. Casalino observed. ''They have a sense of hopelessness about it,
especially those doctors in smaller practices and primary care physicians.
Instead of spending time coordinating care for their patients and talking to
them, they are dealing with formularies and obtaining prior authorizations. The
weakest and worst paid physicians in the system are the ones most burdened.''

He added: ''Unhappy people aren't that great for anybody. And unhappy physicians
aren't good for patients.''

Join the discussion on the Well blog, ''Why Some Doctors Don't Like Their
Jobs.''

URL: http://www.nytimes.com

LOAD-DATE: April 30, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


