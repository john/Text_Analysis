                   Copyright 2009 The New York Times Company


                             248 of 2021 DOCUMENTS


                               The New York Times

                            August 1, 2009 Saturday
                              Late Edition - Final

On Health, Consensus Lies Beneath the Chaos

BYLINE: By ROBERT PEAR and DAVID M. HERSZENHORN; Carl Hulse contributed
reporting.

SECTION: Section A; Column 0; National Desk; Pg. 10

LENGTH: 1176 words

DATELINE: WASHINGTON


House members headed home on Friday, leaving behind the outlines of a nearly $1
trillion health care overhaul that is sure to draw fire from a variety of
interests, but also shows the beginnings of a consensus that would provide
insurance for more Americans and give them new rights in dealing with insurers.

As a final act before recessing until September, one crucial panel, the House
Committee on Energy and Commerce, approved landmark health legislation that
could ultimately lead to coverage for about 95 percent of Americans and create a
new government-run insurance program.

The 31-to-28 vote occurred at 9:05 p.m. Friday, at the end of a session that
began at 10 a.m. Five Democrats joined all 23 Republicans on the panel in voting
no.

Congress still has plenty of work to do in September to blend competing,
sometimes contradictory health measures, but lawmakers have found a good deal of
common ground on proposals that would profoundly change the health system.

Lawmakers of both parties agree on the need to rein in private insurance
companies by banning underwriting practices that have prevented millions of
Americans from obtaining affordable insurance. Insurers would, for example, have
to accept all applicants and could not charge higher premiums because of a
person's medical history or current illness. All insurers would have to offer a
minimum package of benefits, to be defined by the federal government, and nearly
all Americans would be required to have insurance.

''The glory days are coming to an end for the health insurance industry,''
Speaker Nancy Pelosi said Friday.

Ms. Pelosi predicted that insurers would not block passage of the legislation,
President Obama's top domestic priority.

Lawmakers also agree on the need to provide federal subsidies to help make
insurance affordable for people with modest incomes. For poor people, Medicaid
eligibility would be expanded.

The chaos on Capitol Hill, combined with bitter disagreements over how to pay
for the legislation and the role of a public plan, has obscured the areas of
potential consensus.

''There is wide agreement on the two elements of the legislation that the public
cares about most: insurance market reforms and the expansion of coverage, with
subsidies,'' said Drew E. Altman, the president of the Kaiser Family Foundation,
which focuses on health policy.

Details of the major House and Senate bills differ, but most employers would
have to provide insurance or contribute to the cost of coverage for employees,
with exceptions for some small businesses.

Democrats also agree that Congress should create some type of government
insurance plan or nonprofit cooperative, which would compete with private
insurers. Mr. Obama says the public plan would keep insurers honest, but
Republicans say it could eventually drive private insurers from the market,
leaving consumers with fewer choices.

Members of both parties in both chambers want to create health insurance
exchanges, where people could shop for insurance and compare policies.

Lawmakers also agree on proposals to squeeze hundreds of billions of dollars out
of Medicare by reducing the growth of payments to hospitals and many other
health care providers. They are committed to rewarding high-quality care, by
paying for the value, rather than the volume, of services.

The major bills offer the promise of more affordable insurance for people who
are uninsured, including those with chronic illnesses. Under the legislation, it
might be easier for people to switch jobs because they would not have to stay in
less desirable jobs just to retain health insurance. The bills promise relief to
people with huge out-of-pocket health costs and would eliminate co-payments for
many preventive services.

But high-income people and some businesses would face new taxes. In an analysis
of the House bill, the Congressional Budget Office estimated that people without
insurance would pay $29 billion in penalties over the next 10 years, while
employers not offering insurance would pay penalties totaling $163 billion.

The Energy and Commerce Committee was unable to deliver a bill when the nation
last tried to guarantee universal coverage, under President Bill Clinton, in
1994. Asked what was different this time, the current chairman, Representative
Henry A. Waxman, Democrat of California, said: ''The leadership of President
Obama. He made it a very strong, clear priority. He had a mandate from the
American people to pass legislation that would provide every American with
affordable coverage.''

Moreover, Mr. Waxman said: ''The issue is a lot more severe than it was in the
1990s. Fewer stakeholders -- doctors, patients, hospitals or insurance companies
-- want the present system to continue. It will bankrupt the country.''

House Democratic leaders said the health care bill would be on the floor in
September, after they meld versions approved by three committees. The Senate
will be in session next week, but the Finance Committee chairman confirmed
Thursday that his panel was not ready to take up the issue.

Action by House committees gives Mr. Obama a fragile political victory. But it
fell far short of his original goal, which called for both houses of Congress to
approve the legislation before the August recess.

The Energy and Commerce Committee voted, 47 to 11, to establish a procedure for
federal approval of generic versions of expensive biotechnology drugs.
Representative Anna G. Eshoo, Democrat of California, said this change could
save the government $9 billion over 10 years.

Democrats on the Energy and Commerce Committee cleared the way for approval of
their bill by adopting a package of amendments bridging  differences among
liberal, moderate and conservative members of the party.

Mr. Waxman said the latest agreement supplemented a deal struck Wednesday with
fiscally conservative Blue Dog Democrats on the panel.

To avoid cutting premium subsidies for low-income people, Mr. Waxman said,
Democrats found additional savings elsewhere.

Representative Diana DeGette, Democrat of Colorado, said the Blue Dog deal would
hold down costs. But, she said, ''it was paid for on the backs of people who
cannot afford health insurance,'' so liberals objected.

Under the Democratic proposals, insurers would be required to get ''prior
approval from the government before increasing premiums over a certain amount.''

Representative Tammy Baldwin, Democrat of Wisconsin, said this requirement was
needed to curb the growth of premiums.

''Over the last decade,'' Ms. Baldwin said, ''small businesses and individuals
have experienced double-digit increases in premiums, and that contributes
mightily to the level of uninsurance.''

The Democratic amendments stipulate that ''savings generated by this package
must go toward making premiums more affordable for lower-income people.''

Democrats on the Energy and Commerce Committee would also authorize the health
and human services secretary to negotiate prescription drug prices for Medicare
beneficiaries.

URL: http://www.nytimes.com

LOAD-DATE: August 1, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: A nearly empty hearing room on Friday in Washington during a
break of the House Energy and Commerce Committee.
 Members of the House including Speaker Nancy Pelosi listened as the House
majority leader, Steny H. Hoyer, spoke on Friday. (PHOTOGRAPHS BY BRENDAN
SMIALOWSKI FOR THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


