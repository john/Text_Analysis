                   Copyright 2009 The New York Times Company


                             1073 of 2021 DOCUMENTS


                               The New York Times

                           November 24, 2009 Tuesday
                              Late Edition - Final

Inside the Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 749 words


International

OBAMA'S WARMING TO CHINA

Is Seen as a Slight in India

India sees the warmer relationship between Washington and Beijing under the
Obama administration as a threat to its own rise as a global power. Many there
worry that India is being relegated to a regional role on par with its troubled
neighbors, Afghanistan and Pakistan. PAGE A6

SCRAPING BY IN KABUL

Six families have banded together to eke out  day-to-day survival in Kabul, the
capital of  Afghanistan, illustrating the failure of the government to create
jobs or root out the corruption that makes poverty so difficult to overcome.
PAGE A12

BRITISH ANGER OVER IRAQ

Official Defense Ministry documents leaked to a British newspaper have revealed
a climate of animosity among senior British officers toward American military
commanders after the 2003 invasion of Iraq. PAGE A13

National

HEALTH CARE DEBATE

Revives Abortion Groups

The debate on health care has brought renewed vigor to the issue of abortion,
and lobbyists on both sides are exploiting it with fund-raising appeals. PAGE
A19

ETHICS CHARGES FOR SANFORD

Gov. Mark Sanford of South Carolina will face formal ethics charges on 37 counts
of using his office for personal financial gain, according to a list of
accusations issued by the state ethics commission. PAGE A18

Poll Numbers Worry Democrats A20

Three Faiths, One Friendship A14

New York

YEARS AFTER GAMBLING DEAL,

No Decision on Aqueduct

Gov. George E. Pataki agreed in October 2001 to authorize electronic slot
machines at the Aqueduct racetrack in Queens. Eight years later, state officials
still cannot agree on an operator for the aging track, even as experts say a
refurbished operation would generate more than $1 million a day. PAGE A25

Business

PUSHING FOR STANDARDS

On All-Terrain Vehicles

In the market for all-terrain vehicles, the skyrocketing growth of Chinese
imports is becoming the latest challenge for the Consumer Product Safety
Commission, which is embarking on a global campaign to improve their safety.
PAGE B1

BEWARE THE COSTS OF OUTRAGE

Two amendments about the economy won committee approval, including one that
gives Congress far-reaching authority over the Federal Reserve. But the
amendments, which capitalize on voter outrage over bailouts, could have
unintended consequences. Andrew Ross Sorkin, Dealbook. PAGE B1

. PAGE B1

PAYING FOR JET'S OVERRUNS

The Pentagon's top acquisition official said that Lockheed Martin, the prime
contractor for the new F-35 fighter jet, would have to cover part of the
increased costs of the program. PAGE B3

A RECALL ON CRIBS

More than 2.1 million drop-side cribs by Stork Craft Manufacturing are being
recalled after reports of four infant suffocations. PAGE B5

Sports

AN N.B.A. STAR EVOLVES

From Phenom to Veteran

At 31, Kobe Bryant is now nestled between Kareem Abdul-Jabbar and Jerry West
among the Los Angeles Lakers' all-time leading scorers. He is in his 14th N.B.A.
season, having played in the league for nearly half his life, and continues to
improve and mold his game. PAGE B10

UNIVERSITY DROPS FOOTBALL

Northeastern announced that it was dropping football, leaving many players
feeling they had been betrayed by the administration. William C. Rhoden, Sports
of the Times. PAGE B13

Mauer Wins A.L. M.V.P. Award B10

Fallout From Soccer Scandal  B10

Arts

A TRUMPETER WHO HELPED

Remake the Culture

With ''Pops,'' a new biography of Louis Armstrong, the cultural historian Terry
Teachout restores this jazzman to his deserved place in the pantheon of American
artists. Review by Michiko Kakutani. PAGE C1

BOOK REKINDLES OLD DEBATE

The book ''The Invention of the Jewish People,'' spent months on the best-seller
list in Israel and is now available in English. Mixing respected scholarship
with dubious theories, the author, Shlomo Sand, has sparked a new wave of
coverage in Britain and has provoked spirited debates online and in seminar
rooms. PAGE C1

How Recession Impacted Artists C1

Review: Miami Museum of Art C1

Science Times

PLAN FOR DOMINICAN TOWN

Faces Environmental Test

A team of conservation biologists and researchers from Columbia University
believe the poor town of Miches, in the Dominican Republic, has great potential
for low-impact  eco-tourism. To make their visions a reality, they have begun a
sweeping effort to identify and repair problems in the town and region and to
capitalize on their assets. PAGE D1

Op-ed

DAVID BROOKS PAGE A33

BOB HERBERT PAGE A33

URL: http://www.nytimes.com

LOAD-DATE: November 24, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


