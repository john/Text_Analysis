                   Copyright 2010 The New York Times Company


                             1473 of 2021 DOCUMENTS


                               The New York Times

                            February 5, 2010 Friday
                              Late Edition - Final

Speed Bump on Incentives

BYLINE: By MICHELLE ANDREWS

SECTION: Section A; Column 0; National Desk; Pg. 16

LENGTH: 406 words


Both the Senate and House health care bills propose incentives to help corporate
wellness programs intended to help employees stay healthier and to keep a lid on
a company's insurance costs. But those programs have hit a legal speed bump with
the recent enactment of a law that generally prohibits employers and insurers
from asking workers about their family medical history.

Workplace wellness programs are common these days. More than half of employers
offer programs to stop smoking or to manage weight, according to a 2009 survey
by Aon Consulting. More than a third have on-site fitness centers.

The first, and sometimes only, step in enrolling an employee in any wellness
program is often to ask him to complete a health risk assessment containing a
dozen or more questions, including some about the employee's family history of
medical conditions like high cholesterol or diabetes.

But the Genetic Information Nondiscrimination Act of 2008 makes it illegal to
discriminate on the basis of genetic information in either health coverage or
employment. That means that health plans cannot use genetic information to make
coverage decisions or set health insurance premiums.

This year, insurers and other companies that roll out new wellness programs for
employers are no longer permitted to ask people about their genetics or family
history in health risk assessments if the answers are tied to any sort of
reward, like a premium discount.

Financial incentives -- like cash bonuses and reduced premiums -- are a popular
tool to encourage employees to fill out health questionnaires and participate in
wellness activities. Legislators had hoped to expand those incentives. The
Senate health reform bill, for example, would permit employees who participate
in wellness programs and who meet certain health targets to receive discounts of
up to 50 percent on their health insurance premiums.

The new restrictions ''were somewhat of a disappointment to employers,'' said
J.D. Piro, who leads the health law consulting group at Hewitt Associates.

Still, Mr. Piro acknowledged, ''You can certainly run an effective program in
the absence of that information.''

Although family history is a critical part of a person's disease-risk profile,
experts note, the actions promoted in wellness programs -- quitting smoking,
exercising, eating a healthy diet -- are beneficial no matter what is in a
person's DNA.

MICHELLE ANDREWS

URL: http://www.nytimes.com

LOAD-DATE: February 5, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


