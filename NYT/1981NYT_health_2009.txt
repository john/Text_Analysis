                   Copyright 2010 The New York Times Company


                             1981 of 2021 DOCUMENTS


                               The New York Times

                              May 16, 2010 Sunday
                              Late Edition - Final

When the Toast Is: 'To Your Health'

BYLINE: By ALICE FEIRING.

Alice Feiring, who lives in New York City, is the author of ''The Battle for
Wine and Love.''; E-mail: modernlove@nytimes.com

SECTION: Section ST; Column 0; Style Desk; MODERN LOVE; Pg. 6

LENGTH: 1398 words


THE night after President Obama's health care bill passed, my mother had me on
the phone, taking me through her panic attack. She loved her health insurance;
it was hard won and she didn't want to cash it in for what she viewed as some
''schleppy government issue.'' Then she turned her anger on me: ''How long are
you going to let Ronny pay for yours?''

Six years ago, Ronny -- my most profound love to date -- broke off our
decade-plus together for the reason that he couldn't do monogamy anymore. I was
slain. He was aggrieved. He also insisted on maintaining me -- with light bulbs,
coffee grinder repairs and health insurance.

''For how long?'' I asked.

''For as long as I can,'' he answered.

Ronny and I had counted on growing old together, and what little was mine had
always been his and vice versa. I had nursed his many wounds and pulled him
through law school. Being a wine writer, my earning potential was nowhere near
what his was about to become. No matter how hard-working I am, paying for health
insurance was not in my future. That Ronny agreed to cover me earned him
gentleman status -- to all but my mother, who frankly hated him.

Meanwhile, I continued to cook him an occasional dinner. We would do a two-step
up and down my railroad flat. We lost our relationship and entered a ghost world
of attachment, partly bound by our insurance arrangement.

I was saved from my mother's phone call by the sound of my buzzer. There was a
downpour so I rushed to let in my friend. Dinner and talk were on tap. When she
reached my door, her dark wool redingote drenched, she looked like a wet beagle,
with huge brown eyes.

About 15 years my junior, she'd been going through a hard time -- shaky
relationship, father with a recent cancer diagnosis -- and was eager to see me.

I had splurged on a few of the season's first morels and roasted them with
slender asparagus. Chick food. I set out the Riedels. Next to them I placed a
couple of wines. Our choices were either a 1982 Cune Imperial or a 2005 Gonon
St. Joseph.

I didn't waste time. ''What's going on?'' I asked.

''Now?'' she said. ''Why don't we wait a bit?''

The air was heavy and it wasn't from the outside humidity. We made small talk
while we contemplated the bottles. The older one, the Cune Imperial Rioja, we
couldn't resist. I raised the glass to my nose, which wrinkled in
disappointment; aromas were off. Something about the fruit's life had been
snuffed. In professional reflex she and I vigorously swirled, as if urging the
wine to wake up, then raised the glasses to smell. No luck.

''So?'' I asked, ready to hear she was leaving her relationship or wanted advice
on opening a wine bar.

Her face was impassive. Then she said: ''It's not about me, Alice. It's about
you.''

Few things shock me, but this one blindsided me. I raced fruitlessly through the
possibilities. I wasn't seeing anyone, so there was no intrigue. I couldn't
imagine any salacious gossip floating about attached to my name. In the wine
world I'm known as a pot stirrer, but of late I hadn't even had the burner under
my pots turned on.

Her big eyes stared at me. She took a breath and announced quietly, ''I've been
seeing Ronny.''

Note to self: Check parts to see if they're all there. Arms? Bowels? Hands?
Check. Heart? Yes! Not even any fluttering. For months I had suspected the
long-endured pain of the Ronny and Alice breakup had run its course. It only
took six years to work its virus-like way out of my system. Too long, but ours
hadn't been a relationship as much as it was Tolstoy. And here it was. The
proof. I was out of the woods. And believe me, after several years of deep
mourning this was a revelation.

Still, hearing her confession put a knot in my gut. I'd had enough drama in my
life and did not need to be dragged into this tightly wound plotline of his
girlfriends, their new affair and various pre-existing relationships. I thought:
''For this I made you dinner? Wouldn't a coffee have been more appropriate?''

Trying to sort out my emotions, I said, ''There's something wrong with this
wine, no?''

She stuck her nose into the glass.

I said, ''It's all wrong,'' and sniffed for signs of life.

''Yes, it is,'' she agreed.

I opened up the bottle of syrah, the Gonon St. Joseph, and hoped for the best.

Her eyes traced my movements as if looking for cracks. ''You seem relieved,''
she said.

Relieved? I was almost giddy. A friend had slept with my old love and all I felt
was pity for her, not pain for me.

She and Ronny had connected over my health, as it turns out. Back in October I
was dolled up for a press dinner and doing my best to be charming, telling some
tale. Mid-sentence a brick lodged into my gray matter, in that place where many
fine words once roamed. Though wine is my subject, I hadn't been drinking much,
so that wasn't the problem.

I panicked. Stroke? Brain cancer? Psychosis? Something was very wrong and I
scrambled for a graceful way to stop speaking before I started to perseverate
like some crazy woman on a psychiatric ward.

I scanned the next table to find my friend. In an instant she was kneeling at my
chair and then guided me out. In the vestibule she handed me a bag and my
jacket. I looked at them with no recognition and insisted, ''But these are not
mine.''

She hesitated, then said gently: ''They are yours, Alice. Trust me.''

(Later my doctor suggested I'd suffered a psychic seizure. But as my EEG was
normal, he wished me luck and sent me on my way.)

When Ronny found out she had rescued me, he wanted to hear her eyewitness
report. After my health was discussed, I guess they found other points of
interest.

Now she had questions about him for me -- his habits, history -- that I had no
interest in answering. I kept dwelling on this darn insurance, a symbol in the
Feiring household that went way back.

Forty years ago, when I was in high school, my father left for the neighbor's
wife and became a deadbeat before it was trendy. Even back then, $130 a week in
alimony wasn't much cash, especially for a Buick-driving lawyer who loved
hand-tailored shirts.

Early into her heartbreak, my mother gussied up to meet my father at some fish
house on the water. I'm not sure what she was hoping for -- perhaps peace,
perhaps money. She returned home early in tears. ''The bastard looked at me full
of pity and asked, 'How are you going to afford health insurance?' ''

My normally mild-mannered, nondrinking mother pitched her martini in his face.
After years of being a housewife, and with little more than a lapsed
bacteriology degree, she learned the jewelry business and eventually paid cash
for her own health insurance, along with everything else. To this day you can't
buy the woman a cup of coffee.

''No one is buying me,'' she'll say. And perhaps that was why the Obama health
care plan so terrified her. It was also probably why she was appalled that I
allowed Ronny to pay for my health insurance.

But my mother and I had different ideas on give and take and what it means to
love, and how to manage the co-dependence, the leaning into each other, that is
necessary for intimacy to evolve. When asked why she never had another man in
her life, she answered, ''I had my chances.''

I, too, had my chances, both before Ronny and after. And I hope I have a few
more yet. But only now do I wonder if I messed up romantic potential over my
larger-than-life insurance provider, whom I felt I owed ... something.
Everything comes with strings. He may have taken my youth, but I took his
kindness. I'll probably be writing Alice and Ronny stories forever, but it was
time for me to stop believing in the big one, that the keeper of my health
insurance was the love of my life.

THE second bottle of wine, the Gonon, usually filled with elegance and horse
sweat, was closed. Neither Ronny's new girl nor I had much appetite, and
besides, I had burned the morels.

''What does this mean for our friendship?'' she asked after we tried and failed
to make that second bottle of wine work.

''Nothing right now,'' I said.

And I meant it, though I couldn't wait for her to leave. When she finally headed
off into the dark, I went to research my new insurance options. While paying for
my own is the best solution, if some schleppy government issue is possible, the
time may have come to let Ronny's plan go and take Obama's for a spin.

URL: http://www.nytimes.com

LOAD-DATE: May 16, 2010

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY BRIAN REA)

PUBLICATION-TYPE: Newspaper


