                   Copyright 2010 The New York Times Company


                             1766 of 2021 DOCUMENTS


                               The New York Times

                             March 26, 2010 Friday
                              Late Edition - Final

Fix Health Reform, Then Repeal It

BYLINE: By PAUL RYAN.

Paul Ryan, a Republican, is a representative from Wisconsin.

SECTION: Section A; Column 0; Editorial Desk; OP-ED CONTRIBUTOR; Pg. 27

LENGTH: 792 words

DATELINE: Washington


ON Thursday night, Congress sent to President Obama the reconciliation package
to remove some of the embarrassing provisions in his signature legislative
achievement, health care reform. But a serious fix for what ails health care in
America will entail far more than merely tweaking the new law of the land; we
will need to repeal the entire faulty architecture of the government behemoth
and replace it with real reform.

To be clear: it is not sufficient for those of us in the opposition to await a
reversal of political fortune months or years from now before we advance action
on health care reform. Costs will continue their ascent as the debt burden
squeezes life out of our economy. We are unapologetic advocates for the repeal
of this costly misstep. But Republicans must also make the case for a reform
agenda to take its place, and get to work on that effort now.

So what can we do?

Health care experts across the political spectrum acknowledge that a fundamental
driver of health inflation is the regressive tax preference for employer-based
health insurance. This discriminatory tax treatment lavishes the greatest
benefit on the most expensive plans while providing no support for the
unemployed, the self-employed or those who don't get coverage from their
employer.

Reform-minded leaders like Senator Ron Wyden, Democrat of Oregon, and Senator
Tom Coburn, Republican of Oklahoma, pushed legislative proposals that would
directly address this issue. I helped write a plan that would replace the bias
in the tax code with universal tax credits so that all Americans have the
resources to purchase portable, affordable coverage that best suits their needs,
with additional support provided for those with lower incomes.  All these ideas,
though, were dismissed early on, as they didn't fit with the government-driven
plan favored by the majority. But going forward it's important that we
reconsider this regressive tax issue.

Then, when helping Americans with pre-existing conditions obtain coverage, we
should focus on innovative state-based solutions, including robust high-risk
pools, reinsurance markets and risk-adjustment mechanisms. I intend to continue
advancing true patient-centered reforms like attaching tax benefits to the
individual rather than the job, breaking down barriers to interstate
competition, and promoting transparency and consumer-friendly coverage options.

We should ensure that health care decisions are made by patients and their
doctors, not by bureaucrats, whether at an insurance company or a government
agency. By inviting market forces into health care, we can encourage a system
where doctors, insurers and hospitals compete against one another for the
business of informed consumers.

We must also immediately begin dealing with our crushing debt burdens, which
this legislation will worsen. The Democrats' fiscal arguments never did add up:
they claim that their program will reduce the deficit even though the federal
government will pick up the tab for more than 30 million uninsured Americans and
subsidize millions more. Even after accounting for the $569 billion in tax
increases and $523 billion in Medicare cuts,  the true costs of this legislation
-- concealed by timing gimmicks, hidden spending and double-counting -- will
make the deficit explode, plunging us deeper into debt.

Washington already has no idea on how to pay for its current entitlement
programs, as we find ourselves $76 trillion in the hole. Our country cannot
afford to avoid a serious conversation on entitlement reform. By taking action
now, we can make certain that our entitlement programs are kept whole for those
in and near retirement, while devising sustainable health and retirement
security for future generations.

The case for attempting health care reform was not difficult to make.
Skyrocketing health care costs are driving more and more families and businesses
to the brink of bankruptcy, leaving affordable coverage out of reach for
millions of Americans and accelerating our path to fiscal ruin. The challenge
was how to deal with the seemingly inexorable increase in health care costs.

Yet the Congressional majority went at this goal backward: with the force of the
federal government, cover all Americans -- then figure out which screws to twist
to contain costs. Democrats opted for this approach because their concern was
never about costs. It was about expanding coverage through an expansion of
government.

As the dust settles from this historic and fiscally calamitous week, we have to
try to steer this country back in the right direction. The opposition must
always speak with vigor and candor on the need for wholesale repeal and for real
reform to fix what's broken in health care.

URL: http://www.nytimes.com

LOAD-DATE: March 26, 2010

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY KEN ORVIDAS)

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


