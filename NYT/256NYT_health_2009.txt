                   Copyright 2009 The New York Times Company


                             256 of 2021 DOCUMENTS


                               The New York Times

                             August 3, 2009 Monday
                              Late Edition - Final

Treasury Auctions Set for This Week

SECTION: Section B; Column 0; Business/Financial Desk; Pg. 2

LENGTH: 497 words


The Treasury's schedule of financing this week included Monday's regular weekly
auction of new three- and six-month bills and an auction of four-week bills on
Tuesday.

According to traders, at the close of the New York cash market on Friday, the
rate on the outstanding three-month bill was 0.18 percent. The rate on the
six-month issue was 0.25 percent, and the rate on the four-week issue was 0.12
percent.

The following tax-exempt fixed-income issues are scheduled for pricing this
week:

TUESDAY

Fairfax County, Va., Redevelopment and Housing,  $93.5 million of revenue bonds.
Competitive.

Mecklenburg, N.C., $100 million of general obligation bonds. Competitive.

ONE DAY DURING THE WEEK

Bexar County, Tex., Hospital District (a political subdivision of the State of
Texas), $280.6 million of debt securities. Siebert Brandford Shank.

California Statewide Communities Development Authority, $78.1 million of senior
living revenue bonds for Southern California Presbyterian Homes. Ziegler Capital
Markets.

Carnegie Mellon University, Pa., $170 million of higher educational facilities
revenue bonds. Merrill Lynch.

City of Riverside, Calif., $240 million of sewer revenue bonds. Wells Fargo.

Dulles Toll Road, Va., $826 million of revenue bonds for Metrorail and Capital
Improvement projects. Citigroup Global Markets.

Dulles Toll Road, Va., $359.5 million of debt securities for Metrorail and
Capital Improvement projects. Morgan Stanley.

FYI Properties, $298.8 million of lease revenue bonds for the State of
Washington. Barclays Capital.

Government of Guam Department of Education, $69 million of certificates of
participation for the John F. Kennedy High School project. Piper Jaffray.

Harris County, Tex., $98.5 million of debt securities for Cultural Education
Teco project. RBC Capital Markets.

Illinois Finance Authority, $66.6.3 million of revenue bonds for Riverside
Health System. Goldman Sachs.

Metro Atlanta Rapid Transit, $225 million of sales tax revenue bonds. Merrill
Lynch.

New Hampshire Health and Education Facilities Authority, $138.9 million of
revenue bonds for Dartmouth-Hitchcock Obligated Group. Morgan Stanley.

North Texas Tollway Authority System, $810.4 million of debt securities for
Build America Bonds. Goldman Sachs.

North Texas Tollway Authority, $424.6 million of first-tier tax-exempt current
interest bonds. Morgan Stanley.

North Texas Tollway Authority, $178.3 million of revenue bonds. Morgan Stanley.

Northern Illinois Municipal Power Agency, $160 million of debt securities.
Citigroup Global Markets.

Orange County, Fla., $131 million of water district refinancing certificates of
participation. Citigroup Global Markets.

Rockefeller University, N.Y., $100 million of debt securities. JPMorgan
Securities.

University of Chicago Hospital,  $50 million of debt securities. JPMorgan
Securities.

Washington Health Care Facilities Authority, $116.7 million of revenue bonds for
Seattle Children's Hospital. Goldman Sachs.

URL: http://www.nytimes.com

LOAD-DATE: August 3, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: List

PUBLICATION-TYPE: Newspaper


