                   Copyright 2009 The New York Times Company


                             598 of 2021 DOCUMENTS


                               The New York Times

                           September 15, 2009 Tuesday
                              Late Edition - Final

An Organ of Many Talents, at the Root of Serious Ills

BYLINE: By NATALIE ANGIER

SECTION: Section D; Column 0; Science Desk; BASICS; Pg. 2

LENGTH: 1218 words


Should anybody in the reliably pestilent health care debate be casting about for
a mascot organ to represent some of the biggest medical crises that we Americans
face, allow me to nominate a nonobvious candidate: the pancreas.

It may lie in the hidden depths of the abdominal cavity, and its appearance,
size and purpose may be obscure to the average person. Yet the pancreas turns
out to be a linchpin in two epidemics that are all too familiar.

As the organ entrusted with the manufacture of insulin and other hormones that
help control blood sugar, the pancreas gone awry is a source of diabetes, which
afflicts more than 23 million people in this country, including the newest
member of the Supreme Court.

And as the tireless brewer of digestive juices that help shear apart the
amalgamated foodstuffs that we consume each day, the pancreas is at the
frontlines of our expanding waistlines, the mass outbreak of fatness that has
already claimed 60 percent of Americans and shows no sign of slackening.

Researchers are discovering that the pancreas helps mediate much of the
appetite-related cross talk between the brain and the gastrointestinal tract,
the streams of chemical signals that say, I'm starving down here, how about some
dinner, or, enough already, step away from that dessert cart and no one will be
hurt. By better understanding the precise role of the pancreas in conveying
sensations of hunger or satiety, suggested Rodger A. Liddle of Duke University
Medical School, we may find new ways to combat obesity.

Other researchers are intrigued by the pancreas's ability to shield itself from
harm, to churn out huge quantities of enzymes that can rapidly reduce a
cheeseburger and fries to so many particles of amino acids, carbohydrates and
fats, without digesting its own tissue in the process. They suspect that the
organ's set of self-protective mechanisms has a terrible downside, and helps
explain why pancreatic cancer can be so difficult to treat -- insights that are
just beginning to offer hope in the fight against one of the most lethal of all
malignancies.

If you've been remiss in appreciating your pancreas, don't feel bad: the
ancients were, too. Early anatomists were certainly aware of the pancreas but
mostly ignored it, and the organ's name reflects that ho-hum attitude. Pancreas
is Greek for all-meat, a reference to its seeming homogeneity from one end to
the other.

Much of the neglect may well have been practical. ''The pancreas has always been
difficult to study,'' Dr. Liddle said. For one thing, it's hidden. Measuring
some six to eight inches in length, and slippery and lobular to the touch, the
yellowish-brown pancreas is located deep in the abdomen, wedged between the
stomach and the spinal cord and extending horizontally right above the
waistline. Think of it as an oblong eel, the tail tucked into the stomach, the
head butting up against the curve of the duodenum, of the small intestine.

Add to that inaccessibility a prima donna sensitivity. ''If you do anything to
the pancreas, you initiate an inflammatory response,'' Dr. Liddle said. ''It
tends to become inflamed more easily than other organs.'' In fact, inflammation
of the pancreas, or pancreatitis, is a relatively common and often debilitating
condition, brought on by excess alcohol, drug reactions, gallstones, genetic
predisposition or other reasons. Unfortunately, said Dr. Anthony Kalloo, a
professor of medicine at the Johns Hopkins University School of Medicine, the
symptoms of pancreatitis, like chronic abdominal pain radiating into the back,
could be misdiagnosed or dismissed as a hypochondriac's lament. As a result, Dr.
Kalloo said, patients do not always receive the right pain medications, the
optimal diet, surgery when necessary.

For all the difficulty of studying the pancreas, researchers eventually came to
appreciate the organ as a gland of many talents, serving both an exocrine role
-- secreting its products through ducts, as the breast secretes milk and the
sweat glands perspiration -- and an endocrine role, fabricating hormones and
squirting them into the bloodstream, as the ovaries and testes dispense sex
hormones and the thyroid thyroxine.

Roughly 90 percent of the pancreas is devoted to its exocrinic role of
generating digestive enzymes and funneling them into the small intestine, a
burbling broth that flows forth from the pancreas at a rate of perhaps a quart a
day.

The other 10 percent of pancreatic tissue consists of so-called islet cells, the
endocrine players that synthesize insulin and glucagon to manipulate and titrate
blood sugar, the body's energy currency, as needed. In people with Type 1, or
juvenile-onset, diabetes -- among them Justice Sonia Sotomayor of the Supreme
Court -- an autoimmune reaction ends up destroying many of these islet cells,
resulting in the need for lifelong insulin injections. Among sufferers of Type
2, or adult-onset, diabetes, the reasons for insulin imbalance are more varied,
and the condition can often be treated through diet and exercise alone.

Imagine the pancreas as a tree, Dr. Liddle suggested. The trunk and branches are
the ducts that deliver digestive juices, the leaves the factories that make
digestive enzymes, and the islet cells birds' nests scattered throughout -- in
the tree but not of it.

When cancer strikes, it generally arises in the ductal tissue of the pancreas,
the woody parts of our metaphoric tree, and intriguingly, they feel the part.
''These tumors are rock-hard masses,'' said Peter Olson, an oncology researcher
at the University of California, San Francisco. ''They're white on dissection,
very tough and fibrous.''

Pancreatic cancer is almost impossible to cure. About 34,000 Americans will be
diagnosed with it this year, and nearly as many will die of it. As doctors have
long known, some of that lethality is positional: there is no easy way to screen
the deep-set pancreas for early signs of malignancy, and by the time symptoms
arise, the cancer has already spread to other organs.

Another reason for the ferocity, however, might be the nature of the tumors
themselves. Most cancers are thought to spur the growth of new blood vessels to
supply them with the extra oxygen and nutrients necessary for frenzied cell
division, but pancreatic tumors are markedly devascularized. ''The number of
blood vessels in a pancreatic tumor is 10 percent what it is in normal tissue,
of the pancreas or anywhere else,'' said David A. Tuveson of the Cambridge
Research Institute in England. The results are devastating. In the anoxic
microenvironment beneath the fibrous, bloodless capsule, any malignant cells
that survive become increasingly unstable and virulent, like superroaches
proliferating in the wake of a pesticide bomb. Moreover, without blood vessels,
nothing can get into the tumor to kill the renegade cells, so chemotherapy is
almost useless.

Reporting recently in the journal Science on results with genetically engineered
mice, Dr. Tuveson and his colleagues described a new approach to treating
pancreatic cancer, in which the tumors were revascularized and thus made
sensitive to cancer drugs. Clinical trials are now under way to test the basic
strategy in people, and with all due caveats, Dr. Tuveson said, ''I am
cautiously optimistic.''

URL: http://www.nytimes.com

LOAD-DATE: September 15, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY SERGE BLOCH)

PUBLICATION-TYPE: Newspaper


