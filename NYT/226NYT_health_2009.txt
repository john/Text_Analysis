                   Copyright 2009 The New York Times Company


                             226 of 2021 DOCUMENTS


                               The New York Times

                              July 27, 2009 Monday
                              Late Edition - Final

Competing Brands Of Republicanism

BYLINE: By JOHN HARWOOD

SECTION: Section A; Column 0; National Desk; THE CAUCUS; Pg. 12

LENGTH: 839 words


In this uncertain moment for the party of Lincoln, behold the jaunty, robust
specimen of Republican centrism.

Gov. Arnold Schwarzenegger of California strides into a conference room in his
Sacramento office with a smile, having wrestled down his state's mammoth budget
deficit in a compromise with a Democratic-controlled Legislature.

Public service ''brings me joy'' just as Hollywood stardom did, he declares,
walking a visitor toward the outdoor tent where he shares stogies with fellow
politicians in the search for common ground. As sour as the nation feels toward
most incumbents, tourists still crane to glimpse the Austrian emigre who first
gained fame as a champion bodybuilder.

Now, across the country, see the slight, dour spokesman for orthodox
Republicanism. Senator Mitch McConnell of Kentucky, leader of the shrunken
Republican minority, will never be mistaken for a bodybuilder or a movie star.

In his Southern-accented monotone, the Alabama native urges colleagues on
Capitol Hill to block President Obama's agenda and lower their sights toward
incremental changes in health and energy policy. While Mr. Schwarzenegger
exhorts the White House to ''never give up,'' Mr. McConnell criticizes the
president's push to ''raise taxes in the middle of a recession'' to cover those
without insurance.

Six months after President George W. Bush left the stage, neither brand of
Republicanism fares especially well with the public. But there is no doubt which
holds more midsummer box-office appeal among the party faithful -- and it is not
the Terminator's.

At Center Stage

Combining star power with can-do enthusiasm, Mr. Schwarzenegger seems at first
blush a model for Republican resurgence. His blend of fiscal conservatism and
social moderation produced two lopsided election victories in the blue-state
colossus of California.

''It's important that we are in the center,'' Mr. Schwarzenegger said in an
interview. ''Because that's where the action is.''

Yet that formulation has proved more successful in campaigns than governance.
California's political culture represents an exaggerated version of the
nation's, with starkly polarized parties and rules that inhibit leaders from
acting.

Mr. Schwarzenegger has appealed directly to Californians -- most recently in
springtime ballot initiatives designed to attack the deficit by capping spending
and raising taxes. Voters shouted ''No!'' -- forcing last week's deal to cut
spending on state services so deeply that even the governor declined to declare
victory.

The results have estranged him from both sides, producing Bush-like approval
ratings of under 40 percent. As he prepares to leave office next year, he
sometimes sounds closer to Mr. Obama than his own party.

Mr. Schwarzenegger does not rule out an Obama administration post, and urges the
president to press ahead with a health care overhaul that tracks his own failed
push on the issue in California.

''People say, 'Well, he has taken on too many things,' '' Mr. Schwarzenegger
said of Mr. Obama. ''But if he doesn't take on the too many things, then they
say, 'Well, he hasn't taken on enough.' ''

Contrasting his approach with that of Sarah Palin, the conservative celebrity
who has quit the Alaska governorship, he concluded, ''I would never give up.''

Strength in Numbers

Inside the Capitol, Mr. McConnell has a different Republican message: stick
together in resisting Mr. Obama's ambition.

''We have an example, in my view, of not doing it right, and that was the
hurry-up job on the stimulus,'' Mr. McConnell said in an interview. ''They said
we had to get it done almost immediately. If we didn't do it, unemployment would
go over 8 percent. We did do it; unemployment is now going to go over 10
percent.''

He continued, ''And the assumption that doing health care is going to help the
economy, which the president's been selling, is utter nonsense.''

That message gains traction with every new caution from the Congressional Budget
Office, every tick up in unemployment, every slip in Mr. Obama's approval
ratings. It emboldens a cadre of Senate Republicans -- including Senators
Michael B. Enzi of Wyoming, Charles E. Grassley of Iowa and Olympia J. Snowe of
Maine -- in their protracted negotiations with the Finance Committee chairman,
Senator Max Baucus, Democrat of Montana, on a possible compromise.

''I view them as somewhat like agents of the rest of our conference,'' Mr.
McConnell said. ''They don't want to be part of an agreement that doesn't have
widespread support among Republican senators.''

That is a daunting negotiating burden, given the ideological gulf between the
two parties. And because Republican resistance makes centrist Democrats waver,
too, it means Mr. Obama's ''don't bet against us'' confidence faces a severe
test even if Democrats move ahead without bipartisan support.

''The Senate is a place where it's pretty hard to steamroll the minority,'' said
Mr. McConnell, flexing political muscles that have begun looking unexpectedly
formidable.

URL: http://www.nytimes.com

LOAD-DATE: July 27, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Gov. Arnold Schwarzenegger's centrism in California appeals less
to the Republican Party base than the resistance push of Senator Mitch McConnell
of Kentucky.

PUBLICATION-TYPE: Newspaper


