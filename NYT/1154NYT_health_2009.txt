                   Copyright 2009 The New York Times Company


                             1154 of 2021 DOCUMENTS


                               The New York Times

                           December 10, 2009 Thursday
                              Late Edition - Final

Atheists' Ad Campaign

SECTION: Section A; Column 0; Editorial Desk; LETTER; Pg. 42

LENGTH: 226 words


To the Editor:

Re ''Approaching Holidays Prompt Atheist Campaign'' (news article, Dec. 2): As a
former elected head of the American Humanist Association for 14 years, I am
embarrassed by the A.H.A.'s ''good without God'' campaign of signs on transit
vehicles. Humanists are philosophical naturalists, but more important than
advertising, one item of the humanist worldview is emphasizing the many positive
positions we hold in common with a wide range of religious believers.

I refer to such matters as peace, civil liberties, religious freedom, the
environment, social justice, democracy, women's rights and so on.

Our planetary society does not have the luxury of engaging in angry debates
about philosophy. We, all of us, are faced with immediate problems like global
warming, endless wars, environmental degradation, denial of civil liberties,
widespread economic turndown, misogynistic patriarchalism, the triumph of greed
and selfishness over empathy, unemployment and the need for health care reform.

Progressive and mainstream humanists, Catholics, Protestants, Jews and others of
good will need to concentrate on what unites us, not on what divides us.
Divisive ad campaigns invite blowback and stimulate both ends of the religious
spectrum to engage in fruitless bouts of name-calling and invective.

Edd Doerr Silver Spring, Md., Dec. 2, 2009

URL: http://www.nytimes.com

LOAD-DATE: December 10, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


