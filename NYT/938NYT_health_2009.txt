                   Copyright 2009 The New York Times Company


                             938 of 2021 DOCUMENTS


                               The New York Times

                           November 5, 2009 Thursday
                              Late Edition - Final

Democrats in Congress See Election as Giving New Urgency to Their Agenda

BYLINE: By CARL HULSE

SECTION: Section A; Column 0; National Desk; Pg. 25

LENGTH: 898 words

DATELINE: WASHINGTON


Blaming election setbacks on a drop in voter enthusiasm, Congressional Democrats
said Wednesday that losses in governors' races in Virginia and New Jersey -- and
a striking House win in New York -- should give new urgency to their legislative
agenda, including a sweeping health care overhaul.

As they assessed the results, Democratic lawmakers and party strategists said
their judgment was that voters remained very uneasy about the economy and did
not see Democrats producing on the health, energy and national security changes
they promised when voters swept them to power only a year ago.

''Most of us ran on that,'' said Representative Gerald E. Connolly, Democrat of
Virginia and president of the party's freshman class. ''We must deliver. I need
to give Democrats something to be excited about.''

Democrats still face the political dilemma that has dogged their health care
efforts this year and will continue to face tough choices as they take up issues
like curbing global warming. Should they concentrate on motivating their core
supporters, many of whom appeared to stay away from the polls in New Jersey and
Virginia, by taking a more liberal approach, for example by embracing a public
health insurance option?

Or do they try to write health, energy and fiscal policies that can attract
independents, who broke for Republicans in Tuesday's voting, in order to protect
more moderate Democrats in conservative districts where spending and the rising
debt are top worries?

''We have to do something, but it has to be right,'' said Representative Frank
Kratovil Jr., a first-term Maryland Democrat who won his seat away from a
Republican last year. ''My biggest concern is cost. The impact on the deficit is
a real problem.''

Republicans portrayed the election outcome as a repudiation of Democratic
policies and predicted significant Congressional gains next year despite
Tuesday's embarrassing loss in a longtime House Republican stronghold in upstate
New York.

''If we can continue to stand on principle and oppose these liberal policies and
show the American people that we have better solutions that are common sense
solutions, we are going to do fine,'' Representative John A. Boehner of Ohio,
the Republican leader, said. He predicted that the Democratic rank-and-file,
fearing for their political futures, would press its leadership to rethink the
party's direction.

Yet several Democratic lawmakers said the election could have the opposite
result and spur Democrats on in an effort to recapture the coalition of young,
minority, urban and suburban Democrats and independents who were central to the
2008 election victories.

''I think the issues of health care and energy independence are important issues
the president has outlined early on, and those are issues we ought to focus
on,'' said Representative Allen Boyd, a centrist Florida Democrat.

From a purely Congressional perspective, Tuesday was a positive night for
Democrats as they retained a California seat in a special election and picked up
the seat in upstate New York partly as a result of a Republican Party feud. The
winner of that race, Bill Owens, has already assured Speaker Nancy Pelosi of
California that he will support the party health care proposal that could reach
the floor this weekend, aides said.

Mr. Owens and John Garamendi, a California Democrat who won a House seat in the
Bay Area vacated by a Democrat, could be sworn in as early as Thursday, bringing
the party breakdown in the House to 258 to 177 in favor of Democrats, a net
increase of one.

''This was a victory for health care reform,'' Ms. Pelosi said. ''From our
standpoint, we picked up votes last night.''

While not discounting the Republican wins in Virginia and New Jersey, Democrats
said the New York and California House races were the only contests that
centered on Congressional issues and Democrats won both despite months of
Republican attacks on the legislative priorities of President Obama and
Congressional Democrats.

''The governors of Virginia and New Jersey don't have a vote on the Obama
agenda,'' said Representative Chris Van Hollen of Maryland, chairman of the
Democratic Congressional Campaign Committee.

Yet there were ominous signs for Congressional Democrats in the results, notably
in Virginia, where Democrats picked up three Republican seats last year and
acknowledge that they will have difficulty holding on to them. Republicans noted
that in two of those freshman districts, the Republican victor for governor,
Robert F. McDonnell, won by a more than 20-point margin.

''The election in New York may provide a momentary victory for Democrats, but
the results in two gubernatorial contests tell us more about what 2010 holds in
store for the party in power,'' said Representative Pete Sessions of Texas, the
chairman of the National Republican Congressional Committee.

In the Senate, some Democrats said the voting  on Tuesday was likely to give
some lawmakers from more conservative parts of the country further pause in
considering how to vote on health care. But Democrats in both chambers said that
their views on how to vote on the issue were being shaped by multiple factors
and that the election results would play little role.

''My decision is based on the substance of the bill and how it affects my
constituents and nothing else,'' said Mr. Boyd, the Florida Democrat.

URL: http://www.nytimes.com

LOAD-DATE: November 5, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Representative Gerald E. Connolly of Virginia, a freshman
Democrat, said, ''We must deliver.''(PHOTOGRAPH BY BRENDAN SMIALOWSKI FOR THE
NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


