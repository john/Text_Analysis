                   Copyright 2009 The New York Times Company


                             1050 of 2021 DOCUMENTS


                               The New York Times

                            November 22, 2009 Sunday
                              Late Edition - Final

House Call

BYLINE: By DEBORAH SOLOMON

SECTION: Section MM; Column 0; Magazine Desk; QUESTIONS FOR JOSEPH CAO; Pg. 14

LENGTH: 632 words


A first-term congressman from New Orleans, you are the one and only Republican
in the House who voted in favor of the Democrats' health care plan. Have you
been scolded by the Republicans for breaking rank?  Not my party leadership, but
only by Republicans all over the United States who are disappointed with my
vote. But not at all from the party leadership.

So not by Eric Cantor, for instance, the House minority whip, who, just days
before the vote, claimed at a rally that ''not one Republican'' would support
the bill?   Correct. Actually, I just had lunch with Eric Cantor down here in
New Orleans, and he was very supportive of who I am and what I have to do to
represent the district. He fully understands the politics of the district.

Did you support the bill to curry favor with your constituents? You represent a
mostly black district that is among the poorest in the nation.   This is a
personal position of mine. I do believe that we need health care reform. I do
believe that we as a government have a duty to help those who are in need but
who cannot help themselves.

So you're saying you voted out of personal conviction, not politics?    Correct.
I spent six years in the Society of Jesus, training to be a priest. I always
adhere to what I call ''the politics of the Gospel.'' You have to take care of
the poor, take care of the widows, visit the sick, help those who cannot help
themselves.

Why did you become a Republican?    Because of their strong pro-life stance.
That alone.

Do you find it awkward being a Republican whose ideas on both economic and
social policy diverge from those of your party?    It's not awkward because
we're all professionals. We know that we are a diverse group.

Republicans need you precisely because they're not a diverse group. They like
having you because you're not another pale white face.   I believe that they
like me for who I am, not because they want the Republican Party to look
something different than just white.

Of the 177 Republicans in the House, are there any other Asian-Americans?
There's one, Steve Austria. He's from Ohio. He is a Filipino-American.

You were born in Saigon, where your father, a soldier, was taken prisoner by the
Communists, and your mother arranged to send you off to the States when you were
8 years old.   My mother decided to send her only two boys because she feared
that if we were to grow up in Vietnam, we would be drafted into the military by
the Communists and probably sent off fighting some war and get killed. My
parents came over to the U.S. in 1991, 15 years after I did.

You lived with an uncle in Indiana. What was school like for you?   Instead of
going to the third grade, they actually put me back in first. I was two years
older than all of my classmates.

Was that difficult?    No, because I am petite and I looked quite young. I'm
5-foot-2. Now I look 18, even though I'm 42.

Have you been back to Vietnam?  1994 was the first time. Then in 2001 after I
got married.

On your honeymoon?  Yes, which was a bad mistake. We stayed with relatives, and
it was noisy, it was hot, kids were coming, people were coming, we didn't have
any quiet time together.

What is it like in South Vietnam now?   Well, economically people are a lot
better off, but there's still a great deal of religious persecution, human
rights violations. I am working very hard to change that.

You were in New Orleans when Katrina struck.   We had eight feet of water in the
house. I decided to run for office because I thought that I could contribute to
rebuilding this area.

You won your House seat when you defeated William Jefferson, who had been
accused of hiding bribe money in his freezer.   Correct.

Louisiana is known for corruption.   Not anymore. We are squeaky clean.

URL: http://www.nytimes.com

LOAD-DATE: November 22, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO (PHOTOGRAPH BY BRADY FONTENOT)

DOCUMENT-TYPE: Interview

PUBLICATION-TYPE: Newspaper


