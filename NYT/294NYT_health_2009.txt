                   Copyright 2009 The New York Times Company


                             294 of 2021 DOCUMENTS


                               The New York Times

                           August 12, 2009 Wednesday
                              Correction Appended
                              Late Edition - Final

Senator Goes Face to Face With Dissent

BYLINE: By IAN URBINA and KATHARINE Q. SEELYE; Sean D. Hamill contributed
reporting.

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1058 words

DATELINE: LEBANON, Pa.


They got up before dawn in large numbers with angry signs and American flag
T-shirts, and many were seething with frustration at issues that went far beyond
overhauling health care.

More than 1,000 people showed up here Tuesday morning in this largely Republican
town in central Pennsylvania for a town-hall-style meeting with Senator Arlen
Specter, though the auditorium could seat only 250. Like many of the dozens of
such meetings held by members of Congress over the last few weeks, this one was
punctuated with rowdy moments, and interviews with many of those who showed up
made it clear just how much underlying dissent motivated them.

Many said the Obama administration's plans for a new health care system were
just another example of a federal government that had again gone too far, just
as it had, they said, with the economic stimulus, the auto industry bailout and
the cap-and-trade program.

''This is about the dismantling of this country,'' Katy Abram, 35, shouted at
Mr. Specter, drawing one of the most prolonged rounds of applause. ''We don't
want this country to turn into Russia.''

Ms. Abram described herself as a stay-at-home mother from Lebanon, and in many
ways she was representative of the almost entirely white and irritable crowd,
most of whom were from the area. Based on interviews with several dozen people
who attended, it appeared that about 80 percent of those who showed up opposed
the planned changes to the health care system.

Many said they heard about the meeting from e-mail alerts sent by conservative
and antitax groups like the Constitutional Organization of Liberty and the Berks
County Tea Party, along with Mr. Specter's own mailings.  Some voiced sentiments
that were heard recently on  conservative radio shows, though those interviewed
said they resented being characterized as mobs or puppets of lobbyists,
emphasizing that they represented only themselves. ''I demand my voice!'' read
one sign outside. ''You work for me,'' was a  refrain yelled inside the
auditorium.

At the same time, those who favor a health care overhaul, urged to attend by
unions and liberal groups like the Service Employees International Union and
Health Care for America Now, said they were motivated by concern that the
government might not go far enough. Only the government, they say, can take on a
problem as big as  health care.

But in the end, their ability to ask a question at the meeting depended on how
early they got in line. Many of the union members who showed up to support
health care reform did not arrive early enough to get into the auditorium at the
Harrisburg Area Community College, and thus were largely not represented among
the 30 questioners called on by Mr. Specter. It was the angriest people who got
in line first.

''All union members to the back, I got here early,'' one man  in the line told
latecomers.

John Stahl, chairman of the Berks County Tea Party, a local branch of
conservatives, was one of those who helped recruit opponents of change to the
event. A former truck salesman, Mr. Stahl, 65, said he was laid off about 18
months ago. Since May, his group has organized four protests in the state
opposing taxes and the stimulus plan, but none have attracted the crowds like
health care, he said.

''We believe there are several issues out there that leave the existence of the
Republic at risk,'' he added, ''not the least of which is this Obamacare.''

The meeting came just over a week after Mr. Specter and Health and Human
Services Secretary Kathleen Sebelius were booed and jeered  at a meeting in
Philadelphia on Aug. 3.

Hoping to avoid similar unrest, Mr. Specter tried to control the event by
imposing a rigid format. Only the first 30 people who wanted to speak were given
cards allowing them to ask questions. He allotted 90 minutes for the meeting and
was careful to let people speak their piece. He gave succinct answers before
moving on to the next question. Because of concerns about a potentially unruly
crowd, the Capitol Police sent three extra officers from Washington.

In addition, Mr. Specter and his staff controlled the microphones. And he stood
face to face with his questioners,  a move, he said later in an interview, that
he had hoped might make it harder for people to scream at him.

But for all his efforts, tempers boiled over 15 minutes into the meeting.
Standing two feet from the senator, Craig Anthony Miller, 59, shouted, ''You are
trampling on our Constitution!'' A half-dozen security people quickly swarmed
but refrained from touching him as Mr. Specter, raising his voice, said sternly,
''Wait a minute! Wait a minute!'' He said the man had the right to leave.

Mr. Miller, shaking, stood his ground. He said he was furious that the senator's
staff had limited the questioning. ''One day,'' he said to loud applause, ''God
is going to stand before you, and he's going to judge you!''

Mr. Specter shouted into his microphone that demonstrators disrupting the
proceedings would be thrown out.

The meetings come at a vulnerable moment for Mr. Specter, who faces what could
be a tight Democratic primary next spring. While those on the right excoriated
him at the meeting for betrayal because he switched parties in April, he has
also been attacked by his opponent in the primary, Representative Joe Sestak, as
being a Republican aligned with former President George W. Bush.

But most of those who spoke Tuesday seemed unlikely to vote in the Democratic
primary. Many seemed concerned about issues that are either not in the health
care legislation or are peripheral to the debate in Washington -- abortion,
euthanasia, coverage of immigrants, privacy.

''It says plainly right there they want to limit the type of care elderly can
get,'' said Laurel Tobias, an office manager from Lebanon, referring to a bill
in the House. ''They are talking about killing people.''

Standing by a bus that takes her from meeting to meeting, Amy Menefee,
spokeswoman for Americans for Prosperity, said the real issue was the expansion
of government favored by President Obama. Proponents of the overhaul voiced the
opposite fear, also citing larger issues at stake.

''This isn't just about health care,'' said Carolyn Doric of Harrisburg, ''it's
about political power and a means to regain political power.'' Ms. Doric did not
get into the meeting.

URL: http://www.nytimes.com

LOAD-DATE: August 12, 2009

LANGUAGE: ENGLISH

CORRECTION-DATE: August 13, 2009



CORRECTION: An article on Wednesday about a town hall meeting on health care in
Pennsylvania presided over by Senator Arlen Specter described remarks by one
attendee, Katy Abram, incorrectly. When she told Senator Specter, ''This is
about the dismantling of this country,'' she was speaking in a forceful voice,
but she was not shouting. (At another point in her comments, she did shout.)

GRAPHIC: PHOTOS: ''You are trampling on our Constitution!'' Craig Anthony Miller
shouted at Senator Arlen Specter at a meeting in Lebanon, Pa.(PHOTOGRAPH BY
DAMON WINTER/THE NEW YORK TIMES)(pg. A1)
Nancy Gusti, 73, top, supported Senator Arlen Specter on Tuesday, while Richard
Hoke, above left, and Robert Nasuti argued.(PHOTOGRAPHS BY JESSICA KOURKOUNIS
FOR THE NEW YORK TIMES)
After a loud, rowdy meeting with 250 people in an auditorium, Mr. Specter exited
to an even larger crowd gathered outside.(PHOTOGRAPH BY DAMON WINTER/THE NEW
YORK TIMES)(pg. A14) CHART: Frequently Asked Questions: Answers to some of the
questions that have been asked at public meetings on health care.(Sources:
Congressional bills
 PolitiFact)(pg. A14)

PUBLICATION-TYPE: Newspaper


