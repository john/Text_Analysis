                   Copyright 2009 The New York Times Company


                             563 of 2021 DOCUMENTS


                               The New York Times

                          September 12, 2009 Saturday
                              Late Edition - Final

Putting America on a Healthier Diet

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 20

LENGTH: 915 words


To the Editor:

Re ''Big Food vs. Big Insurance,'' by Michael Pollan (Op-Ed, Sept. 10):

Mr. Pollan rightly contends that health care reform will be ineffective unless
the country's increasing obesity problem is addressed. But because the food
industry is only part of the problem, reforming it is only part of the solution.

The other part of the problem is the American consumer. While food producers
provide an array of unhealthy fare, how, what and when we eat are personal
choices.

Mr. Pollan praises attempts to tax sugary sodas because these products add empty
calories to our diets, particularly for our youth. Yet sugar-free sodas have
been available and widely consumed for 40 years. The choice is the consumer's.

If we are to make headway on this issue, we must have comprehensive physical
education and health education in our schools and incentives supporting healthy,
active lifestyles and nutritional food choices for all citizens.

Like all industries, the food producers are driven by their bottom line. Only
when consumers begin to demand healthier food will the industry change.

Anne-Marie Hislop  Davenport, Iowa, Sept.  10, 2009

To the Editor:

I applaud Michael Pollan's recognition that obesity is the ''elephant in the
room'' in the health care debate, but dissent on his solutions.

Taxing specific products such as soft drinks or creating yet another educational
program will not get the job done. Multiple studies have demonstrated that
''fat'' taxes will not appreciably lower obesity rates, while attempts to change
consumer eating behavior have historically come up short.

The real enemy is the number of excess calories available for consumption,
regardless of the source. The only way to slim down this beast is to engage the
food industry.

Rather than alienate or overregulate the industry, my recommendation is to put
into effect tax incentives that would entice food companies to sell fewer
calories. If they cut their calories, they would be rewarded. If they continued
to spew excess calories on the public, they would risk losing favorable tax
treatments.

This approach is well worth discussing. Our nation's health depends on it.

Henry J. Cardello  Chapel Hill, N.C., Sept.   10, 2009

The writer is a former food industry executive and author of ''Stuffed: An
Insider's Look at Who's (Really) Making America Fat.''

To the Editor:

Eating well and exercising are important, but not necessarily a panacea against
disease.

I am a 55-year-old woman who is slim, eats a healthy organic diet, takes ballet
classes and practices yoga on a weekly basis.

I had breast cancer in 2003 and learned I had Stage 4 tonsil cancer in 2008. My
out-of-the-pocket costs for my recent treatment for tonsil cancer totaled
$15,000.

As part of my follow-up care, I need thousands of dollars of dental work, plus
expensive magnetic resonance imaging every six months for the next three years.
My monthly health insurance premium, for me alone, has gone up to $662.

Michael Pollan is correct in targeting agribusiness for contributing to obesity,
but he does a grave disservice to me, and Americans in general, when he links
the dire consequences of not having strong and meaningful health care reform
with the honorable, but separate, issue of food industry reform.

Francesca Pastine  San Francisco, Sept.  10, 2009

To the Editor:

As a big fan of Michael Pollan, I was delighted to read ''Big Food vs. Big
Insurance.''

I am 65, look 50, and weigh 10 pounds more than when I graduated from high
school, where I lettered in two sports. I work out three or four times a week,
recently added two weekly yoga classes, take stairs whenever possible and have
no major health issues.

My ''diet'' is to eat as much as I need, and no more. If my weight is up a
little any morning, I just eat less that day.

My wife and I usually split the massive entrees at restaurants, we eat very
little meat, and our snacks are fruits and nuts. And yes, I indulge -- with a
little delicious dark chocolate and low-fat ice cream every day.

I don't eat junk food or buy the soft drinks and other reconstituted muck that
American agribusiness currently substitutes for real food.

When Americans demand that restaurants and agribusiness put our health first, I
will no longer be unusual.

James G. Goodale  Houston, Sept.  10, 2009

To the Editor:

Michael Pollan's essay on the role of the food industry in contributing to
obesity and associated chronic diseases may have some merit, but only because
too many consumers make poor dietary choices, meal after meal, day after day.

Are we really going to blame the food industry for providing foods we enjoy but
overindulge in? When did personal responsibility go out the window?

Most of us like a good hamburger with all the ''fixings,'' maybe even fries and
a shake with it. But is the provider to blame when we consume them day after
day, and couple this with other food choices that are high in calories and fat,
with little or no exercise to offset these poor dietary choices?

The old saying that there are no good foods or bad foods, only good or bad
diets, is still relevant.

Rather than play the blame game, we should direct our efforts at better
educating consumers on the importance of balancing caloric intake with energy
output.

Taxing or blaming the food industry may add more money to the government
coffers, and make some feel better, but it has no public benefit.

James Stanley  Jasper, Ga., Sept.  10, 2009

URL: http://www.nytimes.com

LOAD-DATE: September 12, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY ERIC HANSON)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


