                   Copyright 2009 The New York Times Company


                             862 of 2021 DOCUMENTS


                               The New York Times

                           October 21, 2009 Wednesday
                              Late Edition - Final

A Plan for Higher Payments

BYLINE: By MILT FREUDENHEIM

SECTION: Section A; Column 0; National Desk; Pg. 24

LENGTH: 249 words


The Senate Finance Committee has quietly recommended that the millions of
elderly Americans who buy so-called Medigap insurance plans be charged new
co-payments for doctors' visits starting in 2015.

Consumer advocates predict that the changes, proposed in the committee's
1,500-page health care bill, will meet stiff opposition from lobbyists for the
elderly  as the final version of the legislation takes shape.

Traditional Medicare pays about 60 percent, on average, of enrollees' medical
bills, so millions of enrollees buy Medigap plans that offer complete ''first
dollar'' coverage for some services, picking up all of what an enrollee  would
otherwise have to pay out-of-pocket.

''Seniors really like 'first dollar' coverage,'' said James P. Firman, president
of the National Council on Aging, a consumer advocacy group. ''They don't want
to deal with extra paperwork and arguments over bills.''

The new co-payments are intended to push elderly patients to think twice before
consulting their doctors. Some studies have found that Medigap policyholders use
at least 25 percent more health care services than the generally lower-income
Medicare enrollees who do not have Medigap policies.

Mr. Firman said he favored alerting enrollees to the real cost of
Medicare-covered treatments, which are mostly paid by the program, through
taxes, deductibles and premiums. But as for the co-pay proposal, he said, ''I
think the consumer backlash to co-pays will be significant.''

MILT FREUDENHEIM

URL: http://www.nytimes.com

LOAD-DATE: October 21, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


