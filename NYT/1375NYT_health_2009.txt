                   Copyright 2010 The New York Times Company


                             1375 of 2021 DOCUMENTS


                               The New York Times

                            January 19, 2010 Tuesday
                              Late Edition - Final

After Career as Their Advocate, Coakley May Face Voters' Wrath

BYLINE: By LIZ ROBBINS

SECTION: Section A; Column 0; National Desk; Pg. 20

LENGTH: 588 words


Even during a fierce campaign for Senate, Martha Coakley speaks with quiet
fervor, a serious woman who has been arguing issues since she was a standout on
her Western Massachusetts high school debate team.

Ms. Coakley, the state's attorney general, gained international recognition as a
methodical county prosecutor during the 1997 trial of Louise Woodward, a British
au pair convicted of killing a baby boy in her care. Her composed television
appearances helped her become the first woman elected district attorney in
Middlesex County, the state's most populous, a year later. In 2006, just as
easily, she swept the race for attorney general. Since then, she has won
settlements from Boston's Big Dig contractors and from Wall Street firms that
engaged in deceptive practices.

A straightforward progressive on issues from abortion rights to same-sex
marriage to the environment, Ms. Coakley, 56, has said she will be the 60th vote
in the Senate in favor of health care legislation if she wins the seat long held
by Edward M. Kennedy in Tuesday's special election.

Ms. Coakley captured 47 percent of the vote in the Dec. 9 Democratic primary
against three opponents. She was seen as rarely making a misstep, but since then
she has been criticized for running a lackluster campaign until polls started
showing her Republican opponent, Scott Brown, a state senator, was galvanizing
independent voters.

President Obama and Mr. Kennedy's widow, Victoria Reggie Kennedy, have made
impassioned late pleas on her behalf in the past three days. Ms. Coakley told a
crowd in Pittsfield on Monday that the special election was not just about
health care, but about the future of the country.

Mr. Brown appears to be her polar opposite in personality and politics. Ms.
Coakley, who wears crisp pantsuits and short, layered blond hair, has been
called cool and guarded, talking little about her personal life other than her
marriage, at age 47, to a former Cambridge deputy police superintendent. She
grew up in a Roman Catholic family in blue-collar North Adams, a daughter of an
insurance salesman. She graduated from Williams College and from Boston
University law school and joined the Middlesex district attorney's office at 33.

In her bid to become the first woman elected to the United States Senate from
Massachusetts, Ms. Coakley has thrown her support behind the proposed health
care overhaul, the issue that has given this special election a national focus.
She has said, though, that she supports a public option to encourage competition
and reduce costs.

As attorney general, Ms. Coakley investigated subprime lending practices and
helped provide relief for Massachusetts homeowners beset by foreclosures. In
2009, Goldman Sachs agreed to pay up to $60 million to end an inquiry by her
office into whether the firm helped promote unfair home loans in the state.

Ms. Coakley supports President Obama's proposal to tax financial institutions to
recoup taxpayers' investments and would vote to end tax cuts that favor wealthy
Americans.

However, Ms. Coakley has said she does not support Mr. Obama's decision to send
30,000 troops to Afghanistan, saying instead that Afghan leadership must be
strengthened.

On civil liberties issues, Ms. Coakley was the first state attorney general to
sue the federal government to overturn a section of the 1996 Defense of Marriage
Act, which defines marriage as being between a man and woman. She also opposes
the death penalty and advocates civilian trials for terrorism suspects.

URL: http://www.nytimes.com

LOAD-DATE: January 19, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Alex Okrent of Washington, a volunteer for Martha Coakley,
scouring a precinct map in Boston. (PHOTOGRAPH BY: GRETCHEN ERTL FOR THE NEW
YORK TIMES)

PUBLICATION-TYPE: Newspaper


