                   Copyright 2009 The New York Times Company


                             1175 of 2021 DOCUMENTS


                               The New York Times

                            December 13, 2009 Sunday
                              Correction Appended
                              Late Edition - Final

SECTION: Section WK; Column 0; Week in Review Desk; WELL, THAT'S SETTLED, THEN
DEC. 6-12; Pg. 2

LENGTH: 492 words


THE NEWS The federal government said it would pay $3.4 billion to settle claims
that for decades the Interior Department shortchanged 384,000 American Indians
of revenue from their land.

BEHIND THE NEWS The deal ends 13 years of litigation, including 10 trips through
federal appeals courts. The dispute stems from an 1887 move by Congress to
divide some tribal lands among individual Indian owners and then lease out
timber and mineral rights on their behalf. A lead plaintiff, Elouise Cobell,
said she believed that the Indians were owed more, but that it was better to
settle than spend more years in court.

THE NEWS The decade of 2000 to 2009 appears to have been the warmest one in the
modern record, the World Meteorological Organization reported.

BEHIND THE NEWS The agency's annual analysis, issued earlier than usual to
coincide with the second day of global climate talks in Copenhagen, rebutted
renewed challenges from skeptics over the scientific basis for concerns about
global warming. Though there were fluctuations from year to year, over all, the
last 10 years were ''warmer than the 1990s, which were warmer than the 1980s,
and so on,'' said Michel Jarraud, the chief of the agency.

THE NEWS After a hotly contested Democratic primary, an inconclusive
six-candidate general election, a runoff between the two top vote-getters and
then a recount of the runoff, officials declared that Kasim Reed, a lawyer and
former state legislator, had won the race for mayor of Atlanta.

BEHIND THE NEWS Though Mary Norwood, a city councilwoman who ran as an
independent, outpolled Mr. Reed by nearly 7,600 votes on Election Day, he beat
her by a 714-vote margin in the Dec. 1 runoff, which, unusually, featured even
higher voter turnout than the general election did.

THE NEWSJenny Sanford filed  for divorce from her husband, Gov. Mark Sanford of
South Carolina.

BEHIND THE NEWS The filing put an end to what Ms. Sanford said were many
attempts at reconciliation over nearly six months after Mr. Sanford admitted at
a teary news conference in June that he had been carrying on an affair with a
woman in Argentina. A state legislative subcommittee looking into ethics charges
against Mr. Sanford voted  to recommend censuring him for bringing ''ridicule,
dishonor, disgrace and shame'' on South Carolina, but not to recommend
impeachment.

THE NEWS The Senate majority leader, Harry Reid of Nevada, announced a deal
among liberal and centrist Democrats to resolve one of the health-care
overhaul's most intractable issues: a government-run health plan to compete with
private insurers.

BEHIND THE NEWS The deal calls for privately run national insurance plans akin
to those for federal workers, plus a chance for people 55 to 64 to buy Medicare
coverage, with the ''public option'' as a backup if goals aren't met. The
compromise satisfied no one fully, but Democrats said it had a better chance of
passage than the straight public option.

URL: http://www.nytimes.com

LOAD-DATE: December 13, 2009

LANGUAGE: ENGLISH

CORRECTION-DATE: December 20, 2009



CORRECTION: A capsule summary last Sunday about the mayoral race in Atlanta
misstated the events leading to the Dec. 1 runoff. The winner, Kasim Reed,
outpolled other Democrats in the general election, not in a party primary;
mayoral elections there are nonpartisan, with no primaries. (Go to Article)

GRAPHIC: PHOTOS (PHOTOGRAPHS BY BRENDAN SMIALOWSKI/AGENCE FRANCE-PRESSE -- GETTY
IMAGES
JOHN MCCONNICO/ASSOCIATED PRESS
DAVID WALTER BANKS FOR THE NEW YORK TIMES
ALICE KEENEY/ASSOCIATED PRESS
 LUKE SHARRETT/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


