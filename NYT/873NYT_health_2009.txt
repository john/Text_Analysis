                   Copyright 2009 The New York Times Company


                             873 of 2021 DOCUMENTS


                               The New York Times

                            October 23, 2009 Friday
                              Late Edition - Final

Skepticism About Overhaul

BYLINE: By DALIA SUSSMAN

SECTION: Section A; Column 0; National Desk; WE THE PATIENTS; Pg. 25

LENGTH: 214 words


Americans are increasingly gloomy about the costs and quality of their health
care if Congress passes overhaul  legislation, according to the latest USA
Today/Gallup poll.

The survey results underscore the political challenges for supporters of a
health care overhaul, as few Americans see benefits for themselves in changing
the system.

Only about 2 in 10 expect the quality and costs of their health care to improve.
And the 49 percent who now predict that their costs would rise  is up seven
percentage points since the same question was asked in September. The 39 percent
who now say the quality of their care would be worse is up six points since last
month. And 37 percent say their coverage would get worse, compared with 33
percent a month ago.

But as Gallup's news release about the poll notes, it is unclear whether
supporters of a health care overhaul will need to convince Americans that their
own costs and care would improve -- or  if it would be enough to convince them
that their health care would not get worse, as Congress tries to extend coverage
to tens of millions of people who have none.

The USA Today/Gallup poll was conducted Oct. 16 to 19 with 1,521 adults
nationwide. The margin of sampling error is plus or minus three percentage
points.

DALIA SUSSMAN

URL: http://www.nytimes.com

LOAD-DATE: October 23, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


