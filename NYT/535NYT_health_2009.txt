                   Copyright 2009 The New York Times Company


                             535 of 2021 DOCUMENTS


                               The New York Times

                          September 10, 2009 Thursday
                              Late Edition - Final

SECTION: Section A; Column 0; National Desk; SOME QUALMS REMAIN; Pg. 26

LENGTH: 242 words


Elaine H. Carl, 72, who lives at the Sunrise Lakes retirement village in
Sunrise, Fla., said she liked ''part of his speech'' but did not think that
Americans should be required to buy insurance.

Ms. Carl said some of her fears about Medicare had been at least temporarily
soothed. ''I do believe what he's saying, that nothing will be taken from us,''
she wrote. ''But I don't know; I still have my qualms. It's so easy to say one
thing and then turn around and do another. People have been hurt so many times
by listening to what has been promised, and they end up in a sinkhole. I'm still
very much afraid. I don't know where the money is going to come from to pay for
all of these things.''

She said she also was not convinced there would not be rationing of care. ''I
still don't believe it,'' she wrote, ''because I feel if a person reaches a
certain age and they have an incurable disease they're going to let them lay
there like a dog.''

The Public Response: During the August Congressional recess, Kevin Sack, a New
York Times national correspondent, traveled from South Florida to Washington
State to sample opinion about health care overhaul from a variety of
stakeholders, including elderly Americans, insurance company workers and medical
students. On Wednesday night, The Times asked some of those he had encountered
for their views on President Obama's speech to a joint session of Congress. Here
is a sampling of their e-mail replies.

URL: http://www.nytimes.com

LOAD-DATE: September 10, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO (PHOTOGRAPH BY MICHAEL F. McELROY FOR THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


