                   Copyright 2010 The New York Times Company


                             1956 of 2021 DOCUMENTS


                               The New York Times

                              May 4, 2010 Tuesday
                              Late Edition - Final

THE NEW LANDSCAPE; Parsing the New Law on Long-Term Care

BYLINE: By PAULA SPAN

SECTION: Section D; Column 0; Science Desk; Pg. 6

LENGTH: 1197 words


Call it Senator Edward M. Kennedy's final bequest.

The Community Living Assistance Services and Supports Act, or Class Act, the
first national plan to help the great majority of Americans who have no
insurance for long-term care, became law in March. Even though there was little
fanfare --  the measure was just one piece of the broader health care overhaul
-- the idea had been  hugely important to Mr. Kennedy and his staff, who  had
been working on the current version of the plan since 2003.

The program underwent a number of changes during the health care debate, so some
projections are now out of date. And many  details have yet to be established by
the secretary of health and human services, Kathleen Sebelius, who is going to
be one busy woman for the next couple of years.

But we do know enough to answer some basic questions. Four experts helped
respond to some of the many queries from readers on the New Old Age blog. They
are Howard Bedlin, vice president for advocacy at the National Council on Aging;
Barbara Manard, a health economist with  the American Association of Homes and
Services for the Aging; Jesse Slome, executive director of the American
Association for Long-Term Care Insurance; and Connie Garner, who after 17 years
is about to leave her job on the Senate Health, Education, Labor and Pensions
Committee.

WHO'S ELIGIBLE?  The simplest answer: Working people. Not those who are already
retired (unless they continue to work part time), or  nonworking spouses, or the
unemployed. Participants must pay premiums for a vesting period of  five years
before they can receive benefits, and they have to continue working for three of
those years.

Part-time workers are eligible, though they must earn enough each year to pay
Social Security taxes, a threshold that is now about $1,200. The self-employed
and anyone whose employer declines to offer Class Act coverage will also be able
to sign up through a mechanism yet to be determined.

WHAT IF YOU ALREADY HAVE AN ILLNESS OR DISABILITY?  You can still enroll, as
long as you can work for three years and pay premiums for five. The  law
prohibits excluding people  with pre-existing conditions like  diabetes.

WHEN DOES IT START?  The act takes effect next Jan. 1, but the health and human
services secretary has until October 2012 to present the full rules. Experts
expect  enrollment to begin in 2013.

WHAT WILL IT COST?  We're all waiting for Ms. Sebelius and a panel to tell us.
In November, the Congressional Budget Office assumed an average monthly premium
of $123 (less for young enrollees, more for older ones).

That  estimate is based on the assumption that only 5 to 6 percent of those
eligible will participate. But that assumption may be conservative: Class is an
''opt out'' program,  meaning that if your employer participates, you're
automatically included unless you decline.  Premiums will be lower if there are
more people in the risk pool.

WHAT WILL IT PROVIDE? A cash benefit, to be determined by Ms. Sebelius. The
Congressional Budget Office assumed a cash benefit of $75 a day, available once
a participant needed help with two to three activities of daily living (eating,
bathing, dressing, using the toilet, transferring from bed to chair to
wheelchair, continence care) or the equivalent amount of assistance required
because of cognitive impairment. The law says the average minimum benefit must
be at least $50 a day. Once you qualify, those benefits continue as long as you
need care. Remember that about 40 percent of the people receiving long-term care
are not elderly; they're young people who have accidents or develop chronic
illnesses, so a lifetime benefit can mean many years of payments. And yes, those
benefits will rise with inflation.

Isn't that -- as Laurs2 from Florida commented in the blog --  a ''pitifully
inadequate'' amount?

The Class Act was never intended to pay the full cost of 24-hour home care or a
nursing home. But many disabled people can stay out of nursing homes, or delay
admission, with moderate levels of assistance that supplement family care.

Even $50 a day could bring in a home care aide for a few hours to help with
bathing, laundry or meals. It would also pay most of the cost of an adult day
program ($67 a day). Nationally, the average cost of an assisted-living facility
was $37,572 last year, according to the annual MetLife Market Survey; a $75
daily benefit would pay almost three-quarters of that.

HOW WILL IT BE PAID FOR? WON'T PREMIUMS AND COSTS INEVITABLY SOAR?  The law
specifically states that no tax dollars shall be used to support it; the
premiums it takes in must pay for the benefits it pays out.

Since this represents the country's first effort at public long-term-care
insurance, we can't say with assurance whether the optimists who helped get the
law passed or the pessimists who tried to derail it are correct.

''It hasn't been tried,'' said Ms. Manard, the  economist. ''But what we have
tried isn't working. We have to try something new.''

WHAT IF SICK AND OLD PEOPLE SIGN UP FOR COVERAGE BUT YOUNG AND HEALTHY PEOPLE
DON'T?  This phenomenon, known as adverse selection, is a legitimate worry.
People whom no private insurer would accept --  who already have diagnoses of,
say, multiple sclerosis but can work part-time for three years -- are sure to
jump in. They'd be foolish not to. But that is a problem only if too few others
sign up and pay premiums.

So a marketing campaign for the Class Act will take aim at the young and the
healthy -- many of whom will also unexpectedly need long-term care insurance.

Mr. Slome, who heads the insurance industry group, says that the law is a Trojan
horse, that the government will eventually convert it into a tax-financed
entitlement program. ''Inevitably, it will morph into Medicare Part E,'' he
said. Though the law prohibits the use of tax dollars, Mr. Slome said, ''If
you're 30 today and you think the rules aren't going to change in 50 years,
you're living in a state that's legalized marijuana.''

WHAT'S THE OUTLOOK?  Long-term care insurance rarely comes cheap. Either you buy
it earlier in life and pay less per year -- but for more years -- or you buy it
later in life and pay much higher premiums. But it's far more expensive not to
have it. Two-thirds of older Americans will need long-term care, the best
projections indicate.

In a way, the Class Act will give the country an option as appealing or as
unattractive as we deserve. If we understand that paying for long-term care is
largely a personal responsibility, then large numbers of people will buy
insurance through the Class Act, which becomes a sustainable, affordable and
helpful program. If too many people close their eyes and shrug their shoulders,
trusting that their children or Medicare or God or someone else will provide,
the Class Act will run into trouble. And so will they and their families.

This is the fourth in a series of articles on how the health care overhaul will
affect everyday lives. This week's author, Paula Span, writes the New Old Age
blog, which can be found -- along with previous articles in the series -- at
nytimes.com/health.

URL: http://www.nytimes.com

LOAD-DATE: May 4, 2010

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY DAVID PLUNKERT)

PUBLICATION-TYPE: Newspaper


