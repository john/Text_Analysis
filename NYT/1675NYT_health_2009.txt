                   Copyright 2010 The New York Times Company


                             1675 of 2021 DOCUMENTS


                               The New York Times

                            March 17, 2010 Wednesday
                              Late Edition - Final

Drug Giants Lag Where Sales Boom, Study Says

BYLINE: By NATASHA SINGER

SECTION: Section B; Column 0; Business/Financial Desk; Pg. 1

LENGTH: 669 words


The leading drug makers need to move quickly to adapt to a new world order in
which some emerging markets are set to overtake some established national
markets in pharmaceutical sales, according to a report issued Tuesday.

Next year, the report predicted, drug sales in China will outpace those of
France and of Germany, while Brazil will be buying more medications than
Britain. The report was issued by IMS Health, a research company based in
Norwalk, Conn., that tracks prescriptions and other data on drug sales.

Unless the world's current leaders in brand-name drugs move more nimbly to
expand into those emerging markets, they will miss the big growth opportunities
and cede those markets to local players, the report said.

Annual growth of pharmaceutical sales in mature markets like the United States
and Western Europe has slowed to the low single digits in the last eight years.
The slowdown is a result of the worldwide economic crisis, but also of patent
expirations on a variety of name-brand drugs, growing use of generic drugs,
reduced investments in biotechnology and tighter government restrictions on the
pharmaceutical market, the report said.

The United States drug market had sales last year of about $300 billion, with an
annual growth rate of 5 percent, IMS said. And even if Congress passes health
care legislation, which, according to a recent Credit Suisse report, could
increase drug sales by $10.7 billion, the impact on the growth rate would be
minimal.

By contrast, IMS said, 17 emerging markets are poised for significant growth.
The report grouped the countries, which IMS has called the ''pharmerging
markets,'' into three tiers in descending order of market value growth. China
alone occupies the top tier. The second tier comprises Brazil, Russia and India,
while the third tier includes Venezuela, Poland, Argentina, Turkey and Mexico.

Last year, those countries accounted for $123 billion -- about 16 percent -- of
more than $770 billion in global drug sales, IMS said. The emerging market sales
represented 37 percent of industry growth.

By 2013, those same countries are estimated to bring in an additional $90
billion in sales accounting for 48 percent of industry growth, the report said.
Over all, emerging markets will represent about 21 percent of total drug sales
in 2013, IMS executives said in an interview.

It estimated that China, the leader of the pack in emerging markets, would
account for $40 billion in additional sales by 2013. ''These traditionally
peripheral economies are gearing up to turn the tables on the established
pharmaceutical world order,'' the report said.

Certainly, developed markets like the United States and Japan still represent a
vast majority of pharmaceutical sales. Even so, the report urged leading drug
makers to move faster to capitalize on high-growth emerging markets, where they
already face competition from entrenched domestic producers with
well-established brands.

A few European drug makers, including Novartis, Sanofi-Aventis and
GlaxoSmithKline, have been forging ahead by acquiring local companies or
increasing their local partnerships in those countries -- or by making major
investments in research and development in developing markets.

But, over all, the leading drug makers are underperforming in emerging markets.
The top 15 pharmaceutical companies, including Pfizer, Merck and Eli Lilly,
together derive less than 10 percent of their sales from emerging markets, IMS
said.

To build profitable businesses in these countries, drug makers must tailor their
approaches to the specific dynamics and challenges of each market, Murray
Aitken, a senior vice president at IMS, said in an interview Tuesday. Some
emerging markets for pharmaceuticals have been particularly hard hit by the
worldwide economic crisis, he said.

''In Romania, the situation is rather dire,'' Mr. Aitken said. ''Turkey is in
bad shape.''

He added, ''Welcome to the realities of doing business in these sorts of
environments.''

URL: http://www.nytimes.com

LOAD-DATE: April 23, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: A lab that makes H1N1 flu vaccine in China, where drug sales are
expected to exceed those of France and Germany in 2011. (PHOTOGRAPH BY CHINA
DAILY, VIA REUTERS) (B7)

PUBLICATION-TYPE: Newspaper


