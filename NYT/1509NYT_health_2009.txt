                   Copyright 2010 The New York Times Company


                             1509 of 2021 DOCUMENTS


                               The New York Times

                            February 14, 2010 Sunday
                              Late Edition - Final

Inside The Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 1098 words


International

FEW BARRED CANDIDATES ARE RESTORED TO IRAQI BALLOT

Iraq's election commission said that most of the 515 candidates disqualified
from next month's parliamentary election would stay off the ballot because of
claims that they retain links to the Baath Party. PAGE 13

UKRAINIAN CONTESTS VOTE

Prime Minister Yulia V. Tymoshenko, the apparent loser in Ukraine's presidential
election, said she would not concede, denouncing the results as having been
tainted by widespread fraud. PAGE 9

ADVISER MAY FACE WARRANTS

An adviser to members of an American church group arrested in Haiti may have
several legal charges against him in the United States as well as a warrant for
his arrest in El Salvador, records show. PAGE 10

POWER STRUGGLE IN PAKISTAN

A simmering power struggle in Pakistan erupted into open confrontation when the
country's top judge, Iftikhar Muhammad Chaudhry, suspended a judicial
appointment by President Asif Ali Zardari. PAGE 11

OBAMA PICKS ISLAMIC ENVOY

President Obama has appointed Rashad Hussain, a deputy White House counsel, to
be his special envoy to the Organization of the Islamic Conference, officials
said.  PAGE 11

National

CALIFORNIA HEALTH INSURER DELAYS LARGE RATE INCREASE

Amid criticism from the Obama administration, California's largest for-profit
health insurer, Anthem Blue Cross, delayed for two months increasing rates by as
much as 39 percent. The state insurance commissioner is investigating whether
the increases are legal. PAGE 21

INTERSTATE HEALTH INSURANCE

As Republicans and Democrats search for common ground in the health care debate,
one idea that both sides seem to agree on is that health insurance should be
sold across state lines. Prescriptions by David M. Herszenhorn.  PAGE 20

CANDIDATES IN RHODE ISLAND

Two prominent Democrats in Rhode Island said they would run for the
Congressional seat held by Patrick J. Kennedy, who has said he would not seek
re-election. PAGE 21

For Sale: Oregon History  23

New York

TAKING A GLIMPSE INSIDE A TROUBLED YOUTH PRISON

Edwin, 18, was recently released from New York's juvenile prison system, which
is under fire for its abysmal and sometimes dangerous conditions. His story
echoes those of hundreds of others. PAGE 26

Sports

DUTCH SPEED SKATER SETS OLYMPIC RECORD

Sven Kramer's performance at the Richmond Olympic Oval not only won his country,
the Netherlands, a Gold Medal, it earned the speed skater a new Olympic record,
besting the one his countryman Jochem Uytdehaage set in Salt Lake City in 2002.
PAGE 11

JETER'S NEXT CONTRACT

The biggest issue and the biggest nonissue that will hover over the Yankees this
season is that Derek Jeter is in the final year of his contract. Parting ways
with Jeter would be devastating to team's brand, but no less so to Jeter's
legacy. Tyler Kepner, On Baseball. PAGE 1

RACER WITH BABY ON BOARD

Jimmie Johnson is the odds-on favorite to win the Cup series title again. But a
daunting roadblock now stands in the way of that potential destiny: fatherhood.
PAGE 6

Spring Training Is Near  2

Obituaries

WILLIAM TENN, 89

He wrote satirical science fiction at a time when few writers in the genre
displayed a sense of humor, and brought biting wit, restless intelligence and a
supple prose style to classic themes like time travel and alien-human
interactions. PAGE 24

Sunday Business

IN DETROIT, THERE IS LIFE AFTER THE BIG 3 CARMAKERS

Dozens of manufacturers are discovering there is life in Michigan beyond the
auto industry. Over the last two years, multinationals and start-ups alike have
been coming to the state to build, buy or design a hodgepodge of products.  PAGE
1

FUTURE BAILOUTS OF AMERICA

Once a coterie, the Future Bailouts of America Club now includes many companies.
And if the government won't reduce the size of the safety net, it should at
least tell us the price tag. Fair Game by Gretchen Morgenson. PAGE 1

Arts & Leisure

POST-MINIMAL TO THE MAX

There is a whole lot of art making going on right now. All different kinds. But
you'd hardly know it from the contemporary art that New York's major museums
have been serving up, particularly this season, Roberta Smith writes.  PAGE 1

A DIRECTOR'S SIDE PROJECT

Arin Arbus, the associate artistic director of Theater for a New Audience, is
something of an Off Broadway luminary. But most Saturdays, one can still find
her at a medium-security men's prison, where she directs inmates. PAGE 4

Magazine

THE TEXTBOOK CRUSADE

Conservative activists on the Texas Board of Education say that the authors of
the Constitution intended the United States to be a Christian nation. And they
want the school books to say so. PAGE 32

Amateur Genetic Engineering 40

Book Review

PRIDE AND AVARICE

Adam Haslett's first novel, ''Union Atlantic,'' is set chiefly in 2002 and tucks
9/11 into its narrative. But Haslett tracks a different calamity: the global
economic crisis that accelerated after 2001. PAGE 9

Sunday Styles

BREAKING FOR BAND RECITALS

When Michelle Obama arrived in Washington, she declared herself the mom in
chief, and mothers watched as she juggled her duties as first lady with her
responsibilities as a mother. But her husband, the president, conducts an
unabashed juggling act of his own. PAGE 1

Automobiles

FOR $199 A MONTH

For just $199 a month, plus tax, after you've put $2,199 down, you get the
base-trim 2010 Honda Accord LX on a three-year lease. And if the cut-rate Honda
isn't to your taste, you can find similar deals.  PAGE 1

Week in Review

THE MEANING OF VICTORY

The battle for the previously little-known town of Marja in Afghanistan, the
heart of Taliban country, has begun. But in the end, the success of the
offensive this weekend will rise and fall on what happens after the gunfire
ends. PAGE 1

Editorial

SMALL IDEAS WON'T FIX IT

The ideas about health care reform that the Republicans are championing would
barely make a dent in the huge numbers of Americans without insurance and the
ever-escalating costs of health care. Week in Review, PAGE 7

Op-Ed

THOMAS L. FRIEDMAN

Middle East politics today is a struggle between 1977 and 1979 -- and 1979 is
still winning.  Week in Review, Page 8

MAUREEN DOWD

Smackdown in the Oval: Barack Obama brings Dick Cheney in for a chat. Week in
Review, Page 9

NICHOLAS D. KRISTOF

Research suggests that the roots of political judgments may lie partly in the
hard-wiring of our brains.  Week in Review, Page 10

PUBLIC EDITOR

Does a newspaper have an obligation to address rumors about its reporting? Clark
Hoyt takes a look. Week in Review, Page 8

URL: http://www.nytimes.com

LOAD-DATE: February 14, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


