                   Copyright 2010 The New York Times Company


                             1533 of 2021 DOCUMENTS


                               The New York Times

                            February 21, 2010 Sunday
                              Late Edition - Final

Up Next! On Live TV! Battle Over...Health?

BYLINE: By DAVID M. HERSZENHORN and SHERYL GAY STOLBERG; Robert Pear contributed
reporting.

SECTION: Section A; Column 0; National Desk; Pg. 15

LENGTH: 1191 words

DATELINE: WASHINGTON


When he jousts with Congressional Republicans over health care policy during a
televised meeting on Thursday, President Obama will seek to portray his
adversaries as sharing many of the broad goals of his legislation and also
strive to unify Congressional Democrats to press ahead and adopt a bill, senior
White House officials and leading Democrats say.

But Mr. Obama, top White House advisers and Congressional leaders of both
parties are under no illusion that the meeting will resolve more than a
half-century of disagreements over health care policy. Instead, Democrats say,
they hope the event will create a climate that helps revive their legislation in
Congress and prove to the American public that they are willing to hear out
Republicans and even adopt their ideas.

''We may not be able to resolve all the disagreements, but we ought to be able
to thrash out areas of broad agreement,'' said David Axelrod, Mr. Obama's senior
adviser. ''The fact is, there are broad areas of agreement on elements of this,
and hopefully that will become apparent here.''

Mr. Axelrod added, ''Sitting side by side working through these issues is better
than not sitting side by side and dealing with distortions.''

Republican leaders have not yet committed to attending the session and have said
they doubt the sincerity of Mr. Obama's bipartisan overtures, given his refusal
to discard the Democrats' legislation and start over. But senior Republican
aides said that party leaders planned to participate and that a chief goal would
be to portray the president as defying the will of the American people if he
continues pushing for an expansive and expensive bill.

The meeting is fraught with risk, and also offers potential rewards, for each
side.

White House officials said that by Monday they would unveil Mr. Obama's own
comprehensive proposal, focused on uniting Democrats who spent much of the past
year deeply divided on many points. Administration officials and Congressional
Democrats have expressed hopes that the meeting will help generate support for a
plan to attach health care legislation to a budget bill, which would prevent a
Republican filibuster in the Senate.

Even some senior White House officials are divided about whether a comprehensive
bill is still viable and privately voice uncertainty about the outcome of the
meeting.

Many Democrats in Congress said they doubted that it was feasible to pass a
major health care bill with a parliamentary tool called reconciliation, which is
used to speed adoption of budget and tax legislation. Reconciliation requires
only 51 votes for passage in the Senate, but entails procedural and political
risks.

''If we took a vote now, we would not have 51 votes for that approach,'' said a
Senate Democratic aide. ''The president would have to do a major sales job. He
is the only person who has the political capital to do it. But his focusing on
health care means that our efforts to focus on jobs are likely to be drowned
out.''

Mr. Obama and his fellow Democrats are also pursuing a parallel political
strategy: to use the televised event to put Republicans and their ideas on
display. It is a gamble that Americans will actually watch an in-the-weeds
discussion of health care policy and conclude either that Republicans are
unwilling to negotiate or that their policy ideas -- emphasizing tax incentives
and state innovations -- will not work.

''This is now the crucial hour for health reform,'' said Senator Ron Wyden,
Democrat of Oregon. ''This is the make-or-break time, and the public is going to
get to see it on TV. The impressions people get on Feb. 25 can be impressions
that people take into the voting booth in November, so they can say, 'Who put us
first? Who said it was more important to do what was right for America, rather
than just bicker?' ''

Even as Republicans have denounced the meeting as ''political theater,'' they,
too, sense an opportunity.

Representative Eric Cantor of Virginia, the House Republican whip, said in an
interview that he believed Republicans ought to attend -- not to negotiate with
the president, but to use the session to make the case to the American people
that the Democrats' bill ought to be thrown out.

''Republicans, I think, are up to the task of explaining why this bill is not
for America,'' Mr. Cantor said. ''We will be there to present a better way.''

Some of the initial jockeying at the meeting is likely to be over the basic
question of what goals policymakers should focus on.

Mr. Obama has said he will challenge Republicans to show how their proposals
would provide insurance to more than 30 million Americans over 10 years, which
is what the Democrats' bills are projected to do.

But Republicans have argued for months that broadly expanding coverage is not a
realistic goal, given the weak economy, and that lawmakers should focus on
reducing health care costs.

The Senate Republican leader, Mitch McConnell of Kentucky, said Americans did
not want an expensive bill. ''Anyone who thinks they want a bill that raises a
half-trillion in new taxes and slashes Medicare for our seniors simply isn't
listening,'' Mr. McConnell said in a statement on Saturday.

Before the policy debate, however, come the practical arrangements. White House
officials and Congressional Republicans are negotiating details of the meeting,
including seating charts, who will speak and in what order, the number of staff
members who can attend and the positioning of television cameras.

However the meeting plays out, Mr. Obama and his team will still face a
formidable challenge in trying to muster the needed votes. The House adopted its
health care bill on Nov. 7 on a vote of 220 to 215, with one Republican,
Representative Anh Cao of Louisiana, in favor.

But since then, one Democratic supporter (John P. Murtha of Pennsylvania) has
died, another (Robert Wexler of Florida) has resigned and Mr. Cao has said he
would vote against the bill, meaning some Democrats who opposed the measure in
November would need to switch sides to pass it.

Mr. Obama, in his weekly radio and Internet address on Saturday, urged
Congressional leaders to attend the meeting in good faith.

''I don't want to see this meeting turn into political theater, with each side
simply reciting talking points and trying to score political points,'' he said.
''Instead, I ask members of both parties to seek common ground in an effort to
solve a problem that's been with us for generations.''

Some Democrats expressed skepticism. ''There's hope for a breakthrough here, but
the odds of that are not very good,'' said a top Democratic aide who has worked
for years on health care. ''This is a media event.''

Democratic Congressional leaders are far less interested than the White House in
playing nice with Republicans and instead are looking to the president to twist
the arms of rank-and-file Democrats to produce the needed votes.

''Once we get a bill,'' a Senate Democratic leadership aide said,  ''we are
still going to need a heck of a lot of help from the administration to help us
get a bill through the House and Senate.''

URL: http://www.nytimes.com

LOAD-DATE: February 21, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: Eric Cantor, the House Republican whip, says his party should
attend, not negotiate. (PHOTOGRAPH BY CLIFF OWEN/ASSOCIATED PRESS)
 President Obama is set to meet with Republicans this week in a televised
session on health policy. Each side is honing its strategy. (PHOTOGRAPH BY
MICHAEL REYNOLDS/EUROPEAN PRESSPHOTO AGENCY)

PUBLICATION-TYPE: Newspaper


