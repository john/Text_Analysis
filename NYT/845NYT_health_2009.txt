                   Copyright 2009 The New York Times Company


                             845 of 2021 DOCUMENTS


                               The New York Times

                            October 18, 2009 Sunday
                              Late Edition - Final

Obama Lashes Back at Insurance Industry Over Its Opposition to Health Plan

BYLINE: By PETER BAKER

SECTION: Section A; Column 0; National Desk; Pg. 24

LENGTH: 851 words

DATELINE: WASHINGTON


President Obama mounted a frontal assault on the insurance industry on Saturday,
accusing it of using ''deceptive and dishonest ads'' to derail his health care
legislation and threatening to strip the industry of its longstanding exemption
from federal antitrust laws.

In unusually harsh terms, Mr. Obama cast insurance companies as obstacles to
change interested only in preserving their own ''profits and bonuses'' and
willing to ''bend the truth or break it'' to stop his drive to remake the
nation's health care system. The president used his weekly radio and Internet
address to challenge industry assertions that legislation will drive up
premiums.

''It's smoke and mirrors,'' Mr. Obama said. ''It's bogus. And it's all too
familiar. Every time we get close to passing reform, the insurance companies
produce these phony studies as a prescription and say, 'Take one of these, and
call us in a decade.' Well, not this time.''

Rather than trying to curb costs and help patients, he said, the industry is
busy ''figuring out how to avoid covering people.''

''And they're earning these profits and bonuses while enjoying a privileged
exemption from our antitrust laws,'' he said, ''a matter that Congress is
rightfully reviewing.''

The president's attack underscores the sharp break between the White House and
the insurance industry as the health care debate moves closer to a climax. When
Mr. Obama took office, he and his advisers had hoped to keep insurers at the
table to forge a consensus. But as the months passed, the strains grew -- until
this past week, when industry-financed studies  attacking the Democratic plan
signaled an open rupture.

The White House is concerned that the insurance industry may undermine public
support for changing the health care system, much as it did when it helped doom
President Bill Clinton's overhaul efforts in the 1990s. The new industry studies
are fueling broader criticism that the plans advanced by Democrats will cost too
much, raise consumer expenses and insert the government more deeply into the
health care sector.

The report, issued by America's Health Insurance Plans, concluded that premiums
would rise 18 percent more under provisions of a Senate bill than they would
otherwise in the next decade, to an average of nearly $26,000 for families and
$9,700 for individuals in 2019. The White House dismissed the study as ''deeply
flawed'' and noted that it did not account for subsidies in the bill to help
people who could not afford insurance.

Insurance representatives rejected Mr. Obama's assertions, saying they favor
bipartisan overhaul but believe that the current versions would in some ways
make things worse.

''Reform needs to work and deliver on the promise made to the American people
that everyone will have quality, affordable coverage,'' Karen Ignagni, president
of America's Health Insurance Plans, said in a statement. Citing the studies
warning of price increases, she added, ''We believe these issues can and must be
resolved.''

Jeff Smokler, a spokesman for the Blue Cross and Blue Shield Association, said
his group was most concerned with provisions that he said would allow people to
wait to buy coverage until they are sick. ''The result of such a system would be
higher costs for everyone,'' Mr. Smokler said. ''This is counterproductive to
the goals of health care reform.''

In the official Republican response to Mr. Obama's address, Representative Kevin
Brady of Texas said the Democratic plans would create a ''one-size-fits-all
plan'' that would reduce competition and increase costs.

Mr. Obama on Saturday praised the Senate bill passed by the Finance Committee
last week as ''a reform proposal that has both Democratic and Republican
support.'' Senator Olympia J. Snowe of Maine is the only Republican in either
house of Congress to  support any of the five Democratic health care bills.

The president complained bitterly about the insurers' attack on the legislation.
''The insurance industry is rolling out the big guns and breaking open their
massive war chest to marshal their forces for one last fight to save the status
quo,'' he said.

His signal of support for reviewing the industry's antitrust exemption put him
in league with Democratic leaders in Congress pushing for repeal or revision of
the McCarran-Ferguson Act, which was passed in 1945 to keep regulation of
insurers in the hands of the states. Although he did not explicitly endorse
overturning it, a spokesman said it was the first time he had raised the matter
publicly as president.

Senator Harry Reid of Nevada, the Democratic leader, testified at a Judiciary
Committee hearing on Wednesday in favor of getting rid of the exemption. A day
later, Representative Nancy Pelosi of California, the House speaker, said,
''There is tremendous interest in our caucus'' in such a move.

Assistant Attorney General Christine A. Varney, the head of the antitrust
division at the Justice Department, testified at the Senate hearing that
repealing McCarran-Ferguson would create more competition, which could help
reform industry practices.

URL: http://www.nytimes.com

LOAD-DATE: October 18, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


