                   Copyright 2009 The New York Times Company


                             235 of 2021 DOCUMENTS


                               The New York Times

                             July 30, 2009 Thursday
                              Late Edition - Final

Massachusetts Adjusts a Cut, Providing Some Health Care for 30,000 Immigrants

BYLINE: By ABBY GOODNOUGH

SECTION: Section A; Column 0; National Desk; Pg. 19

LENGTH: 491 words

DATELINE: BOSTON


Massachusetts trimmed its ambitious plan to provide health care for virtually
all its residents on Wednesday when the legislature failed to restore enough
money to the budget to provide full benefits for 30,000 legal immigrants.

It did, however, provide for partial coverage, relieving some supporters of the
program, who had feared that the cuts would be deeper.

Last month, to help close a gaping deficit, the legislature eliminated health
insurance for the immigrants, which cost about $130 million a year. Wednesday's
vote restored $40 million  -- about 30 percent -- leaving unclear just how much
care the affected immigrants would qualify for. Those affected are permanent
residents who have had green cards for less than five years.

It was the first retreat for the health care experiment just as Congress looked
to the state as a model.

''It's a first step, and we're pleased they will be covered,'' Eva Millona,
executive director of the Massachusetts Immigrant and Refugee Advocacy
Coalition, said of the partial restoration. ''But we still remain very concerned
as to what type of coverage they will receive.''

Ms. Millona and other critics of the original  cut had said it unfairly affected
taxpaying residents and reversed progress toward near-universal coverage.

Gov. Deval Patrick, a Democrat, wanted lawmakers to restore $70 million to the
program, but legislative leaders resisted, saying too many other vital programs
would have to be cut.

The affected immigrants are covered under Commonwealth Care, a subsidized
insurance program for low-income residents that is central to the state's
landmark universal health care law.

The reduced benefits, which will affect only nondisabled adults ages 18 to 65,
will take effect in September. Ms. Millona said she hoped that the immigrants
would at least remain eligible for emergency care and some preventive care.

Under the 1996 federal law that overhauled the nation's welfare system, the
30,000 immigrants affected by the loss of coverage do not qualify for Medicaid
or other federal aid. Massachusetts is one of the few states -- others are
California, New York and Pennsylvania -- that nonetheless provide at least some
health coverage for such immigrants.

Because of its three-year-old law, Massachusetts has the country's lowest
percentage of uninsured residents: 2.6 percent, compared with a national average
of 15 percent. The law requires that almost every resident have insurance, and
to meet that goal, the state subsidizes coverage for those earning up to three
times the federal poverty level, or $66,150 for a family of four.

But the recession has made an already difficult experiment far more challenging.
Enrollment in Commonwealth Care has risen sharply in recent months, to 181,000,
as more people have lost jobs. That increase, combined with plummeting state
revenues, made it impossible to maintain last year's level of service, state
officials said.

URL: http://www.nytimes.com

LOAD-DATE: July 30, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Gov. Deval Patrick sought greater financing for immigrants'
benefits. (PHOTOGRAPH BY JOSH REYNOLDS/ASSOCIATED PRESS)

PUBLICATION-TYPE: Newspaper


