                   Copyright 2009 The New York Times Company


                             1275 of 2021 DOCUMENTS


                               The New York Times

                            December 27, 2009 Sunday
                              Late Edition - Final

Inside The Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 1088 words


International

BRAZIL TRIES TO PREVENT LAND GRABS IN THE AMAZON

For the first time Brazil is keeping track of who owns land in the Amazon, and
who should be held accountable when forest is cleared illegally. In the process,
Brazil hopes to curb deforestation.  PAGE 6

ATTACKS ON IRAQI SHIITES

A roadside bomb exploded in the eastern part of Baghdad, adding to a rise in
sectarian violence during the 10-day observance of Ashura, a Shiite religious
holiday.  PAGE 8

ISRAELI STRIKES KILL 6

The Israeli military killed six Palestinians, three in the West Bank whom it
accused of killing a Jewish settler and three in Gaza who it said were crawling
along the border wall planning an attack..  PAGE 8

CZECH COMMUNISTS FACE BAN

Two decades after the Velvet Revolution overthrew Communist rule, a group of
senators in the Czech Republic is pushing to ban the Communist Party, which some
see as a national embarrassment. PAGE 10

HURDLES IN INDONESIA

In the five years since a tsunami hit Indonesia, reconstruction has been plagued
by problems, such as poor coordination and the remains of a 30-year separatist
conflict. PAGE 11

National

WHEN LEGISLATION PASSES, BUT THE DEBATE DOESN'T END

Democrats worked feverishly to successfully pass sweeping health care overhauls
in the Senate and House. But it is worth remembering that sometimes even after
legislation is signed into law, the effort to defeat it doesn't stop.
Prescriptions by Reed Abelson. PAGE 4

MONTANA TOWN FACES CHOICE

The people of Denton, Mont., population 300, are feeling the lure of financial
incentives offered by a giant freight line which is competing with Denton's own
nonprofit railroad, Central Montana Railroad. PAGE 12

A HARD NUMBER ON GRANTS

The Department of Education is awarding hundreds of millions of dollars to
states in a $4 billion grant competition. The department estimates that it will
take each state about 681 hours to prepare its proposal, but state officials say
that estimate is far too low.  PAGE 12

New York

TWO WAYS TO ICE SKATE:

Plain or Glamorous

With the world warming, this might be a good time to go ice skating. But where?
New Yorkers can skate in grand style, or with few frills. City Critic by Ariel
Kaminer. PAGE 28

Sports

THE YEAR IN SPORTS: ON THE FIELD, AND OFF

Perhaps the most shocking story of 2009 was Tiger Woods's scandal, which left
those who have followed his career looking for clues.  PAGE 4

People -- and horses -- made their mark on the athletic field too, including Kim
Clijsters, Caster Semenya, Zenyatta and Rachel Alexandra. PAGE 5

The championship scorecard was filled in with familiar names (Yankees, Steelers
and Lakers), but athletes' private lives garnered much of the attention. PAGE 6

OFF THE BENCH, INTO STARDOM

Mark Titus rarely plays for Ohio State's basketball team. Yet he is one of the
game's most popular players, thanks to his blog, which details his life as a
benchwarmer. PAGE 9

BEIJING HELPED GYMNAST

Bridget Sloan -- who helped the United States women win an Olympic silver medal
in the team event in Beijing -- has risen to the top of her sport. PAGE 9

Obituaries

TIM COSTELLO, 64

A truck driver who became a labor advocate and theorist, he was the founder of
an organization that fought globalization. PAGE 31

Sunday Business

BACK FROM THE BRINK (BUT WATCH YOUR STEP)

Most Americans felt as if they had been hit in the head by a 4-iron in 2008. In
2009 -- with more than a few bumps along the way -- the situation brightened.
But a look back at 2009 reveals that the year's five biggest business stories
won't have an ending until 2010. PAGE 1

ON TO THE NEXT BOARDROOM

One might think that board members managing companies that cratered would be
effectively barred from serving as directors at other companies. But many just
slither on to the next boardroom. Fair Game by Gretchen Morgenson. PAGE 1

Arts & Leisure

JUST LIKE DANCING, WITH SOUSAPHONES

Football season is also marching-band season, a particular form of American folk
art. The viewing window is a short one, culminating in college football's
postseason bowl games and a marching-band showcase in the coming weeks. PAGE 1

MANY HANDS, ONE VISION

Novartis' chief executive, Daniel L. Vasella, is halfway through transforming a
decrepit complex into one of the most ambitious undertakings in a decade. And he
has an army of renowned architects to help. PAGE 1

Magazine

THE LIVES THEY LIVED

The New York Times Magazine presents its annual look at some of the noteworthy
lives that ended in the last year, including famous figures, like Irving Penn
and Ted Kennedy, as well as lesser-known names.  PAGE 17

Book Review

MAN OF DARKNESS

In ''Koestler: The Literary and Political Odyssey of a Twentieth-Century
Skeptic,'' Michael Scammell seeks to show that something more than frivolity or
opportunism lay behind Koestler's ever-shifting preoccupations and allegiances,
Christopher Caldwell writes. PAGE 1

Sunday Styles

THE YEAR IN STYLE

In a year when a flailing economy sent creative types scurrying for aesthetic
foxholes, a single unlikely figure raised the flag for style and its power to
confound, bewitch and amuse: Lady Gaga. PAGE 1

Blogs Crashed Fashion's 1st Row 1

Automobiles

THE YEAR IN CARS

With bankruptcies, bailouts and plunging sales, the auto industry had a terrible
year. As automakers tried to keep a lap ahead of the Grim Reaper, shedding lines
and selling brands, it became difficult to keep track of the players left on the
road. SPORTSSUNDAY, PAGE 10

Travel

THE PARMA OF STENDHAL

There's the Parma that sprang from Stendhal's ''The Charterhouse of Parma,'' and
then there's the real Italian city. Both are worth exploring. PAGE 1

Editorial

NEXT STEP ON HEALTH REFORM

The Senate bill is a big improvement, but if lawmakers truly want to serve their
constituents, they would include provisions from the House bill. WEEK IN REVIEW,
PAGE 11

OUT OF THE GATE

The New York Racing Association's ban on breeders or trainers who sell horses
for slaughter is welcome but late.  WEEK IN REVIEW, PAGE 11

Op-Ed

MAUREEN DOWD

Kevin's back! The columnist's conservative brother takes over her column and
turns it a blazing shade of red.  Week in Review, Page 14

NICHOLAS D. KRISTOF

Pati Castillo, a 30-year-old Honduran who lost her waitressing job in Houston,
hasn't been in contact with her family for two months. Her story is an example
of the devastating toll the American recession has on neighborhoods around the
world. Week in Review, Page 14

URL: http://www.nytimes.com

LOAD-DATE: December 27, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


