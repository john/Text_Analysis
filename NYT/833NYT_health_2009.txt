                   Copyright 2009 The New York Times Company


                             833 of 2021 DOCUMENTS


                               The New York Times

                           October 17, 2009 Saturday
                              Late Edition - Final

White House to Let Insurers Warn Medicare Recipients About Pending Health Bills

BYLINE: By ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 10

LENGTH: 684 words

DATELINE: WASHINGTON


The Obama administration on Friday backed away from an order that had prohibited
insurance companies from warning Medicare recipients about the possible loss of
benefits under pending legislation to overhaul the health care system.

Medicare officials set off a political storm when they tried to stop such
communications last month. Under new guidelines, insurance companies can
communicate with Medicare beneficiaries on pending legislation, provided they do
not use federal money to do so. In addition, insurers must get permission from
beneficiaries before sending them information about legislation or asking them
to join grass-roots advocacy efforts.

Beneficiaries can provide consent in writing, by telephone or on a Web site, the
guidelines say.

At the same time, the Obama administration cited Humana on Friday for violation
of Medicare rules, saying the company had misled beneficiaries by telling them
they could lose valuable benefits under the legislation being pushed by
President Obama and Democrats in Congress.

The actions on Friday were the latest skirmish between the Obama administration
and the insurance industry. For months, the White House and insurance companies
had worked together on efforts to overhaul the health care system. But the
relationship deteriorated this week after two insurance trade groups issued
reports saying the legislation would cause big increases in premiums.

Nearly one-fourth of the 45 million Medicare beneficiaries are in private plans.
To help offset the cost of subsidizing coverage for the uninsured, Democrats
have proposed cutting payments to these Medicare Advantage plans, which they say
are overpaid.

In September, Medicare officials told Humana and other insurers to stop sending
information about the proposals to Medicare beneficiaries.

Representative Dave Camp of Michigan, the senior Republican on the Ways and
Means Subcommittee on Health, said Friday that the Obama administration was
''backing away from its inappropriate and unconstitutional gag order on Medicare
Advantage plans.''

''I am relieved that the administration is no longer misusing its regulatory
authority to prohibit plans from communicating factual information to seniors
about the Medicare cuts in health care reform,'' Mr. Camp said.

Thomas T. Noland Jr., a senior vice president of Humana, said Friday, ''We are
pleased that this issue has been resolved.''

In letters sent to more than 900,000 subscribers in September, Humana said that
if the proposed cuts become law, ''millions of seniors and disabled individuals
could lose many of the important benefits and services that make Medicare
Advantage health plans so valuable.''

Humana also urged beneficiaries to call their members of Congress and defend the
Medicare Advantage program.

In an official ''notice of noncompliance'' sent to Humana on Friday, the Obama
administration said the company had violated its ''data use agreement'' with the
government by using Medicare enrollment records to contact beneficiaries. It
also said the Humana letters were ''misleading to beneficiaries'' and thus
violated Medicare rules.

Humana is based in Kentucky, the home of Mitch McConnell, the Senate Republican
leader. He said last month that the Obama administration was ''using the full
weight of the federal government's enforcement powers to stifle free speech.''

The Obama administration had a strong incentive to resolve the dispute with
Humana. Republican senators had put a ''hold'' on Mr. Obama's nominees for jobs
at the Department of Health and Human Services.

The administration cracked down on Humana at the urging of Senator Max Baucus,
Democrat of Montana and chairman of the Finance Committee. He insisted the
legislation would strengthen Medicare, not cut benefits.

But the nonpartisan Congressional Budget Office told lawmakers that some people
in Medicare Advantage plans could lose some of the extra benefits they now
receive if Congress made the proposed cuts.

Democrats say private plans are paid 14 percent more than it would cost to cover
the same people in traditional Medicare.

URL: http://www.nytimes.com

LOAD-DATE: October 17, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


