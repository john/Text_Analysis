                   Copyright 2009 The New York Times Company


                             514 of 2021 DOCUMENTS


                               The New York Times

                            September 7, 2009 Monday
                              Late Edition - Final

White House Official Resigns After Flood of G.O.P. Criticism

BYLINE: By JOHN M. BRODER; Brian Stelter and Sarah Wheaton contributed reporting
from New York.

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1210 words

DATELINE: WASHINGTON


White House officials on Sunday tersely accepted the resignation of the
administration's special adviser for environmental jobs after a number of his
past statements became fodder for conservative critics and Republican officials.

The adviser, Van  Jones, a controversial and charismatic community organizer and
''green jobs'' advocate from the San Francisco Bay Area, signed a petition in
2004 questioning whether the Bush administration had allowed the terrorist
attacks of September 2001 to provide a pretext for war in the Middle East.

He also used a vulgarity to refer to Republicans just before being appointed to
his White House post early this year, and he has publicly supported Mumia
Abu-Jamal, who is on death row for the murder of a Philadelphia police officer.

Mr. Jones was a relatively minor figure in the administration, in charge of a
small White House program advocating for jobs in energy-efficient industries.
But he threatened to become a significant distraction as President Obama is
planning a prime-time address on health care to a joint session of Congress on
Wednesday night in an effort to regain traction on the issue.

The timing and manner of Mr. Jones's departure were unusual, coming in a pair of
written statements just after midnight Saturday on a holiday weekend after the
controversy over him, fueled on conservative talk radio and television, seemed
to catch the Obama administration by surprise.

Mr. Obama's plan to speak to public school students on Tuesday has also drawn
the ire of conservatives, and critics of his health care proposals have vocally
expressed their opposition throughout the summer.

Mr. Jones's hiring and departure again raised questions about the quality of the
White House personnel vetting process and the proliferation of so-called policy
czars who are not subject to Senate confirmation or legislative oversight.

The Obama administration entered office promising the most thorough scrutiny
ever of candidates for senior jobs, including an extensive questionnaire and
time-consuming background checks that have left many senior posts vacant for
months. But the process seems to have missed Mr. Jones's most inflammatory
comments and associations, as well as the tax problems that scuttled the
nominations of former Senator Tom Daschle to two top health policy posts and
Nancy Killefer as chief performance officer.

A White House official suggested that Mr. Jones's post was not seen as senior
enough to warrant the full vetting given other officials. The official spoke on
condition of anonymity because the authorized White House account was delivered
by administration officials in televised interviews on Sunday.

Robert Gibbs, the White House press secretary, said that Mr. Obama did not
endorse Mr. Jones's views and that he had quickly accepted Mr. Jones's
resignation so the controversy did not swamp the administration's larger
mission.

''Well, what Van Jones decided was that the agenda of this president was bigger
than any one individual,'' Mr. Gibbs said on ''This Week with George
Stephanopoulos'' on ABC. ''The president thanks Van Jones for his service in the
first eight months, helping to coordinate renewable energy jobs and lay the
foundation for our future economic growth.''

It was as cool a goodbye as is seen in Washington, intended to put an end to a
week of intense criticism of Mr. Jones -- who has been associated with a number
of radical protest movements, including one rooted in Marxist theory -- on
television and radio talk shows and on the Internet.

Chief among those keeping the story alive was Glenn Beck, the conservative host
of a Fox News Channel program. Mr. Beck began criticizing Mr. Jones in July,
first in segments on his syndicated talk radio show and then, on July 23, on his
Fox News program, said Christopher Balfe, the president of Mr. Beck's production
company.

Mr. Beck, who regularly draws almost three million viewers on Fox News, called
Mr. Jones a ''communist-anarchist radical.'' A few days later, Mr. Beck called
Mr. Obama a racist on a Fox News show, leading Color of Change, an activist
group co-founded by Mr. Jones four years ago, to call on Mr. Beck's advertisers
to stop sponsoring his television program. Color of Change says Mr. Jones is no
longer affiliated with the group.

Mr. Balfe emphasized that Mr. Beck had spoken about Mr. Jones's background
before Color of Change ''began targeting Glenn.'' In a statement Sunday, Mr.
Beck said that Americans had demanded answers about Mr. Jones, but that
''instead of providing them, the administration had Jones resign under cover of
darkness.''

As the advertiser campaign heightened, Mr. Beck devoted more time to Mr. Jones's
past remarks. Dozens of advertisers issued statements to distance themselves
from Mr. Beck's show in the past month, but Fox said no revenue had been lost.

Though Mr. Jones, 40, had apologized twice during the week for earlier comments,
he was combative in his resignation letter. ''On the eve of historic fights for
health care and clean energy, opponents of reform have mounted a vicious smear
campaign against me,'' Mr. Jones said. ''They are using lies and distortions to
distract and divide.''

He said he had received numerous calls and notes from supporters urging him to
stay and fight. But, he said, ''I cannot in good conscience ask my colleagues to
expend precious time and energy defending or explaining my past. We need all
hands on deck, fighting for our future.''

Mr. Jones could not be reached for further comment.

Mr. Jones's boss, Nancy Sutley, chairwoman of the White House Council on
Environmental Quality, unsentimentally accepted her aide's departure. ''Over the
last six months, he has been a strong voice for creating 21st-century jobs that
improve energy efficiency and utilize renewable resources,'' Ms. Sutley said in
a statement. ''We appreciate his hard work and wish him the best moving
forward.''

On Friday, Representative Mike Pence of Indiana, chairman of the House
Republican conference, called on Mr. Jones to resign, and Senator Christopher S.
Bond, Republican of Missouri, called for a hearing on Mr. Jones's appointment.
Mr. Obama has appointed more than two dozen special advisers, or ''czars,'' who
are not subject to the confirmation process.

Before joining the Obama administration, Mr. Jones wrote a book on environmental
issues called ''The Green Collar Economy'' and co-founded several nonprofit
groups, including the Ella Baker Center for Human Rights, which promotes
alternatives to violence and prisons, and Green for All, which works to bring
energy-related jobs to inner cities.

Former Vice President Al Gore and Speaker Nancy Pelosi, among others, endorsed
his environmental work, and he has been a sought-after motivational speaker.

Howard Dean, the former chairman of the Democratic National Committee, defended
Mr. Jones on Sunday, saying he was being penalized for not realizing what the
petition he signed in 2004 was.

''This guy's a Yale-educated lawyer,'' Mr. Dean said on ''Fox News Sunday.''
''He's a best-selling author about his specialty. I think he was brought down,
and I think it's too bad. Washington's a tough place that way, and I think it's
a loss for the country.''

URL: http://www.nytimes.com

LOAD-DATE: September 7, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Van Jones, a White House adviser on environmental jobs, stepped
down this weekend. (PHOTOGRAPH BY ETHAN MILLER/GETTY IMAGES) (pg.A9)

PUBLICATION-TYPE: Newspaper


