                   Copyright 2009 The New York Times Company


                             525 of 2021 DOCUMENTS


                               The New York Times

                           September 8, 2009 Tuesday
                              Late Edition - Final

As Obama Speech Nears, Details on a Compromise

BYLINE: By JACKIE CALMES and ROBERT PEAR; Sheryl Gay Stolberg contributed
reporting from Cincinnati.

SECTION: Section A; Column 0; National Desk; Pg. 14

LENGTH: 1254 words

DATELINE: WASHINGTON


As President Obama and top advisers drafted his eagerly awaited health care
speech to Congress, new details emerged Monday about fees and coverage limits
under a proposal being floated by the chairman of a crucial Senate committee.

The proposal from the lawmaker, Senator Max Baucus, who heads the Finance
Committee, would impose new fees on some sectors of the health care industry,
but none on individuals, to help offset initial costs estimated at $880 billion
over 10 years, according to officials familiar with the outline.

The plan, circulating among some committee members of both parties, would also
offer the option of lower-cost insurance, with protection only against the costs
of catastrophic illnesses, to those 25 and younger. In addition, it would
provide basic Medicaid coverage to millions of low-income people who are
currently ineligible for the program, but the benefits would be less
comprehensive than  standard Medicaid.

Mr. Baucus, Democrat of Montana, will try on Tuesday to win support from the
three Republicans and two other Democrats on his committee with whom he has been
deliberating for months. Ultimately, however, he will need a majority of the
committee's 23 members, several of whom are resentful at being excluded.

Mr. Obama plans to meet Tuesday at the White House with the top Democrats in
Congress, Speaker Nancy Pelosi and Senator Harry Reid, to coordinate strategy.

Administration officials declined to discuss either Mr. Baucus's plan or the
president's speech, which Mr. Obama will deliver Wednesday night to a joint
session of Congress. But the officials welcomed the chairman's draft as
important progress just as lawmakers are returning this week from their summer
recess.

Still, while the Finance Committee is expected to produce a moderate bill with
the best chance of passage in the Senate, it is not clear whether the White
House regards it as a template. Four other committees with jurisdiction -- three
in the House, one in the Senate -- approved versions of health legislation
before Congress recessed.

The presidential address was scheduled by the White House last week in part to
pressure Mr. Baucus to act. Many Democrats say Mr. Obama has for too long
deferred to him, sapping momentum from the president's chief domestic priority.

Mr. Obama's Labor Day meeting with his advisers followed his return from an
A.F.L.-C.I.O. picnic in Cincinnati, where he gave a rousing campaign-style pitch
for his health care initiative that previewed some themes for Wednesday night.
The president told thousands of cheering unionized workers that Congress should
stop debating, because ''it's time to act and get this done.''

''I've got a question for all those folks who say we're going to pull the plug
on Grandma,'' the president thundered. ''What's your answer? What's your
solution? And you know what? They don't have one. Their answer is to do
nothing.''

As previously reported, Mr. Baucus is not calling for a government-run insurance
plan, or ''public option,'' to compete against private insurers. Instead, his
committee's group of negotiators has coalesced around the idea of forming
nonprofit, member-owned insurance cooperatives in the states.

Republicans oppose the public option, calling it an invitation to a health care
system run entirely by the government, and some moderate-to-conservative
Democrats are leery as well.

In his address, Mr. Obama is nonetheless expected again to describe a public
option as his preferred way to ''keep insurance companies honest,'' as he often
puts it, and encourage better coverage at a lower cost. At the same time, he
will make clear that enactment of health care legislation should not hinge on
whether it includes the public option, a message sure to anger liberals,
including many in the House.

''I continue to believe that a public option within that basket of insurance
choices would help improve quality and bring down costs,'' Mr. Obama told his
union audience Monday, provoking loud applause.

But remarks by the White House press secretary, Robert Gibbs, on the flight to
Cincinnati reflected an effort by the White House to play down the importance of
a public option to the larger overhaul. Mr. Gibbs said a public option would not
affect most Americans -- up to 180 million -- because they already have
insurance through employers.

The White House's straddle reflects the Obama team's recognition that the more
liberal House will not pass a health care bill without a public insurance
option, while the Senate will not pass one with it.

''The problem with this kind of important speech,'' said Michael S. Berman, a
longtime Democratic strategist who was an aide to Vice President Walter F.
Mondale, is that ''it is pretty darn hard to put together something that is
satisfying to enough people.''

To help pay for his plan, Mr. Baucus would impose fees of $6 billion a year on
insurance companies, $4 billion a year on manufacturers of medical devices and
$750 million a year on clinical laboratories.

Mr. Baucus has apparently dropped the idea of requiring Medicare beneficiaries
to pay 20 percent of the amounts charged for laboratory tests. That will allow
him to say his plan does not directly increase costs to beneficiaries.

Alan B. Mertz, president of the American Clinical Laboratory Association, a
trade group, said his members would fight the proposed fee on labs. Under
current law, Mr. Mertz noted, laboratories face a 2 percent cut in their
Medicare payments in January, and in 10 of the last 12 years they received no
increase in their Medicare reimbursement rates.

Mr. Baucus's proposal would offer low-cost catastrophic insurance as an option
for people 25 and younger.  Policy experts say many people in this age group
cannot afford comprehensive coverage or see no need for it.

''The idea of a stripped-down benefits package for people who have a good income
and choose not to buy health insurance makes a lot of sense,'' said Stan Dorn, a
senior research associate at the Urban Institute. ''But a catastrophic insurance
policy does not make sense for lower-income people, because they cannot afford
medical care short of catastrophic expenses. A catastrophic policy does not
cover routine care.''

Mr. Baucus's plan would also expand Medicaid, starting in 2014, to cover
millions of low-income people, including many childless adults who never
qualified before. Benefits offered to such newly eligible adults would generally
be less generous than the comprehensive benefits available to other Medicaid
recipients.

For years, governors have wanted more discretion to tailor Medicaid benefits to
the needs of different population groups. But Jocelyn A. Guyer, co-executive
director of the Center for Children and Families at Georgetown University,
expressed concern. ''Low-income people without children tend to have extensive
health care needs -- higher rates of mental illness, physical disability and
chronic conditions,'' she said.

Mr. Baucus's proposal would also require health insurance companies to report
the proportion of premium dollars spent on things other than medical care.
Hospitals would be required to list standard charges for all services.

The information could be useful to consumers. But insurance companies say the
data on their expenses can be misleading because the costs of some activities
that benefit patients, like ''disease management'' and the use of health
information technology, may be classified as administrative rather than medical.

URL: http://www.nytimes.com

LOAD-DATE: September 8, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: President Obama arriving in Washington on Monday after giving a
speech at the A.F.L.-C.I.O. Labor Day picnic in Cincinnati. (PHOTOGRAPH BY
STEPHEN CROWLEY/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


