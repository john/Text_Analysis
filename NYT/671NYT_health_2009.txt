                   Copyright 2009 The New York Times Company


                             671 of 2021 DOCUMENTS


                               The New York Times

                          September 23, 2009 Wednesday
                              Late Edition - Final

Bill Approved To Expedite A Successor To Kennedy

BYLINE: By ABBY GOODNOUGH and CARL HULSE; Abby Goodnough reported from Boston
and Carl Hulse from Washington; Katie Zezima contributed reporting from Boston.

SECTION: Section A; Column 0; National Desk; Pg. 14

LENGTH: 859 words

DATELINE: BOSTON


Fulfilling one of Edward M. Kennedy's dying wishes, the Massachusetts State
Senate approved a bill on Tuesday allowing a temporary replacement for the late
senator.

The measure was pushed through by the Democratic leadership in an effort to
deliver what might be a crucial vote when health care overhaul legislation comes
before Congress this fall. Mr. Kennedy had made health reform one of his
lifetime priorities.

The vote in the State Senate, 24 to 16, means that Gov. Deval Patrick could
appoint an interim senator within days.

In Washington, aides said that Senator Harry Reid of Nevada, the majority
leader, pumped his fist after a note about the Massachusetts vote was passed to
him in his conference room.

Mr. Patrick, a Democrat who is close to President Obama, has refused to discuss
potential appointees, but senior Democrats in Washington said Tuesday that Paul
G. Kirk Jr., a former aide to Mr. Kennedy and chairman of the Democratic
National Committee, was a likely choice.

The Democrats, who spoke on condition of anonymity, said they believed that
Michael S. Dukakis, the former governor and 1988 presidential nominee, said to
be under consideration, was out of the running and would not be named.

Other possibilities include Evelyn Murphy, a former lieutenant governor; and
Charles Ogletree, a professor at Harvard Law School.

A spokesman for Mr. Patrick, Kyle Sullivan, would not discuss a timeframe for an
appointment, saying only that the governor was pleased with the vote.

The appointee would take Mr. Kennedy's place in the Senate only until a special
election is held Jan. 19 but could play a critical role in Congressional
Democrats' efforts in the coming months.

''It means that Massachusetts will be represented by two senators in one of the
most important debates of our times,'' said Senator Richard J. Durbin of
Illinois, the No. 2 Democrat in the Senate, referring to the health care fight.

Democrats in Washington were not willing to publicly discuss whether they had a
preference for the interim appointee. But some party officials said concerns had
been raised privately about the prospect of Mr. Dukakis taking the seat, saying
he was a reminder of an era of Democratic failure. They also worried that Mr.
Dukakis, 75, might exhibit an independent streak when their main goal at the
moment is to hold Democrats as a bloc.

But an editorial in The Boston Globe on Tuesday said Mr. Dukakis would be the
best choice, saying that he ''knows how politics works and can get his phone
calls returned.''

Some Democrats said that appointing Mr. Kirk, now the chairman of the John F.
Kennedy Library Foundation in Boston, would make sense because of his close ties
to the Kennedy family.

Even as Democrats moved toward filling the Kennedy vacancy, the health problems
of another senator resurfaced, a reminder of the precariousness of the party's
hold on 60 votes. Senator Robert C. Byrd, 91, Democrat of West Virginia,
suffered a fall at his Virginia home on Tuesday and was hospitalized. Aides said
he was being kept in the hospital as a precautionary measure.

In Massachusetts, where the succession measure won passage in the House of
Representatives last week, the floor debate focused on the complaint of
Republicans, and even some Democrats, that the bill was overly partisan.
Governors here had the power to fill Senate vacancies until 2004, when the
Democratic majority in the legislature changed the law to require a special
election. Democrats worried then that if Senator John Kerry were elected
president, Gov. Mitt Romney, a Republican, would appoint a Republican.

''Any way you slice it,'' Senator Richard Tisei, the Senate minority leader,
said of the bill, ''it's bad precedent and bad politics.''

All 5 Republicans and 11 Democrats opposed the measure. Therese Murray, the
Senate president, remained publicly noncommittal until just before the vote,
despite calls from the Obama administration and from Victoria Reggie Kennedy,
Mr. Kennedy's widow. Ms. Murray ultimately supported the measure.

The bill does not prohibit the temporary appointee from seeking Mr. Kennedy's
seat in the special election -- legislators feared that such a condition would
not pass constitutional muster -- but Mr. Patrick has said he would ask the
appointee to promise not to run.

New laws normally cannot take effect in Massachusetts for 90 days, but governors
can put them into effect immediately by declaring an emergency. The bill must be
put to a final procedural vote in both chambers of the legislature on Wednesday
before being sent to Mr. Patrick.

Under current law, Mr. Kennedy's seat would remain empty until the special
election in January. But shortly before he died of brain cancer last month, Mr.
Kennedy asked the state legislature to change the law and allow Mr. Patrick to
name a temporary successor until the special election could be held.

Senator Robert A. O'Leary, a Democrat who represents Mr. Kennedy's home town of
Hyannis, appealed to his colleagues to approve the measure out of respect for
the late senator.

''I for one think that is something we need to honor,'' he said.

URL: http://www.nytimes.com

LOAD-DATE: September 23, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: Therese Murray, the Massachusetts Senate president, did not
commit to support a succession measure until just before the vote.(PHOTOGRAPH BY
CHARLES KRUPA/ASSOCIATED PRESS)
Michael S. Dukakis, left, was said to be out of the running for Edward M.
Kennedy's Senate seat, while Paul G. Kirk Jr., right, a former aide to Mr.
Kennedy, was said to be a likely choice.(PHOTOGRAPH BY ELISE AMENDOLA/ASSOCIATED
PRESS)
 (PHOTOGRAPH BY LISA POOLE/ASSOCIATED PRESS)(pg. A16)

PUBLICATION-TYPE: Newspaper


