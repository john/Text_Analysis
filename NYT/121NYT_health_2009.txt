                   Copyright 2009 The New York Times Company


                             121 of 2021 DOCUMENTS


                               The New York Times

                             July 8, 2009 Wednesday
                              Late Edition - Final

Health Deals Could Harbor Hidden Costs

BYLINE: By DAVID M. HERSZENHORN and SHERYL GAY STOLBERG

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1057 words

DATELINE: WASHINGTON


The deals, trumpeted loudly by the White House, would each help pay for a
sweeping overhaul of the health care system.

First, it was a broad consortium of health industry groups -- doctors,
hospitals, drug makers and insurers, all promising to slow the growth of medical
spending by 1.5 percent. Then, it was the big drug makers, promising savings of
$80 billion over 10 years, by lowering the cost of medicine for the elderly.

On Wednesday, it will be major hospital associations, pledging to save more than
$150 billion over a decade. And a deal with doctors is said to be on tap next.

In each case, the Obama administration hailed the agreements as historic. But
what has been little discussed is what the industry groups will be getting in
return for their cooperation, whether or not the promised savings ever
materialize.

The short-term political benefits are clear. Senior White House officials say
the deals are building momentum that will help propel the health care
legislation past potential opponents in the private sector and on Capitol Hill.

Rather than running advertisements against the White House, the most influential
players in the  industry are inside the room negotiating with administration
officials and leading lawmakers, like Senator Max Baucus, chairman of the
Finance Committee.

''The very groups we have been talking to have been the most vocal opponents of
health care reform; they are now becoming the vocal proponents for health care
reform,'' said Rahm Emanuel, the White House chief of staff.

But some lawmakers said the deals, while seemingly helpful, could raise false
expectations by obscuring how much the industry is demanding  for its
concessions.

''I'm delighted to hear that people are stepping up to help reduce costs,'' said
Senator Christopher J. Dodd, Democrat of Connecticut, who is leading the Senate
health committee, ''but I want to know what the ask is, and the ask sometimes
can exceed the value of your cost savings.''

Senator Olympia J. Snowe, Republican of Maine, who could provide a critical
swing vote, said she had not signed on to any of the White House deals. ''It's
one thing for the president to reach that agreement, but it's another thing for
Congress to reach that agreement,'' Ms. Snowe said. ''We have yet to evaluate
what are the specifics and particulars. So it's uncertain. It could be helpful.
I just don't know.''

As part of their deal with the White House, pharmaceutical companies say they
won an agreement from Mr. Baucus to oppose efforts by House Democrats to sharply
reduce what the government pays for drugs for some Medicare recipients
previously covered by Medicaid.

The deal with doctors could come at a steep price: a $250 billion fix to a
12-year-old provision in federal law intended to limit the growth of Medicare
reimbursements. The American Medical Association and other doctors' groups have
sought to change or repeal the provision, and they are likely to try to extract
that as their price for boarding the Obama train, people tracking the
negotiations said.

Wal-Mart, the nation's largest private-sector employer, agreed recently to
support requiring all big companies to insure their workers. In exchange,
Wal-Mart said it wanted a guarantee that the bill would not ''create barriers to
hiring entry-level employees'' -- in effect, code words to insist that lawmakers
abandon the idea of requiring employers to pay part of the cost for workers
covered by Medicaid, the government insurance plan for the poor.

''It's kind of a give-and-take, quid pro quo kind of environment,'' said Tom
Daschle, President Obama's first choice for health secretary, who remains in
touch with the White House on health care issues. ''I think that the
stakeholders wouldn't do this if they didn't think there was something in it for
them.''

But, Mr. Daschle said, there is something in it for Congress and the White
House, too: By getting on board early, groups like the drug makers and hospitals
will be ''owners of this process, and as owners they have to continue to defend
it and support it.''

Over the past year, Mr. Baucus, Democrat of Montana, has strong-armed industry
groups, warning them not to publicly criticize the process if they want to stay
in negotiations.

Mr. Baucus, in turn, has said little about his talks with industry players. On
Tuesday, he said only that he was ''heartened'' by how many groups were
supporting the health care overhaul.

But Rick Pollack, executive vice president of the American Hospital Association,
credited Mr. Baucus and his staff with reaching the agreement.

''Hospitals have been long committed to expanding coverage for the millions of
Americans that don't have health insurance,'' Mr. Pollack said. ''We see this as
a historic opportunity to achieve that objective.''

In the case of hospitals, Mr. Pollack said negotiators had worked hard to come
to terms not just on the financial issues but also on broad and complex policy
matters intended to improve the health care system.

He said hospitals had agreed to about $150 billion in savings after securing
assurances that lower reimbursements would come after an insurance expansion
that would guarantee that more patients pay their bills.

''We do believe in that regard in particular there are safeguards that are part
of this proposal,'' Mr. Pollack said.

He said Mr. Baucus and White House officials had been responsive to the industry
on many points, including plans to bundle payments for certain services and
penalize hospitals when patients were readmitted for potentially preventable
reasons.

Democrats are trying to keep the cost of the legislation to about $1 trillion
over 10 years. In a sign of a deepening rift among Senate Democrats, the
majority leader, Harry Reid of Nevada, on Tuesday urged Mr. Baucus to drop a
plan that would pay for the bill partly by taxing some employer-provided health
benefits.

Aides said Mr. Reid, speaking for other Senate Democratic leaders, was concerned
that Mr. Baucus had not included any provision for a government-run insurance
program to compete with private insurers, which is favored by Mr. Obama and many
other Democrats. At a meeting later in the day, aides said, Mr. Baucus pushed
back against Mr. Reid, saying his own proposal offered the only clear path to
Senate approval.

URL: http://www.nytimes.com

LOAD-DATE: July 8, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


