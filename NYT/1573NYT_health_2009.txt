                   Copyright 2010 The New York Times Company


                             1573 of 2021 DOCUMENTS


                               The New York Times

                            February 26, 2010 Friday
                              Late Edition - Final

After the Summit

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL; Pg. 26

LENGTH: 584 words


The main lesson to draw from Thursday's health care forum is that differences
between Democrats and Republicans are too profound to be bridged. That means
that it is up to the Democrats to fix the country's dysfunctional and hugely
costly health care system.

At the meeting, President Obama laid out his case for sweeping reform that would
provide coverage to 30 million uninsured Americans and begin to wrestle down the
rising cost of medical care and future deficits. The Republicans insisted that
the country cannot afford that -- and doesn't need it. The House Republican
leader, John Boehner, trotted out the old chestnut that the United States has
the ''best health care system in the world.''

This isn't a question of boosterism or patriotism. If there's any doubt about
whether to stick with the status quo, Americans just need to look at their
relentlessly rising premiums or think about where -- or even whether -- they can
get coverage if they lose their jobs.

Thursday's meeting -- more than seven hours broadcast for the hardy -- was
billed as a last-ditch effort to try to find common ground. There was plenty of
wonkish discussion. Each party put its best face forward in a mostly civil
exchange of ideas, and both professed to see some areas of potential agreement.

Mr. Obama seemed ready to take stronger steps toward malpractice reform, a top
issue for Republicans. And he agreed with Senator John McCain that a special
deal to protect Florida residents enrolled in private Medicare plans was hard to
defend.

Republicans stuck to their script and argued for small solutions, such as
letting people buy insurance in other states that might allow skimpier -- and
thus cheaper -- coverage. That is a formula for helping healthy people cut costs
while driving up premiums for sick people unable to get similar coverage.

Republicans balked at any big expansion of Medicaid or any big subsidies to help
middle-class Americans buy insurance on new exchanges. As a result, their plans
would cover only three million uninsured over the next decade, a tenth of what
the Democrats are proposing.  That is not enough.

Mr. Obama should jettison any illusions that he can win Republican support by
making a few more changes in bills that already include many Republican ideas.
Republican speakers made clear that the only thing they would accept is starting
over from scratch. That would be the end of sweeping reform.

The Republicans tried to wring a pledge from Mr. Obama that he would not resort
to ''budget reconciliation,'' a parliamentary maneuver to sidestep a filibuster
in the Senate and pass legislation by a simple majority. Reconciliation is a
last resort. But Republicans and Democrats have both used it for major bills in
the past. The president wisely refused to tie his hands.

Here is a basic fact: If the House Democrats voted tomorrow to approve the
Senate bill, health care reform would become the law of the land.

The president and Speaker Nancy Pelosi should push the House to accept the
fundamentally sound Senate bill. If they still cannot garner enough votes from
their own caucus, they should alter the Senate bill slightly with parallel
legislation that could be passed with budget reconciliation.

Mr. Obama needs to keep explaining to Americans that this health care reform is
critical -- to give them security, to hold down costs and ease the strain on
federal budgets. His main challenge, and his best chance, for passing it is to
get his own party in line.

URL: http://www.nytimes.com

LOAD-DATE: February 26, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


