                   Copyright 2009 The New York Times Company


                             923 of 2021 DOCUMENTS


                               The New York Times

                            November 3, 2009 Tuesday
                              Late Edition - Final

2 Bills and the Drug Industry

BYLINE: By DUFF WILSON

SECTION: Section A; Column 0; National Desk; Pg. 20

LENGTH: 265 words


The health care bill that the full House may take up later this week is much
tougher on the drug industry than what the Senate is considering. And that could
prove one of the challenges for lawmakers when the legislation eventually
reaches a House-Senate conference committee.

The Senate health committee did not address drug pricing, but the Senate Finance
Committee's version includes a deal, supported by the White House, that aims to
save Medicare enrollees and the government $80 billion over 10 years.

The House bill would exact a much higher cost on the drug industry -- about $140
billion over 10 years, Nadeam Elshami, a spokesman for Speaker Nancy Pelosi,
said Monday.

The House provisions, promoted by Representative Henry A. Waxman, Democrat of
California, would include new rebates to cut prices for more than six million
low-income older people who were transferred from Medicaid to higher-priced
Medicare drug benefits in 2006.

The House bill would also close the Medicare drug coverage gap -- the so-called
doughnut hole -- that now forces people to pay the full cost of their drugs each
year when spending reaches $2,700, until it exceeds $6,100.

The Senate Finance Committee bill provides for 50 percent discounts on
brand-name drugs for people in the doughnut hole. The House would add industry
rebate money to close the gap altogether by 2019.

And the House bill would authorize Medicare, for the first time, to negotiate
drug prices directly with manufacturers. The Senate Finance Committee bill
specifically precludes giving Medicare that power. DUFF WILSON

URL: http://www.nytimes.com

LOAD-DATE: November 3, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


