                   Copyright 2010 The New York Times Company


                             1783 of 2021 DOCUMENTS


                               The New York Times

                             March 28, 2010 Sunday
                              Late Edition - Final

With No Jobs, Plenty of Time For Tea Party

BYLINE: By KATE ZERNIKE

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1136 words


SOUTH BEND, Ind. -- When Tom Grimes lost his job as a financial consultant 15
months ago, he called his congressman, a Democrat, for help getting government
health care.

Then he found a new full-time occupation: Tea Party activist.

In the last year, he has organized a local group and a statewide coalition, and
even started a ''bus czar'' Web site to marshal protesters to Washington on
short notice. This month, he mobilized 200 other Tea Party activists to go to
the local office of the same congressman to protest what he sees as the
government's takeover of health care.

Mr. Grimes is one of many Tea Party members jolted into action by economic
distress. At rallies, gatherings and training sessions in recent months,
activists often tell a similar story in interviews: they had lost their jobs, or
perhaps watched their homes plummet in value, and they found common cause in the
Tea Party's fight for lower taxes and smaller government.

The Great Depression, too, mobilized many middle-class people who had fallen on
hard times. Though, as Michael Kazin, the author of ''The Populist Persuasion,''
notes, they tended to push for more government involvement. The Tea Party
vehemently wants less -- though a number of its members acknowledge that they
are relying on government programs for help.

Mr. Grimes, who receives Social Security, has filled the back seat of his
Mercury Grand Marquis with the literature of the movement, including Glenn
Beck's ''Arguing With Idiots'' and Frederic Bastiat's ''The Law,'' which
denounces public benefits as ''false philanthropy.''

''If you quit giving people that stuff, they would figure out how to do it on
their own,'' Mr. Grimes said.

The fact that many of them joined the Tea Party after losing their jobs raises
questions of whether the movement can survive an improvement in the economy,
with people trading protest signs for paychecks.

But for now, some are even putting their savings into work that they argue is
more important than a job -- planning candidate forums and get-out-the-vote
operations, researching arguments about the constitutional limits on Congress
and using Facebook to attract recruits.

''Even if I wanted to stop, I just can't,'' said Diana Reimer, 67, who has
become a star of the effort by FreedomWorks, a Tea Party group, to fight the
health care overhaul. ''I'm on a mission, and time is not on my side.''

A year ago, Ms. Reimer's husband had been given a choice -- retire or be fired.
The couple had been trying to sell their split-level home in suburban
Philadelphia to pay off some debt and move to a small place in the city.

But real estate agents told them the home would sell for about $40,000 less than
they paid 19 years ago -- not enough to pay off their mortgage.

Then Ms. Reimer saw a story about the Tea Party on television. ''I said, 'That's
it,' '' she recalled. ''How can you get this frustration out, have your voices
heard?''

She liked that the Tea Party was patriotic, too. ''They said the Pledge of
Allegiance and sang the national anthem,'' she said.

She had taken a job selling sportswear at Macy's. But when her husband found her
up early and late taking care of Tea Party business, he urged her to take a
leave. When the store did not allow one, she quit.

''I guess I just found my calling,'' she said.

Ms. Reimer, now a national coordinator for the Tea Party Patriots, also found a
community. Directing protesters to Congressional offices on Capitol Hill before
the vote on health care this month, Representative Steve King, an Iowa
Republican who has become a Tea Party hero, stopped to welcome her by name. ''I
should have known you'd be here,'' he said, embracing her.

A Tea Party member from North Carolina recognized Ms. Reimer from Massachusetts,
where she led crews knocking on doors in the snow for Scott Brown, the state's
new Republican senator. ''Our slave master,'' the man said, greeting her.

Ms. Reimer often wells up talking about her work. ''I'm respected,'' she said,
her voice breaking. ''I don't know why. I don't know what is so special. But I'm
willing to do it.''

She and others who receive government benefits like Medicare and Social Security
said they paid into those programs, so they are getting what they deserve.

''All I know is government was put here for certain reasons,'' Ms. Reimer said.
''They were not put here to run banks, insurance companies, and health care and
automobile companies. They were put here to keep us safe.''

She has no patience for the Obama administration's bailouts and its actions on
health care. ''I just don't trust this government,'' Ms. Reimer said.

Jeff McQueen, 50, began organizing Tea Party groups in Michigan and Ohio after
losing his job in auto parts sales. ''Being unemployed and having some time, I
realized I just couldn't sit on the couch anymore,'' he said. ''I had the time
to get involved.''

He began producing what he calls the flag of the Second American Revolution, and
drove 700 miles to campaign for Mr. Brown under its banner. Flag sales, so far,
are not making him much. But he sees a bigger cause.

''The founding fathers pledged their lives, their fortunes and their sacred
honor,'' he said. ''They believed in it so much that they would sacrifice.
That's the kind of loyalty to this country that we stand for.''

He blames the government for  his unemployment. ''Government is absolutely
responsible, not because of what they did recently with the car companies, but
what they've done since the 1980s,'' he said. ''The government has allowed free
trade and never set up any rules.''

He and others do not see any contradictions in their arguments for smaller
government even as they argue that it should do more to prevent job loss or cuts
to Medicare. After a year of angry debate, emotion outweighs fact.

''If you don't trust the mindset or the value system of the people running the
system, you can't even look at the facts anymore,'' Mr. Grimes said.

Tea Party groups like FreedomWorks recognize that they are benefiting from the
labor of many people who have been hit hard economically. But its chairman, the
former House majority leader Dick Armey, argued that their ranks will remain
strong -- and connected -- even as members find work.

''I see these folks as pretty much the National Guard,'' Mr. Armey said. ''They
will go back to their day jobs, they will go back to their Little League and
their bridge club. But they will have their activism at the ready, and they will
stay in touch.''

Mr. Grimes, for his part, is thinking of getting a part-time job with the Census
Bureau. But he is also planning, he said,  to teach high school students about
the Constitution and limits on government powers.

''I don't think that the unemployment thing is going to change,'' he said.

URL: http://www.nytimes.com

LOAD-DATE: March 28, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: Tea Party supporters, many of whom argue for smaller government
even as they argue it should do more, rallied Saturday in Searchlight, Nev.,
Senator Harry Reid's hometown. (PHOTOGRAPHS BY JIM WILSON/THE NEW YORK TIMES)
(A24)

PUBLICATION-TYPE: Newspaper


