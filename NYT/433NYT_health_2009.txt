                   Copyright 2009 The New York Times Company


                             433 of 2021 DOCUMENTS


                               The New York Times

                             August 28, 2009 Friday
                              Late Edition - Final

Wooing Older Americans

BYLINE: By REED ABELSON

SECTION: Section A; Column 0; National Desk; MEDICARE VS. MEDICARE, PART II; Pg.
12

LENGTH: 208 words


Amid speculation about what a health care overhaul might mean for Medicare, what
do the people who actually run the program have to say?

Health and Human Services Secretary Kathleen Sebelius, whose purview includes
the federal Medicare program, sought to allay the concerns of older Americans on
Thursday, issuing a report subtitled ''Protecting Coverage and Strengthening
Medicare.''

The report contends that the proposals being discussed in Washington will
benefit older Americans by preventing Medicare from going bankrupt and by
addressing issues that older people already face, like the high out-of-pocket
costs for prescription drugs.

''Health insurance reform will protect the coverage seniors depend on, improve
the quality of care and help make Medicare strong,'' Ms. Sebelius said.

But persuading Medicare enrollees that it is in their best interest to support
changes in health care may still be an uphill battle. Bills now in Congress do
mean to squeeze savings out of Medicare, on the assumption that doctors and
hospitals can be more efficient.

Ms. Sebelius argues, however, that older Americans will be worse off if nothing
is done. ''The status quo is unsustainable and unacceptable for seniors,'' she
said. REED ABELSON

URL: http://www.nytimes.com

LOAD-DATE: August 28, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO

PUBLICATION-TYPE: Newspaper


