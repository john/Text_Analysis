                   Copyright 2010 The New York Times Company


                             1370 of 2021 DOCUMENTS


                               The New York Times

                            January 18, 2010 Monday
                              Correction Appended
                              Late Edition - Final

President and Democrats Push Hard to Salvage a Flailing Candidacy

BYLINE: By JEFF ZELENY; Michael Cooper and Katie Zezima contributed reporting.

SECTION: Section A; Column 0; National Desk; Pg. 13

LENGTH: 1116 words

DATELINE: BOSTON


President Obama came to Massachusetts on Sunday in hopes of rescuing the
flailing candidacy of Martha Coakley, the Democratic candidate in an election on
Tuesday that will determine whether the party preserves a 60-vote majority in
the Senate needed to keep alive health care legislation and the rest of the
president's agenda.

''Understand what's at stake here, Massachusetts,'' Mr. Obama said, imploring
those who supported him to take heed of the magnitude of the race to fill the
seat of the late Senator Edward M. Kennedy. ''It's whether we're going forwards
or backwards.''

The Democratic Party was deploying its full arsenal here for the final 48 hours
of the battle between Ms. Coakley and Scott Brown, a Republican state senator,
whose popularity has grown as he tapped into an angry and frustrated electorate.
As the contest lurched toward an unpredictable finish, both sides believed the
race would hinge on women voters not aligned with either party.

Democratic leaders in Congress and at the White House were bracing for what they
said was a real possibility that Ms. Coakley could lose the race. The most
alarming fact in polls and internal research, several party advisers said, was
that Ms. Coakley was still falling behind Mr. Brown among voters who had a
favorable view of the president.

In a rally here, Mr. Obama did not explicitly say that the overhaul of the
nation's health insurance system was on the line, even though Democrats in
Washington were devising contingency plans to salvage the bill if Mr. Brown
wins. Instead, he highlighted his populist proposal to tax large banks, a theme
that Democrats intend to carry through the midterm elections.

''Bankers don't need another vote in the United States Senate,'' Mr. Obama said
over booming applause in a basketball arena at Northeastern University.
''They've got plenty.''

The special election here has become the center of national politics, as several
party strategists with ties to the state have arrived. Stephanie Cutter, a
former Kennedy aide and veteran of the last two presidential campaigns, is
helping to shape Ms. Coakley's message, which has been criticized as uneven.
Scores of others took leave from their jobs in the administration and on Capitol
Hill to lend a hand over the three-day weekend.

The television airwaves were flooded by the candidates and special-interest
groups, which were pouring hundreds of thousands of dollars into weekend
advertising. While the contest has become nationalized because of its impact on
the health care legislation, the campaign also was filled with a barrage of
exchanges over abortion, same-sex marriage and other social issues.

A conservative group called the American Future Fund ran advertisements painting
Ms. Coakley as a profligate spender who will raise taxes. The Chamber of
Commerce ran an advertisement praising Mr. Brown's ''fiscal responsibility.'' A
group against the health care bill, Rethink Reform, broadcast a commercial that
mentions neither candidate, but warns the elderly that the bill would expand the
deficit and harm their care.

In a rally of his own at Mechanics Hall in Worcester, which drew nearly as many
people as the Obama rally, Mr. Brown said the ''Democratic machine'' had been
caught off guard by his surging popularity. He surrounded himself with local
legends, including Curt Schilling, the retired Red Sox pitcher, who became a
character in the race after Ms. Coakley incorrectly referred to him as a Yankee
fan.

''Air Force One is here,'' Mr. Brown said. ''Now, it wasn't exactly a scheduled
visit. It was sort of a last-minute thing, and the political machine that
controlled the Senate seat, he was told, was going to stay that way.''

While most national Republicans have publicly steered clear of the race to avoid
interfering with Mr. Brown's image as a pickup-driving fellow next door, the
efforts behind the scenes intensified Sunday. Senator John McCain, Republican of
Arizona and Mr. Obama's rival in the 2008 election, was among those who asked
his supporters to make calls on Mr. Brown's behalf.

Hours after Mr. Obama left town Sunday, Mr. Brown announced that he had received
a Democratic endorsement from the former mayor of Quincy, James Sheets.

Though Democrats enjoy a voter registration edge of three-to-one over
Republicans in the state, the race was turning on voters who are not formally
aligned with either party. If enough independents turn out, strategists said,
the traditional Democratic advantage could be overcome.

Thomas M. Menino, the mayor of Boston, expressed the urgency of the moment on
Sunday to a crowd of fellow Democrats, declaring: ''Get off our butts. Let's go
out there and find our friends.''

As Mr. Obama appeared alongside Ms. Coakley, thousands of phone calls were being
placed to voters by out-of-state volunteers. Organizing for America, the
outgrowth of the Obama campaign's army of volunteers, essentially took over the
get-out-the-vote operation for Ms. Coakley.

Jeremy Bird, the deputy director of the organization, said volunteers made
90,000 calls on Saturday to voters in the state and were on their way to
repeating that performance on Sunday. In a conference call for volunteers across
the country, Mr. Bird said: ''Don't cross your fingers. Dial them.''

The Democrats were striving to reprise the strategy that helped elect the
president. But unlike when Mr. Obama ran for office, a good share of the
frustration and anger welling among voters was directed at the Democratic
establishment, which Republicans worked to tap into. The differences between the
two approaches were palpable.

''People are frustrated and angry,'' Mr. Obama said. ''They have every reason to
be. I understand.''

The president, who was making his first political appearance of the midterm
election year, acknowledged the anger, but he accepted none of the blame. He
sought to cast the election as a choice between furthering a populist Democratic
agenda or emboldening an obstructionist Republican one.

''We have had one year to make up for eight,'' Mr. Obama said. ''It hasn't been
quick, it hasn't been easy. But we've begun to deliver on the change you voted
for.''

As Ms. Coakley was surrounded by Mr. Obama and a parade of Democratic members of
Congress and local officeholders, Mr. Brown held his ''people's rally'' at
Mechanics Hall. By design, there were no Republican officials, but rather a cast
of local legends, including Mr. Schilling; Doug Flutie, the former quarterback
for the New England Patriots; and John Ratzenberger, who played the affable
mailman Cliff Clavin on ''Cheers.''

''You guys get this,'' Mr. Flutie said. ''Everybody loves an underdog story.''

URL: http://www.nytimes.com

LOAD-DATE: January 18, 2010

LANGUAGE: ENGLISH

CORRECTION-DATE: January 19, 2010



CORRECTION: An article yesterday about President Obama's visit on Sunday to
Massachusetts in support of the state's Democratic senatorial candidate, Martha
Coakley, misspelled, in some editions, the surname of the deputy director of
Organizing for America, which is handling the get-out-the-vote effort for Ms.
Coakley. He is Jeremy Bird, not Byrd.

GRAPHIC: PHOTO: Martha Coakley, the Democratic candidate in the Massachusetts
Senate race, greeted President Obama on Sunday in Boston. (PHOTOGRAPH BY DOUG
MILLS/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


