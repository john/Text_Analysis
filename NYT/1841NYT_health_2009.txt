                   Copyright 2010 The New York Times Company


                             1841 of 2021 DOCUMENTS


                               The New York Times

                             April 3, 2010 Saturday
                              Late Edition - Final

Urologist Posts His Politics On His Florida Office Door

BYLINE: By DAMIEN CAVE; Amy Green contributed reporting from Mount Dora, Fla.

SECTION: Section A; Column 0; National Desk; Pg. 11

LENGTH: 497 words


MIAMI -- Doctors take the Hippocratic oath promising to do no harm, but once
that requirement is fulfilled, do they have a right to choose patients based on
politics?

One Republican urologist outside Orlando has stirred up a tempest by suggesting
that they do. A sign hanging this week in the office of Dr. Jack Cassell clearly
states a preference for patients who agree with his opposition to the president,
and to the recently passed health care overhaul.

''If you voted for Obama,'' says the taped-up sign, ''seek urologic care
elsewhere. Changes to your healthcare begin right now. Not in four years.''

Perhaps it was just a matter of time before the partisan rancor surrounding
health care found its way into patient care. Civil rights law prevents
discrimination based on sex or religion, but experts say that political
differences are not specifically protected  --  consider it a pre-existing
condition that can still be used for patient filtering.

The logic for some, especially here in a contentious swing state like Florida,
seems to be simple: Stand up and stay separated. If states, counties or towns
can be red and blue, Republican and Democratic, why not every place  within
those areas? Why not make sure that even in doctors' offices people can feel
secure in knowing they are sharing space with those who are share their views?

Dr. Cassell, 56, could not be reached for comment. The phone at his office was
continuously busy Friday, and the doors were locked before 4 p.m.

In an interview with The Orlando Sentinel, he insisted that he would not refuse
to treat a patient because of politics. ''That would be unethical,'' he said.

But he added that he would not mind if Democrats saw the sign and found another
doctor. His wife is a Republican candidate for the local county commission. And
Mount Dora, where Dr. Cassell's office is located, is a town of 10,000 that
leans conservative, so perhaps his position is just smart family politics.

That seemed to be the case Friday afternoon, when an older man in a yellow shirt
dropped a letter of support into the doctor's mailbox. Several passers-by also
praised the doctor for taking a public stand against the health care overhaul.

''You're going to see this more and more,'' said Bob Cowan, 58, ''People are
very angry.''

Patients who disagree may feel less inclined to speak up. At least one patient
referred to Dr. Cassell has sought care elsewhere, according to the office of
Representative Alan Grayson. Mr. Grayson, a Democrat who was one of the boldest
supporters of health care in the House, represents the neighborhood where Dr.
Cassell's office sits. His response was characteristic outrage.

''I think it's disgusting,'' he said. ''I know that most people go into health
care because they want to help sick people. They don't have some political
agenda. I think it's outrageous that someone would try to press his political
agenda.''

He added, ''I think the sore losers are out in force.''

URL: http://www.nytimes.com

LOAD-DATE: April 3, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO (PHOTOGRAPH BY DEIRDRE LEWIS/THE ORLANDO SENTINEL)

PUBLICATION-TYPE: Newspaper


