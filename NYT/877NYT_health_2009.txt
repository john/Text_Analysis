                   Copyright 2009 The New York Times Company


                             877 of 2021 DOCUMENTS


                               The New York Times

                           October 24, 2009 Saturday
                              Late Edition - Final

Breaking Down Savings

BYLINE: By ANNE UNDERWOOD

SECTION: Section A; Column 0; National Desk; AN OVERHAUL'S COST; Pg. 11

LENGTH: 270 words


Arthur Ullian is president of the National Council on Spinal Cord Injury and
co-author of a coming analysis in the Proceedings of the National Academy of
Sciences on the economic benefits of a health care overhaul. Mr. Ullian spoke
with Anne Underwood, a freelance writer, and the following is an excerpt:

QUESTION. The Congressional Budget Office projects costs of around $800 billion
over 10 years for the Senate Finance Committee bill.

ANSWER. It's not sustainable. But that's because no one is taking into account
the health improvements that will result from covering millions of the
uninsured.

Q. How does that help?

A. There are two parts to this theory. The first relates to the expansion of the
labor force as a result of improved health, stemming from health care reform and
advances in medicine. ... We project that by 2020 there will be an additional
8.5 million workers over age 65. The increased tax revenues would add $312
billion to the U.S. Treasury in 2020 alone.

The second part comes from Medicare savings. By providing the uninsured access
to health care, millions of people will reach the Medicare eligibility age
healthier than they do now, spending fewer Medicare dollars. We project that by
2020 the savings will be $242 billion. By 2030, they will be $530 billion.

Q. Combining the two factors -- the revenue from taxes plus the savings to
Medicare -- are you saying that a health overhaul becomes revenue-neutral?

A. The added revenue plus savings add up to $554 billion in 2020, or $1.457
trillion in 2030. That's way in excess of the estimated costs of health reform

ANNE UNDERWOOD

URL: http://www.nytimes.com

LOAD-DATE: October 24, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


