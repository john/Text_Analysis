                   Copyright 2009 The New York Times Company


                             198 of 2021 DOCUMENTS


                               The New York Times

                             July 23, 2009 Thursday
                              Late Edition - Final

Wishes and Qualms Of Centrists on Health Bill

SECTION: Section A; Column 0; National Desk; Pg. 20

LENGTH: 314 words


Centrist lawmakers from both parties are crucial to the success of a health care
deal in Congress. They all want to cover the uninsured and pass a bill
overhauling the system, but raise various concerns about the specifics.

Senator Ben Nelson,

Democrat of Nebraska

One of the Senate's most conservative Democrats, he asked President Obama to
slow the health care overhaul process in a meeting with the president last week.
Worries about costs; says legislation must not add to federal budget deficits.

Senator Susan Collins,

Republican of Maine

Says current bills do not do enough to reward high-quality, low-cost health care
. Says she is ''very skeptical'' of proposed new government health insurance
plan, based on experience with Maine's Dirigo health program. Is concerned about
possible new costs to families, employers and state governments.

Representative Mike Ross,

Democrat of Arkansas

Point man on health care for the Blue Dogs, a coalition of fiscally conservative
House Democrats. Is concerned about cost of the legislation, possible adverse
effects on small businesses and possible underpayment of rural hospitals by
proposed new government health insurance plan.

Representative Charlie Melancon,

Democrat of Louisiana

Worries that new government insurance plan could become a big entitlement
program. Says Congress must ''find every penny of savings within our current
public programs and get more value for every health care dollar we spend.''

Senator Olympia J. Snowe,

Republican of Maine

Is skeptical about taxing employer-provided health benefits. Says it is
inconceivable that the Senate could pass a big health bill before its August
recess. Says a new public insurance plan should be established only as a last
resort, if competition among private insurers, new federal regulations and
''delivery system reforms'' fail to rein in soaring health costs.

URL: http://www.nytimes.com

LOAD-DATE: July 23, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS

PUBLICATION-TYPE: Newspaper


