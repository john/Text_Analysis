                   Copyright 2009 The New York Times Company


                             282 of 2021 DOCUMENTS


                               The New York Times

                             August 9, 2009 Sunday
                              Late Edition - Final

Inside the Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 985 words


International

IRAQI FORCES TAKE LEAD, BUT STILL LEAN ON THE U. S.

With American forces no longer stationed in Iraqi cities, a new dynamic has
emerged between Iraqi and American troops. And as both sides settle into new
roles, resentment and frustration are showing. Page 6

PLOT TO KILL INDONESIA CHIEF

Police officials said they foiled plans by an Islamist group to assassinate
Indonesia's president. Officials declined to confirm reports that, in a separate
raid, Southeast Asia's most wanted terrorism suspect was killed. Page 8

SUICIDE ATTACK IN MAURITANIA

A suicide bomber wounded two guards just outside the walls of the French Embassy
in Nouakchott, Mauritania's capital, an official at the French Embassy said.
Page 9

COUP DIVIDES HONDURANS

A growing divide between supporters and critics of the ousted president is
threatening stability in Honduras. And rhetoric from both sides makes
reconciliation seem unlikely any time soon. Page 8

FATAH PRESIDENT CONFIRMED

Mahmoud Abbas, running unopposed, won nearly all the delegates at a party
conference in the West Bank city of Bethlehem. Page 8

Russia Vows to Fix Deadly Traffic 8

National

DRUG INDUSTRY TO RUN ADS BACKING WHITE HOUSE PLAN

The pharmaceutical industry has authorized its lobbyists to spend as much as
$150 million on television commercials supporting a health care overhaul, people
briefed on the plans said. PAGE 14

SOTOMAYOR IS SWORN IN

Justice Sonia Sotomayor was officially sworn in as the first Hispanic and third
female Supreme Court Justice. PAGE 10

BOSTON EMBRACES CYCLISTS

Boston is working to shed its reputation as a minefield for cyclists by
installing new bike lanes, racks and paths.  PAGE 10

Metropolitan

HOT WHEELS, WARM HEARTS; S.U.V. CLUBS ADD A PURPOSE

A unique kind of S.U.V. club has emerged in New York, with fraternity-like
rituals, jaunty aliases and matching vests. The clubs have also added a twist to
S.U.V. ownership: community service. PAGE 1

PREPARING THE PONIES

Horse racing has no more quintessential tradition than the morning works. In the
mists of dawn, trainers put their steeds through their pre-competition paces.
PAGE 10

Sports

A LEGEND IN GOLF AND LIFE TO BE HONORED BY P.G.A.

After 63 years, Bill Powell's Clearview Golf Club still stands as a monument to
a man who battled racism in relative obscurity most of his life. But on the eve
of the P.G.A. Championship, he will receive the organization's highest award for
his many achievements.  PAGE 1

DIVORCE RATES IN THE N.F.L.

Polls, studies and anecdotal evidence suggest that the divorce rate for N.F.L.
players is between 60 and 80 percent, which is higher than that of the general
population, but comparable to other sports.  PAGE 1

ORTIZ DENIES USING STEROIDS

At a news conference at Yankee Stadium, the Boston Red Sox slugger David Ortiz
said he had been careless about using supplements and vitamins that may have
triggered a positive test for performance-enhancing drugs in 2003. PAGE 3

Obituaries

CARLEEN HUTCHINS, 98

An innovator in violin construction, she conducted hands-on acoustic research,
harnessing technology so that modern hands might build instruments to rival the
work of 17th-  and 18th-century masters, all the while reimagining the idea of
what a violin could be.  PAGE 20

Sunday Business

MEDICAL PRESCRIPTIONS AS A PUBLIC COMMODITY

Many people think their prescriptions are private. But prescriptions, and all
the information on them, are bought and sold in a murky marketplace. That may
change if protections from the Obama administration are enforced. PAGE 1

IMPERFECT POLITICS OF PAY

The executive compensation bill that the House passed is supposed to help
prevent risky executive pay in corporate America. But there are questions about
whether it will. Fair Game, by Gretchen Morgenson. PAGE 1

Arts & Leisure

WHY BABY BOOMERS WON'T LET GO OF WOODSTOCK

That moment of muddy grace -- when thousands of people gathered in Bethel (not
Woodstock), N.Y., listened to great musicians, enjoyed legal and illegal
pleasures and felt like a giant community --endures 40 years later. PAGE 1

SPOON-FED CINEMA

There may be no more incisive rendering of Hollywood's self-image than the
overgrown child played by Adam Sandler in ''Funny People.'' And perhaps no
truer, more damning mirror held up to the audience, A. O. Scott writes. PAGE 1

Magazine

KARZAI IN HIS LABYRINTH

The Afghan president is isolated and distrusted, and even if he manages to be
re-elected this month, that's not likely to change. PAGE 26

Book Review

NATURAL MAN

How the city-born Theodore Roosevelt became one of the greatest forces in
American conservation is the subject of Douglas Brinkley's ''Wilderness
Warrior.''  PAGE 1

Travel

THE TWO FACES OF GHANA

This West African country, once the last stop for countless slaves headed across
the sea, is now a welcoming destination with a vibrant culture. PAGE 1

Automobiles

AN AWAKENING FOR LINCOLN

There yet is another new crossover: the Lincoln MKT, a wagon powered by the
first of Ford's EcoBoost engines.  SportsSunday, PAGE 1

Editorial

THE MASSACHUSETTS MODEL

Opponents of national health care reform claim that Massachusetts's experiment
is a fiscal disaster. But the state has shown that it is possible to both insure
a majority of citizens and pay the bills. Now it must tackle the relentless rise
in medical costs. Week in Review, PAGE 7

Their Gamble, Our Money   Week in Review, Page 7

The Dream of Hydrogen   Week in Review, Page 7

Op-Ed

NICHOLAS D. KRISTOF

Your soul needs a long walk in the wilderness. Here are some tips to help with
the journey.   Week in Review, PAGE 10

MAUREEN DOWD

The crazy train rolls on, but it's hard to tell who's driving.  Week in Review,
PAGE 9

THOMAS L. FRIEDMAN

A dynamic emerging on the ground in the West Bank carries big potential for the
Palestinians.  Week in Review, PAGE 8

URL: http://www.nytimes.com

LOAD-DATE: August 9, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


