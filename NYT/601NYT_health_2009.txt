                   Copyright 2009 The New York Times Company


                             601 of 2021 DOCUMENTS


                               The New York Times

                           September 15, 2009 Tuesday
                              Late Edition - Final

New Objections to Baucus Health Care Proposal

BYLINE: By ROBERT PEAR and DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 23

LENGTH: 886 words

DATELINE: WASHINGTON


Two of the three Republicans in a small group trying to forge a bipartisan
compromise on health care have requested numerous major changes in a proposal
drafted by the chairman of the Senate Finance Committee, reducing the chances
that he can win their support.

The Republicans, Senators Michael B. Enzi of Wyoming and Charles E. Grassley of
Iowa, have catalogued their concerns in documents sent to the chairman, Senator
Max Baucus, Democrat of Montana.

President Obama has broadly endorsed many elements of Mr. Baucus's proposal,
which White House officials praise as a possible template for comprehensive
health legislation, Mr. Obama's top domestic priority.

Six members of the Finance Committee -- three Democrats and three Republicans --
have been trying since June to agree on a bill that would expand health
insurance coverage and rein in health costs.

Senator Kent Conrad, Democrat of North Dakota, said Monday that the cost of the
package, originally put at $1 trillion over 10 years, was now less than $880
billion.

But many governors in both parties still have deep concerns about a provision
that would require states to pay some of the cost of covering millions of
additional low-income people under Medicaid. And Mr. Enzi indicated that he
shared their concerns.

A summary of the senators' views, prepared by the Finance Committee, says Mr.
Enzi believes that the federal government should pay ''100 percent of the cost
of the Medicaid expansion, in order to avoid an unfunded mandate'' for states,
which ordinarily share Medicaid costs with the federal government.

Mr. Enzi and Mr. Grassley have also objected to the fees that Mr. Baucus wants
to impose on health insurance companies, clinical laboratories and manufacturers
of medical devices. Such fees would help finance coverage of the uninsured.

Mr. Enzi and Mr. Grassley also told Mr. Baucus that health legislation must
include language affirmatively prohibiting the use of federal money to pay for
abortion. The restriction, they said, should apply to any subsidies that help
low-income people buy insurance. In addition, they said, health plans should not
be obliged to provide abortion. Thus, they said, the bill should ''include a
conscience clause to protect entities from being required to contract with
abortion providers.''

By contrast, a Democrat participating in the negotiations, Senator Jeff Bingaman
of New Mexico, told colleagues that the legislation should ''remain silent'' on
abortion, according to the committee documents.

Mr. Baucus and other senators agree that illegal immigrants should not benefit
from the health care overhaul in any way. Mr. Enzi and Mr. Grassley want  a
five-year waiting period for legal immigrants to receive tax credits, or
subsidies, to help them buy insurance.

Mr. Obama and most Democrats support a proposal that would require most
Americans to carry health insurance. Under Mr. Baucus's proposal, a family that
went without coverage would be subject to a penalty of up to $3,800 a year.

The committee documents show that Mr. Grassley has reservations about this
approach. He believes that ''the individual responsibility to have health
coverage should be reconsidered and replaced with a reinsurance policy to ensure
that affordable health coverage is available to everyone in a voluntary system,
with a lower overall cost for the package,'' one document says.

Under a reinsurance program, people carry private insurance, but the government
might pick up some or all of the cost of claims exceeding a certain amount.

The third Republican in the group, Senator Olympia J. Snowe of Maine, is pushing
another idea to make insurance readily available. She urged her colleagues to
''allow private insurance companies to offer national plans, with uniform
benefit packages that are offered across state lines.''

Mr. Baucus said the Finance Committee would take up the legislation and start
voting on it next week. That would allow the full Senate to consider the
legislation next month.

But Senate and House leaders have missed many self-imposed deadlines in the
past. Senate leaders would need to meld any Finance Committee bill with one
approved in July by the Senate health committee, and that could be a challenge.

The Finance Committee, like Democratic leaders of the House, intend to expand
Medicaid to cover anyone with income less than 133 percent of the poverty level
($29,327 for a family of four).

Under a bill approved by the House Energy and Commerce Committee, the federal
government would pick up all the additional Medicaid costs for two years, 2013
and 2014, but states would have to pay 10 percent of the cost for the newly
eligible Medicaid recipients starting in 2015.

Members of the Senate Finance Committee said they might require states to pay a
larger share, perhaps 15 percent or 20 percent of the new costs.

Governors of both parties object to such requirements as an ''unfunded
mandate.''

In a letter to the Senate, Gov. Mitch Daniels of Indiana, a Republican, said:
''States will likely have to pick up the tab for this extension of Medicaid. We
have estimated that the price for Indiana could reach upwards of $724 million
annually. These additional costs will overwhelm our resources and obliterate the
reserves we have fought so hard to protect.''

URL: http://www.nytimes.com

LOAD-DATE: September 15, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Senator Max Baucus met with members of the news media on Monday
to discuss bipartisan negotiations on health care.(PHOTOGRAPH BY LUKE
SHARRETT/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


