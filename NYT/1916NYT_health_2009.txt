                   Copyright 2010 The New York Times Company


                             1916 of 2021 DOCUMENTS


                               The New York Times

                            April 21, 2010 Wednesday
                              Late Edition - Final

Reform and Massachusetts

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL; Pg. 26

LENGTH: 727 words


Four years after Massachusetts enacted its ambitious health care reform, the
state has achieved its goal: covering most of the uninsured without seriously
straining its budget. Most citizens seem to like it.

Massachusetts cannot stop there. It also needs to figure out how to rein in the
escalating costs of medical care and health insurance. The new national reform
law includes many provisions designed, over time, to reduce costs, but
Massachusetts will have to move sooner.

The Massachusetts Taxpayers Foundation, a business-supported nonpartisan
watchdog, calls the state's health reform ''remarkably successful'' in expanding
coverage to 97 percent of residents at a modest incremental cost to taxpayers,
consistent with projections.

It also has strong popular support. Even after Senator Scott Brown won election
this year with a pledge to block national health care reform, half of his voters
(and two-thirds of all voters) told pollsters that they supported their own
state's similar reforms.

While an independent candidate for governor, Timothy Cahill, the state
treasurer, is denouncing reform as a ''fiscal train wreck'' -- a view eagerly
embraced by national critics of reform -- the newly chosen Republican candidate,
Charles Baker, did not even mention the issue in his speech to his party's
convention.

So what is really going on? Massachusetts is struggling to pay its Medicaid
bills, as are most states in this deep recession. Health reform increased the
number of poor people eligible for Medicaid. Half of the added costs were
matched by federal funds, and the problem should dissipate in all states as the
recession recedes.

Critics also point to sudden jumps in premium rates (up to 34 percent) proposed
for individuals and small businesses. That is not because of reform. Remember
California's Anthem Blue Cross, which announced increases of up to 39 percent
for individuals this year?

But now that Massachusetts has required all individuals to buy insurance and all
employers to provide it or pay penalties, it will have to quickly deal with the
problem. State regulators recently rejected almost 90 percent of the proposed
increases. Even if the rejections survive legal challenges, they are a
short-term fix.

Like the rest of the nation, the state needs to deal with the underlying issue:
the relentlessly rising prices charged by health care providers. Those are
driven in part by costly new technologies and treatments. In Massachusetts, it
is exacerbated by the outsized bargaining power of prestigious teaching
hospitals and regionally dominant community hospitals.

When Massachusetts's politicians designed their reform, they calculated that
achieving near-universal coverage first would then give all participants in the
health care system an incentive to help rein in costs. There are encouraging
signs that that is starting to happen.

One hospital consortium, Partners HealthCare, that had been accused of
extracting unjustifiably high reimbursements from insurers, has recently offered
$40 million to help reduce big premium increases for small businesses and says
it wants to be ''part of the solution'' in finding ways to reduce costs.

State leaders have commissioned studies and held hearings to come up with more
fundamental solutions. Legislative leaders seem determined to end within five
years the prevailing fee-for-service system, in which doctors and hospitals are
paid for the volume of care they provide whether or not it is high quality or
needed.

One possible substitute might be a system in which groups of doctors and
hospitals are paid a fixed sum to provide whatever care a patient needs over the
course of a year. There are also proposals in the Legislature to let the
government regulate provider payments as a temporary solution, and some experts
think regulation may be the most effective way to tame costs in the long run.

Massachusetts's experience offers some useful lessons for the national reform
effort. That begins with the fact that once citizens have near-universal
coverage, they like it -- no matter what current polls and politicians may say.
And while the federal reform law is confronting the cost problem from the start
with a slew of pilot projects to determine what works best, the administration
and Congress will need to press hard to expand every promising approach.

URL: http://www.nytimes.com

LOAD-DATE: April 23, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


