                   Copyright 2010 The New York Times Company


                             1943 of 2021 DOCUMENTS


                               The New York Times

                             April 30, 2010 Friday
                              Late Edition - Final

States Decide On Running New Pools For Insurance

BYLINE: By ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 15

LENGTH: 652 words


WASHINGTON -- The fight over the new health care law shifted Thursday to the
states, as some governors claimed federal money to run a new insurance pool for
people with serious medical problems, while officials in other states said they
would not operate the program.

Friday is the deadline for states to tell the Obama administration whether they
want to run the high-risk insurance pool for uninsured people with pre-existing
conditions, or whether they will leave the task to Kathleen Sebelius, the
secretary of health and human services.

Democratic officials in Montana, Pennsylvania, Washington and Wisconsin, among
other states, said they intended to operate the program under contract with the
federal government. They were joined by Gov. Arnold Schwarzenegger of
California, a Republican, who gave a rousing endorsement of President Obama's
health plan at a news conference.

But Republican officials in Georgia, Indiana, Nebraska and Nevada turned down
the opportunity to run the high-risk pool, as did at least one Democratic
governor, Dave Freudenthal of Wyoming.

Mr. Freudenthal said he worried that his state's federal allotment of $8 million
''may prove insufficient'' to subsidize coverage for the next three and a half
years. The temporary federal program runs from July to Jan. 1, 2014, when
insurers will be required to accept all applicants.

Gov. Jennifer M. Granholm of Michigan, a Democrat, hailed the high-risk pool as
''a first step in providing health care coverage for those who currently don't
have any.'' Mr. Schwarzenegger said, ''We are ready to roll up our sleeves and
work with the federal government.'' California expects to receive $761 million.

More than a dozen states have sued the federal government, challenging a
provision of the new law that will require most Americans to carry insurance.
But Mr. Schwarzenegger said, ''The federal government has the right to force you
into having a health care plan.''

Karen E. Timberlake, secretary of the Wisconsin Department of Health Services,
said she and Gov. James E. Doyle, a Democrat, had decided to participate in the
federal program. The state expects to receive $73 million from the federal
government.

In Pennsylvania, Amy Kelchner, a spokesman for Gov. Edward G. Rendell, a
Democrat, said he was eager to participate.

Jonathan E. Seib, health policy adviser to Gov. Christine Gregoire of
Washington, a Democrat, said: ''Even though the money is limited, it can provide
assistance that would not otherwise be available to people with pre-existing
conditions. We will manage the program within the dollars available, $102
million over three years.''

But Gov. Dave Heineman of Nebraska, a Republican, said, ''We are very concerned
that funding will not be sufficient,'' even if the state limited enrollment in
the new pool.

The insurance commissioner of Georgia, John W. Oxendine, a Republican running
for governor, described the pool as ''the first step in the recently enacted
federal takeover of the United States health care system.''

Another Republican, Gov. Gary R. Herbert of Utah, said federal officials had not
been able to tell him what would happen if the state exhausted its allocation of
federal money before 2014.

''I have strong concerns that the program is severely underfunded and will
ultimately result in yet another unfunded mandate on our state,'' Mr. Herbert
said.

Rate-Increase Plan Withdrawn

LOS ANGELES (AP) -- Anthem Blue Cross withdrew plans to raise health insurance
rates for Californians by as much as 39 percent after an independent audit
determined that the company's justification for raising premiums was based on
flawed data, the state insurance commissioner, Steve Poizner, said Thursday.
Anthem said separately that it would file a new application for a rate increase,
perhaps as soon as next month. It added that any errors in its original
application were inadvertent.

URL: http://www.nytimes.com

LOAD-DATE: April 30, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


