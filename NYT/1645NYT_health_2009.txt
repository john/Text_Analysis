                   Copyright 2010 The New York Times Company


                             1645 of 2021 DOCUMENTS


                               The New York Times

                             March 12, 2010 Friday
                              Late Edition - Final

Health Reform Myths

BYLINE: By PAUL KRUGMAN

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 27

LENGTH: 803 words


Health reform is back from the dead. Many Democrats have realized that their
electoral prospects will be better if they can point to a real accomplishment.
Polling on reform  --  which was never as negative as portrayed  --  shows signs
of improving. And I've been really impressed by the passion and energy of this
guy Barack Obama. Where was he last year?

But reform still has to run a gantlet of misinformation and outright lies. So
let me address three big myths about the proposed reform, myths that are
believed by many people who consider themselves well-informed, but who have
actually fallen for deceptive spin.

The first of these myths, which has been all over the airwaves lately, is the
claim that President Obama is proposing a government takeover of one-sixth of
the economy, the share of G.D.P. currently spent on health.

Well, if having the government regulate and subsidize health insurance is a
''takeover,'' that takeover happened long ago. Medicare, Medicaid, and other
government programs already pay for almost half of American health care, while
private insurance pays for barely more than a third (the rest is mostly
out-of-pocket expenses). And the great bulk of that private insurance is
provided via employee plans, which are both subsidized with tax exemptions and
tightly regulated.

The only part of health care in which there isn't already a lot of federal
intervention is the market in which individuals who can't get employment-based
coverage buy their own insurance. And that market, in case you hadn't noticed,
is a disaster  --  no coverage for people with pre-existing medical conditions,
coverage dropped when you get sick, and huge premium increases in the middle of
an economic crisis. It's this sector, plus the plight of Americans with no
insurance at all, that reform aims to fix. What's wrong with that?

The second myth is that the proposed reform does nothing to control costs. To
support this claim, critics point to reports by the Medicare actuary, who
predicts that total national health spending would be slightly higher in 2019
with reform than without it.

Even if this prediction were correct, it points to a pretty good bargain. The
actuary's assessment of the Senate bill, for example, finds that it would raise
total health care spending by less than 1 percent, while extending coverage to
34 million Americans who would otherwise be uninsured. That's a large expansion
in coverage at an essentially trivial cost.

And it gets better as we go further into the future: the Congressional Budget
Office has just concluded, in a new report, that the arithmetic of reform will
look better in its second decade than it did in its first.

Furthermore, there's good reason to believe that all such estimates are too
pessimistic. There are many cost-saving efforts in the proposed reform, but
nobody knows how well any one of these efforts will work. And as a result,
official estimates don't give the plan much credit for any of them. What the
actuary and the budget office do is a bit like looking at an oil company's
prospecting efforts, concluding that any individual test hole it drills will
probably come up dry, and predicting as a consequence that the company won't
find any oil at all  -- when the odds are, in fact, that some of the test holes
will pan out, and produce big payoffs. Realistically, health reform is likely to
do much better at controlling costs than any of the official projections
suggest.

Which brings me to the third myth: that health reform is fiscally irresponsible.
How can people say this given Congressional Budget Office predictions  --
which, as I've already argued, are probably too pessimistic  --  that reform
would actually reduce the deficit? Critics argue that we should ignore what's
actually in the legislation; when cost control actually starts to bite on
Medicare, they insist, Congress will back down.

But this isn't an argument against Obamacare, it's a declaration that we can't
control Medicare costs no matter what. And it also flies in the face of history:
contrary to legend, past efforts to limit Medicare spending have in fact
''stuck,'' rather than being withdrawn in the face of political pressure.

So what's the reality of the proposed reform? Compared with the Platonic ideal
of reform, Obamacare comes up short. If the votes were there, I would much
prefer to see Medicare for all.

For a real piece of passable legislation, however, it looks very good. It
wouldn't transform our health care system; in fact, Americans whose jobs come
with health coverage would see little effect. But it would make a huge
difference to the less fortunate among us, even as it would do more to control
costs than anything we've done before.

This is a reasonable, responsible plan. Don't let anyone tell you otherwise.

URL: http://www.nytimes.com

LOAD-DATE: March 12, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


