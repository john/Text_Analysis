                   Copyright 2010 The New York Times Company


                             1845 of 2021 DOCUMENTS


                               The New York Times

                             April 3, 2010 Saturday
                         The New York Times on the Web

In N.C., Obama Hails Jobs Report

BYLINE: By PETER BAKER

SECTION: Section ; Column 0; National Desk; Pg.

LENGTH: 718 words


CHARLOTTE, N.C. -- President Obama declared on Friday that ''we are beginning to
turn the corner'' after a deep recession that nearly threw the economy into
another Great Depression. He hailed a new government report showing stronger job
growth and credited his policies with helping businesses rebuild.

Visiting a plant that received federal money from his economic stimulus package
and that is now creating new jobs, Mr. Obama said ''today is an encouraging
day'' because the fruits of his programs were becoming visible. But he cautioned
that more difficult times were still ahead before the country reclaimed the
economic ground lost in the past few years.

''This has been a harrowing time for our country, and it's easy to grow cynical
and wonder whether America's best days are behind us, especially after such a
crisis,'' Mr. Obama told a crowd of workers in a hangarlike building, surrounded
by racks of battery material. ''What we can see here at this plant is, the worst
of the storm is over, that brighter days are still ahead.''

At the same time, Mr. Obama said, ''While we've come a long way, we've still got
a ways to go.'' He added: ''Government can't reverse the toll of this recession
overnight. And government on its own can't replace the eight million jobs that
have been lost.''

Mr. Obama was responding to a Labor Department report indicating that the
economy added 162,000 nonfarm jobs in March, the most in three years. He noted
that a year ago, the economy was losing as many as 700,000 jobs a month.

To an extent, the growth in March was exaggerated by the government's hiring of
48,000 census workers to conduct the once-a-decade national head count, jobs
that will mainly last only a few months. Moreover, the unemployment rate
remained steady at 9.7 percent in March, and economists expect it to rise later
in the year as discouraged workers resume looking for jobs.

Republicans pointed out that though Mr. Obama was emphasizing the payroll
numbers, unemployment still remained significantly higher than the White House
initially forecast it would be at this point even if Congress had not enacted
the $787 billion stimulus package last year. What then, they asked, had the
stimulus actually achieved?

Still, the new figures provided a political boost for the president less than a
week after he signed the final elements of his landmark health care overhaul
into law and finished an arms control treaty with Russia. After months on the
defensive --  with his poll numbers falling, his legislative program stalled and
the economy still fallow --  Mr. Obama now appears to feel reinvigorated.

To highlight his economic agenda, he flew to Charlotte to visit Celgard, a firm
that produces material for lithium batteries and has received $49 million from
the stimulus program. The company is expanding its Charlotte plant and building
a new facility in nearby Concord. Mr. Obama said the stimulus money was helping
Celgard create 300 new jobs directly and as many as 1,000 new jobs for suppliers
and contractors.

But The Charlotte Observer, citing the Energy Department, reported that Celgard
had not yet begun spending any of the stimulus money. And at least some
residents of the area were not satisfied, as they gathered along Mr. Obama's
motorcade route holding up signs like ''You Lie,'' ''Vote Them Out'' and ''Stop
Spending My Future.''

North Carolina is the fourth swing state Mr. Obama has visited in the past week
or so, following Iowa, Virginia and Maine. He won all four of them in 2008, but
the White House is carefully tending to all of them given the flight of
independents from Mr. Obama recorded in recent polls.

Taking questions from Celgard workers, Mr. Obama fielded mostly friendly
inquiries. But when a woman asked why he was raising taxes in the health care
program, ''because we're overtaxed as it is,'' Mr. Obama bemoaned what he called
''a whole lot of misinformation'' and talked for a long time about the virtues
of the overhaul before actually addressing taxes.

When he did respond to her question, he said that the health care program was
financed in part by ''some additional taxes that we think are fair,'' and he
cited a provision imposing Medicare taxes on capital gains and dividend income,
which will affect primarily wealthier Americans.

URL: http://www.nytimes.com

LOAD-DATE: April 3, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


