                   Copyright 2010 The New York Times Company


                             1811 of 2021 DOCUMENTS


                               The New York Times

                             March 30, 2010 Tuesday
                              Late Edition - Final

Voices

SECTION: Section D; Column 0; Science Desk; Pg. 4

LENGTH: 395 words


The dramatic passage of health care reform was a historic turning point in the
effort to repair a deeply broken system. But by itself, the legislation will not
be enough to address the needs of many people of color, who face higher rates of
infant mortality, chronic disease and disability, and premature death than white
Americans do.

The new law will help expand insurance coverage and improve access to health
care providers in underserved communities, among other benefits. But the major
reasons for the persistence of racial and ethnic health inequalities are
socioeconomic inequality and differences in neighborhood living conditions --
both of them fueled by residential segregation. These are the issues that
policymakers must tackle if we are to improve opportunities for good health for
all.

It's not just what the bill will do; it's also what it won't do.

It won't do anything to lower premiums. Insurance premiums will roughly double
over the next 6 to 10 years for people with group coverage through work, just as
they would have without the legislation. For nearly 37 million people in the
individual insurance market, the new minimum package of benefits means that they
can expect to pay 10 to 13 percent more than they would have if the bill hadn't
passed.

About 57 percent of these workers will receive at least some subsidies to help
offset the cost, but 43 percent will have to bear the full cost. True, they will
receive more benefits, but it will not be by their choice.

Everyone should recognize that there will be no parting of the heavens. Changes
in a $2.6 trillion industry will not come quickly. For the immediate future, the
number of uninsured will most likely keep on rising. Spending will keep
increasing at excessive rates. The quality of care will not be instantly
transformed.

But a process of reform that will take a generation to unfold must begin now.
And that requires a sustained effort by health reform supporters to explain to
the public what is in the bill and to clear away misconceptions created by
opponents.

Consumers stand to gain enormously from efficient, well-designed health
insurance exchanges. But state officials opposed to reform may well stall and
create roadblocks. So supporters should mobilize now to see that as many states
as possible move promptly to enact the laws necessary to create these exchanges.

URL: http://www.nytimes.com

LOAD-DATE: March 30, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: BRIAN D. SMEDLEY: Vice president and director, Health Policy
Institute at the Joint Center for Political and Economic Studies.
MICHAEL D. TANNER: Senior fellow, Cato Institute.
 HENRY J. AARON: Senior fellow, Brookings Institution.

PUBLICATION-TYPE: Newspaper


