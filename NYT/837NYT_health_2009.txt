                   Copyright 2009 The New York Times Company


                             837 of 2021 DOCUMENTS


                               The New York Times

                           October 17, 2009 Saturday
                              Late Edition - Final

Impatiently Waiting

BYLINE: By CHARLES M. BLOW

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 19

LENGTH: 477 words


When, Mr. President? When will your deeds catch up to your words? The people who
worked tirelessly to get you elected are getting tired of waiting. According to
a Gallup poll released on Wednesday, Americans' satisfaction with the way things
are going in the country has hit a six-month low, and those decreases were led,
in both percentage and percentage-point decreases, by Democrats and
independents, not by Republicans.

The fierce urgency of now has melted into the maddening wait for whenever.

Take health care reform. Because of the president's quixotic quest for
bipartisanship, he refused to take a firm stand in favor of the public option.
In that wake, Democrats gutted the Baucus bill to win the graces of Olympia
Snowe  -- a Republican senator from a state with half the populationof Brooklyn,
a senator who is defying the will of her own constituents. A poll conducted
earlier this month found that 57 percent of Maine residents support the public
option and only 37 percent oppose it.

She is certainly living up to the state's motto: Dirigo. That's Latin for ''I
lead.'' And the Democrats have followed. For shame.

When will the president take the risk of standing up for his convictions on
health care instead of sacrificing good policies for good politics? (Or maybe
not even good politics since a one-sided compromise is the same as a surrender.)

And health care is only one example.

On the same weekend that gay rights protesters marched past the White House, the
president again said that his administration was ''moving ahead on don't ask
don't tell.'' But when? This month? This year? This term?

As we prepare to draw down troops from the disaster that was the war in Iraq, we
may commit more troops to the quagmire that is the war in Afghanistan and the
government may miss its deadline for closing the blight that is the prison at
Guantanamo Bay, Cuba.

Obama pledged to stem the tide of job losses and foreclosures and to reform the
culture of the financial sector. Well, the Dow just hit 10,000 again while the
national unemployment rate is about to hit 10 percent. And the firms we propped
up are set to dole out record bonuses while home foreclosures have hit record
highs. Main Street is still drowning in crisis while Wall Street is awash in
Champagne. When will this imbalance be corrected?

Candidate Obama pledged to make the rebuilding of New Orleans a priority, but
President Obama whisked into the city on Thursday for a visit so brief that one
Louisiana congressman dubbed it a ''drive-through daiquiri summit.'' The
president spent more time on the failed Olympic bid in Copenhagen than he did in
the Crescent City.

At the town hall in New Orleans, Obama appealed for patience. He said, ''Change
is hard, and big change is harder.'' Is that the excuse? Now where have I heard
that before? Oh, yeah. From George Bush.

URL: http://www.nytimes.com

LOAD-DATE: October 17, 2009

LANGUAGE: ENGLISH

GRAPHIC: CHART: Percent of people responding that, in general, they are
satisfied with the way things are going in the United States. (Source: Gallup)

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


