                   Copyright 2009 The New York Times Company


                             908 of 2021 DOCUMENTS


                               The New York Times

                            October 30, 2009 Friday
                              Late Edition - Final

The House Health Reform Bill

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL; Pg. 30

LENGTH: 648 words


The Senate should pay attention to the health care reform bill unveiled on
Thursday by House Democratic leaders. The bill would greatly expand coverage of
the uninsured while reducing budget deficits over the next decade and probably
beyond. It includes a public option that is weaker than we would like, but it
still deserves to be approved by the House.

The coverage expansions would carry a net cost to the federal government of $894
billion over the next decade, according to the Congressional Budget Office. Yet
the bill would generate enough revenue from new taxes and from savings in
Medicare to offset that cost and reduce the deficit by $104 billion over the
course of the decade.

The chief source of tax revenue would be a surcharge on the portion of annual
income above $1 million for couples and $500,000 for individuals. The wealthy
prospered enormously from tax cuts under the Bush administration. It is fitting
that they pay a heavy share of the cost of health care reform.

The bill requires employers, except for small businesses, to offer health
coverage to their workers and pay a substantial share of the premiums or face a
big penalty. That would be a useful prod to make insurance more available and
affordable to employees.

The bill would meet President Obama's insistence that health care reform not add
to the deficit -- provided Congress holds firm on slowing the growth rate of
payments to health care providers serving Medicare. Of special importance, the
trend line for deficits would be heading down toward the end of the decade,
suggesting that it would continue on down thereafter. This is a fiscally prudent
bill, not a reckless dash toward ever-higher deficits as Republicans contend.

(To make ends meet, the Democrats dropped a costly fix for the unrealistic
formula used to reimburse doctors under Medicare. That will be tackled in
separate legislation, and ought to be paid for with new revenue.)

Under this bill, the number of uninsured would plummet. Since Congress is
determined to exclude illegal immigrants, the salient fact is that by 2019, the
bill would provide insurance to 96 percent of all nonelderly citizens and legal
residents, leaving about 12 million of them uninsured. It would achieve this
feat by making a lot more people eligible for Medicaid, a program for the poor,
and by helping tens of millions of low-  and moderate-income people buy policies
on new insurance exchanges, in which private plans and possibly a public plan
would compete for people who lack employer-provided insurance or work in small
companies.

Speaker Nancy Pelosi wanted a strong, money-saving public option that would pay
hospitals and doctors based on Medicare rates, but she could not win over enough
conservative Democrats. Her fallback is to have the secretary of health and
human services negotiate rates with health care providers as private insurers
do.

The Congressional Budget Office considers this so weak that it might attract
only 6 million of an estimated 30 million people buying insurance on the
exchanges in 2019. Its premiums might exceed the average private plan, in part
because the sickest people might migrate to the public plan.

Still, the House bill has a lot of provisions for consumers to like. It would
require insurers to allow young people through age 26 to remain on their
parents' policies. It would provide immediate help to people who have been
uninsured for several months or denied coverage because of pre-existing
conditions. It would speed elimination of a gap in drug coverage for Medicare
beneficiaries (the so-called doughnut hole) and would give the government power
to negotiate drug prices on behalf of Medicare beneficiaries, a promising way to
reduce costs.

The bill would take a long stride toward universal coverage while remaining
fiscally responsible. Senate leaders should try to do as well.

URL: http://www.nytimes.com

LOAD-DATE: October 30, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


