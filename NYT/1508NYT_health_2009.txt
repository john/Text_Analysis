                   Copyright 2010 The New York Times Company


                             1508 of 2021 DOCUMENTS


                               The New York Times

                            February 14, 2010 Sunday
                              Late Edition - Final

Laugh Lines

SECTION: Section WK; Column 0; Week in Review Desk; Pg. 2

LENGTH: 161 words


DAVID LETTERMAN

They got a lot of snow in Washington, D.C. And the city came to the biggest
standstill they've had since the Democrats got the supermajority.

Last night, Bob Dylan performed at the White House in honor of Black History
Month. Because when you think of black history, you think of a mumbling, white,
Jewish guy from Minnesota.

JIMMY FALLON

The entire East Coast is covered with snow banks and snow drifts, or as Toyota
drivers call them -- ''cushions.''

So cold out there the writing on Sarah Palin's hand said, ''Economy,'' ''jobs,''
''put on gloves, stupid.''

JIMMY KIMMEL

Writing stuff on your hand? It's not a good idea. It's actually why President
Bush invaded Iraq instead of Iran. He wrote it down. I guess he played, like,
nine holes of golf. And now here we are.

JAY LENO

Dr. Conrad Murray was charged today with the death of Michael Jackson. He is
headed to jail. Could lose his medical license. So, see, there's health care
reform.

URL: http://www.nytimes.com

LOAD-DATE: February 14, 2010

LANGUAGE: ENGLISH

GRAPHIC: CARTOONS: Rob Rogers Pittsburgh Post-Gazette, United Feature Syndicate
Joel Pett: Lexington Herald-Leader, CartoonArts International
Walt Handelsman: Newsday, Tribune Media Services
 Jack Ohman: The Oregonian, Tribune Media Services

PUBLICATION-TYPE: Newspaper


