                   Copyright 2009 The New York Times Company


                             246 of 2021 DOCUMENTS


                               The New York Times

                            August 1, 2009 Saturday
                              Late Edition - Final

Dodd to Have Surgery for Prostate Cancer but Vows to Seek Re-election

BYLINE: By RAYMOND HERNANDEZ; Liz Robbins contributed reporting.

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 12

LENGTH: 749 words

DATELINE: WASHINGTON


Senator Christopher J. Dodd of Connecticut announced on Friday that he had
prostate cancer, in an early, treatable form, and that he would press ahead with
his re-election campaign.

Mr. Dodd, 65, a five-term incumbent, said that a blood test during an annual
physical six weeks ago indicated the possibility that he had cancer. A
subsequent biopsy confirmed the diagnosis, he said.

He said he would have an operation this month at Memorial Sloan-Kettering Cancer
Center in Manhattan to remove the prostate while Congress is in recess. He said
he would take a few weeks to recuperate at home.

''I am going to be fine in all this,'' said Mr. Dodd, who spoke from his office
in Hartford with his wife, Jackie Clegg Dodd, alongside him. ''This is very
common. If you've got to have cancer, I am told by some doctors, it's the
slowest growing, the best one to have, the most manageable.''

His announcement came as a surprise to people in politics and elicited sympathy
from many, including his likely Republican rival in the 2010 Senate race, former
Representative Rob Simmons. It also set off questions about how the cancer would
affect his political future.

At his news conference, Mr. Dodd moved to dispel any notion that he might not
seek another term. ''I'm running for re-election,'' he said without prompting.
''Now, I'll be a little leaner and a little meaner. But I am running.''

Then he added with a laugh: ''I'll be running without a prostate. That will make
a better candidate, I think.''

Mr. Dodd is the chairman of the Senate banking committee,  but he is currently
at the center of the government's battle over health care, acting as chairman of
the Senate Health, Education, Labor and Pensions Committee for his close friend
Senator Edward M. Kennedy, the Massachusetts Democrat, who is fighting brain
cancer.

''We can talk in abstractions about health care, but what I'm going through,
many people do,'' Mr. Dodd said, using the occasion to remind people about the
health care debate. Even though he knew of his cancer while overseeing the bill,
he said he did not want to make himself an example -- until now.

''I have the benefit of being in Congress,'' he said. ''Having a good health
care plan is not available to everyone.''

Mr. Dodd has traditionally cruised to re-election. But revelations about his
personal finances, and criticism that he received favorable treatment from a
mortgage lender have weakened his standing and jolted him into early advertising
and campaign-style events to try to win back support from his constituents.

He refinanced two of his homes through Countrywide Financial, the subprime
mortgage lender, where he was put into a special category of customers called
Friends of Angelo, referring to Angelo Mozilo, chief executive of Countrywide.
Mr. Dodd has denied receiving any favorable treatment from the lender.

In addition, questions have been raised about whether he understated the value
of a home he owns in Ireland, which he purchased  with a developer whom he later
bought out.

Despite a barrage of television commercials asserting that Mr. Dodd is tough on
lobbyists, a Quinnipiac University poll last week showed that 55 percent of
Connecticut voters say Mr. Dodd is not honest and trustworthy.

Even as he has assumed a leading role in the health care debate, Mr. Dodd has
received controversial support for his re-election campaign from the
Pharmaceutical Research and Manufacturers of America, the industry's lobbying
arm. The support came at a time when he was trying to distance himself from
powerful special interest groups that Republicans said he was too close to.

Mr. Dodd has raised more than $550,000 from drug company representatives over
his career, according to the Center for Responsive Politics.

Mr. Dodd will be out of the limelight for the surgery and convalescence.

According to the Prostate Cancer Foundation, the disease affects one in six men
and is the most common non-skin cancer in the United States.

Mr. Dodd said that in the last six weeks he had spoken with fellow senators and
others who have survived prostate cancer, including Senator John F. Kerry of
Massachusetts and Senator Saxby Chambliss, from Georgia, as he decided whether
to have surgery.

The foundation's Web site says that the cure rate for men whose prostate cancer
is diagnosed in early stages is very high, and that nearly 100 percent of men
whose condition is diagnosed at this stage will be disease-free after five
years.

URL: http://www.nytimes.com

LOAD-DATE: August 1, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Senator Christopher J. Dodd, who is overseeing the progress of
sweeping health care legislation, announced Friday that he had prostate cancer.
His wife, Jackie Clegg Dodd, was at his side.(PHOTOGRAPH BY GEORGE RUHE FOR THE
NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


