                   Copyright 2009 The New York Times Company


                             996 of 2021 DOCUMENTS


                               The New York Times

                            November 13, 2009 Friday
                              Late Edition - Final

Medicare Doctor Payments

SECTION: Section A; Column 0; Editorial Desk; LETTER; Pg. 30

LENGTH: 210 words


To the Editor:

Re ''The Medical Industry Grumbles, but It Stands to Gain'' (news analysis,
Business Day, Nov. 9):

Repeal of the broken Medicare physician payment formula is an essential element
of health reform to ensure the security and stability of Medicare for the
elderly and baby boomers -- and military families that rely on the Tricare
benefits system.

Without repeal, next year's cut to physicians is projected at 21 percent, with
more in years to come -- which may force physicians to limit the number of new
Medicare patients they can treat.

The gap between payments and costs will make it very difficult for physicians to
keep their doors open to all Medicare patients and make quality improvements to
their practices that benefit all patients.

As Congress considers new coverage commitments to the American people through
health reform, it must ensure that commitments already made are fulfilled
through passage of the Medicare Physician Payment Reform Act of 2009 (H.R.
3961). This bill will permanently repeal the broken physician payment formula
and preserve access to care for the elderly, baby boomers and military families.

J. James Rohack Chicago, Nov. 9, 2009

The writer, a doctor, is president of the American Medical Association.

URL: http://www.nytimes.com

LOAD-DATE: November 13, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


