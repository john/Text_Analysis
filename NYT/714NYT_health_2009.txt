                   Copyright 2009 The New York Times Company


                             714 of 2021 DOCUMENTS


                               The New York Times

                           September 29, 2009 Tuesday
                              Late Edition - Final

Abortion Fight Adds to Debate On Health Care

BYLINE: By DAVID D. KIRKPATRICK

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1013 words

DATELINE: WASHINGTON


As if it were not complicated enough, the debate over health care in Congress is
becoming a battlefield in the fight over abortion.

Abortion opponents in both the House and the Senate are seeking to block the
millions of middle- and lower-income people who might receive federal insurance
subsidies to help them buy health coverage from using the money on plans that
cover abortion. And the abortion opponents are getting enough support from
moderate Democrats that both sides say the outcome is too close to call.
Opponents of abortion cite as precedent a 30-year-old ban on the use of taxpayer
money to pay for elective abortions.

Abortion-rights supporters say such a restriction would all but eliminate from
the marketplace private plans that cover the procedure, pushing women who have
such coverage to give it up. Nearly half of those with employer-sponsored health
plans now have policies that cover abortion, according to a study by the Kaiser
Family Foundation.

The question looms as a test of President Obama's campaign pledge to support
abortion rights but seek middle ground with those who do not. Mr. Obama has
promised for months that the health care overhaul would not provide federal
money to pay for elective abortions, but White House officials have declined to
spell out what he means.

Democratic Congressional leaders say the latest House and Senate health care
bills preserve the spirit of the current ban on federal abortion financing by
requiring insurers to segregate their public subsidies into separate accounts
from individual premiums and co-payments. Insurers could use money only from
private sources to pay for abortions.

But opponents say that is not good enough, because only a line on an insurers'
accounting ledger would divide the federal money from the payments for
abortions. The subsidies would still help people afford health coverage that
included abortion.

Lawmakers pushing the abortion restrictions say they feel the momentum is on
their side, especially because the restlessness of other Democratic moderates is
making every vote count.

At least 31 House Democrats have signed various recent letters to the House
speaker, Nancy Pelosi, urging her to allow a vote on a measure to restrict use
of the subsidies to pay for abortion, including 25 who joined more than 100
Republicans on a letter delivered Monday.

Representative Bart Stupak of Michigan, a leading Democratic abortion opponent,
said he had commitments from 40 Democrats to block the health care bill unless
they have a chance to include the restrictions.

After months of pushing the issue, Mr. Stupak said in an interview, Mr. Obama
finally called him 10 days ago. ''He said: 'Look, try to get this thing worked
out among the Democrats. We want you to work it out within the party,' '' Mr.
Stupak said, adding that Mr. Obama did not say whether he supported the
segregated-money provision or a more sweeping restriction. ''We got his
attention, which we never had before.''

After the president called, Mr. Stupak said, Ms. Pelosi agreed to meet with Mr.
Stupak on Tuesday to discuss his proposals for the first time, her office
confirmed. Her spokesman, Nadeam Elshami, said in a statement, ''As we have
throughout the process, we are meeting with our members to listen to their
concerns, consulting with the administration, and making progress.''

The Senate Finance Committee is expected to vote this week on a proposed
amendment from Senator Orrin G. Hatch, Republican of Utah, to restrict the use
of federal subsidies.

Advocates on both sides said that if the committee does not adopt the amendment
they expect a very close contest over the issue when the bill reaches the floor.
Two Democratic abortion-rights opponents, Senator Bob Casey Jr. of Pennsylvania
and Senator Ben Nelson of Nebraska, are pushing the issue.

Mr. Casey voted in the Senate health committee for a proposal to restrict the
use of the subsidies; it was defeated by one vote. Mr. Nelson is considered a
pivotal vote needed to pass the overall bill. ''Senator Nelson does not believe
that taxpayer dollars should be used in any way to fund abortion,'' his
spokesman said.

Jim Manley, a spokesman for the Democratic leader, Senator Harry Reid, said that
Mr. Reid believes that the latest drafts of legislation already accomplish that
goal.

Supporters of the current segregated-money model argue that 17 state Medicaid
programs that cover elective abortions use a similar system, dividing their
federal financing from state revenues they use to pay for procedures.

''The language of the compromise is very clear,'' said Representative Rosa
DeLauro, Democrat of Connecticut, ''it prohibits the use of federal funds to pay
for abortions.'' (The bills would also mandate the availability in each state of
at least one plan that covers abortion and at least one that does not.)

Nancy Keenan, president of Naral Pro-Choice America, argued that if the bill
blocked the use of subsidies for abortion coverage, private insurers would stop
covering abortion because those plans would be excluded from the federally
subsidized programs.

''Women who already have this coverage would lose it,'' Ms. Keenan said.

Advocates of tighter restrictions note that the health insurance program for
federal employees complies with the ban on abortion financing by excluding any
plans that offer abortion.

And under the Hatch amendment in the Senate or the Stupak proposals in the
House, women would be free to pay extra for an insurance ''rider'' that would
cover abortions.

The United States Conference of Catholic Bishops, which has lobbied for decades
to persuade the government to provide universal health insurance, says it
opposes the bill unless it bans the use of subsidies for plans that cover
abortion.

''We have said to the White House and various Senate offices that we could be
the best friends to this bill if our concerns are met,'' Richard M. Doerflinger,
a spokesman for the bishops on abortion issues, said in an interview. ''But the
concerns are kind of intractable.''

URL: http://www.nytimes.com

LOAD-DATE: September 29, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Representative Bart Stupak, a Democratic opponent of abortion,
is pressing for a ban on health care subsidies for abortions.(PHOTOGRAPH BY
JEWEL SAMAD/AGENCE FRANCE-PRESSE -- GETTY IMAGES)(A26)

PUBLICATION-TYPE: Newspaper


