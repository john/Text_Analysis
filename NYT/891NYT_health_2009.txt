                   Copyright 2009 The New York Times Company


                             891 of 2021 DOCUMENTS


                               The New York Times

                            October 27, 2009 Tuesday
                              Late Edition - Final

Costly Toll of Swine Flu

SECTION: Section D; Column 0; Science Desk; LETTERS; Pg. 4

LENGTH: 133 words


To the Editor:

Between the lines of ''Flu Story: A Pregnant Woman's Ordeal'' (Oct. 20) is a
powerful rationale for the need for comprehensive health care reform.

The facts of the story are: a husband believes he contracted the flu and passed
it along to his pregnant wife. She was hospitalized for four months. After her
lungs collapsed the third time, her baby was delivered by emergency C-section
and died after seven minutes.

Months later, Aubrey Opdyke is still struggling to recover. Health insurance
covered only 80 percent of her hospital bills. This young family owes $200,000
in medical bills. Should this family really have to suffer any more? We must
provide medical coverage that covers all, or the vast majority, of our medical
bills -- for all Americans.

Alexandra Olins

Winooski, Vt.

URL: http://www.nytimes.com

LOAD-DATE: October 27, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO (PHOTOGRAPH BY SCOTT WEISMAN FOR THE NEW YORK TIMES)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


