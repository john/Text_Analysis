                   Copyright 2009 The New York Times Company


                             180 of 2021 DOCUMENTS


                               The New York Times

                              July 20, 2009 Monday
                              Late Edition - Final

Costs and Benefits

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL; Pg. 18

LENGTH: 866 words


Democrats pushing for health care reform got serious jolts last week from
critics who warned that their proposed legislation would do little to slow
spiraling health care costs.

A group of conservative Democrats vowed that they would join Republicans to
block action in a crucial House committee unless more cost restraint was
imposed. And Douglas Elmendorf, the respected head of the Congressional Budget
Office, warned that none of the bills he has seen impose fundamental changes in
how medical care is delivered and paid for.

Health care reform is vitally important both to cover tens of millions of
uninsured Americans -- a moral imperative -- and to bring down the relentlessly
rising costs of care. Those costs are straining not only the government's
budgets for Medicare and Medicaid. American businesses are struggling to provide
insurance for their employees. Millions of Americans are struggling to pay high
medical bills and rising premiums; many are just a pink slip away from being
uninsured.

Mr. Elmendorf's testimony should prod Democratic leaders to come up with more
and better ways to restrain costs for federal programs and the health care
system as a whole. The White House was certainly paying attention. It called for
the creation of an independent expert body to propose fair payment rates and
other cost-saving reforms for Medicare. That is a sound idea that could lessen
the influence of high-pressure lobbying -- by drug companies, insurers,
hospitals and doctors --  that inevitably drives up costs.

The pending bills do an excellent job of providing health insurance for millions
of uninsured Americans at a cost of roughly $1 trillion over the next decade --
mostly for subsidies to low- and middle-income people and expansion of the
Medicaid program for the poor.

President Obama has rightly insisted -- and Congressional leaders have agreed --
that this expansion of coverage must not drive up the deficit over the next 10
years. It has to be fully paid for by new taxes or big savings in Medicare and
other federal programs. Whatever legislation emerges from Congress is expected
to do so -- or face a veto.

The knottier problem is how to slow the rate of increase in health care costs.
Those costs are growing faster than inflation, wages and the overall economy.
This is the issue that Mr. Elmendorf was addressing, and he seemed to be
thinking beyond the 10-year budget window to the likely impact on future
deficits.

He is right to be concerned. If the government simply extends subsidized
insurance to millions of uninsured people but fails to force fundamental changes
in the delivery or financing of health care, then federal health care costs will
keep escalating at excessive rates. That will drive up deficits in subsequent
decades  unless new taxes are imposed or new savings found.

The budget office is concerned only with impacts on federal spending and the
deficit. But any changes to Medicare's policies would reverberate through the
whole health care system as health care providers changed their practices and
private insurers followed Medicare's lead.

The House bill makes a good start in the right direction by proposing changes in
Medicare reimbursement rates and various pilot programs to encourage greater
productivity and less costly forms of care. But more could and should be done.

Medicare ought to be empowered, for example, to reduce its payment rates to the
highest-cost hospitals and most inefficient doctors. That is probably the best
way to get them to stop providing needless tests and treatments that don't
improve the health of the patient.

Medicare should also be allowed to use the results of comparative effectiveness
research to set reimbursement policies favoring the best treatments.
Unfortunately, as currently written, the House bill calls for that research but
prohibits Medicare from using it to set rates.

All of these ideas seem well worth trying. The long-run solution is to move away
from Medicare's fee-for-service system that rewards doctors and hospitals for
each additional service they provide and move into a new system where doctors
and hospitals are organized and rewarded in ways that encourage both low-cost
and high-quality care.

The problem is that nobody is sure of the best way to do that. The House bill is
right to set up pilot programs to test some approaches. But more ideas should be
developed and tested.

Much to the anger of leading Democrats, Mr. Elmendorf also suggested that
Congress might consider taxing employer-provided health benefits. He seemed to
mean the most generous policies that many economists think encourage excessive
use of medical care.

That could raise several hundred billion dollars to pay for the expansion of
coverage and, unlike the taxes on wealthy Americans proposed in the House bill,
is likely to keep pace with future increases in health care costs.

A tax on employer-provided benefits  would probably also encourage workers to
choose  lower-cost policies and use health care more sparingly. But it is
politically risky and it could turn many Americans away from supporting health
care reform. It might best be considered as a last resort.

URL: http://www.nytimes.com

LOAD-DATE: July 20, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


