                   Copyright 2009 The New York Times Company


                             1121 of 2021 DOCUMENTS


                               The New York Times

                            December 4, 2009 Friday
                              Late Edition - Final

Doctors Divide on Bill

BYLINE: By KEVIN SACK

SECTION: Section A; Column 0; National Desk; SECOND OPINIONS; Pg. 21

LENGTH: 282 words


Even though the American Medical Association offered qualified support for the
Senate health care bill this week, many other medical groups are unqualified in
their opposition.

A coalition representing 240,000 physician specialists, like the American
College of Surgeons and the American Society of Cataract and Refractive Surgery,
said it ''must oppose the bill as currently written.''

The group sent a letter this week to the Senate majority leader, Harry Reid of
Nevada, taking issue with a variety of provisions in the Senate bill.

They include the establishment of a Medicare advisory board with the authority
to set reimbursement policy, increased reporting on physician errors and
outcomes, an excise tax on elective cosmetic surgery, and measures that might
increase payments to primary-care doctors at the expense of specialists.

Each of those provisions was also among the concerns cited by the A.M.A. But the
association, while stopping short of endorsing the bill, expressed support for
the legislation's central elements.

Not so, in the case of the California Medical Association, which represents
35,000 physicians. It declared this week that it opposed the current Senate
legislation, joining counterparts in Texas and Florida that took stands in late
November.

The California doctors emphasized that the Senate bill did not make adjustments
in a Medicare payment formula that would otherwise result in deep cuts in
physician payments in coming years. The House passed a measure last month to
avoid the cuts.

''There is no way health care reform can work if patients can't get access to a
doctor,'' said Dr. Brennan Cassidy, the California group's president.

KEVIN SACK

URL: http://www.nytimes.com

LOAD-DATE: December 4, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


