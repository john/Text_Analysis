                   Copyright 2010 The New York Times Company


                             1542 of 2021 DOCUMENTS


                               The New York Times

                            February 22, 2010 Monday
                              Late Edition - Final

New Schools, to Train New Doctors

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 18

LENGTH: 847 words


To the Editor:

Re ''After Years of Quiet, Expecting a Surge in U.S. Medical Schools'' (front
page, Feb. 15):

Opening new medical schools will not ameliorate the shortage of primary-care
physicians in the United States without major changes to the current health care
system. Without payment reform and reducing student debt to make primary care
more financially viable, medical students will continue pursuing lucrative
specialties.

Interestingly, entering any specialty will become exceedingly difficult without
a corresponding increase in residency positions. Without increased positions,
the number of medical graduates is projected to exceed available first-year
residency positions by 2016. Because of already increasing class sizes, reports
of difficulty securing residency positions emerged in the 2009 Match.

Securing a residency will be particularly difficult for international medical
graduates who currently train in predominantly undesired residency positions (up
to half of residency positions in some primary-care fields).

We need a system that encourages medical students to enter primary care and also
provides adequate training opportunities for the increasing numbers of medical
school graduates.

Vineet Arora

Celine Goetz

Valerie G. Press

Chicago, Feb. 15, 2010

The writers are, respectively, a doctor and assistant dean of the University of
Chicago Pritzker School of Medicine; a third-year medical student; and a doctor
and clinical associate at the school.

To the Editor:

The idea that we need to establish new medical schools to train additional
doctors to ensure an adequate number of physicians is questionable. It is very
expensive to establish a medical school. It is not expensive to train a few more
medical students in each of the existing established medical schools in this
country.

This could be easily accomplished given the technological progress in medical
education, which has eliminated some of the factors, like dissection of
cadavers, that made any increase in class size difficult. David N. Silvers

New York, Feb. 15, 2010

The writer is clinical professor of dermatology and pathology at the College of
Physicians and Surgeons of Columbia University.

To the Editor:

There is a severe shortage of physicians in all areas of the country, especially
in much-needed fields like primary care and cardiology. With the growing
population and new medical advances, there has been a major need for more
physicians for a long time.

Our emergency rooms have experienced increased volumes with no increase in
physicians. Physicians are overwhelmed, and patients have suffered because of
lack of medical attention.

In addition, there is a severe shortage of female physicians in competitive
fields like cardiology, neurosurgery and radiation oncology. Heart disease is
the No. 1 killer of both men and women, yet women make up only 10 percent of
practicing cardiologists.

With more medical schools opening, the hope is that more women will graduate and
enter fields where they are much needed. Sheila Gupta

New York, Feb. 15, 2010

The writer is a cardiology fellow at Beth Israel Medical Center.

To the Editor:

We laud the development of new medical schools in the United States. We do not
believe, however, that new physician creation is the sole solution to the coming
health care shortage.

Physicians cannot fill the health care gap without a larger increase in allied
health professionals. St. George's University has recently expanded its nursing
program and public health programs intended to fulfill the expanding health care
needs throughout the world.

A dramatic expansion of well-trained nurses, nurse practitioners, physician
assistants, public health professionals and the like is the most fiscally
responsible method of getting closer to our goal of quality health care for all
in the United States. Charles R. Modica

Hobe Sound, Fla., Feb. 16, 2010

The writer is chancellor of St. George's University in St. George's, Grenada.

To the Editor:

In addition to the need for more doctors, there is an urgent need for training
that is tailored for patients who are over 65.

In the next two decades, the number of Americans who are 65 and older will
nearly double. This population often suffers from multiple chronic conditions,
frailty or complex illnesses, and faces challenges in gaining access to
high-quality, appropriate health care.

Many of the new medical schools billing themselves as different from traditional
medical schools would better serve today and tomorrow's health care needs by
focusing on including geriatrics as a standard component in their primary-care
curriculum.

A good example is Florida State University, which has a medical school with an
explicit focus on ensuring that its graduates are well versed in the care of
older adults.

Medical educators and policy makers should focus on creating a work force of
health care professionals that can fully serve our growing elder population.

Marleise Brosnan

Project Director

Eldercare Workforce Alliance

Washington, Feb. 16, 2010

URL: http://www.nytimes.com

LOAD-DATE: February 22, 2010

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY HOLLY STEVENSON)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


