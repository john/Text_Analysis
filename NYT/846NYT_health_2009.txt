                   Copyright 2009 The New York Times Company


                             846 of 2021 DOCUMENTS


                               The New York Times

                            October 18, 2009 Sunday
                              Late Edition - Final

Frustrated Liberal Lawmaker Balances Beliefs and Realities

BYLINE: By CARL HULSE

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1186 words

DATELINE: WASHINGTON


Representative Earl Blumenauer should be experiencing the most fulfilling days
of his more than 35 years in public service.

The liberal Democrat from Portland, Ore. -- known for his bowties, his Trek
bicycle and a pragmatic brand of progressivism -- embraced Barack Obama's
presidential candidacy early in 2008 and campaigned hard alongside him, steadily
gaining confidence that the young senator from Illinois was the ideal liberal
remedy to eight years of conservative dominance.

Now political reality has set in, testing Mr. Blumenauer's faith that Mr.
Obama's election and big Democratic majorities in Congress would yield quick
advances in the progressive agenda.

Instead of forging ahead, Mr. Blumenauer, 61, finds himself fighting to retain
one of the touchstones for liberals this year, a public insurance option in the
health care overhaul, and is watching his hopes of curbing global warming grow
cold in the Senate. Mr. Blumenauer, a seven-term congressman, is bracing for a
tough vote on sending more troops to Afghanistan while he frets about the
detention facility at Guantanamo Bay remaining open.

''It has been a hard landing for a lot of the people that I represent,'' Mr.
Blumenauer, referring to his largely liberal constituency, said as he assessed
the first months of the Obama administration.

As health care legislation moves to the floor with other major issues close
behind, the question for Mr. Blumenauer and those who share his ideology will be
whether they relent on some of their core beliefs to support less satisfying
compromises, despite being in what, on the surface, is a commanding political
position.

''It is still something that I am struggling with,'' he said.

Mr. Blumenauer is just one example of what might be called the Frustrated Left,
a substantial caucus of Congressional Democrats who dreamed that Mr. Obama would
usher in a new era of liberal problem-solving only to see Congress and the new
administration collide with the old problems of partisanship, internal
disagreement and the challenge of mustering 60 votes to get just about anything
done in the Senate.

While Congressional leaders try to appease moderate and conservative Democrats
who can provide the crucial votes for passage, more liberal Democrats from safer
districts sometimes simmer, feeling that they are being taken for granted while
it is assumed they will get on board when the time comes.

On health care, Democrats are growing more optimistic that they can find a
compromise approach to creating a government-run insurer to compete with the
private sector -- an issue that as much as any other has split the party's
liberals and moderates -- even as progressive voices outside of Congress insist
that there be no compromise.

''The fact is that Earl Blumenauer could stop a bill going through that does not
have a public option in it,'' said Jane Hamsher, founder of the progressive blog
firedoglake.com. ''Is it his loyalty to the party, partisan politics over
principle? We are going to get to see that.''

Mr. Blumenauer strongly favors a public option and in late July was one of more
than 60 Democrats who signed a letter to the leadership saying that,
essentially, they would not back a final bill without an acceptable public plan.
But on health care -- as on other domestic issues, global warming and foreign
policy -- he must weigh whether it makes more sense to take what he can get as
opposed to standing firm and perhaps seeing the overall effort collapse.

''It would be very hard for me to do,'' Mr. Blumenauer said of voting for a
final health care overhaul without a public plan. ''But if it gets to the point
where the choice is doing some things that will make a significant difference
without a public option or letting the whole thing die, that too would be
hard.''

Mr. Blumenauer got on board early with Mr. Obama after concluding that he
offered the chance for a more decisive change in course than Hillary Rodham
Clinton could provide. He first met Mr. Obama at the 2004 Democratic convention
in Boston and endorsed him in late January 2008.

''There was something going on here, this guy has got some real capacity being
able to, I think, connect, communicate,'' remembered Mr. Blumenauer.

Mr. Obama won Oregon and Mr. Blumenauer's district going away, setting sky-high
expectations among his followers in the Pacific Northwest.

Mr. Blumenauer, a member of the tax-writing and climate change committees with a
devotion to trying to improve the livability of American cities, said he did not
think Mr. Obama had shifted his ideological stance since his election and did
not blame the president for the problems slowing the liberal agenda. He said he
saw a combination of factors -- the troubled economy, the sheer scope of the
nation's problems and an unexpected level of Republican opposition -- as the
culprits.

''The combination of the economic shock and frankly the political upset and
outrage has changed the landscape,'' Mr. Blumenauer said. ''The Barack Obama
that I campaigned with is pretty much the same guy. But it is an environment
that is unprecedented and would press anyone's skills.''

Back home, Mr. Blumenauer said his constituents had shown patience with the pace
of things, partly, he suggested, because they were so disenchanted with the Bush
administration.

Activists and pollsters in Oregon said that they agreed but that the patience of
Mr. Blumenauer's liberal base was not unlimited.

''I think people realize you can't do everything precisely all at once,'' said
Steve Novick, a Democratic advocate in Portland who lost a Senate bid in 2008.

Senator Ron Wyden, whose move to the Senate opened up the House seat for Mr.
Blumenauer in 1996, said Oregon residents grasped the complexity of the problems
facing the country. ''Look at what is coming at us: Iraq, Afghanistan, Iran,''
he said. ''There is a sense that there is going to be a lot of heavy lifting,
but people want to stay at it until it happens.''

Even with his frustrations, Mr. Blumenauer said that having a Democratic
administration had paid tangible benefits. The secretaries of the housing and
transportation departments have visited Portland, and he recently hosted Lisa P.
Jackson, the administrator of the Environmental Protection Agency, in his
office. ''They want to be a partner on the cleanup rather than ignoring it,'' he
said, referring to environmental cleanup projects in his state.

And though some of his preferred legislative approaches might be stalled or fall
victim to compromise, Mr. Blumenauer said he believed that Mr. Obama and the
Democratic majorities in Congress would ultimately be successful in advancing a
liberal agenda on the major issues.

''We are going to be working on climate, on health care, on the economy for
every minute of the next two Congresses and beyond,'' he said. ''Will the public
be patient enough? Will the political process hold together?

''This is not going to be easy,'' he said, ''but I think we are seeing a process
that makes me actually optimistic, even though it is not exactly like I would
have liked.''

URL: http://www.nytimes.com

LOAD-DATE: October 18, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Representative Earl Blumenauer with two staff members, Larry
Weinstock and Janine Benner.(PHOTOGRAPH BY STEPHEN CROWLEY/THE NEW YORK
TIMES)(A31) MAP: A Coalition for the Left: Members of the Congressional
Progressive Caucus -- the largest coalition of Democrats in the House --
represent 79 districts across the country, mostly located in urban and suburban
areas in the Northeast and the West. All but four of the districts voted
Democratic in the three presidential elections since 2000.(Note: The caucus has
a total of 82 members. Senator Bernard Sanders, independent of Vermont, and two
delegates from the District of Columbia and the Virgin Islands are not
shown.)(A31)

PUBLICATION-TYPE: Newspaper


