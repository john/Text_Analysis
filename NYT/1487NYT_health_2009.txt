                   Copyright 2010 The New York Times Company


                             1487 of 2021 DOCUMENTS


                               The New York Times

                            February 8, 2010 Monday
                              Late Edition - Final

President Plans A Joint Summit On Health Care

BYLINE: By JEFF ZELENY; David Herszenhorn contributed reporting.

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1111 words

DATELINE: WASHINGTON


President Obama said Sunday that he would convene a half-day bipartisan health
care session at the White House to be televised live this month, a high-profile
gambit that will allow Americans to watch as Democrats and Republicans try to
break their political impasse.

Mr. Obama made the announcement in an interview on CBS during the Super Bowl
pre-game show, capitalizing on a vast television audience. He set out a plan
that would put Republicans on the spot to offer their own ideas on health care
and show whether both sides are willing to work together.

''I want to come back and have a large meeting, Republicans and Democrats, to go
through systematically all the best ideas that are out there and move it
forward,'' Mr. Obama said in the interview from the White House Library.

Mr. Obama challenged Republicans to attend the meeting with their plans for
lowering the cost of health insurance and expanding coverage to more than 30
million uninsured Americans. Republican leaders said they welcomed the
opportunity and called on Democrats to start the debate from scratch, which the
president said he would not do.

The move by Mr. Obama comes after weeks in which the administration has appeared
uncertain about how to proceed on his top domestic priority since Republicans
captured the Senate seat previously held by Senator Edward M. Kennedy. House and
Senate Democrats had been increasingly at odds over what the bill should say,
how to move ahead tactically and, in some cases, whether to continue at all.

The idea for the bipartisan meeting, set for Feb. 25, was reached in recent
weeks, aides said, as part of the White House strategy to intensify its push to
engage Congressional Republicans in policy negotiations, share the burden of
governing and put more scrutiny on Republican initiatives.

Mr. Obama's announcement came after he surprised his rivals in late January by
requesting that a session with House Republicans be open to cameras. That
meeting produced a spirited 90-minute question-and-answer session with the
president that many in the White House viewed as a critical success for Mr.
Obama.

In making the gesture on Sunday, Mr. Obama is in effect calling the hand of
Republicans who had chastised him for not honoring a campaign pledge to hold
health care deliberations in the open, broadcast by C-Span, and for not allowing
Republicans at the bargaining table.

Nancy-Ann DeParle, the director of the White House Office for Health Reform,
briefed Democratic Congressional staff members in a conference call ahead of the
interview, with Katie Couric.

Separately, some Congressional staff members expressed concern that Mr. Obama's
meeting would simply prolong an already tortuous process. And Democrats still
face steep challenges in reconciling the differences between the House and
Senate bills.

Some House Democrats are firmly opposed to a proposed tax on high-cost
employer-sponsored insurance policies, which they think will hit some
middle-class workers and violate Mr. Obama's campaign promise not to raise taxes
on Americans earning less than $250,000 a year.

The president offered a number of questions that his party would have for the
Republicans.

''How do you guys want to lower costs? How do you guys intend to reform the
insurance market so that people with pre-existing conditions, for example, can
get health care?'' he said. ''How do you want to make sure that the 30 million
people who don't have health insurance can get it? What are your ideas
specifically?''

The question for Mr. Obama is how much -- if at all -- he is willing to give on
some of the concepts Democrats have already agreed on, or if he is using the
meeting to lay the groundwork for another effort by Democrats to push the
legislation through without Republican votes.

Mr. Obama did not indicate what he was willing to give up in the negotiations,
nor did he chart a specific legislative strategy for moving a bill through
Congress. Democrats in the House and Senate were hoping to resolve their
differences in the bill, aides said, and present a unified health care plan in
time for the meeting.

Senator Mitch McConnell of Kentucky, the Republican leader, said in a statement
that he welcomed the bipartisan meeting on health care and called on the
president to begin the dialogue ''by shelving the current health spending
bill.''

''The fact is Senate Republicans held hundreds of town halls and met with their
constituents across the country last year on the need for health care reform,
outlining ideas for the step-by-step approach that Americans have asked for,''
Mr. McConnell said. ''And we know there are a number of issues with bipartisan
support that we can start with when the 2,700-page bill is put on the shelf.''

When asked by Ms. Couric if he would agree to discard the bill and start over,
the president said he would not. The starting point, aides said, would be with
the proposals that passed the House and Senate.

It remained an open question whether the meeting could lead to real consensus on
health care, or whether it would serve only to allow Democrats to frame a
political argument against the Republicans going into the midterm campaign.

Republicans were involved in the health care discussions for months last year in
the Senate Finance Committee, but differences with Democrats were never
resolved.

The bipartisan meeting on health care could give Mr. Obama an opportunity to
display the command on health care issues he showed at the meeting with
Republicans. The administration believes that the public is supportive of many
of the provisions in the bill -- particularly taking away the insurance bans for
pre-existing conditions -- but that the debate was overshadowed by a messy
legislative process.

Representative John A. Boehner of Ohio, the Republican leader, said he was
looking forward to the bipartisan discussion. But he joined Mr. McConnell in
calling for  a fresh start to the health care debate.

''The problem with the Democrats' health care bills is not that the American
people don't understand them -- the American people do understand them, and they
don't like them,'' Mr. Boehner said in a statement. ''The best way to start on
real, bipartisan reform would be to scrap those bills and focus on the kind of
step-by-step improvements that will lower health care costs and expand access.''

In the interview on Sunday, Mr. Obama said he did not regret pursuing health
care in the first year of his presidency, even though he intends to place a
higher priority on job creation this year.

''It was the right thing to do then,'' Mr. Obama. ''It continues to be the right
thing.''

URL: http://www.nytimes.com

LOAD-DATE: February 8, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


