                   Copyright 2009 The New York Times Company


                             318 of 2021 DOCUMENTS


                               The New York Times

                            August 15, 2009 Saturday
                              Late Edition - Final

Britain Responds to Criticism of Its Universal Health System

BYLINE: By THE ASSOCIATED PRESS

SECTION: Section A; Column 0; Foreign Desk; Pg. 5

LENGTH: 538 words

DATELINE: LONDON


The British love to mock their National Health Service, but they have been
incensed by its being used as a punching bag in the fight over President Obama's
proposed reforms.

Conservatives in the United States have relied on horror stories from Britain's
system to warn Americans of the dangers of the socialized health care system
that they say President Obama is trying to impose.

In an interview widely interpreted here as an attack on the British health
system, Senator Charles E. Grassley, Republican of Iowa, told a local radio
station last week that ''countries that have government-run health care'' would
not have given Senator Edward M. Kennedy, who suffers from a brain tumor, the
same standard of care as in the United States because they would rather ''spend
money on people who can contribute more to the economy.''

An editorial in an American newspaper, Investor's Business Daily, stated that
the renowned physicist Stephen Hawking, who is almost entirely paralyzed by
amyotrophic lateral sclerosis, or Lou Gehrig's disease, ''wouldn't have a chance
in the U.K.'' Dr. Hawking, who is British, dismissed the assertion as absurd,
and the newspaper has run a correction. ''I wouldn't be here today if it were
not for the N.H.S.,'' Mr. Hawking said.

Britons say the country's universal health care system, which provides free
medical care, is much fairer than the current American system. Behind the
criticism is a popular British view that American society represents unbridled
capitalism run amok, with catastrophic results for people left behind.

The business minister, Lord Mandelson, who is usually pro-American, strongly
criticized the United States' system on Friday, suggesting that it was fine for
the wealthy but failed the poor. ''If you can't pay, you have a very, very
second-rate service or you can't get health service at all,'' he said.

Britain's Labour government has responded to criticism by offering selected
statistics that show Britain outperforming the United States in a number of
categories, including health spending per capita and life expectancy. The United
States has long lagged behind other advanced nations in vital health care
categories, like life expectancy and infant mortality.

Newspapers here have jumped into the debate, with The Daily Mirror calling the
United States ''the land of the fee,'' because of the steep costs patients are
forced to pay.

The National Health Service, one of the world's largest publicly financed
health services, was set up in 1949 with the intention of providing everyone
with access to health care regardless of their ability to pay. Despite the
widespread show of support for the British system recently, the N.H.S. has been
struggling to cope with rising medical costs, and there are fears it could be
overwhelmed if swine flu cases surge this winter.

Doctors and nurses warn that the organization faces a $24 billion deficit, and
hospitals are often overcrowded, dirty and understaffed. Many people have to
wait weeks or months for medical care despite government promises to shorten
waiting lists. But even those who complain say they want it to be improved, not
dismantled or transformed into an American-style, profit-oriented system.

URL: http://www.nytimes.com

LOAD-DATE: August 15, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


