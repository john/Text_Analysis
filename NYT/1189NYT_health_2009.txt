                   Copyright 2009 The New York Times Company


                             1189 of 2021 DOCUMENTS


                               The New York Times

                          December 16, 2009 Wednesday
                              Late Edition - Final

Ignoring Failure's Real Cost

BYLINE: By DAVID LEONHARDT.

E-mail: Leonhardt@nytimes.com

SECTION: Section B; Column 0; Business/Financial Desk; ECONOMIC SCENE; Pg. 1

LENGTH: 1247 words


Greg Woock is the chief executive of Pinger, a fast-growing Silicon Valley
company that makes iPhone applications. So Mr. Woock spends a fair amount of
time interviewing job applicants. In almost every interview, he told me
recently, the applicant asks about Pinger's health insurance plan.

Now think about that for a minute.

In the cradle of American innovation, workers are making career choices based on
co-payments, pre-existing conditions and other minutiae of health insurance.
They are not necessarily making decisions based on what would be best for their
careers and, in turn, for the American economy  --  that is, ''where their
skills match and where they can grow the most,'' as another Silicon Valley
entrepreneur, Cyriac Roeding, says. Health insurance, Mr. Roeding adds, ''is
distorting the decision-making.''

It is impossible to know how much economic damage these distortions are causing,
but they clearly aren't good. Economic research suggests that more than 1.5
million workers who would otherwise have switched jobs fail to do so every year
because of fears about health insurance. Some of them would have moved to
companies where they could have contributed more, and others would have started
their own businesses.

This link between insurance and innovation isn't relevant merely for the obvious
reason that Congress is in the late stages of debating health reform. It is also
relevant because the United States is suffering from an innovation deficit.

Even before the financial crisis, the decade that will end later this month was
on pace to have the slowest economic growth of any since before World War II.
The No. 1 reason, I'd argue, was our innovation deficit.

For most of this decade, the rate at which companies eliminated jobs was
actually lower than in the 1990s (despite the stories you sometimes hear about
the United States having entered a new era of economic instability). The problem
was that companies weren't creating enough new jobs. The rate at which existing
companies added jobs declined 14 percent from  the end of the 1990s to  2007,
according to the Labor Department. The rate at which start-up companies created
jobs fell even more: 24 percent.

Given the consequences of the innovation deficit  --  slower growth, fewer jobs,
lower living standards  --  you would want to look for every possible solution,
wouldn't you?

You'd want to allow more talented immigrants to become citizens, so that the
next Sergey Brin,  Liz Claiborne or Andy Grove, immigrant entrepreneurs all,
didn't end up starting their companies elsewhere. You would want to clean up the
tangled corporate tax code. You would want to finance more basic research.

And you would want to make people feel confident that they could take risks  --
start a new company or join a young one  --  without  worrying about whether
they would still receive adequate medical care.

Congress is now  close to passing a very promising health reform bill. Neither
the bill that the House passed last month nor the bill being debated in the
Senate is good enough yet. But they are close.

Ultimately, health reform will be judged by two yardsticks. The first will be
whether it begins to shift medicine away from the corrosive fee-for-service
system that leads to uneven results and soaring costs. The second will be
whether people can easily buy insurance even if they don't work for a large
company or qualify for Medicare or Medicaid.

Silicon Valley, with its network of venture capital backers, figures out a way
to get most of its workers some kind of health insurance. But many other
entrepreneurs have a harder time of it. Only 46 percent of companies with three
to nine employees offer health insurance, down from 56 percent a decade ago,
according to the Kaiser Family Foundation.

Why? The administrative costs of insurance are high when they aren't spread over
a large group of workers. Insurers also know that the individuals and small
companies who sign up for health plans tend to be the ones with the most medical
problems, pushing premiums for such plans even higher.

So unless the government steps in to create a large pool of workers who can then
buy insurance more cheaply, many small companies will go without it  --  and
some workers will find themselves tethered to the safety of a big company.

As Eric Schmidt, Google's chief executive, told me, ''There clearly are people
who choose to stay in their jobs due to the fact that they don't have insurance
portability.'' Just consider the economic research showing that people married
to someone with health insurance are more likely to work at small companies than
people who aren't so lucky.

The path to a health bill that begins to solve both core problems  --  access
and cost  --  is fairly clear, numerous health economists say. It involves
combining the Senate provisions on cost and quality control (and, ideally,
strengthening them) with the House provisions on insurance access. Ezra Klein,
the blogger and health policy writer, refers to it as a ''health care reform
fantasy draft.''

The House bill does a good job of creating affordable insurance plans for the
uninsured but does little to attack fee-for-service medicine. As a result,
medical costs are likely to stay high for everyone.

The Senate bill, by contrast, has the ''essential elements of a successful,
fiscally responsible reform strategy,'' according to a recent letter from 26
economists, including four Nobel Prize winners and two former directors of the
Congressional Budget Office. But the Senate plan is too rough on the newly
insured. It would force many of them to pay thousands of dollars a year out of
pocket, on top of their premiums, for care.

''Take the Senate on cost control and the House on affordability,'' Jonathan
Gruber of M.I.T. argues, ''and you've the best possible bill.''

And what if Congress  --  distracted by hot-button issues like drug imports,
government-financed abortions and the so-called public option  --  produces
something short of best? Should we then hope that it gives up and take another
crack at health reform in, say, 2025?

Well, remember that this process was never going to be easy and was always going
to be messy. But in the coming days and weeks, the choices before Congress will
be straightforward: How can the bills still be improved? And once all the
negotiating is over, will the final package seem likely to do more good than
harm?

Last week, Medicare's chief actuary came out with a report that offered some
useful guidance on that second question. The report became fodder for critics of
the bills because it estimated that the current Senate plan would raise national
health spending slightly. Without health reform, the country would spend $4.67
trillion on medical care in 2019. With it, we would spend $20 billion more, a
total of $4.69 trillion.

If you dug into the report, however, you discovered the main reason for the
projected increase. Vastly more people  --  about 33 million  --  would have
insurance if the Senate plan passed. ''There are more people insured, and more
people getting treatment,'' the actuary, Rick Foster, said.

Yet health spending would rise by just 0.5 percent. The cost of insuring a given
individual would be significantly reduced, in other words, and millions of
people would no longer have to worry about whether they were insured.

Maybe one of those millions will end up starting the next great American
company.

URL: http://www.nytimes.com

LOAD-DATE: December 16, 2009

LANGUAGE: ENGLISH

GRAPHIC: CHARTS: FALLING COVERAGE: The percentage of people with
employersponsored health insurance has declined. (BILLY
FARRELL/PATRICKMcMULLAN.COM) (pg.B1)
 DISAPPEARING JOB OPENINGS: The rate at which private-sector establishments cut
jobs accelerated during the recent recession, but was still below the 2001 peak,
as of early this year. Job creation, however, dropped sharply in recent years.
(Source: Business Employment Dynamics report, Bureau of Labor Statistics THE NEW
YORK TIMES) (pg.B8)

PUBLICATION-TYPE: Newspaper


