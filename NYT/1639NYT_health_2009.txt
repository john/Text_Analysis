                   Copyright 2010 The New York Times Company


                             1639 of 2021 DOCUMENTS


                               The New York Times

                            March 11, 2010 Thursday
                         The New York Times on the Web

Obama's Student Loan Overhaul Endangered

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section ; Column 0; National Desk; Pg.

LENGTH: 907 words


WASHINGTON -- With Democratic Congressional leaders and the White House
struggling on Wednesday to finalize the details of major health care
legislation, House Democrats were desperately trying to prevent another of
President Obama's top legislative priorities  an ambitious overhaul of student
loan programs  from becoming a casualty of the health care battle.

But Democrats in the Senate, where the private student lending industry has
strong allies, predicted on Wednesday night that the education bill would not be
part of an expedited budget measure containing the final revisions to the health
care legislation. Some Democrats said that such a move would stall the student
loan changes at a minimum for several months, and perhaps kill the overhaul
altogether.

Mr. Obama's plan would end a program in which the government pays private,
for-profit student lending companies to make risk-free loans using taxpayer
money. Instead, the proposed overhaul would broaden the government's existing
direct-lending program, saving billions of dollars that the president had
proposed using to expand Pell grant scholarships for low-income students.

But the education bill is strongly opposed by some Senate Democrats,
particularly those in states where for-profit student lenders are major
employers. In a letter to the majority leader, Senator Harry Reid of Nevada, six
Democrats said they disliked the president's proposal.

''We write to make you aware of our concern with provisions of contemplated
student lending reform that could put jobs at risk,'' the senators wrote.
''Increase our nation's commitment to higher education funding is a priority,
but we must proceed toward this objective in a thoughtful manner that considers
potential alternative legislative proposals, while still delivering an
equivalent amount of savings over the next ten years.

The letter was signed by Senators Thomas R. Carper of Delaware, Blanche Lincoln
of Arkansas, Ben Nelson of Nebraska, Bill Nelson of Florida, Mark Warner of
Virginia and Jim Webb of Virginia.

The private student lenders insist that the government can save money while
allowing them to continue earning some profit on government loans. Supporters of
the legislation have repeatedly refuted those arguments, noting that workers
would still be needed to originate and service loans made directly by the
government. Some private lenders already do that work for the government. The
only difference, supporters of the legislation say, is that private lenders
would not profit, as they do now, by lending out government money and then
selling the loans back to the government.

Provisions in a budget reconciliation bill must  meet budget targets for
reducing the federal deficit. Mr. Conrad said the education bill could no longer
meet that requirement because the projected savings from ending the payments to
private student lenders had decreased, according to a recent cost analysis by
the Congressional Budget Office, while the cost of expanding Pell grants had
grown. As a result, he said the bill as currently written would no longer reduce
the deficit.

The House bill was projected to save $87 billion over 10 years and would have
spent $87 billion on Pell grants and other education initatives. Mr. Reid's
office said a more recent estimate showed the bill would increase future
deficits by about $36 billion.

The bill would save less money because many schools are already switching to the
direct lending program, and would cost more because amid the economic downturn
more people are going back to school and seeking Pell grants and other
assistance.

House Democrats say the bill was never intended to spend more money than it
saved, and that the legislation could easily be adjusted to meet the
reconciliation requirements.

''Obviously, Sallie Mae and other banks, with their fistfulls of cash, are
starting to have their way in the United States Senate,'' one Democratic
Congressional aide said.

In a meeting on Tuesday, Mr. Miller and other House Democrats pressed Mr. Conrad
to rely on an earlier cost analysis of the House bill that had been prepared by
the budget office.

In a meeting on Tuesday Mr. Miller and other House Democrats pressed Mr. Conrad
to rely on an earlier cost analysis of the House bill that had been prepared by
the budget office. Mr. Conrad refused, arguing that even if he could legally
rely on the prior analysis, it was outdated.

In an interview, Mr. Conrad suggested that the best course of action would be
for the education changes to be adopted through a reconciliation bill in next
year's budget process. Congress could approve a new budget resolution later this
spring, and Mr. Conrad said the education bill could be adopted soon after that.
But given the uncertainties of a mid-term election year, some supporters of the
education bill predicted that it would simply die.

''I am strongly supportive of the education package,'' Mr. Conrad said in an
interview. ''But I am also insisting that it be paid for.''

Private, for-profit student lenders, including Sallie Mae, have lobbied fiercely
against the president's plan. But they were unable to stop House Democrats from
approving a bill. Critics of the industry say it reaps large, and unjustified
profits, by originating loans made with taxpayer money.  The industry insists
that it provides valuable services to borrowers and competition for the direct
government lending program.

URL: http://www.nytimes.com

LOAD-DATE: March 11, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


