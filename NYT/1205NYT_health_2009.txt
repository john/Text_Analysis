                   Copyright 2009 The New York Times Company


                             1205 of 2021 DOCUMENTS


                               The New York Times

                            December 18, 2009 Friday
                              Late Edition - Final

Liberal Revolt on Health Care Stings White House

BYLINE: By SHERYL GAY STOLBERG

SECTION: Section A; Column 0; National Desk; Pg. 26

LENGTH: 786 words

DATELINE: WASHINGTON


In the great health care debate of 2009, President Obama has cast himself as a
cold-eyed pragmatist, willing to compromise in exchange for votes. Now ideology
-- an uprising on the Democratic left -- is smacking the pragmatic president in
the face.

Stung by the intense White House effort to court the votes of moderate holdouts
like Senator Joseph I. Lieberman, independent of Connecticut, and Senator Ben
Nelson, Democrat of Nebraska, liberals are signaling that they have compromised
enough. Grass-roots groups are balking, liberal commentators are becoming more
critical of the president, some unions are threatening to withhold support and
Howard Dean, the former Democratic Party chief, is urging the Senate to kill its
health bill.

The White House scrambled Thursday to tamp down the revolt, which has been
simmering for weeks but boiled over when the Senate Democratic leadership,
bowing to Mr. Lieberman, scrapped language allowing people as young as 55 to buy
into Medicare.

David Axelrod, Mr. Obama's senior adviser, began the day by calling in to MSNBC
to urge the party to hold together, warning of a ''tragic outcome'' if Democrats
failed to pass a bill that the White House says would expand health coverage
while reining in costs.

''I don't think you want this moment to pass,'' Mr. Axelrod said. ''It will not
come back again.''

Former President Bill Clinton echoed the theme, issuing a statement in which he
urged fellow Democrats to ''take it from someone who knows: these chances don't
come around every day.'' Mr. Clinton's former chief of staff, John Podesta, a
close Obama ally, pushed back against Mr. Dean on the Internet and on
television.

The backlash is building as Mr. Obama is under increasing pressure from his
party's left flank on issues ranging from the economy to Afghanistan. This week
alone, the House speaker, Nancy Pelosi, declared that she would not insist that
her caucus vote to finance Mr. Obama's troop buildup, while Senator Bernard
Sanders, the Vermont independent and self-avowed democratic socialist, chastised
the president for reappointing Ben S. Bernanke as chairman of the Federal
Reserve.

The left's disenchantment with Mr. Obama on health care harks back to his
decision, before he became president, not to try to replace the current private
insurance system with a single-payer, government-run ''Medicare for all''
system. Throughout the year, at town-hall-style meetings and other public
appearances, he has been dogged by advocates who have complained that he has
sold out.

Instead, the president proposed what he called the public option, a
government-run insurance plan that would coexist with private plans on a new
health insurance exchange. For many on the left who are also dissatisfied with a
deal the White House cut with the pharmaceutical industry, the public option was
itself a compromise.

Now that the Senate Democratic leadership has stripped the last vestige of the
public option -- the Medicare buy-in provision -- from its bill, progressives
are feeling doubly betrayed.

''It's time for the president to get his hands dirty,'' Representative Anthony
Weiner, Democrat of New York, said in a statement this week. ''Some of us have
compromised our compromised compromise. We need the president to stand up for
the values our party shares.''

The passion is most intense in the House, but it appears to be spilling over to
the Senate. In an interview on Thursday, Mr. Sanders, another advocate of a
single-payer system, said he was not certain how he would vote on the bill,
though Democratic leaders have been assuming he would back it in the end.

''I don't sleep well,'' Mr. Sanders said. ''I am struggling with this issue very
hard, trying to sort out what is positive in this bill, what is negative in the
bill, what it means for our country if there is no health insurance legislation,
when we will come back to it.''

The senator added, ''And I have to combine that with the fact that I absolutely
know that the insurance companies and the drug companies will be laughing all
the way to the bank the day after this is passed.''

The White House insists that is not the case. Aides to Mr. Obama argue that the
bill includes a variety of consumer protections -- like ending lifetime coverage
caps and the practice of refusing patients with pre-existing conditions -- that
will make insurers unhappy.

''People need to put this in perspective,'' Dan Pfeiffer, Mr. Obama's
communications director, said in an interview on Thursday. ''Two years ago, the
Democratic Party would have done anything for the opportunity to pass a health
care reform like this. Let's realize how far we've come, and how close we are to
making history.''

URL: http://www.nytimes.com

LOAD-DATE: December 18, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Senator Bernard Sanders, on Capitol Hill on Wednesday, says he
is struggling with how to vote.(PHOTOGRAPH BY STEPHEN CROWLEY/THE NEW YORK
TIMES)

PUBLICATION-TYPE: Newspaper


