                   Copyright 2009 The New York Times Company


                             1290 of 2021 DOCUMENTS


                               The New York Times

                          December 30, 2009 Wednesday
                              Late Edition - Final

Republican In Florida May File Suit Over Bill

BYLINE: By DAMIEN CAVE

SECTION: Section A; Column 0; National Desk; Pg. 19

LENGTH: 493 words

DATELINE: VERO BEACH, Fla.


Florida's attorney general questioned the constitutionality of the federal
health care bill on Tuesday, calling on states to study whether to file suit to
kill a provision requiring that individuals buy health insurance or pay a fine.

The attorney general, William McCollum, a Republican who is running for governor
in 2010, said that the so-called mandate was ''an affront to our country's
principles.'' He added that the fine might be illegal because, in his view, it
is disconnected from any commercial act.

''It's a tax on living,'' Mr. McCollum said in a telephone conference call with
reporters. ''It's a tax on people or a penalty on those who don't do anything.''

Mr. McCollum's stance places him in line with the attorneys general of South
Carolina and nearly a dozen other states who have also threatened to sue over
the mandate.

Health care has already become a major dividing line in Florida's political
campaigns ahead of 2010 elections. As if on cue, Democrats responded within
minutes of Mr. McCollum's announcement on Tuesday.

In a Twitter message, State Senator Dan Gelber, a Democrat who is running for
attorney general, wrote, ''I wish McCollum was as concerned about solving
Florida's health care crisis as he was about stopping the solving of the health
care crisis.''

Mr. Gelber later added: ''There are four million Floridians without health care,
including 800,000 children. Only one state has a higher percentage of
uninsured.''

The argument over the mandate's legality, however, did not necessarily begin
with elected officials. Mr. McCollum's position closely follows what was already
laid out by conservative opponents outside government -- bloggers, former
Justice Department lawyers and research groups.

The Heritage Foundation, for example, posted a lengthy legal analysis on Dec. 9
that argued that Congress had specific, limited powers that did not include
''the distinct constitutional power to compel persons to purchase a contract of
insurance from a private insurance company.''

That report said that the federal health insurance requirement differed from
mandates requiring that drivers be insured because auto insurance is connected
to the choice to drive.

Democrats and some legal scholars have questioned that distinction. Erwin
Chemerinsky, dean of the University of California, Irvine, School of Law, has
said that the health care penalty is similar to fines for failing to obtain auto
insurance because it is connected to commerce -- the inevitable need for medical
care at some point.

Mr. McCollum said the mandate was an effort to solve one problem, the cost of
health care, by causing another, namely the removal of people's freedom to
abstain. He said he hoped that a letter he sent Tuesday to other attorneys
general would yield some form of constructive criticism.

''The attorney general's job is not to address the health care issue as such,''
he said. ''It's to address the legal issues.''

URL: http://www.nytimes.com

LOAD-DATE: December 30, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


