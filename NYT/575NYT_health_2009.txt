                   Copyright 2009 The New York Times Company


                             575 of 2021 DOCUMENTS


                               The New York Times

                           September 13, 2009 Sunday
                              Late Edition - Final

The Body Count At Home

BYLINE: By NICHOLAS D. KRISTOF

SECTION: Section WK; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 18

LENGTH: 810 words


In the debate over health care, here's an inequity to ponder: Nikki White would
have been far better off if only she had been a convicted bank robber.

Nikki was a slim and athletic college graduate who had health insurance, had
worked in health care and knew the system. But  she had  systemic lupus
erythematosus, a chronic inflammatory disease that was diagnosed when she was 21
and gradually left her too sick to work. And once she lost her job, she lost her
health insurance.

In any other rich country, Nikki probably would have been fine, notes T. R. Reid
in his important and powerful new book, ''The Healing of America.'' Some 80
percent of lupus patients in the United States live a normal life span. Under a
doctor's care, lupus should be manageable. Indeed, if Nikki had been a felon,
the problem could have been averted,   because courts have ruled that prisoners
are entitled to medical care.

As Mr. Reid recounts, Nikki tried everything to get medical care, but no
insurance company would accept someone with her pre-existing condition. She
spent months painfully writing letters to anyone she thought might be able to
help. She fought tenaciously for her life.

Finally, Nikki collapsed at her home in Tennessee and was rushed to a hospital
emergency room, which was then required to treat her without payment until her
condition stabilized. Since money was no longer an issue, the hospital performed
25 emergency surgeries on Nikki, and she spent six months in critical care.

''When Nikki showed up at the emergency room, she received the best of care, and
the hospital spent hundreds of thousands of dollars on her,'' her step-father,
Tony Deal, told me. ''But that's not when she needed the care.''

By then it was too late. In 2006, Nikki White died at age 32. ''Nikki didn't die
from lupus,'' her doctor, Amylyn Crawford, told Mr. Reid. ''Nikki died from
complications of the failing American health care system.''

''She fell through the cracks,'' Nikki's mother, Gail Deal, told me grimly.
''When you bury a child, it's the worst thing in the world. You never recover.''

We now have a chance to reform this cruel and capricious system. If we let that
chance slip away, there will be another Nikki dying every half-hour.

That's how often someone dies in America because of a lack of insurance,
according to a study by a branch of the National Academy of Sciences. Over a
year, that amounts to 18,000 American deaths.

After Al Qaeda killed nearly 3,000 Americans, eight years ago on Friday, we went
to war and spent hundreds of billions of dollars ensuring that this would not
happen again. Yet every two months, that many people die because of our failure
to provide universal insurance  -- and yet many members of Congress want us to
do nothing?

Mr. Reid's book is a rich tour of health care around the world. Because he has a
bum shoulder, he asked doctors in many countries to examine it and make
recommendations. His American orthopedist recommended a titanium shoulder
replacement that would cost tens of thousands of dollars and might or might not
help. Specialists in other countries warned that a sore shoulder didn't justify
the risks of such major surgery, although some said it would be available free
if Mr. Reid insisted. Instead, they offered physical therapy, acupuncture and
other cheap and noninvasive alternatives, some of which worked pretty well.

That's a window into the flaws in our health care system: we offer titanium
shoulder replacements for those who don't really need them, but we let
32-year-old women die if they lose their health insurance. No wonder we spend so
much on medical care, and yet have some health care statistics that are worse
than Slovenia's.

My suggestion for anyone in Nikki's situation: commit a crime and get locked up.
In Washington State, a 20-year-old inmate named Melissa Matthews chose to turn
down parole and stay in prison because that was the only way she could get
treatment for her cervical cancer. ''If I'm out, I'm going to die from this
cancer,'' she told a television station.

Mr. and Mrs. Deal say they are speaking out because Nikki wouldn't want anyone
to endure what she did. ''Nikki was a college-educated, middle-class woman, and
if it could happen to her, it can happen to anyone,'' Mr. Deal said. ''This
should not be happening in our country.''

Struggling to get out the words, Mrs. Deal added: ''The loss of a child is the
greatest hurt anyone will ever suffer. Because of the circumstances she endured
with the health care system, I lost my daughter.''

Complex arguments are being batted around in this health care debate, but the
central issue isn't technical but moral. The first question is simply this: Do
we wish to be the only rich nation in the world that lets a 32-year-old woman
die because she can't get health insurance? Is that really us?

URL: http://www.nytimes.com

LOAD-DATE: September 13, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


