                   Copyright 2010 The New York Times Company


                             1705 of 2021 DOCUMENTS


                               The New York Times

                             March 21, 2010 Sunday
                              Late Edition - Final

The Real Arithmetic of Health Care Reform

BYLINE: By DOUGLAS HOLTZ-EAKIN.

Douglas Holtz-Eakin, who was the director of the Congressional Budget Office
from 2003 to 2005, is the president of the American Action Forum, a policy
institute.

SECTION: Section WK; Column 0; Editorial Desk; OP-ED CONTRIBUTOR; Pg. 12

LENGTH: 835 words

DATELINE: Arlington, Va.


ON Thursday, the Congressional Budget Office reported that, if enacted, the
latest health care reform legislation would, over the next 10 years, cost about
$950 billion, but because it would raise some revenues and lower some costs, it
would also lower federal deficits by $138 billion. In other words, a bill that
would set up two new entitlement spending programs -- health insurance subsidies
and long-term health care benefits -- would actually improve the nation's bottom
line.

Could this really be true? How can the budget office give a green light to a
bill that commits the federal government to spending nearly $1 trillion more
over the next 10 years?

The answer, unfortunately, is that the budget office is required to take written
legislation at face value and not second-guess the plausibility of what it is
handed. So fantasy in, fantasy out.

In reality, if you strip out all the gimmicks and budgetary games and rework the
calculus, a wholly different picture emerges: The health care reform legislation
would raise, not lower, federal deficits, by $562 billion.

Gimmick No. 1  is the way the bill front-loads revenues and backloads spending.
That is, the taxes and fees it calls for are set to begin immediately, but its
new subsidies would be deferred so that the first 10 years of revenue would be
used to pay for only 6 years of spending.

Even worse, some costs are left out entirely. To operate the new programs over
the first 10 years, future Congresses would need to vote for $114 billion in
additional annual spending. But this so-called discretionary spending is
excluded from the Congressional Budget Office's tabulation.

Consider, too, the fate of the $70 billion in premiums expected to be raised in
the first 10 years for the legislation's new long-term health care insurance
program. This money is counted as deficit reduction, but the benefits it is
intended to finance are assumed not to materialize in the first 10 years, so
they appear nowhere in the cost of the legislation.

Another vivid example of how the legislation manipulates revenues is the
provision to have corporations deposit $8 billion in higher estimated tax
payments in 2014, thereby meeting fiscal targets for the first five years. But
since the corporations' actual taxes would be unchanged, the money would need to
be refunded the next year. The net effect is simply to shift dollars from 2015
to 2014.

In addition to this accounting sleight of hand, the legislation would blithely
rob Peter to pay Paul. For example, it would use $53 billion in anticipated
higher Social Security taxes to offset health care spending. Social Security
revenues are expected to rise as employers shift from paying for health
insurance to paying higher wages. But if workers have higher wages, they will
also qualify for increased Social Security benefits when they retire. So the
extra money raised from payroll taxes is already spoken for. (Indeed, it is
unlikely to be enough to keep Social Security solvent.) It cannot be used for
lowering the deficit.

A government takeover of all federally financed student loans -- which obviously
has nothing to do with health care -- is rolled into the bill because it is
expected to generate $19 billion in deficit reduction.

Finally, in perhaps the most amazing bit of unrealistic accounting, the
legislation proposes to trim $463 billion from Medicare spending and use it to
finance insurance subsidies. But Medicare is already bleeding red ink, and the
health care bill has  no reforms that would enable the program to operate more
cheaply in the future. Instead, Congress is likely to continue to regularly
override scheduled cuts in payments to Medicare doctors and other providers.

Removing the unrealistic annual Medicare savings ($463 billion) and the stolen
annual revenues from Social Security and long-term care insurance ($123
billion), and adding in the annual spending that so far is not accounted for
($114 billion) quickly generates additional deficits of $562 billion in the
first 10 years.  And the nation would be on the hook for two more entitlement
programs rapidly expanding as far as the eye can see.

The bottom line is that Congress would spend a lot more; steal funds from
education, Social Security and long-term care to cover the gap; and promise that
future Congresses will make up for it by taxing more and spending less.

The stakes could not be higher. As documented in another recent budget office
analysis, the federal deficit is already expected to exceed at least $700
billion every year over the next decade, doubling the national debt to more than
$20 trillion. By 2020, the federal deficit -- the amount the government must
borrow to meet its expenses -- is projected to be $1.2 trillion, $900 billion of
which represents interest on previous debt.

The health care legislation would only increase this crushing debt. It is a
clear indication that Congress does not realize the urgency of putting America's
fiscal house in order.

URL: http://www.nytimes.com

LOAD-DATE: March 21, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


