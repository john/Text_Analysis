                   Copyright 2009 The New York Times Company


                             482 of 2021 DOCUMENTS


                               The New York Times

                           September 3, 2009 Thursday
                              Late Edition - Final

Rodale and the Obamas Make a Case for Health (and Health Care)

BYLINE: By STEPHANIE CLIFFORD

SECTION: Section B; Column 0; Business/Financial Desk; ADVERTISING; Pg. 3

LENGTH: 1004 words


PRESIDENT OBAMA is taking his argument for a health care plan to a new place:
Rodale magazines, where he or his wife appear on coming covers of Prevention,
Men's Health, Women's Health and the new publication Children's Health.

Wait, Children's Health? Are we to expect 6-year-olds with six-pack abs? More on
that in a moment.

First, to the Obamas. The president has been pitching his health care plan
without total success in Congress and in town hall meetings nationwide. Now, he
makes the argument in the pages of the Rodale publications. Peter Moore, editor
of Men's Health, who wrote a cover article on Mr. Obama in November, approached
the White House in the spring with the idea of doing articles focused on health
care in four Rodale magazines. Three will  run in the October issues, while the
Prevention cover is appearing in November.

The Men's Health and Women's Health  articles publicize the Obama health care
plan, with Men's Health strongly endorsing it. A sidebar to the president's
interview there lists  ''five reasons you should care'' about the health care
plan, and each point is positive  --  your premium may go down, your emergency
room care would improve.

Mr. Moore said he approached the article  with a very clear point of view.
''We're not bystanders,'' he said.   ''The whole issue of health care in the
U.S., it's something that we have to feel strongly about. We're health
journalists.

''We know, if anyone does, what's broken there, and so if this comes off as more
of an advocacy piece, it's because we're advocates for health.''

It's Michelle Obama who plays spokeswoman in the  three other magazines. Mr.
Moore interviewed her in Women's Health about her view on health care reform,
her family's experiences dealing with illness and keeping down calories  in the
White House kitchen. Mrs. Obama did not land on the cover of Women's Health,
though. The cover has a small photograph of her, while the actress Christina
Applegate is the main subject. David Zinczenko, editor in chief of Men's Health
and editorial director of Women's Health and Children's Health, said that was
because Rodale wanted to parcel  out the wealth of Obama coverage among four of
the company's magazines, and Women's Health had a readership that was younger
than Mrs. Obama, so it made sense to hold her from that cover.

In Prevention, there isn't a word about the health care debate. Instead Liz
Vaccariello, the editor in chief, poses questions about workout habits, eating
habits, sleep habits and Mrs. Obama's feelings about aging and skin care.
Readers  ''care deeply about the health care plan,'' Ms. Vaccariello said in an
e-mail message,  ''but they don't come to Prevention to read about policy or
politics.''

Rodale is also using Mrs. Obama to make a high-profile introduction of a new
magazine, Children's Health. Mrs. Obama appears on the cover and talks about her
family's eating habits in a feature inside.

The magazine is a special issue, and Rodale is hoping the Obama factor will help
it sell many of the 750,000 copies it is printing, most of them to be sold on
newsstands. While this is a new publication, Mr. Zinczenko emphasized that it
was not a debut.   Rodale is taking a try-it-and-see approach, and there are no
firm plans to make this a regular publication. The content of Children's Health
is less about how to give a baby biceps, and more about advice for anxious,
fitness-obsessed parents. Like its sister publications, it emphasizes how-to
stories: choosing the right formula for your baby, keeping your child from
getting fat, avoiding weight gain yourself after you have children and teaching
your children  how to read food labels.

In terms of advertising, the magazine has some challenges. Rodale will not
submit it to be measured by the circulation or demographic companies, meaning
advertisers will have to take its word on distribution.

And much of what Children's Health is saying about choosing natural, unprocessed
foods, is not, presumably, a message food advertisers would endorse. In his
Children's Health story about Mrs. Obama, Mr. Moore makes this point clearly.
''If you can set a good example, you have a fighting chance of countering the
$1.6 billion a year the food industry spends to sell a chubby lifestyle to your
kids,'' he wrote.

But Children's Health is trying to attract some of that food industry money to
its advertising pages.  ''This is extending a lot of the advertiser
relationships we already have,'' Mr. Zinczenko said.  ''A lot of these companies
are already in the kids' space, whether it's through children's clothing or
children's food brands.'' Excluding house ads, there were only 15 pages of
advertisements in the first 112-page issue. Seven of those came from General
Mills, selling children's cereals like Lucky Charms, with 11 grams of sugar per
serving.  John Haugen, vice president for health and wellness at General Mills,
said that the company used whole grains and fortified ingredients even in its
more sugary cereals, and over all, the message of Children's Health was in line
with his company's message.

''We don't approve every editorial piece that goes into every magazine,'' he
said.  ''The main point is we can agree on the really fundamental things,
steering kids and families to adopt those healthy behaviors.''

Children's Health is also competing for children's advertisers with established
magazines like Parenting and Parents, which both have circulations of more than
two million.

''It's been very hard for anyone to successfully break into that category and
grab enough away to really be a first-tier book,'' said Audrey Siegel, executive
vice president and director of client services at TargetCast TCM, which buys
advertising for clients like Expedia.

Even so, Ms. Siegel said that Rodale's decision to make this a relatively cheap
introduction was wise.

''They try things but then they divest themselves quickly, so it's fail early,
fail often, which is a smart way to do business,'' she said.

URL: http://www.nytimes.com

LOAD-DATE: September 3, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Michelle Obama talked about her family's diet in Children's
Health.

PUBLICATION-TYPE: Newspaper


