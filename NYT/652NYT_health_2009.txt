                   Copyright 2009 The New York Times Company


                             652 of 2021 DOCUMENTS


                               The New York Times

                           September 21, 2009 Monday
                              Late Edition - Final

Corrections

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 55 words


A reader's comment in the Prescriptions column on Thursday  in reaction to the
health care proposal introduced by Senator Max Baucus misstated the maximum
penalty a family might face under the plan for not having insurance coverage. As
an accompanying article reported, the highest possible penalty would be $3,800 a
year, not $1,500.

URL: http://www.nytimes.com

LOAD-DATE: September 21, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Correction

PUBLICATION-TYPE: Newspaper


