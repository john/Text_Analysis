                   Copyright 2009 The New York Times Company


                             1287 of 2021 DOCUMENTS


                               The New York Times

                           December 29, 2009 Tuesday
                              Late Edition - Final

A Less Than Honest Policy

BYLINE: By BOB HERBERT

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 31

LENGTH: 792 words


There is a middle-class tax time bomb ticking in the Senate's version of
President Obama's effort to reform health care.

The bill that passed the Senate with such fanfare on Christmas Eve would impose
a confiscatory 40 percent excise tax on so-called Cadillac health plans, which
are popularly viewed as over-the-top plans held only by the very wealthy. In
fact, it's a tax that in a few years will hammer millions of middle-class
policyholders, forcing them to scale back their access to medical care.

Which is exactly what the tax is designed to do.

The tax would kick in on plans exceeding $23,000 annually for family coverage
and $8,500 for individuals, starting in 2013. In the first year it would affect
relatively few people in the middle class. But because of the steadily rising
costs of health care in the U.S., more and more plans would reach the taxation
threshold each year.

Within three years of its implementation, according to the Congressional Budget
Office, the tax would apply to nearly 20 percent of all workers with
employer-provided health coverage in the country, affecting some 31 million
people. Within six years, according to Congress's Joint Committee on Taxation,
the tax would reach  a fifth of all households earning between $50,000 and
$75,000 annually. Those families can hardly be considered very wealthy.

Proponents say the tax will raise nearly $150 billion over 10 years, but there's
a catch. It's not expected to raise this money directly. The dirty little secret
behind this onerous tax is that no one expects very many people to pay it. The
idea is that rather than fork over 40 percent in taxes on the amount by which
policies exceed the threshold, employers (and individuals who purchase health
insurance on their own) will have little choice but to ratchet down the quality
of their health plans.

These lower-value plans would have higher out-of-pocket costs, thus increasing
the very things that are so maddening to so many policyholders right now: higher
and higher co-payments, soaring deductibles and so forth. Some of the benefits
of higher-end policies can be expected in many cases to go by the boards: dental
and vision care, for example, and expensive mental health coverage.

Proponents say this is a terrific way to hold down health care costs. If
policyholders have to pay more out of their own pockets, they will be more
careful -- that is to say, more reluctant -- to access health services. On the
other hand, people with very serious illnesses will be saddled with much higher
out-of-pocket costs. And a reluctance to seek treatment for something that might
seem relatively minor at first could well have terrible (and terribly expensive)
consequences in the long run.

If even the plan's proponents do not expect policyholders to pay the tax, how
will it raise $150 billion in a decade? Great question.

We all remember learning in school about the suspension of disbelief. This part
of the Senate's health benefits taxation scheme requires a monumental suspension
of disbelief. According to the Joint Committee on Taxation, less than 18 percent
of the revenue will come from the tax itself.  The rest of the $150 billion,
more than 82 percent of it, will come from the income taxes paid by workers who
have been given pay raises by employers who will have voluntarily handed over
the money they saved by offering their employees less valuable health insurance
plans.

Can you believe it?

I asked Richard Trumka, president of the A.F.L.-C.I.O., about this. (Labor
unions are outraged at the very thought of a health benefits tax.) I had to wait
for him to stop laughing to get his answer. ''If you believe that,'' he said,
''I have some oceanfront property in southwestern Pennsylvania that I will sell
you at a great price.''

A survey of business executives by Mercer, a human resources consulting firm,
found that only 16 percent of respondents said they would convert the savings
from a reduction in health benefits into higher wages for employees. Yet
proponents of the tax are holding steadfast to the belief that nearly all would
do so.

''In the real world, companies cut costs and they pocket the money,'' said Larry
Cohen, president of the Communications Workers of America and a leader of the
opposition to the tax. ''Executives tell the shareholders: 'Hey, higher profits
without any revenue growth. Great!' ''

The tax on health benefits is being sold to the public dishonestly as something
that will affect only the rich, and it makes a mockery of President Obama's
repeated pledge that if you like the health coverage you have now, you can keep
it.

Those who believe this is a good idea should at least have the courage to be
straight about it with the American people.

URL: http://www.nytimes.com

LOAD-DATE: December 29, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


