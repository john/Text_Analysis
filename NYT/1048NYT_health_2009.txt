                   Copyright 2009 The New York Times Company


                             1048 of 2021 DOCUMENTS


                               The New York Times

                            November 22, 2009 Sunday
                              Late Edition - Final

Pundit Stakes Out a More Activist Role in Politics

BYLINE: By BRIAN STELTER; Amy Green contributed reporting.

SECTION: Section A; Column 0; National Desk; Pg. 25

LENGTH: 818 words


Glenn Beck, the popular and outspoken Fox News host, says he wants to go beyond
broadcasting his opinions and start rallying his political base -- formerly
known as his audience -- to take action.

To do so, Mr. Beck is styling himself as a political organizer. In an interview,
he said he would promote voter registration drives and sponsor a series of seven
conventions across the country featuring what he described as libertarian
speakers.

On Saturday he held a festive campaign-style rally in The Villages in Florida,
north of Orlando, in which he promoted his recently released book, ''Arguing
With Idiots,'' and announced another book to come next August filled with
right-leaning policy proposals gathered from the conventions.

Mr. Beck provided few details about his plans for the tour, making it unclear if
he truly intends to prod his audience of millions into political action or
merely burnish his media brand ahead of a book release.

Mr. Beck did say the conventions would resemble educational seminars, and he
emphasized that while candidates may align themselves with the values and
principles that he espouses, he would not take the next step to endorse them.

In describing the conventions, he told the crowd on Saturday: ''You're going to
learn about finance. You're going to learn about community organizing. You're
going to learn everything we need to know if you want to be a politician.''

His staff would not say whether particular candidates for office in the 2010
midterm elections would be invited to speak at the conventions or the August
rally.

As for the question of Mr. Beck's intentions, ''He might just be trying to sell
books, but there are much simpler ways to sell books,'' said Ari Rabin-Havt, a
vice president at Media Matters, the liberal media monitoring group. He said Mr.
Beck sounded more like a presidential candidate than a pundit.

Mr. Beck, having used his television and radio pulpit to lay out his list of the
country's impending problems -- deficit spending, health care legislation that
will ''destroy'' the economy, a dearth of ''personal responsibility'' -- says he
now wants to also provide solutions.

In the interview, Mr. Beck, a frequent critic of President Obama, chose his
words carefully but made clear that he intended to help elect politicians
aligned with his limited-government world view. ''We'll be looking for ways to
get people involved in politics,'' he said.

Mr. Beck is not the only media firebrand trying to mobilize Americans
disaffected with a Democratic-controlled government. The radio host Laura
Ingraham is inviting candidates to sign a 10-point pledge on her Web site. Sean
Hannity, on his afternoon radio show and prime-time Fox News program, is
promoting ''Conservative Victory 2010,'' his name for the map on his site that
will spell out questions for candidates.

And the former presidential candidate Mike Huckabee, who has a show on Fox News,
has steered viewers to his Web site, where they can contribute money to his
political action committee in support of conservative candidates.

Pundits have used their media stages to encourage political action before, but
people like Mr. Beck and Mr. Hannity are taking on outsize roles now, political
experts and conservative commentators say. One reason, they say, is the weakened
state of the Republican Party.

The media figures' roles may exacerbate the ideological feuds that are already
roiling the party. For the diffuse tea party movement that taps into
anti-government sentiments, ''the media guys are the closest things we even have
to a leader,'' said Adam Brandon, the vice president for communications at
FreedomWorks, a conservative advocacy group.

These efforts are reminiscent of the Contract With America pledge made by
conservatives during the 1994 elections, though some Republicans who are
uncomfortable with media personalities taking on new political roles note that
that effort originated with lawmakers.

When asked about Mr. Beck at a conference last month, Senator Lindsey Graham,
Republican of South Carolina, said: ''Here's what I worry about. How many people
in my business are going to be controlled by what's said on the radio or in a TV
commercial?''

It was not lost on Mr. Beck's fans that the Saturday rally and book signing were
held in Florida, where the Republican governor, Charlie Crist, has been sharply
criticized by conservatives as he runs for an open seat in the United States
Senate. Mr. Crist's challenger, Marco Rubio, has already signed the pledge on
Ms. Ingraham's Web site, as have a smattering of other conservative candidates.

Already, Mr. Beck's page on FoxNews.com features what it calls ''In or Out
2010,'' a ''simple challenge'' for lawmakers. It includes a pledge to back a
freeze in government spending; oppose all tax increases ''until our economy has
rebounded''; and support stricter immigration enforcement.

URL: http://www.nytimes.com

LOAD-DATE: November 22, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: A crowd lined up on Saturday for a rally and book-signing event
by the conservative Fox News host Glenn Beck in Florida. (PHOTOGRAPH BY GREGG
MATTHEWS FOR THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


