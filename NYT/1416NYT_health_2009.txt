                   Copyright 2010 The New York Times Company


                             1416 of 2021 DOCUMENTS


                               The New York Times

                            January 24, 2010 Sunday
                              Late Edition - Final

Harry Reid Is Complicated.

BYLINE: By ADAM NAGOURNEY.

Adam Nagourney is chief national political correspondent for The New York Times.
His most recent article for the magazine was a profile of Terry McAuliffe during
his campaign for the Democratic gubernatorial nomination in Virginia.

SECTION: Section MM; Column 0; Magazine Desk; Pg. 26

LENGTH: 4846 words


HARRY REID WAS HOARSE and hacking, drawn and more stooped than usual on a Sunday
morning 12 days before Christmas. It was not yet noon, and Reid was in his
second-floor corner office in an empty United States Capitol. He had arrived to
bad news. Joseph Lieberman, the independent Connecticut senator, had announced
on CBS's ''Face the Nation'' that he would not support the Senate health care
plan, which meant that Reid did not have the 60 votes he needed. Lieberman's
announcement, which torpedoed a compromise that Reid helped to midwife, caught
the Senate majority leader by surprise. Reid had spoken with Lieberman two days
earlier, and one of Lieberman's top aides participated in the Saturday-afternoon
conference call that Reid orchestrates for Democratic senators who will be
appearing on the Sunday talk shows. ''He double-crossed me,'' Reid said stiffly,
associates later recounted. ''Let's not do what he wants. Let the bill just go
down.''

Rahm Emanuel, the president's chief of staff, arrived at Reid's office not long
afterward -- casually dressed, a cup of coffee in one hand -- and after a brisk
meeting, a decision was reached: Reid would abandon his compromise, which was
intended to appease proponents of a government-run insurance plan. The
concession would not sit well with a lot of Democrats, not to mention the
powerful constituency of union voters in Nevada, where Reid is up for
re-election in November. But there was little discussion. Reid and Emanuel are
exemplars of the just-get-it-done style of legislating. As it turned dark
outside, Reid began pulling senators aside in the lobby just off the Senate
floor, speaking in a strained whisper as he presented the case in
characteristically pragmatic terms. ''I'm with you; I'm for you,'' he told Tom
Harkin of Iowa, one of the Senate's traditional New Deal Democrats, who was
pained that the public option was dying.

''Harry has a very good way of sort of bringing you back to reality,'' Harkin
told me a few days later, ''this wonderful way of shrugging his shoulders and
saying: 'If you can get Lieberman to vote with you, fine. Otherwise, chalk it up
and move on.' He calmed me down.''

Reid looked disconsolate as he shuffled between the Senate chamber and his
office, festively decorated for Christmas and bustling with senators and White
House officials. The director of the White House Office of Health Reform,
Nancy-Ann DeParle, waved down Reid to solicit his view on what sounded like a
technical dispute about Medicare. Reid listened, nodding wearily, before
drifting out the door. ''You guys figure it out,'' he said.

Lieberman, who disputes that he misled Reid, was not the only senator in the
Democratic caucus complicating the majority leader's life. Ben Nelson of
Nebraska was opposing using government money to pay for coverage of abortions.
Maria Cantwell of Washington was balking as well; she had been drawing Reid into
long, late-night telephone conversations and pressing to make sure that
longshoremen were added to the list of unions exempted from a new tax on costly
health care plans. And Bernie Sanders, the independent senator from Vermont, was
threatening to bolt over the discarding of the public option, a provision that
senators like Lieberman refused to support.

A week later, Reid announced that he struck a deal with Nelson and had his 60th
vote. It was a display of legislative legerdemain that if not pretty -- Nelson's
vote cost a promise of millions of dollars to cover Nebraska's Medicaid expenses
-- was impressive. Reid needed 60 votes to head off a filibuster, and
Republicans were vowing to filibuster everything, which meant he needed all 58
Democrats and both independent senators. He had no margin for error. ''Harry has
the toughest job in Washington,'' President Obama told me one afternoon last
month, leaning forward in his chair in the Oval Office on the eve of the Senate
vote, talking up a legislator whom Obama first met during his four-year tour in
the Senate and whom he now sees as pivotal to his success as president. ''He's
done as good of a job as anybody could have done. He just grinds it out.''

The health care negotiations demonstrated Reid's command of the Senate and his
sway among his fellow Democrats -- which contrasts with his perhaps equally
remarkable inability to master other elements of the contemporary politician's
game. Despite Reid's quiet demeanor, he has an almost pathological propensity to
say things that get him in trouble. He is a model of indiscipline in a city that
feasts on the errant remark. In January, he would have to apologize to President
Obama after being quoted in a new book, ''Game Change,'' by John Heilemann and
Mark Halperin, as saying that Obama would be able to become the nation's first
black president because he was ''light skinned'' and had ''no Negro dialect,
unless he wanted to have one.'' Republicans promptly demanded Reid's resignation
as majority leader, seizing a chance to push Reid to the sidelines in their
ongoing effort to derail health care reform -- and most of the Obama agenda, for
that matter.

However the health care negotiations end -- with a Rose Garden signing or a
last-minute debacle -- Reid has another battle waiting at home: to save his seat
in the Senate. The longer Reid has served as Democratic leader, the more Nevada
voters have turned against him. It's a challenge that regularly confronts
politicians who have to balance the demands of being a Congressional leader with
maintaining political viability back home. For the third time in 16 years,
Republicans are trying to oust one of the top Democratic leaders in Congress, as
they successfully did with the previous Democratic leader in the Senate, Tom
Daschle of South Dakota, in 2004, and with Tom Foley in 1994, when he was the
speaker of the House. If Reid loses in November, it is hard to imagine either
party selecting a Senate or House leader from a competitive state like Nevada
anytime soon. ''He lives in a purple state, and he is pursuing a deep-blue
agenda, and that's tough,'' Lindsey Graham, a Republican senator from South
Carolina, told me. ''He's shown a lot of loyalty to his party and to his caucus
to his detriment, and I admire that.'' The cheers for Reid in Washington are not
being heard back home: a poll conducted this month on behalf of The Las Vegas
Review-Journal found that Nevadans disapproved of the health care bill. Reid's
attempt to slip in financing to offset Nevada's Medicaid costs, precisely the
kind of vote-getting sweetener that could display his worth to Nevadans, came
under fire in Washington, so he withdrew it rather than risk having the bill
unravel.

For all the power and the glamour -- the personal relationship with a president,
the corner office in the Capitol, the place in history -- it is hard to see why
anyone would want to be Harry Reid in today's Washington. Reid spent almost all
of last year as a partisan leader in a partisan battle, which is pretty much
what he should not be doing if he wants Nevadans to send him back for a fifth
term. He as much as anyone has the burden of delivering the president's agenda
through a Senate that has staggered to the brink of dysfunction. ''The
atmosphere in Washington has changed dramatically,'' Reid, who was elected to
the Senate in 1986, told me. By nature a pragmatic deal-cutter, Reid is viewed
with suspicion by the left, which cannot understand why he has to play ball with
the likes of Lieberman. He has to sate a long-deprived and lopsidedly
left-leaning Democratic conference hungry to pass big legislation, anxious that
its 60-vote margin will evaporate after November. ''When I took over in 2001, no
one had high expectations, so everything we did was appreciated,'' Daschle told
me last fall. ''Now there's an expectation that we can do everything.'' Reid
goes home to Nevada to face polls showing him trailing his opponents -- polls
that, if of questionable significance this early, are psychologically
debilitating. ''Numbers at this point are really meaningless, but they haunt you
and are an indication of the work you have to do,'' Daschle said.

That Reid is on the line in the midterm election of Obama's first term seems
appropriate. It was Obama who moved Nevada back into the Democratic column at a
time when Western states had become fertile ground for the party. But the
economic anxiety that has settled over the nation is perhaps nowhere more on
display than in Reid's home state, which has the highest mortgage-foreclosure
rate in the country and is tied with South Carolina and California for the
third-highest unemployment rate. Harry Reid has two different missions to
fulfill this year -- one for Barack Obama and one for Harry Reid. On many days,
the two seem very much at odds.

A SHARP WIND cut across the desert just before Thanksgiving as an S.U.V.
carrying Harry Reid navigated a grid of streets lined with trailers in
Searchlight, Nev., whose population of 798 includes the majority leader of the
United States Senate. ''This used to be a whorehouse!'' Reid exclaimed, pointing
a finger toward a ridge. ''That was one of the biggest whorehouses around. Right
there -- you see the fence over there? -- we put a nice new mobile home for my
mother after my dad killed himself.'' His father had worked in the gold mines
and was an alcoholic; that is one reason, Reid says, that he does not drink.
There was not a building (or a whorehouse) in sight as the S.U.V. rumbled over a
rise and came upon a scattering of tombstones, poking out of the sand and dirt.
Reid ordered the car to stop. The dropping sun threw a golden light over the
desert hills. Reid was wearing jeans and had pulled a jacket over a brown knit
sweater with brown leather elbow pads.''O.K., Searchlight Cemetery. It's been
here since 1900. Where else can you find a place like this?'' he said. ''It's
not owned by the county, not by no one. Whatever they want to put in here, they
put in here.'' He wandered among the tombstones in this common graveyard, grown
over and weathered by the years, pointing out a brother here, a
great-grandmother there. ''As Landra knows, I'm going to be buried here,'' he
said cheerfully, speaking of his wife. ''She knows she's going to be buried
here.''

By reputation and appearance, Reid, who is 70, is one of the blander elected
officials in Washington. Upon closer inspection, he is deeply and deceptively
interesting. He is a senator from Nevada who hates gambling (''The only people
who make money from gambling are the joints and government''); a backroom
deal-maker who does not drink alcohol or coffee; a Washington celebrity who
sniffs at the dinner-and-party circuit. ''Senator Daschle went to dinner almost
every night with someone,'' he told me. ''I go to dinner never, with anyone,
during the week.''He does find time, at least twice a week, to slip on a pair of
black Lycra stretch pants to do yoga with Landra at their apartment in the
Ritz-Carlton. He has an intolerance for fat people, manifested in asides to
aides who seem to be getting portly and an office staff that is suspiciously
slim. He was born out of wedlock. He is certainly one of the few members of the
Senate to have a Grateful Dead poster, signed by the band's members, hanging in
a bathroom at his house. Reid has been an amateur boxer, a Capitol police
officer and the chairman of the Nevada Gaming Commission, where his tenure
inspired death threats and a character in Martin Scorsese's ''Casino.'' The
mezuza on the right side of the doorway in Searchlight is a reminder that Landra
was Jewish before they converted to Mormonism.

Tall and slight, with rimless glasses and neatly trimmed gray hair framing a
gaunt face, Reid does not cut a memorable figure. ''I have beady eyes,'' he
volunteered to me one day. His voice barely rises above a mumble. ''I have
always been one who recognizes my, um, shortcomings,'' he said in November as he
sat with his wife in the living room of the house they built eight years ago on
a remote patch of land dotted with sagebrush and abandoned gold mines, 60 miles
south of Las Vegas. ''Landra and I had a conversation this morning; I hope she's
not upset about me telling you this, but she won't stay upset for long if she
is.'' Their discussion was about a story in The Washington Post that said, as he
put it: '' 'Reid gave a rousing speech, but as usual you could barely hear him.'
People are always telling me, writing, you know: 'He has bad posture. He doesn't
talk very well. He doesn't look very good.' ''

Reid is well known for his stumbles, the malapropism or the just plain weird
moment like when, in a spasm of confusion and fatigue, he initially voted
against his health care bill on Christmas Eve. When Reid goes before the TV
cameras -- which is something he does not enjoy doing and, as he would be the
first to tell you, is not particularly good at -- he often shows up with an
oversize index card with remarks, typed and vetted by his aides, in anticipation
of the sensitive question of the moment. He reads the card dutifully in a
monotone, eyes down, and then takes a few questions before his communications
director, Jim Manley, shouts out, ''Last question!''

Despite, or perhaps because of, these precautions, he takes a schoolboy's
delight in acting out, in breaking the rules of a scripted city. At one point
last year, Reid announced that the health care vote would be delayed until
January no matter what the White House thought, which led Rahm Emanuel to get
into his car and arrive at Reid's office, where after a terse exchange between
two terse men, Reid agreed to the White House's schedule. He referred to angry
protesters at town-hall meetings as ''evilmongers.'' He called George W. Bush a
''liar'' and a ''loser.'' When Bush invited Reid for coffee in the Oval Office
in the final weeks of his presidency, the president's dog walked in, and Reid
says he insulted the president's pet. ''Your dog is fat,'' he said.

''I'm just who I am, O.K.?'' Reid told me before Christmas. ''I didn't take
lessons on how to speak on television, and I don't spend a lot of time worrying
about who I am. I don't like to read stuff about me, but I've become accustomed
to it: you know, 'Reid misspeaks.' I'd rather people were saying, 'Oh, that guy
is a golden-tongued devil.' '' He paused. ''I have no regret over calling
Greenspan a political hack. Because he was. The things you heard me say about
George Bush? You never heard me apologize about any of them. Because he was.
What was I supposed to say? I called him a liar twice. Because he lied to me
twice.''

Manley, who was sitting nearby in Reid's Capitol office, raised his hand to
respectfully revise and extend the senator's remarks. ''Just for the record, he
did apologize for the loser comment,'' he said.

''Yes, but not because he wasn't a loser,'' Reid countered. ''It's because I
shouldn't have said that to a group of kids.'' It was a Las Vegas high-school
civics class. The apology also came after his most prominent Republican
supporter in Nevada, Sig Rogich, a Las Vegas businessman who was a senior
adviser to Ronald Reagan, called Reid to tell him that he had crossed a line.

The comments about Obama's race may be more problematic, particularly as Reid
confronts a bleak re-election landscape in Nevada. After Reid's aides were asked
by the politics editor of The Atlantic, Marc Ambinder, on Jan. 8, a Friday
night, about the passage in the book, they stayed up until 2:30 a.m., fashioning
a statement of apology and drawing up a list of 35 civil rights and political
leaders for Reid to call. Manley called Robert Gibbs, the White House press
secretary, to inform him of what was happening and set up a call between the
majority leader and the president. Reid telephoned Obama the next day. When
Obama asked if there was anything he could do, Reid asked if the president could
put out a statement of support. The White House obliged. (A few days later, a
Reid aide e-mailed to remind me that Reid has a towering portrait of Martin
Luther King Jr. in the foyer of his home in Searchlight.)

For someone whose life is about discretion -- almost without exception,
Democrats describe Reid as trustworthy, loyal and fierce about keeping a
confidence -- Reid can sound surprisingly candid when he discusses his
colleagues. He was unapologetic about his dust-up over delaying the health care
plan with Emanuel, who used to hold the No. 4 leadership post in the House.
''Rahm is a House-oriented guy,'' Reid said in November, as he settled,
unnoticed by the late-afternoon gamblers and diners, into a booth at the
Searchlight Nugget, a lost-in-time casino where coffee goes for 10 cents a cup.
''It's hard for him and most everyone in the White House to understand how the
Senate operates.''

He said he had been shocked by the behavior of Senator John McCain, the Arizona
Republican, since returning from his failed bid for the presidency. ''My
disappointment -- no, that's the wrong word; I'll try to find a better word. My
amazement has been John McCain. I thought he'd turn out to be a statesman, work
for things. He's against everything. He's against everything! He didn't used to
be against everything.''

He said he thought the White House erred in trying to win the support of Olympia
Snowe, the Republican senator from Maine, for a health care compromise. ''As I
look back it was a waste of time dealing with her,'' he said, ''because she had
no intention of ever working anything out.'' And while making clear that he was
not complaining, he said Obama may have been asking for too much in his first
year. ''I personally wish that Obama had a smaller agenda,'' he said. ''It would
be less work.''

Reid is so self-demeaning and ostensibly deferential that it is easy to overlook
his ambition. But it is there: in 2004, he started making calls to line up
support to replace Daschle as minority leader even before Daschle officially
conceded losing his Senate seat. Yet his comfort in the shadows is what has
helped him manage a sea of egos and agendas in a caucus that includes Bernie
Sanders on the left and Ben Nelson on the right. He said he skips the Sunday
talk shows because he is an anemic television presence, but that gesture, as
Reid surely knows, has earned him the loyalty of colleagues who relish the
platform, like Charles Schumer of New York and Richard Durbin of Illinois, two
members of his leadership team. ''He's not a showboater, and I think that
probably does help,'' says David Axelrod, a senior adviser to Obama. But, he
added, it ''probably doesn't help him at home.''

Committee heads, the Senate's traditional barons who lost some power in the past
two decades, have found themselves newly ascendant under Reid. ''Other leaders
who I've worked with and respect, like Tom Daschle, would say, 'We need a task
force; we need to bring in people who aren't chairs,' '' Durbin says. ''Harry is
very respectful to committee chairs, and they are loyal to him as a result of
that.'' Reid is a transactional politician, unashamed to dole out an earmark to
win a vote, a bit of sausage-making that draws him condemnation in editorials.
There are days, aides say, when they almost feel like concierges as Reid --
hearing that a fellow senator is going to Las Vegas, and seeing a way to build
good will and promote his home state -- asks them to line up restaurant
reservations, choice hotel rooms and hard-to-get show tickets. By contrast, ''I
have never heard him threaten a member,'' Durbin says. Reid said he disdains the
tough-guy style of one of his more famous predecessors, Lyndon Johnson. ''I
don't think he'd last for five minutes today,'' Reid said. ''You can't bully.''
Besides, he added: ''I couldn't be Lyndon Johnson if I wanted to be. He was too
crude and physical for me.''

For the White House, Reid is a gift in a challenging year. Conciliatory,
endlessly patient and pragmatic rather than dogmatic, he has different skills
from those of some senators who might otherwise be in his spot -- like Schumer
or Durbin -- and he seems suited to this time and this caucus. Nor is he
perceived as carrying an ideological agenda. Unlike most of his colleagues, he
opposes gun control and abortion, but those views are reflected only when he
votes and do not color the way he manages or negotiates a bill. ''Harry by any
normal criteria would be considered a moderate Democrat,'' Obama told me. ''He's
someone who doesn't think in big ideological terms.''

Reid's legislative skills were on display throughout the months that led to the
Christmas Eve vote. By suddenly inserting a public-insurance option into his
bill -- catching the White House and some of his colleagues by surprise -- he
demonstrated to supporters of the public option that they did not have the votes
for it. He got credit from Democrats like Harkin for doing what he could to try
to pass it. Lieberman took the blame for killing it. And Reid got his 60 votes.

THE NOTION that Reid could lose in November seems ludicrous to Sig Rogich. ''Why
would we ever want to get rid of the majority leader, arguably the most powerful
man in Congress, at a time when Nevada needs him at the table?'' Rogich said,
sitting in his Las Vegas office with a view of four casinos: the MGM Grand,
Bally's, the Paris and the Bellagio. Reid is surely the most influential senator
Nevada has sent to Washington since it became a state 146 years ago. He has
delivered millions of dollars in aid and almost single-handedly blocked the
federal government from turning Yucca Mountain, 90 miles northwest of Vegas,
into a national nuclear-waste repository.

But of course he could lose. Independents -- whose numbers in Nevada have grown
by 25 percent since Reid was last elected -- are put off by the partisan
politics that are, by definition, personified by a majority leader. Yet no
matter how much he has given himself to the service of Obama's agenda, Reid is
not partisan enough for many liberals. And then there are conservative voters,
who certainly appear motivated to come out to vote against what they see as an
out-of-control big-government agenda advanced by Obama and Reid. ''You have a
candidate who hasn't been the most popular elected official in the state, who is
pushing an agenda that is completely at odds with the constituents in his state,
and you have an economic environment that is the worst the state's ever faced,''
says Danny Tarkanian, one of a half-dozen Republicans lining up for the chance
to oppose Reid. He is the son of Jerry Tarkanian, the former University of
Nevada, Las Vegas, basketball coach, as big of a celebrity as you can find in
Nevada.

Polls that show Reid trailing potential opponents may be of little meaning 10
months before an election -- before he has an opponent, much less unleashes the
campaign he is preparing. ''Right now he's shadowboxing against himself,''
Emanuel told me. That said, it is significant that 52 percent of Nevadans had an
unfavorable view of Reid in a poll published by The Las Vegas Review-Journal
this month, before the latest controversy broke; and it's even more significant
that that view did not change despite months of sunny Reid campaign
advertisements on Nevada television stations. Nate Silver, the political blogger
and polling expert, compared Reid's position in the polls with that of Gov. Jon
Corzine of New Jersey last fall; Corzine lost with only 45 percent of the vote.
What's more, nearly one-third of Nevadans eligible to vote in 2010 have
registered since 2004, the last time Reid was on the ballot. The electorate he
will face this November is entirely different from the one that sent him to the
Senate in 1986, a consequence of representing a state that is growing so fast.

''He is vulnerable because he is living in a center-right state and he's lost
touch with the people who put him there,'' says Sue Lowden, another prospective
Republican challenger, a former television news anchor in Las Vegas who was the
No. 2 runner-up for Miss America in 1973 (she was Miss New Jersey). Reid has
been in public life in Nevada since he joined the State Assembly in 1968. ''I
think there's Reid fatigue,'' Jon Ralston, the political columnist for The Las
Vegas Sun, told me. As if that were not enough, there could be two Reids on the
ballot this fall: his son, Rory, is running for governor. No one in the Harry
Reid camp is happy about this; Harry Reid's voice dropped to an even more
imperceptible level than usual when I inquired if he asked Rory to sit this one
out. ''Well, I'll talk about anything except my son,'' he said. ''I'm not going
to do that. He's been a wonderful son, great example to his brothers and sister.
He didn't ask my permission to run, and I didn't think he needed to get it.''

Reid has won and lost tight elections, and he has been arming himself for this
one. He hired a campaign manager a year ago; he already has 14 people on his
campaign payroll; and he gave a strategically timed interview last year to
announce that he would spend $25 million to keep his seat, a staggering and
utterly credible figure that helped finance those early advertisements. His
staff has scoured the business and voting records of all of his potential
challengers.

And he has done what he could to choose his opponent. Jon Porter, a three-term
member of Congress, was considered one of Reid's toughest potential foes --
until he was defeated in 2008 by a Democrat, Dina Titus, who benefited from the
largess of Reid's political operation. Last August, Dean Heller, a Republican
member of Congress and another potentially strong challenger, called Reid to
tell him that he would be sitting this one out; Nevada Democrats suspect Heller
reconsidered the wisdom of abandoning a safe seat to challenge an incumbent with
$25 million in the bank. That leaves a bench of second-tier candidates like
Tarkanian and Lowden, who have far less campaign experience, money and
institutional backing than Reid.That might be the single-best thing going for
him right now.

African-Americans are expected to make up just 5 percent of the Nevada
electorate come November, and the list of people Reid called after President
Obama included some of the top black officials in the state, who quickly
declared their allegiance to the Senate majority leader. Even so, Reid's
comments about Obama do seem likely to feed the enough-already atmosphere that
is hurting Reid back home.

Still, the bigger and more meaningful test will be the fallout from the bill
that has consumed Reid this winter. Obama said he understood why Nevadans are
opposed to the health care bill now, but he says that will change. ''Well, if
you've got insurance companies spending hundreds of millions of dollars scaring
the daylights out of people into thinking that somehow this is a government
takeover of health care, that it's unpaid for, that it means huge new taxes on
them, that it's going to mean higher premiums -- if that's the information
you're getting, shoot, I'd be against it, too,'' the president told me. ''Once
this thing is passed and signed, then suddenly The New York Times and other
newspapers are going to have a big article saying what does this mean for you,
and people will take a look at it and say, 'You know what, this is a lot better
deal than I thought.' And I think that will serve Harry very well.''

Reid better hope so. Two days before Christmas, he sat stiffly in a
straight-back red leather chair, the fireplace behind him casting a glow over
his office on a cold winter morning. From here, Reid could gaze down the
National Mall, white with snow, the Washington Monument on the horizon. That
day, at least, had been a good day for the senator from Nevada. He had his 60
votes, the reviews were in and Reid, self-effacing or not, was eager to share
them with me. He rose from his chair, walked over to his desk and picked up a
clip from Politico, a collection of laudatory quotations highlighted in yellow
by Reid's staff. ''Every one of these -- look, Ross Baker, a professor at
Rutgers -- they all say the same thing,'' he said. ''I'm not tall, dark and
handsome. But here's what it says: 'Behind the scenes, his genius really comes
to the floor. He's a virtuoso, a legislative technician. Whatever else can be
said of health care reform, it's Reid's finest hour.' '' This need for
validation was startling, if understandable. Health care will almost certainly
be the toughest legislative challenge Reid will ever face. But after this, there
are more assignments from Obama in the year ahead, potentially on contentious
issues like immigration and climate change. And they are again going to force
Reid to balance the demands of his president with his own campaign. ''The caucus
has great loyalty to Harry,'' Obama told me. ''So I want Harry there for the
rest of my first term.''

Reid has the fortune of carrying out what some see as the most ambitious
legislative agenda that a president has handed to Congress in a generation. And
that could be his undoing.

URL: http://www.nytimes.com

LOAD-DATE: January 27, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: PHOTO (MM27)
 Reid eschews the limelight but is a pragmatic backroom deal-maker. (PHOTOGRAPHS
BY MICHELE ASSELIN) (MM31)

PUBLICATION-TYPE: Newspaper


