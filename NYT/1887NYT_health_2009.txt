                   Copyright 2010 The New York Times Company


                             1887 of 2021 DOCUMENTS


                               The New York Times

                            April 14, 2010 Wednesday
                              Late Edition - Final

Odd Couple, Fighting Diabetes

BYLINE: By REED ABELSON

SECTION: Section B; Column 0; Business/Financial Desk; Pg. 1

LENGTH: 1283 words


This could be one glimpse of the future of health insurance.

The UnitedHealth Group, one of the nation's largest health insurers, is teaming
up with the Y.M.C.A. and retail pharmacies to try a new approach to one of the
nation's most serious and expensive medical problems: Type 2 diabetes.

Rather than simply continuing to pay ever-higher medical claims to care for its
diabetic customers, UnitedHealth is paying the Y.M.C.A. and pharmacists to keep
people healthier. The result, they hope, will be lower costs and lower premiums
for everyone.

The insurer will announce on Wednesday that it will work with Y ''lifestyle
coaches'' in seven  cities to help people who are at risk for diabetes lower
their odds of developing the disease by losing just a modest amount of weight.
The Y already offers a program that has had success in clinical tests of such
efforts, in 16-week programs that help people learn to eat better and exercise.

The announcement is planned for a conference on diabetes being held this week in
Kansas City, Mo., by the federal Centers for Disease Control and Prevention,
which will also discuss  a complementary effort by the C.D.C. to finance the Y
programs in 10 additional locations around the country. The C.D.C. locations
have not yet been chosen.

The UnitedHealth effort is an example of the new role health insurers may take
on after enactment of the federal health care law, as insurance companies are
forced to cover people regardless of their medical condition. It is also a
response to a growing demand by employers that insurers do more to manage health
costs, beyond collecting premiums and paying doctors.

An estimated 25 million people in this country have Type 2 diabetes, the most
common form of the disease, which generally develops in adults as a result of
obesity and poor diet. An additional 60 million people -- one in four adults --
are considered pre-diabetic and at high risk for developing the disease.

By one estimate, the cost of treating both diabetes and pre-diabetes exceeds
$200 billion a year.

''This is an enormous problem,'' said Dr. Ann Albright, who is overseeing the
C.D.C.'s diabetes efforts. ''Our data tells us it's getting worse.''

In a related effort aimed at people who are already diabetic, UnitedHealth plans
to pay Walgreens pharmacists in the same seven cities to help teach people to
better manage their conditions. The insurer says it hopes to expand the program
to other pharmacy chains interested in providing education and counseling to
diabetic individuals.

UnitedHealth plans to introduce the program in Cincinnati, Columbus and Dayton,
Ohio, and in Indianapolis, Minneapolis, Phoenix and Tucson. It will be available
at no cost to participants, and will be open to adults who are enrolled in
health plans the company offers through employers.

People eligible for the program in several of those cities could include some
employees of  General Electric, which has 304,000 workers  worldwide. The
company praised the program for looking beyond the 15 minutes that a patient
typically spends with a doctor during an office visit, discussing a drug or
treatment.

Instead of focusing on paying for health care, said Dr. Robert Galvin, G.E.'s
chief medical officer, ''this is very emblematic of thinking about health.''

UnitedHealth expects eventually to offer the program through other insurers and
to people enrolled in its private Medicare and Medicaid plans. One insurer,
Medica, a large Minneapolis health plan, has already signed on for its 600,000
members in Minnesota.

UnitedHealth said it was investing tens of millions of dollars in this
initiative, helping the Y develop an online curriculum and collect the data
necessary to track the progress of the people  enrolled in the program. The
company says it will use its own data about the health of its members to
identify people who are pre-diabetic and may not know  it -- and  then tell them
about the program.

''This will absolutely pay for itself,'' said Dr. Deneen Vojta, a senior
executive at UnitedHealth.

Both UnitedHealth and the C.D.C. want to expand on the success of a clinically
proved program that has been  offered by the Y.  Based on  evidence drawn from
that program, people who are pre-diabetic and lose just 5 percent of their body
weight can reduce their chances of developing the disease by almost 60 percent.
The C.D.C. is also considering ways to encourage organizations beside the Y to
develop similar programs.

In the Y programs, UnitedHealth will pay on the basis of how many people
participate as well as how much weight the people in the programs lose. After
undergoing a more intensive 16-week course, individuals will participate in a
yearlong maintenance course.

''We have the experience,'' said Neil Nicoll, the chief executive of the
Y.M.C.A. of the USA, a federation of 2,700 Ys across the country whose goal is
to develop more programs related to chronic disease like diabetes. ''We've
proven the model. We've proven that we can do it at substantially less cost.''

For Marilyn Schenetzke, a retired 67-year-old who lives in Carmel, a suburb of
Indianapolis, the Y course succeeded where other weight-loss programs had
failed. At 5-foot-6, Ms. Schenetzke weighed 227 pounds. She had developed
gestational diabetes, a temporary condition, when she was pregnant. And her
grandfather was diabetic. In reading about diabetes, she realized she was at
risk for developing the Type 2 version.

And yet, she said, ''my doctors had never said one word about it.''

Last November, she began attending the weekly one-hour classes at the Y. The
coach focused on helping the class learn to make better choices about what they
ate and to devote  at least two-and-a-half hours a week to exercise.

''I lost 49 pounds,'' said Ms. Schenetzke. As she attended the classes, she
found herself changing her diet and incorporating a walk or a workout on a
stationary bike into her routine. She has stuck with those habits.

Unlike other programs, where she quickly regained whatever weight she had lost,
this represented a permanent change, she said. ''I thought from the beginning
that it would work.''

The Y's involvement will be helpful in persuading employees of the potential
value of this program, said David Parmly, who oversees health benefits for Pilot
Travel Centers, a Knoxville, Tenn., company that operates rest stops across the
country.

''Unlike conventional gyms, the Y has and always has had a message of health,''
he said. ''They don't push personal trainers on you. They don't seem to be
hawking products.''

Some policy experts say the UnitedHealth program is an example of the steps that
health insurance companies must take to demonstrate their relevance under the
new health care law and as employers pay more attention to holding  down medical
costs.

''Their business is going to be managing care in a highly effective and
cost-effective way,'' said Helen Darling, the president of the National Business
Group on Health, an association of employers that provide health benefits to
their workers. ''You're not just paying claims.''

Ms. Darling cautioned that UnitedHealth's  model must still be shown to work on
a national scale. But given the diabetes program's broad scope, she predicted
that UnitedHealth would be able  to see fairly quickly what worked and what did
not, and to fine-tune any differences in the approaches that may be necessary in
places as varied as Tucson and Minneapolis.

And unlike some of the federally mandated experiments called for in the new
health care law, Ms. Darling said, the results will be quickly apparent.
''Wedon't have to wait five years.''

URL: http://www.nytimes.com

LOAD-DATE: April 14, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Marilyn Schenetzke, who lives outside Indianapolis, lost 49
pounds in a Y program that intends to cut the risk of diabetes. (PHOTOGRAPH BY
TOM STRATTMAN FOR THE NEW YORK TIMES) (B7)

PUBLICATION-TYPE: Newspaper


