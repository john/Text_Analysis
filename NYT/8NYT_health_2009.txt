                   Copyright 2009 The New York Times Company


                              8 of 2021 DOCUMENTS


                               The New York Times

                             June 3, 2009 Wednesday
                              Late Edition - Final

Bill Proposes Immigration Rights for Gay Couples

BYLINE: By JULIA PRESTON

SECTION: Section A; Column 0; National Desk; Pg. 19

LENGTH: 1107 words


Senator Patrick J. Leahy, the Democrat from Vermont who is the powerful chairman
of the Judiciary Committee, is adding another controversial ingredient to the
volatile mix of an immigration debate that President Obama has said he hopes to
spur in Congress before the end of the year.

Mr. Leahy has offered a bill that would allow American citizens and legal
immigrants to seek residency in the United States for their same-sex partners,
just as spouses now petition for foreign-born husbands and wives.

The senator has said the bill should be part of any broad immigration
legislation that Congress considers. To highlight his initiative, known as the
Uniting American Families Act, Mr. Leahy is holding a hearing on Wednesday to
discuss it in the full Judiciary Committee, bypassing the usual subcommittee
hearings.

Also this week, immigrant advocacy groups and labor organizations are opening a
nationwide campaign to hold President Obama to his recent pledge to initiate a
Congressional debate on immigration legislation later this year.

Small-scale rallies took place on Monday in Los Angeles and some 40 other
locations, and immigration groups are converging on Washington on Wednesday for
three days of strategy meetings.

The Obama administration, juggling an array of huge and pressing issues on the
economy and health care reform, has encouraged the mobilization of immigration
advocates, especially Latino groups, while avoiding any legislative battles for
now on the prickly topic of immigration. President Obama has invited a
bipartisan group of lawmakers to the White House next Monday to ''launch a
policy conversation'' about immigration, an administration official said.

The president wants to ''identify areas of agreement, and areas where we still
have work to do,'' said the official, who would only speak on background because
the final plans for the meeting were not settled.

The most contentious part of the immigration legislation that the administration
supports, which is known as comprehensive immigration reform, is a program to
give legal status to more than 11 million illegal immigrants living in the
country. But current proposals also include a variety of measures intended, like
Senator Leahy's, to expand or streamline the legal immigration system.

Mr. Leahy's proposal for same-sex immigration benefits was not included in the
immigration legislation that the Bush administration brought forward in 2007,
which failed after a firestorm of opposition, mainly from Republican voters.

Groups backing the overhaul this year have cobbled together a wide-ranging but
fragile coalition that includes Latino and black groups, Roman Catholic and
evangelical Christian churches, farm workers and commercial farmers, and some
employer groups. In contrast to 2007, organized labor is united this time around
in supporting the overhaul.

The political fault lines opened by Senator Leahy's same-sex bill quickly became
apparent this week. In a letter sent Tuesday, Bishop John C. Wester of Salt Lake
City, the chairman of the Catholic bishops' Committee on Migration, wrote that
the Uniting American Families Act would ''erode the institution of marriage and
family,'' by taking a position ''that is contrary to the very nature of marriage
which pre-dates the Church and the State.''

Bishop Wester addressed his letter to Representative Michael M. Honda, a
California Democrat who has said he will introduce an immigration bill
containing similar same-sex provisions in the House this week.

J. Kevin Appleby, the immigration policy director for the bishops' conference,
said, ''The last thing the national immigration debate needs is another
politically divisive issue added to the mix.''

But Senator Leahy said the bill would eliminate discrimination in immigration
law against gay and lesbian couples.

Under family unification provisions in immigration law, American citizens and
legal residents can petition for residency for their spouses. There is no
numerical limit on permanent residence visas, known as green cards, for spouses
of American citizens, and this is one of the main channels for legal immigration
to the United States. Same-sex couples, though, cannot petition for partners,
and many face the prospect of an immigrant partner's deportation.

Senator Leahy's bill would add the term ''permanent partner'' to sections of
current immigration law that refer to married couples, and would provide a legal
definition of those terms.

''I just think it's a matter of fairness,'' he said Tuesday in an interview,
noting that a number of American allies, including Canada, France and Germany,
recognize same-sex couples in immigration law.

The Judiciary Committee is to hear testimony on Wednesday from Shirley Tan, 43,
the mother of twin 12-year-old boys who are United States citizens because they
were born here. Ms. Tan has raised them with her partner of 20 years, Jay
Mercado, who like Ms. Tan is from the Philippines. Although Ms. Mercado became a
naturalized American citizen in 1998, she has not been able to gain legal
immigration status for Ms. Tan.

Ms. Tan said she came to this country fleeing a cousin who was released from
prison in the Philippines after he served 10 years for the murders of her mother
and her sister. Ms. Tan said she had been severely injured in the 1979 attack by
the cousin.

She applied for political asylum in the United States, she said, but did not
receive notice when it was denied years later. She remained here with a
provisional legal status until last Jan. 28, when federal immigration agents
carrying a deportation order came to the home she shares with Ms. Mercado, 48,
in Pacifica, Calif.

Since her arrest, Ms. Tan has been able to remain legally in the country because
of a private bill introduced on her behalf by Senator Dianne Feinstein, Democrat
of California.

Ms. Tan said she feared returning to live in the Philippines, in part because of
concern that she and Ms. Mercado would face anti-gay discrimination there.

''People are cruel,'' she said. ''I don't know if I can expose my boys to
narrow-minded people.''

Opponents of the Leahy bill argue that it would foster immigration fraud because
it would be difficult for immigration officers to determine whether same-sex
couples had an established relationship.

Supporters said the bill would assist about 36,000 same-sex couples nationwide.
Rachel B. Tiven, the executive director of Immigration Equality, a group that
advocates for gay rights legislation, said the bill had improved chances this
year because of recent same-sex marriage victories in Iowa, Maine and Vermont.

URL: http://www.nytimes.com

LOAD-DATE: June 3, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Shirley Tan, with her 12-year-old twin sons, Jashley, left, and
Joriene Mercado. Ms. Tan, a Filipino, has been unable to gain legal residence,
while her partner, who is also a woman, has.(PHOTOGRAPH BY ANDREW COUNCILL FOR
THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


