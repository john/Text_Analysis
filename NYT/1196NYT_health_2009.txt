                   Copyright 2009 The New York Times Company


                             1196 of 2021 DOCUMENTS


                               The New York Times

                           December 17, 2009 Thursday
                         The New York Times on the Web

Massachusetts Antismoking Plan Gets Attention

BYLINE: By ABBY GOODNOUGH

SECTION: Section ; Column 0; National Desk; Pg.

LENGTH: 970 words

DATELINE: BOSTON


When Massachusetts began offering virtually free treatments to help poor
residents of the state stop smoking in 2006, proponents hoped the new Medicaid
program would someday reap benefits.

But state officials never expected it would happen so soon.

New state data show a steep drop in the smoking rate among poor people. When the
program started, about 38 percent of poor Massachusetts residents smoked. By
2008, the smoking rate for poor residents had dropped to about 28 percent, a
decrease of about 30,000 people in two and a half years, or one in six smokers,
said Lois Keithly, director of the state's Tobacco Cessation and Prevention
Program.

There are also indications that the drop has lowered rates of hospitalization
for heart attacks and emergency room visits for asthma attacks, she said.

The data has not yet been peer-reviewed. But the numbers have already grabbed
national attention, with several United States senators and antismoking
advocates using the data to push for similar new Medicaid coverage for tobacco
addiction in the national health care legislation.

Senators Richard J. Durbin, Democrat of Illinois, and Bernard Sanders,
independent of Vermont, have introduced an amendment that would do so, and the
Senate could vote on it by the weekend.

If the amendment fails, Senator Tom Harkin, Democrat of Iowa, said he would try
another avenue: seeking an expansion through a conference committee that will
ultimately reconcile the House and Senate bills.

''We should be able to find an opening,'' Mr. Harkin said in an interview.
''This is one demonstrable way we can actually bend the cost curve and keep
people healthy.''

The Massachusetts program, part of the state's landmark universal health care
law, covers almost the entire cost of counseling and prescription drugs for
Medicaid enrollees trying to quit smoking. Most states do not provide nearly as
much help for poor smokers.

The rate of smoking among Medicaid patients had not changed in a decade before
the program was introduced. It was much higher than the rate in the general
population, which stands at about 18 percent.

Under the current Senate health care bill, only pregnant women on Medicaid would
qualify for comprehensive smoking-cessation treatment, including drugs and
counseling.

The bill passed by the House last month would provide broader Medicaid coverage
of such treatment, and antismoking groups are lobbying the Senate to adopt that
approach.

Under the Massachusetts program, Medicaid recipients from age 18 to 64 are
eligible for 180 days of antismoking drugs, including Chantix and bupropion, and
16 counseling sessions per year. Co-payments do not exceed $3, Ms. Keithly said.
The state spent $11 million on the program in its first two years, she said.

Insurance plans for higher-income residents provide less coverage for
smoking-cessation treatment, Ms. Keithly said, or none at all.

''This is really the gold standard,'' she said.

Dr. Michael Siegel, a professor at the Boston University  School of Public
Health, said the main finding -- a significant drop in the smoking rate of
Medicaid recipients -- was ''very compelling.''

State researchers also looked at the smoking rate for uninsured residents, who
also tend to be poor, over the same period and found it did not change.

''That's something you need to see,'' Dr. Siegel said, ''because if they had
just found that the rate among Medicaid recipients dropped, it might be
coincidental or dropping among everyone.''

The researchers found that smokers who took advantage of the cessation program
had much lower rates of hospitalization for heart attacks and emergency room
visits for asthma attacks than before starting the treatment.

But Dr. Siegel and other researchers said those findings were not as persuasive
because the state had not used a control group.

''It's intriguing but still quite preliminary,'' said Dr. Nancy A. Rigotti,
director of the Tobacco Research and Treatment Center at Massachusetts General
Hospital.

Still, Dr. Rigotti said, Congress should take the Massachusetts data seriously
as it debates health care reform.

She said that pregnant women -- the only Medicaid recipients who would receive
broad smoking-cessation benefits under the Senate plan -- are often advised to
avoid stop-smoking drugs.

Senator Harkin said that even though the Congressional Budget Office has
predicted that covering smoking-cessation treatment for pregnant women alone
would save $100 million in health care costs over 10 years, concern about
upfront costs has made many senators reluctant to expand the benefits to all
Medicaid recipients.

''It's very frustrating,'' he said.

According to the American Lung Association, Massachusetts is among only six
states that offer extensive stop-smoking benefits for Medicaid recipients. The
others are Indiana, Minnesota, Nevada, Oregon and Pennsylvania.

Massachusetts is the only one of those states that has reported a significant
drop in the smoking rate for Medicaid patients; several experts said the others
might not have promoted their programs as aggressively or kept track of results.

Terry F. Pechacek, associate director for science for the Office on Smoking and
Health at the Centers for Disease Control in Atlanta, said he had reviewed the
Massachusetts data and found the numbers ''shocking'' given that smoking rates
around the nation have barely budged since 2004.

About 20.6 percent of Americans were smokers in 2008, the C.D.C. reported last
month, down from 20.9 percent in 2004.

Smoking-related illnesses cost the Medicaid system more than $22 billion a year,
Dr. Pechacek said -- about 11 percent of overall Medicaid expenditures.

''The fact is we need to expand it to everyone,'' he said of the type of
coverage offered in Massachusetts.

URL: http://www.nytimes.com

LOAD-DATE: December 17, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


