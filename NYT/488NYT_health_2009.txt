                   Copyright 2009 The New York Times Company


                             488 of 2021 DOCUMENTS


                               The New York Times

                            September 4, 2009 Friday
                              Late Edition - Final

Culture Fans, It's Not Too Late For Summer Fun

BYLINE: By MIKE HALE

SECTION: Section C; Column 0; Movies, Performing Arts/Weekend Desk; Pg. 1

LENGTH: 191 words


EVEN we have to admit: It hasn't been a very entertaining summer.

There have been successful movies, but none  -- whether Hollywood blockbuster or
art-house upstart  --  have managed to remain in the national conversation
beyond their opening weekends. Nothing onstage has equaled the drama of the
health care debate or the Henry Louis Gates Jr.  arrest. The biggest news in
music was the death of Michael Jackson; the biggest events on television  --
with apologies to ''Mad Men''  --  were the funerals of Mr. Jackson and Senator
Edward M. Kennedy.

And when we weren't talking about all that bad news, we were talking about the
rain.

But art is always happening, whether we notice or not. As the long wet summer of
2009 winds down, critics and reporters of The New York Times have some
suggestions for offerings that you might have been too distracted to appreciate
but can still get to before the weather takes its final turn. From Shakespeare
under the stars to classical music near Woodstock, N.Y.,  from a serious movie
to a seriously silly television series, on Pages 2 and 3 are ways to make the
Labor Day weekend entertaining. MIKE HALE

URL: http://www.nytimes.com

LOAD-DATE: September 4, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY DANIEL PELAVIN)

PUBLICATION-TYPE: Newspaper


