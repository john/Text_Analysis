                   Copyright 2010 The New York Times Company


                             1496 of 2021 DOCUMENTS


                               The New York Times

                            February 12, 2010 Friday
                              Late Edition - Final

G.E. Aims to Put a Human Face on Its Role in Health Care

BYLINE: By STEPHANIE CLIFFORD

SECTION: Section B; Column 0; Business/Financial Desk; ADVERTISING; Pg. 3

LENGTH: 1044 words


GENERAL ELECTRIC, for one, still believes in advertising.

As the Olympics begin, the company is introducing its biggest campaign ever
aimed at consumers. Called Healthymagination, it publicizes G.E.'s role in the
world of doctors and hospitals. In the United States alone, G.E. expects to
spend more than $80 million this year on the campaign.

Its role in health care is technical: G.E. makes and sells medical devices, like
machines that measure bone density and perform M.R.I. scans. But the advertising
focuses on the personal. ''In the past, we have always shown the hardware, and
that's great -- it works on one level -- but we wanted to make a point here that
this was about better health for more people,'' said Don Schneider, executive
creative director for BBDO New York, the Omnicom Group agency that created most
of the ads.

Beth Comstock, senior vice president and chief marketing officer at G.E., said
that the company was trying to make health care more accessible -- both in terms
of consumers understanding it and in terms of providing cheaper, more widely
available devices. (By 2015, G.E. has said, it will invest $6 billion in
research and development for health care products and technology.)

''This is a way to start to build awareness for G.E. in this space,'' Ms.
Comstock said.

Ms. Comstock said that the health care debate in Washington ''didn't really
affect the timing,'' although it had renewed peoples' interest in the topic.

But Timothy Calkins, clinical professor of marketing at Kellogg School of
Management at Northwestern University, said he suspected otherwise.

''A lot of health care companies have really gotten kicked around a bit in the
recent health care debates,'' Mr. Calkins said. ''Sometimes it's easy for
politicians to go after the pharmaceutical industry and the medical device
industry and say that they're the cause of the problem, so they're a fairly easy
target. It's important to get out there and be a part of the debate.''

The campaign begins Friday, with consumer-focused efforts ranging from Olympic
television advertising to sponsored toolbars on the Web site Digg.com.

Although G.E. has traditionally used more business-oriented advertising media,
the goal with this campaign was not necessarily to get viewers to buy or use
G.E. devices, but to emphasize its broader role in health care. Mainstream media
was a better fit for that, said Judy Hu, global executive director for
advertising and branding at G.E.

The television spots, created by BBDO, are big productions. ''Stadium'' shows
doctors running onto a football field like star players, as the crowd goes wild.
For that, BBDO rented Candlestick Park in San Francisco and hired a local
marching band. ''Take a Look'' shows doctors performing checkups in different
eras. It opens with a Bronze Age setting where the people are cloaked in fur
capes, then goes to ancient China, the Middle Ages in Europe, Depression-era
America, and finally a modern hospital where the doctor is using a handheld G.E.
imaging device to examine a boy's abdomen.

''It does look like a big production, and it is a big production, but we're very
conscious with costs,'' Ms. Hu said, noting that digital backgrounds were used
in ''Take a Look'' and for the stadium crowd in ''Stadium.''

And, Ms. Comstock said, big TV spots were expected at something like the
Olympics. ''You have a big stage,'' she said. ''It deserves big storytelling.''

Though consumer-created spots were among the most popular during the Super Bowl,
G.E.'s polished approach made sense, said Kevin Lane Keller, professor of
marketing at the Tuck School of Business at Dartmouth.

''Given what they're trying to accomplish with their corporate image,'' he said,
''having an ad that has that kind of quality level, and helps them to tell their
story -- and does it in a visually arresting manner -- helps them in an Olympic
setting to break through,'' he said.

Less expensive Web efforts balance the sleek TV ads, for a total of almost 60
videos.

Howcast, which makes how-to Web videos, will produce G.E.-sponsored segments
that will appear on its site, on YouTube and on blogs, with subjects like ''How
to party your way to better health.'' G.E. will also introduce an application
for the iPhone and for Android-powered smartphones to view Howcast health
videos, and an app called ''Morsel'' that gives daily health tips. And selected
YouTube stars, like Alphacat and iJustine will make Howcast health-related
videos based on their visitors' suggestions. ''They're pushing the edge for
G.E., but I think to reach this audience, it'll be interesting to see how far we
can go,'' Ms. Hu said.

Other elements of the digital campaign largely focus on ''the 'health spreading
contagiously' idea,'' Mr. Schneider said, referring to the digital sharing of
health-related ideas.

For health articles on sites like Digg.com and Healthline.com, G.E. will sponsor
the toolbars that allow visitors to e-mail the stories or post them to Twitter
or Facebook. The toolbars will be surrounded by a light-blue graphic with G.E.'s
logo and the text ''Share this healthy idea.''

The Healthymagination Web site will show the top health articles being shared,
along with news, articles and statistics on topics like managing chronic
disease.  Big Spaceship, a Brooklyn-based agency, BBDO and other agencies worked
on digital elements.

The print and billboard component, created by BBDO, shows simple messages, like
''Healthymagination is saving billions in health care costs,'' over photographs
of people's faces.

It has struck deals with Meredith, the publisher of magazines like Parents, to
create special Web sites about health. And G.E. will sponsor health research
from The Economist, along with a special section of The Financial Times.

For the Olympics, G.E. is running 403 billboards and signs in Vancouver,
including on streetcars and in transit stations and the airport. Those show
see-through images of athlete's bodies that G.E. technology might produce, and
carry lines like ''The Olympic Games, in much greater definition.''

''It's this idea of simplifying complexity, but with a sense of optimism,'' Ms.
Comstock said. ''It's a way to bring to life that technology can be an answer.''

URL: http://www.nytimes.com

LOAD-DATE: February 12, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: TV ads focus on doctors through the ages, above left, and
doctors hailed as ''true heroes'' in a stadium.

PUBLICATION-TYPE: Newspaper


