                   Copyright 2009 The New York Times Company


                             1123 of 2021 DOCUMENTS


                               The New York Times

                            December 4, 2009 Friday
                              Late Edition - Final

Two Issues Hold Key In Senate Health Votes

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 21

LENGTH: 278 words


No matter what legislators are fighting about on the Senate floor, two issues in
the health care debate will be key to the final outcome:  insurance coverage for
abortion and a proposed government-run health insurance plan, or public option.

Neither issue  has figured greatly in the Senate floor debate so far, behind the
scenes in the Capitol they are the issues that senators, particularly Democrats,
are talking about more than any others.

To get the health care bill approved, the Senate majority leader, Harry Reid of
Nevada, will need to broker compromises on each issue that can win the backing
of 60 senators.

Senator Ben Nelson, Democrat of Nebraska and an opponent of abortion rights, is
readying an amendment that could be the first big test on how abortion is to be
handled. Mr. Nelson, above,  favors language, similar to that in the bill passed
by the House, that would bar coverage of abortion by any insurance plan bought
even partly with federal subsidies.

His amendment is expected to be on the floor early next week, and Mr. Nelson has
warned that he would oppose the bill if the final language did not meet his
approval.

Liberal senators have heavily focused so far  on the public option, which they
say is needed to compete with private insurers. But it is clear they will have
to compromise. On Thursday, Senator Thomas R. Carper, a centrist Democrat from
Delaware, met with colleagues to pitch his idea for yet another alternative
deal, a variation of the trigger plan favored by Senator Olympia J. Snowe,
Republican of Maine.

On Friday the show may be on the Senate floor, but the real action will be
elsewhere.

DAVID M. HERSZENHORN

URL: http://www.nytimes.com

LOAD-DATE: December 4, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO

PUBLICATION-TYPE: Newspaper


