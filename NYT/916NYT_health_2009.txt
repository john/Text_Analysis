                   Copyright 2009 The New York Times Company


                             916 of 2021 DOCUMENTS


                               The New York Times

                            November 1, 2009 Sunday
                              Late Edition - Final

Supply-Side Ideas, Turned Upside Down

BYLINE: By N. GREGORY MANKIW.

N. Gregory Mankiw is a professor of economics at Harvard. He was an adviser to
President George W. Bush.

SECTION: Section BU; Column 0; Money and Business/Financial Desk; ECONOMIC VIEW;
Pg. 4

LENGTH: 913 words


BARACK OBAMA is, in many ways, the left's answer to Ronald Reagan.

Both came to office as charismatic and self-confident leaders, elected in times
of economic crisis and determined to move the economy in a new direction. What
is less obvious, however, is that the signature domestic issue in President
Obama's first year in office -- health care reform -- is shaping up to be the
antithesis of President Reagan's supply-side economics.

The starting point for Ronald Reagan was the idea that people respond to
incentives. The incentives that he most worried about were those provided by the
tax system. According to his budget director, David A. Stockman, Mr. Reagan
would regale the staff with stories of how he, as an actor, used to alter his
work schedule in response to the tax code.

''You could only make four pictures, and then you were in the top bracket,'' Mr.
Reagan would say. ''So we all quit working after four pictures and went off to
the country.''

The key economic concept here is the marginal tax rate, which measures the
percentage of a family's incremental income to which the government lays claim.
During Mr. Reagan's time in office, the top marginal tax rate on earned income
fell to 28 percent from 50 percent.

The verdict on supply-side economics is mixed. The most striking claim
associated with the theory -- that cuts in marginal rates could generate so much
extra work effort that tax revenue would rise -- is unlikely to apply except in
extreme cases. But substantial evidence supports the more modest proposition
that high marginal tax rates discourage people from working to their full
potential. Mr. Reagan's behavior as a movie actor is a case in point.

President Obama has said he wants to raise marginal tax rates on high-income
taxpayers. Yet under his policies, the largest increases in marginal tax rates
may well apply not to the rich but to millions of middle-class families. These
increases would not show up explicitly in the tax code but, rather, implicitly
as part of health care reform.

The bill that recently came out of the Senate Finance Committee illustrates the
problem. Under the proposed legislation, Americans would have the opportunity to
buy health insurance through government-run exchanges. Depending on a family's
income, premiums and cost-sharing expenses, like co-payments and deductibles,
would be subsidized to make health care more affordable.

A family of four with an income, say, of $54,000 would pay $9,900 for health
care. That covers only about half the actual cost. Uncle Sam would pick up the
rest.

Now suppose that the same family earns an additional $12,000 by, for example,
having the primary earner work overtime or sending a secondary worker into the
labor force. In that case, the federal subsidy shrinks, so the family's cost of
health care rises to $12,700.

In other words, $2,800 of the $12,000 of extra income, or 23 percent, would be
effectively taxed away by the government's new health care system.

That implicit marginal tax rate of 23 percent is a significant disincentive. And
it comes on top of the explicit marginal tax rate the family already faces from
income and payroll taxes. Altogether, many families would face marginal rates at
or above the 50 percent level that animated the Reagan supply-side revolution.

One might hope that such a large climb in marginal rates is a bug in the Senate
Finance bill, one that could be fixed before the legislation became law. But
there is no simple fix. Higher marginal tax rates are an integral part of the
Obama health plan.

Here's why:

Health reformers start with the problem that some people are expensive to
insure, because of pre-existing health conditions. Their solution is to require
insurers to sell insurance to everyone (a policy called guaranteed issue) at the
same price (called community rating).

This solution, however, causes another problem. For healthy people, insurance is
now a bad bet. A person without significant medical needs has an incentive to
wait -- to buy insurance later if and when he gets sick, a decision that raises
the cost of insurance for everyone else. This problem, according to the
reformers, calls for another solution: a mandate requiring people to buy health
insurance.

But this mandate leads to yet another problem. Requiring an expensive purchase
like health insurance can be onerous for low-income families. So the health
reformers offer subsidies.

Which brings us back to marginal tax rates. If large health insurance subsidies
were offered to all Americans, regardless of income, the program's cost would be
exorbitant, requiring substantial increases in explicit taxes. So, instead, the
subsidies are phased out as income rises. As a result, we get implicit marginal
rates like those in the Senate Finance bill.

NONE of this necessarily means that health reform is not worth doing. President
Obama's push for reform is premised on the belief that access to good health
care should be a right of all Americans -- a proposition better judged by
political philosophers than economists.

But we should not forget the cost of translating that noble aspiration into
practical policy. As a matter of economic logic, President Obama's goal of
universal health insurance cannot help but undermine former President Reagan's
goal of lower marginal tax rates. Future generations of Americans may find
health insurance more affordable, but they will also find hard work less
financially rewarding.

URL: http://www.nytimes.com

LOAD-DATE: November 1, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY DAVID G. KLEIN)

PUBLICATION-TYPE: Newspaper


