                   Copyright 2009 The New York Times Company


                             576 of 2021 DOCUMENTS


                               The New York Times

                           September 13, 2009 Sunday
                              Late Edition - Final

Where Politics Don't Belong

BYLINE: By TYLER COWEN.

Tyler Cowen is a professor of economics at George Mason University.

SECTION: Section BU; Column 0; Money and Business/Financial Desk; ECONOMIC VIEW;
Pg. 4

LENGTH: 888 words


FOR years now, many businesses and individuals in the United States have been
relying on the power of government, rather than competition in the marketplace,
to increase their wealth. This is politicization of the economy. It made the
financial crisis much worse, and the trend is accelerating.

Well before the financial crisis erupted, policy makers treated homeowners as a
protected political class and gave mortgage-backed securities privileged
regulatory treatment. Furthermore,  they allowed and encouraged  high leverage
and the expectation of bailouts for creditors, which had been practiced numerous
times, including the precedent of Long-Term Capital Management in 1998. Without
these mistakes, the economy would not have been so invested in leverage and real
estate and the financial crisis would have been much milder.

But  we are now injecting politics ever more deeply into the American economy,
whether it be in finance or in sectors like health care. Not only have we failed
to learn from our mistakes, but also we're repeating them on an ever-larger
scale.

Lately the surviving major banks have reported brisk profits, yet in large part
this reflects astute politicking and lobbying rather than commercial skill. Much
of the competition was cleaned out by bank failures and consolidation, so giants
like Goldman Sachs and JPMorgan had an easier time getting back to profits. The
Federal Reserve has been lending to banks at near-zero interest rates  while
paying higher interest on the reserves the banks hold at the Fed. ''Too big to
fail'' policies mean that the large banks can raise money more cheaply because
everyone knows they are safe counterparties.

President Dwight D. Eisenhower warned of the birth of a military-industrial
complex. Today we have a financial-regulatory complex, and it has meant a
consolidation of power and privilege. We've created a class of politically
protected ''too big to fail'' institutions, and the current proposals for
regulatory reform further cement this notion. Even more worrying, with so many
explicit and implicit financial guarantees, we are courting a bigger financial
crisis the next time something major goes wrong.

We should stop using political favors as a means of managing an economic sector.
Unfortunately, though, recent experience with health care reform shows we are
moving in the opposite direction and not heeding the basic lessons of the
financial crisis. Finance and health care are two separate issues, of course,
but in both cases we're making the common mistake of digging in durable
political protections for special interest groups.

One disturbing portent came over the summer when it was reported that the Obama
administration had promised deals to doctors and to pharmaceutical companies
under the condition that they publicly support health care reform. That's
another example of creating favored beneficiaries through politics.

If these initial deals are falling apart, it is only because reform met with
unexpected resistance. Even after Mr. Obama's speech Wednesday night, we're
still at the point where the medical sector is enshrined as ''too big to take a
pay cut,'' which is not so far removed from the banking motto of ''too big to
fail.''   In finance and health care, a common political dynamic has created
similar trends, namely, out-of-control costs, weak accountability, and the use
of immediate revenue patches to postpone dealing with  fundamental problems.

Even worse, these political deals threaten open discourse. The dealmaking may be
inhibiting some people in health care  from speaking out in opposition to the
administration's proposals. Robert Reich, who served as secretary of labor in
the Clinton administration, deserves credit for complaining about this
arrangement, but not enough people are asking where such dealmaking might stop.

The banking sector has been facing similar constraints; if bankers criticize the
Treasury or the Fed, they risk losing their gilded cages and could get a bad
deal when the next bailout comes. When major economic sectors can be influenced
in this way, are we really very far from the nightmare depicted by Ayn Rand in
''Atlas Shrugged''?

So if we're looking for a major lesson from our banking mess, it is undoubtedly
this: We have made a grave mistake in politicizing the economy so deeply, and
should back away now. In health care,  the Obama administration should drop its
medical sector deals and try to sell a  reform plan -- in whatever form Mr.
Obama chooses -- on its own merits. That's not only good for health care, but
also good for the American polity. And in the longer run, that will be good for
banking, too. If nothing else, without controlling health care costs, the
American government will not stay solvent  -- and that will be the biggest
financial crisis of them all.

In short, we should return both the financial and medical sectors and, indeed,
our entire economy to greater market discipline. We should move away from the
general attitude of ''too big to take a pay cut,'' especially when the taxpayer
is on the hook for the bill. If such changes sound daunting, it is a sign of how
deep we have dug ourselves in. We haven't yet learned from the banking crisis,
and we're still moving in the wrong direction pretty much across the board.

URL: http://www.nytimes.com

LOAD-DATE: September 13, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING DAVID G. KLEIN)

PUBLICATION-TYPE: Newspaper


