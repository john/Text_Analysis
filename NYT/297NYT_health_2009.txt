                   Copyright 2009 The New York Times Company


                             297 of 2021 DOCUMENTS


                               The New York Times

                           August 12, 2009 Wednesday
                         The New York Times on the Web

As Health Care Debate Rages, Obama Takes to the Stump

BYLINE: By JIM RUTENBERG and JACKIE CALMES; Jeff Zeleny contributed reporting.

SECTION: Section ; Column 0; National Desk; Pg.

LENGTH: 1785 words

DATELINE: WASHINGTON


President Obama tried on Tuesday to defuse fears about his plan to overhaul the
nation's health care system, an issue at the center of one of the fiercest
public-policy debates in decades, telling a friendly audience in New Hampshire
that a lot of misinformation is being spread.

''If you like your health-care plan, you can keep your health-care plan,'' the
president told a gathering in Portsmouth, N.H. ''You will not be waiting in any
lines. This is not about putting the  government in charge of your health
insurance.''

''For all the chatter and the yelling and the shouting and the noise, what you
need to know is this: If you don't have health insurance, you will finally have
quality, affordable options, once we pass reform. If you do have health
insurance, we will make sure that no insurance company, or a government
bureaucrat, gets between you and the care that you need.''

The president went on, to applause. ''And we will do this without adding to our
deficit over the next decade, largely by cutting out the waste and insurance
company giveaways in Medicare that aren't making any of our seniors healthier,''
he said.

The president said some Republicans had been helpful, and that he hoped to
achieve health care reform on a bipartisan basis, ''but the most important thing
is getting it done for the American people.''

The New Hampshire event was part of a campaign to fight questionable but
potentially damaging charges that the president's vision would inevitably lead
to ''socialized medicine,'' ''rationed care'' and even forced euthanasia for the
elderly.

The tone of the national debate over the future of health care has become
increasingly emotional, even bitter, as reflected in comments by lawmakers
across the political specrtum.

In introducing a Web site to defend the president's proposals, White House
officials were tacitly acknowledging a difficult reality: they are suddenly at
risk of losing control of the public debate over a signature issue for Mr. Obama
and are now playing defense in a way they have not since last year's campaign.

Senator Bernard Sanders of Vermont, an independent who is one of the most
liberal members in either House, said on Tuesday that ''the Republicans are the
party of do-nothingism, and because of them it is very hard to move forward.''

But Mr. Sanders said in an interview on MSNBC that ''frankly, the Democrats have
not handled this as clearly and effectively as they might have.''

A different perspective was offered by Representative Peter King, a Long Island
Republican far to the right of Mr. Sanders. Mr. King said it was quite
understandable that many Americans are not enthusiastic about ''the radical type
of reform that President Obama's talking about.''

The health care system should be changed ''incrementally'' rather than by major
surgery, Mr. King said in another interview on MSNBC. The congressman said he
thought the White House had made a tactical error in its approach on health
care. ''It may not be perfect,'' he said, conceding that Americans ''may have
problems with it.''

''But it's not the rabid-type issue that had to be solved by Aug. 1 of this
year, the way President Obama was saying,'' Mr. King said.

And Senator Arlen Specter of Pennsylvania, a Republican-turned-Democrat, faced a
crowd of emotional constituents in Lebanon, Pa. One participant drew loud
applause when he said illegal immigrants should not be covered (Mr. Specter
agreed), and another complained that the legislation was as complicated ''as a
Russian novel.''

Mr. Specter said the federal government has a ''social compact'' with the
American people to ''take care of people who need some help.'' He pledged again
not to vote for something that would add to the budget deficit, and he tried to
reassure the crowd that people who are happy with their present health insurance
do not have to worry about losing it.

''So far, no bill has passed the Congress,'' Mr. Specter noted. ''In the House
of Representatives, five committees have passed bills, but the House has not
passed a bill. In the Senate, we're still working on a bill, trying to get
bipartisanship.''

''I know the American people are sick and tired of Republicans and Democrats
fighting, and the American people would like to see some bipartisanship and
coming together in the public interest,'' the senator said, in an appeal for
calm that was not entirely successful.

President Obama, speaking at a summit of North American leaders in Mexico on
Monday, sounded an optimistic note, predicting that ''the American people are
going to be glad that we acted to change an unsustainable system so that more
people have coverage.''

But aides to Mr. Obama said the rapidly escalating threat to his health care
plans had led him to order them to come up with a crisper message.

And Democratic Party officials enlisted in the fight by the White House
acknowledged in interviews that the growing intensity of the opposition to the
president's health care plans -- within the last week likened on talk radio to
something out of Hitler's Germany, lampooned by protesters at Congressional
town-hall-style meetings and vilified in television commercials -- had caught
them off guard and forced them to begin an August counteroffensive.

In the process, the administration has had a harder time getting across the
themes it wanted to strike in this period: that the current system is
unsustainable and that Mr. Obama's plan holds concrete benefits for people who
already have health insurance as well as for those who do not.

''We all had a good sense that some of this was going to take place,'' said Brad
Woodhouse, the communications director for the Democratic National Committee.
''To be fair, I think we were probably a little surprised -- just a little -- at
the use of swastikas and the comparisons to Adolf Hitler and the Third Reich
that even Rush Limbaugh has fanned the flames on. And we were a little surprised
at the mob mentality.'' (Mr. Woodhouse's use of the phrase ''mob mentality'' was
itself part of the Democratic effort to paint opponents speaking out against the
plan as part of an unruly but organized effort.)

For some of Mr. Obama's supporters, the newly galvanized opposition to his
proposed policies provided a troubling flashback to the successful effort to
stop President Bill Clinton's similarly ambitious plans 16 years ago -- a fight
Mr. Obama's aides had studied carefully to avoid making the same fatal mistakes.

White House officials say such fears are unwarranted, arguing that the
conservative protests are getting outsize coverage on cable news. ''Don't
associate loud with effective,'' Rahm Emanuel, the White House chief of staff,
said in an interview, adding that he detected no anxiety from supportive
lawmakers in politically vulnerable districts. ''What is coming across is a lot
of noise and a lot of heat without a lot of light.''

And White House officials say their August counteroffensive is a break from the
Clinton approach, which is now viewed as having failed to adequately address
critics.

Mr. Obama will take the lead this week as he continues a series of public
meetings to counter the opposition, events White House officials hope will offer
a high-profile opportunity to confront and rebut critics.

As part of the effort, Speaker Nancy Pelosi and Representative Steny H. Hoyer,
the House majority leader, wrote an opinion article in USA Today onMonday
calling conservative protests at Congressional town-hall-style meetings
''un-American'' for ''drowning out opposing views.'' (That prompted a swift
rebuke from the House minority leader, Representative John A. Boehner, among
other Republicans.)

New television commercials disputing the conservative attacks are in the works,
Mr. Woodhouse said, and allied members of Congress have been sent home for the
August break with a set of poll-tested talking points intended to shift the
focus to the administration's advertised benefits of the plan from the scary
situations opponents have laid out.

''There's a whole set of rumors that the old playbook would tell you not to do
anything about because you draw attention,'' said Dan Pfeiffer, the White House
deputy communications director. ''The lesson we've learned is you ignore these
rumors at your peril, and the right answer is to take them head on in as big a
way as possible.''

It was only weeks ago that Mr. Obama was pressing both the House and the Senate
to complete work on their versions of health legislation before recessing for
the summer, a goal that went unmet as divisions erupted among Democrats as well
as between the two parties.

After getting much of what he wanted on high-risk initiatives like the economic
stimulus package and bailouts of banks and auto companies, Mr. Obama had yet to
face the full force of conservative opposition to his policies. Some supporters
now wonder whether his earlier glide path left him unprepared for the sudden
surge of opposition from conservative groups, which have found a rallying point
on health care.

''The expectation was that things have gotten so bad in the last 16 years that
there would be consensus on the need to act this time,'' said Howard Paster, who
was Mr. Clinton's chief lobbyist in 1993. ''That was a mistake, that
assumption.''

Mr. Obama's team won early, high marks for diverging from the Clinton approach,
specifically by emphasizing the need to control costs and improve coverage for
those who are already insured instead of making the same moral-duty argument Mr.
Clinton had about the need to cover the uninsured.

Yet once Congress started filling in the details this summer and its analysts
began pricing the House and Senate packages, the estimates of the government's
cost caused sticker shock again.

And once again that drew taxpayers' attention to the main reason for those
costs: covering the uninsured, through more Medicaid spending and subsidies for
people to buy insurance and small businesses to provide it.

That helped conservatives who had been struggling to gain traction on health
care to speak to a constituency that has managed to gain significant anti-Obama
attention this year, the fiscally hawkish ''tea party'' activists opposed to the
president's spending. They have dismissed Mr. Obama's promises that his plan
will be fully paid for through offsetting spending cuts or increased taxes, and
have cast the plan as a costly takeover of health care by the government.

''I think the combination of spending a trillion dollars that we don't have and
another rushed process really triggered this,'' said Matt Kibbe, the president
of the conservative group FreedomWorks. ''People started paying attention.''

URL: http://www.nytimes.com

LOAD-DATE: August 12, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


