                   Copyright 2010 The New York Times Company


                             1708 of 2021 DOCUMENTS


                               The New York Times

                             March 21, 2010 Sunday
                              Late Edition - Final

DEMOCRATS RALLY TO OBAMA'S CALL FOR HEALTH VOTE

BYLINE: By DAVID M. HERSZENHORN and ROBERT PEAR; Carl Hulse contributed
reporting.

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1768 words


WASHINGTON -- President Obama and House Democratic leaders on Saturday closed in
on the votes needed to pass landmark health care legislation, with the outcome
hinging on their efforts to placate a handful of lawmakers who wanted the bill
to include tighter limits on insurance coverage for abortions.

Mr. Obama, in an emotional address at the Capitol, exhorted rank-and-file House
Democrats to approve the bill, telling them they were on the edge of making
history with a decisive vote scheduled for Sunday.

''Every once in a while a moment comes where you have a chance to vindicate all
those best hopes that you had about yourself, about this country,'' he said.
''This is one of those moments.''

The president declared: ''We have been debating health care for decades. It has
now been debated for a year. It is in your hands.''

With the ground shifting by the hour, House Democratic leaders dropped a plan to
approve the Senate health  bill without taking a direct vote on it. That
proposed maneuver had outraged Republicans and caused consternation among some
Democrats.

Thousands of opponents of the bill circled the Capitol chanting angry slogans.
Some of the anger was directed at black lawmakers, including several who said
that some demonstrators had hurled racial insults at them.

Speaker Nancy Pelosi refused to give abortion opponents a separate vote on that
issue, a sign of her confidence that she could clinch the votes without further
changes to the bill.

Democrats said they would vote Sunday on the Senate bill and on revisions to it
included in a budget reconciliation measure.

Democrats said the outcome would be the same: the Senate bill would be sent to
Mr. Obama, who would sign it into law, and the reconciliation bill would go to
the Senate, which could take it up within days.

At the Capitol rally with Mr. Obama, the Senate majority leader, Harry Reid of
Nevada, assured House Democrats that their Senate colleagues would act quickly
on the reconciliation bill, including final revisions to the health care
measure. ''I have the commitments of a significant majority of the United States
Senate to make that good law even better,'' he said.

As Mr. Obama concluded his remarks, Ms. Pelosi appeared to be within 8 votes of
the 216 she needed to approve the bill, with 19 Democrats still uncommitted.
Republicans are all expected to oppose the bill.

The House Republican leader, John A. Boehner of Ohio, said the Republicans could
still prevail. ''The American people are making their voices heard, here on
Capitol Hill and across America,'' he said. ''It's time for Washington Democrats
to listen.''

In his speech, Mr. Obama drew chortles from lawmakers -- and laughed at himself
-- when he suggested that perhaps Republicans were hoping to spare Democrats
political pain by blocking the bill.

''Mitch McConnell, John Boehner, Karl Rove, they are all warning you of the
horrendous impact if you support this legislation,'' the president said,
referring to the Senate and House Republican leaders and a top adviser to former
President George W. Bush.

''Now, it could be that they are suddenly having a change of heart and they are
deeply concerned about their Democratic friends; they are giving you the best
possible advice in order to ensure that Nancy Pelosi remains speaker and Harry
Reid remains leader and all of you keep your seats,'' Mr. Obama joked. ''That's
a possibility.''

He continued, ''But it may also be possible that they realize that after health
reform passes and I sign that legislation into law, it's going to be a little
harder to mischaracterize what this legislation has been all about.''

Ms. Pelosi expressed total confidence in the bill's prospects. ''We are on the
verge of making great history for the American people,'' she said.

The speaker on Saturday rejected a proposal by Representative Bart Stupak,
Democrat of Michigan and a leader of the abortion opponents, to revise the
abortion provisions after the bill was adopted, a step that is typically used to
make minor or technical changes and that requires approval of both the House and
the Senate.

Instead, Democratic officials said they were pursuing the idea of promising that
Mr. Obama would issue an executive order prohibiting the use of taxpayer money
for abortions. They said that approach would not mollify Mr. Stupak but could
win the support of others still undecided because of their views on abortion.

Ms. Pelosi said she would not allow separate votes on abortion or other
controversial issues. ''Not on abortion, not on public option, not on
single-payer, not on anything,'' she said.

Behind the scenes, Ms. Pelosi was working aggressively to address the concerns
of anti-abortion Democrats. She met with at least three of those lawmakers --
Representatives Christopher Carney and Kathy Dahlkemper of Pennsylvania and
Steve Driehaus of Ohio -- none of whom had publicly committed their vote.

The official action on Saturday was in the House Rules Committee, which labored
to set the formal terms of the debate for Sunday's showdown. Shortly before
midnight, the committee completed its work and proposed the parameters for the
historic floor fight, which will entail two hours of formal debate on the
legislation. The committee, controlled by Democrats, also limited the ability of
Republicans to derail the proceedings and allowed for the vote to be postponed
if Democrats chose to do so.

Before Democrats decided to take a direct vote on the Senate health care bill,
Representative Joe L. Barton, Republican of Texas, described the plans to
approve it without a direct vote as ''a sleight-of-hand subterfuge'' that would
allow lawmakers to avoid accountability.

''This process corrupts and prostitutes the system'' and could ''unleash a
cultural war'' over the legislation, Mr. Barton said.

Democrats, however, tried to keep the focus on the substance of the legislation,
which seeks to extend coverage to 32 million uninsured Americans.

''We are on the verge of taking a decisive step to providing access to all
Americans to affordable quality health care,'' said Representative Henry A.
Waxman, Democrat of California and chairman of the Energy and Commerce
Committee. ''If we do nothing, the system will go bankrupt, premiums will keep
skyrocketing and benefits will keep getting slashed.''

The legislation would cost $940 billion over 10 years, according to the
Congressional Budget Office, with the expense more than offset by revenues from
new taxes and fees and reductions in spending on Medicare and other government
programs. With those changes, the bill would reduce future deficits by $138
billion over that time period, the budget office estimate found.

At a news conference on Saturday, 13 House Republican freshmen assailed the
measure. ''Let's kill this bill,'' said Representative Cynthia M. Lummis,
Republican of Wyoming.

The late-hour maneuvering on abortion mirrored a similar process in November
before the House adopted its version of the health care legislation.

In November, however, Mr. Stupak succeeded in winning approval of tight limits
on insurance coverage of abortions in the House  bill. The current package now
includes language from the bill passed in the Senate and negotiated by two
Democrats, Senators Bob Casey of Pennsylvania and Ben Nelson of Nebraska, who
have built up solid credentials in their political careers as abortion
opponents.

Once again, Mr. Stupak was opposed by a group of lawmakers who favor abortion
rights, led by Representative Diana DeGette, Democrat of Colorado. And once
again, at the center of the storm was Ms. Pelosi, the first woman to serve as
House speaker, who is a champion of abortion rights.

The abortion issue has divided Roman Catholic groups in the United States, with
the United States Conference of Catholic Bishops opposing the bill and other
organizations, including the Catholic Health Association and a coalition of nuns
from leading religious orders, favoring it.

Mr. Stupak and many of the lawmakers insisting on the tighter restrictions are
Catholic, as is Ms. Pelosi, and all have cited their faith in justifying their
position on the legislation.

In a sign of the emotion around the issue, Representative Dale E. Kildee,
Democrat of Michigan, who is Catholic and opposes abortion, announced his
support for the legislation in a statement pointing out that he had once studied
for the priesthood. He said he had consulted his priest and concluded that the
abortion restrictions in the Senate bill were sufficient.

Democratic leaders said they hoped an executive order by Mr. Obama would clarify
that the legislation was not intended to change existing federal law and policy
that generally bar the use of taxpayer money for abortions.

''That is very good if the president does that,'' said Representative Lynn
Woolsey, Democrat of California and co-chairwoman of the Congressional
Progressive Caucus. ''He will clear it up. The Senate bill does not allow
federal funds to be used for abortion.''

But Representative Anh Cao of Louisiana, the only Republican who voted for the
bill in November, said he could not support the current measure because of its
''expansion of abortion, an absolute moral evil.''

Democratic lawmakers and top aides have been working round the clock trying to
address flare-ups over elements of the bill. They said they had worked out an
agreement to resolve one of the last remaining issues: a dispute over geographic
disparities in Medicare payments.

The agreement could lead to higher payments to doctors and hospitals in states
like Iowa, Minnesota, Oregon, Washington and Wisconsin, where Medicare rates are
relatively low but studies suggest that the quality of care is high.

Kathleen Sebelius, the secretary of health and human services, sent a letter to
Congress saying she would commission studies by the National Academy of Sciences
to investigate the issue and recommend solutions.

''The current geographic variation in Medicare reimbursement rates is
inequitable,'' Ms. Sebelius said.

Outside the Capitol on Saturday, several House members said, there was an ugly
tone to comments made by some demonstrators against three black lawmakers:
Representatives Andre Carson of Indiana, Emanuel Cleaver II of Missouri and John
Lewis of Georgia, all Democrats.

An aide to Mr. Lewis, a leader of the civil rights movement in the 1960s, said
that as he walked to the Capitol, racial slurs were shouted at Mr. Lewis. A
spokesman for Mr. Cleaver said that a protester spat on the congressman as he
was walking to the Capitol for a vote.

URL: http://www.nytimes.com

LOAD-DATE: March 21, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Representatives Lincoln Diaz-Balart, left, David Dreier and
Louise M. Slaughter at a Saturday hearing of the House Rules Committee, which
was setting formal terms for Sunday's showdown. (PHOTOGRAPH BY BRENDAN
SMIALOWSKI FOR THE NEW YORK TIMES) (A19)

PUBLICATION-TYPE: Newspaper


