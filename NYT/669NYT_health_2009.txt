                   Copyright 2009 The New York Times Company


                             669 of 2021 DOCUMENTS


                               The New York Times

                          September 23, 2009 Wednesday
                              Late Edition - Final

Democrat vs. Democrat

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 21

LENGTH: 216 words


The Group of Six is over, done. What's left is the Group of One: Senator Olympia
J. Snowe, Republican of Maine.

Ms. Snowe, below, is the only member of her party on the Senate Finance
Committee willing to consider supporting the proposed health care legislation.
The other Republicans on that panel will vote against it, and so on Wednesday
internal policy fights among Democrats will move to the forefront.

Senate leaders say they expect that Democratic senators will ultimately fall in
line to support the health care bill. But there is a lot to argue about between
now and then.

One potential flashpoint is an amendment by Senator John D. Rockefeller IV,
Democrat of West Virginia, to delete a provision that would create private
nonprofit insurance cooperatives to compete with private insurers.

Mr. Rockefeller would prefer a government-run insurance plan. But that idea is
opposed by Ms. Snowe and others.

And Senator Ron Wyden, Democrat of Oregon, will argue that whether the bill
creates a public plan or co-ops, the vast majority of the roughly 180 million
Americans who now have insurance through their employers will not be able to
choose either.

Instead, they would be locked into their existing coverage. And Mr. Wyden has an
amendment to change that.

DAVID M. HERSZENHORN

URL: http://www.nytimes.com

LOAD-DATE: September 23, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO

PUBLICATION-TYPE: Newspaper


