                   Copyright 2009 The New York Times Company


                             585 of 2021 DOCUMENTS


                               The New York Times

                           September 13, 2009 Sunday
                              Late Edition - Final

Drug Makers Back New Plan

BYLINE: By DUFF WILSON

SECTION: Section A; Column 0; National Desk; THE AD CAMPAIGN; Pg. 37

LENGTH: 347 words


The drug industry's trade group plans to roll out a series of television
advertisements in coming weeks specifically to support Senator Max Baucus's
health care overhaul proposal, according to an industry official involved in the
planning.

The move would be a follow-up to the deal that drug makers struck in June with
Mr. Baucus, right, and the White House. Under that pact, the industry agreed to
various givebacks and discounts meant to reduce the nation's pharmaceutical
spending by $80 billion over 10 years.

Shortly after striking that agreement, the trade group -- the Pharmaceutical
Research and Manufacturers of America, or PhRMA -- also set aside $150 million
for advertising to support health care legislation.

President Obama has cited the deal with the group as signifying a new era of
cooperation. But some critics say the advertising fund could be wielded against
alternative approaches to health care legislation. Some House Democrats,
including Henry A. Waxman of California, are seeking drug industry givebacks not
covered in the deal with Mr. Baucus and the White House.

Up to now, the group, led by former Representative Billy Tauzin, a Republican
from Louisiana, has contributed $12 million toward an advertising campaign
coordinated by a coalition called Americans for Stable Quality Care. Early
advertisements focused on general subjects like ''Eight Ways Reform Matters to
You.''

But an industry official involved in the discussions said the group and its
advertising money would now be aimed specifically at the approach being pushed
by Mr. Baucus, Democrat of Montana and chairman of the Senate Finance Committee.

The group's advertising spending has been criticized by people who say drug
makers should be forced to make deeper concessions than they have agreed to with
Mr. Obama and Mr. Baucus.

James Love,  director of the liberal nonprofit research group Knowledge Ecology
International, is one of those critics. ''Essentially what the U.S. got was not
$80 billion,'' he said, ''but $150 million in Obama campaign contributions.''
DUFF WILSON

URL: http://www.nytimes.com

LOAD-DATE: September 13, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO

PUBLICATION-TYPE: Newspaper


