                   Copyright 2009 The New York Times Company


                             370 of 2021 DOCUMENTS


                               The New York Times

                             August 21, 2009 Friday
                              Late Edition - Final

Protesting Scheduled Cuts

BYLINE: By ANNE UNDERWOOD

SECTION: Section A; Column 0; National Desk; MAKING SENSE OF THE HEALTH CARE
DEBATE; Pg. 18

LENGTH: 298 words


Health care rallies have suddenly become a rite of late summer. The one
scheduled for Friday morning in downtown Orlando, Fla., has been advertised as
featuring doctors angry about the prospect of lower Medicare payments.

Many physicians of all sorts fear that a health care overhaul could reduce
government payments in the Medicare and Medicaid programs.

But the Orlando rally, organized by cardiologists, is focused specifically on
Medicare reimbursement cuts already proposed for next January, some of which are
required by a 1997 statute that can be overridden only by Congress. (Congress
has in fact taken action to prevent such cuts each year since 2003.)

Organizers say there are 1,200 confirmed attendees, including 150 doctors and
more than 1,000 patients -- some bused in from as far as Tampa, about 80 miles
away.

If the cuts take effect on Jan. 1, 2010, as proposed, they will reduce
reimbursements for a variety of standard heart procedures, in some cases by more
than one-third.

''Who's going to suffer? Ultimately, the patients,'' said Dr. Andrew Taussig,
president of the Florida Hospital Cardiovascular Institute, and one of the
organizers. ''It will affect cost, quality and accessibility of care.''

Critics do not dispute the doctors' right to protest fee cuts. But they say the
doctors are not only putting posters and sign-up sheets in their offices, but
are also speaking to patients individually in some cases.

''If doctors are just providing information in their waiting rooms, that's
fine,'' said Arthur L. Caplan, director of the Center for Bioethics at the
University of Pennsylvania. ''But if it's in the examining room, and if the
implication is, 'I'm not going to be here to care for you in a few months,' that
bothers me a lot.'' ANNE UNDERWOOD

URL: http://www.nytimes.com

LOAD-DATE: August 21, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


