                   Copyright 2009 The New York Times Company


                             962 of 2021 DOCUMENTS


                               The New York Times

                            November 9, 2009 Monday
                              Late Edition - Final

Paranoia Strikes Deep

BYLINE: By PAUL KRUGMAN

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 23

LENGTH: 807 words


Last Thursday there was a rally outside the U.S. Capitol to protest pending
health care legislation, featuring the kinds of things we've grown accustomed
to, including large signs showing piles of bodies at Dachau with the caption
''National Socialist Healthcare.'' It was grotesque -- and it was also ominous.
For what we may be seeing is America starting to be Californiafied.

The key thing to understand about that rally is that it wasn't a fringe event.
It was sponsored by the House Republican leadership -- in fact, it was
officially billed as a G.O.P. press conference. Senior lawmakers were in
attendance, and apparently had no problem with the tone of the proceedings.

True, Eric Cantor, the second-ranking House Republican, offered some mild
criticism after the fact. But the operative word is ''mild.'' The signs were
''inappropriate,'' said his spokesman, and the use of Hitler comparisons by such
people as Rush Limbaugh, said Mr. Cantor, ''conjures up images that frankly are
not, I think, very helpful.''

What all this shows is that the G.O.P. has been taken over by the people it used
to exploit.

The state of mind visible at recent right-wing demonstrations is nothing new.
Back in 1964 the historian Richard Hofstadter published an essay titled, ''The
Paranoid Style in American Politics,'' which reads as if it were based on
today's headlines: Americans on the far right, he wrote, feel that ''America has
been largely taken away from them and their kind, though they are determined to
try to repossess it and to prevent the final destructive act of subversion.''
Sound familiar?

But while the paranoid style isn't new, its role within the G.O.P. is.

When Hofstadter wrote, the right wing felt dispossessed because it was rejected
by both major parties. That changed with the rise of Ronald Reagan: Republican
politicians began to win elections in part by catering to the passions of the
angry right.

Until recently, however, that catering mostly took the form of empty symbolism.
Once elections were won, the issues that fired up the base almost always took a
back seat to the economic concerns of the elite. Thus in 2004 George W. Bush ran
on antiterrorism and ''values,'' only to announce, as soon as the election was
behind him, that his first priority was changing Social Security.

But something snapped last year. Conservatives had long believed that history
was on their side, so the G.O.P. establishment could, in effect, urge hard-right
activists to wait just a little longer: once the party consolidated its hold on
power, they'd get what they wanted. After the Democratic sweep, however,
extremists could no longer be fobbed off with promises of future glory.

Furthermore, the loss of both Congress and the White House left a power vacuum
in a party accustomed to top-down management. At this point Newt Gingrich is
what passes for a sober, reasonable elder statesman of the G.O.P. And he has no
authority: Republican voters ignored his call to support a relatively moderate,
electable candidate in New York's special Congressional election.

Real power in the party rests, instead, with the likes of Rush Limbaugh, Glenn
Beck and Sarah Palin (who at this point is more a media figure than a
conventional politician). Because these people aren't interested in actually
governing, they feed the base's frenzy instead of trying to curb or channel it.
So all the old restraints are gone.

In the short run, this may help Democrats, as it did in that New York race. But
maybe not: elections aren't necessarily won by the candidate with the most
rational argument. They're often determined, instead, by events and economic
conditions.

In fact, the party of Limbaugh and Beck could well make major gains in the
midterm elections. The Obama administration's job-creation efforts have fallen
short, so that unemployment is likely to stay disastrously high through next
year and beyond. The banker-friendly bailout of Wall Street has angered voters,
and might even let Republicans claim the mantle of economic populism.
Conservatives may not have better ideas, but voters might support them out of
sheer frustration.

And if Tea Party Republicans do win big next year, what has already happened in
California could happen at the national level.  In California, the G.O.P. has
essentially shrunk down to a rump party  with no interest in actually governing
--  but that rump remains big enough to prevent anyone else from dealing with
the state's fiscal crisis.  If this happens to America as a whole, as it all too
easily could, the country could become effectively ungovernable in the midst of
an ongoing economic disaster.

The point is that the takeover of the Republican Party by the irrational right
is no laughing matter. Something unprecedented is happening here  --  and it's
very bad for America.

URL: http://www.nytimes.com

LOAD-DATE: November 9, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


