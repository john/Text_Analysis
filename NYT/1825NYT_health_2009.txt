                   Copyright 2010 The New York Times Company


                             1825 of 2021 DOCUMENTS


                               The New York Times

                            March 31, 2010 Wednesday
                              Late Edition - Final

Obama Signs Overhaul of Student Loan Program

BYLINE: By PETER BAKER and DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 14

LENGTH: 608 words


ALEXANDRIA, Va. -- President Obama signed legislation on Tuesday to expand
college access for millions of young Americans by revamping the federal student
loan program in what he called ''one of the most significant investments in
higher education since the G.I. Bill.''

Mr. Obama went to a community college where the wife of his vice president
teaches to draw attention to the student loan overhaul attached to the final
piece of health care legislation that passed last week. In signing the bill, Mr.
Obama put the final touches on his health care program but used the occasion to
highlight the education provisions.

''That's two major victories in one week,'' he told students and guests at the
Alexandria campus of Northern Virginia Community College, where Jill Biden
teaches English. While he praised the health care overhaul, the president said,
''what's gotten overlooked amid all the hoopla, all the drama of last week, is
what's happened with education.''

The new law will eliminate fees paid to private banks to act as intermediaries
in providing loans to college students and use much of the nearly $68 billion in
savings over 11 years to expand Pell grants and make it easier for students to
repay outstanding loans after graduating. The law also invests $2 billion in
community colleges over the next four years to provide education and career
training programs to workers eligible for trade adjustment aid after dislocation
in their industries.

The law will increase Pell grants along with inflation in the next few years,
which should raise the maximum grant to $5,975 from $5,550 by 2017, according to
the White House, and it will also provide 820,000 more grants by 2020.

Students who borrow money starting in July 2014 will be allowed to cap
repayments at 10 percent of income above a basic living allowance, instead of 15
percent. Moreover, if they keep up payments, their balances will be forgiven
after 20 years instead of 25 years -- or after 10 years if they are in public
service, like teaching, nursing or serving in the military.

Mr. Obama portrayed the overhaul of the student loan program as a triumph over
an ''army of lobbyists,'' singling out Sallie Mae, the nation's largest student
lender, which he said spent $3 million on lobbying to stop the changes. ''For
almost two decades, we've been trying to fix a sweetheart deal in federal law
that essentially gave billions of dollars to banks,'' he said. The money, he
said, ''was spent padding student lenders' pockets.''

But Sallie Mae said the law would cost jobs, telling news outlets that it may
have to eliminate a third of its 8,500 positions nationwide. Senator Lamar
Alexander, a Republican whose state of Tennessee is home to big players in the
private student lending industry, said the changes over all would cost 31,000
private-sector jobs.

In a statement, Mr. Alexander said students would be overcharged on their loans
with the proceeds to be used to pay for the health care law, and he bemoaned the
government's getting more deeply into the lending business. ''The Obama
administration's motto,'' he said, ''is turning out to be: 'If we can find it in
the Yellow Pages, the government ought to try to do it.' ''

The House Republican leader, Representative John A. Boehner of Ohio, criticized
both the health and education components of the bill, calling them ''two
job-killing government takeovers that are already hurting our economy.''

Mr. Boehner added, ''As the White House continues to 'sell' this new law, we are
seeing the same pattern we've seen for the past year: the more the American
people learn about it, the less they like it.''

URL: http://www.nytimes.com

LOAD-DATE: March 31, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: President Obama on Tuesday promoted the new education loan law
at Northern Virginia Community College in Alexandria.
 The Health Care and Education Reconciliation Act of 2010 and the official pens
after the signing ceremony at the college. (PHOTOGRAPHS BY LUKE SHARRETT/THE NEW
YORK TIMES)

PUBLICATION-TYPE: Newspaper


