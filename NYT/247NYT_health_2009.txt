                   Copyright 2009 The New York Times Company


                             247 of 2021 DOCUMENTS


                               The New York Times

                            August 1, 2009 Saturday
                              Late Edition - Final

Obama's Pledge to Tax Only the Rich Can't Pay for Everything, Analysts Say

BYLINE: By JACKIE CALMES

SECTION: Section A; Column 0; National Desk; POLITICAL MEMO; Pg. 10

LENGTH: 1232 words

DATELINE: WASHINGTON


Behind Democrats' struggle to pay the $1 trillion 10-year cost of President
Obama's promise to overhaul the health care system is their collision with
another of his well-known pledges: that 95 percent of Americans ''will not see
their taxes increase by a single dime'' during his term.

This will not be the last time that the president runs into a conflict between
his audacious agenda and his pay-as-you-go guarantee, when only 5 percent of
taxpayers are being asked to chip in. Critics from conservative to liberal warn
that Mr. Obama has tied his and Congress's hands on a range of issues, including
tax reform and the need to reduce deficits topping $1 trillion a year.

''You can only go to the same well so many times,'' said Bruce Bartlett, a
Treasury official in the Reagan administration.

In the budget, Mr. Obama and Congress have already agreed to let the Bush tax
cuts for the most affluent expire after 2010, as scheduled, but to extend them
for everyone else. The top rates, now 33 percent and 35 percent, will revert to
Clinton-era levels of 36 percent and 39.6 percent.

The critics do not have a beef with the government's taking more from the
wealthiest Americans, especially given the growing income gap between the rich
and everyone else. They object to doing so for health care over other pressing
needs.

''I want to tax the rich to reduce the deficit,'' said Robert D. Reischauer, a
former director of the Congressional Budget Office who heads the Urban
Institute, a center-left research group. Similarly, Mr. Bartlett, a conservative
analyst who often chastises Republicans for their antitax absolutism, supports
overhauling the tax code to raise revenues.

As these analysts recognize, taxing the rich has its limits both economically
and politically, such that members of Congress are not likely to tap that well
again and again.

Polls show strong majorities supporting higher taxes on those earning more than
$250,000 a year, Mr. Obama's target group. Yet some Congressional Democrats are
fearful of Republicans' attacks that ''soak the rich'' tax increases will douse
small-business owners, too, even if the number of those affected is far less
than Republicans suggest.

Also, higher rates like those in the House health care legislation could lead to
tax avoidance schemes, reducing the government's collections and warping
business decisions, analysts say.

The House measure calls for surtaxes ranging from 1 percent on annual income of
$280,000 to 5.4 percent on income of $1 million and more. The millionaires'
surtax would push the top tax rate to 45 percent, the highest since the 1986 tax
code overhaul lowered all rates in return for jettisoning a raft of tax breaks
for businesses and individuals.

But the effective top rate would be higher still, counting the 2.9 percent
Medicare payroll tax and state and local income taxes. In the highest-tax states
of Oregon, Hawaii, New Jersey, New York and California, it would be 57 percent,
according to the conservative Heritage Foundation.

In the health debate, Democrats emphasize that they are not just raising taxes
on the rich, but cutting spending, too, mostly for Medicare payments to doctors,
hospitals and insurance companies.

Also, the Democrats say, at least they are trying to pay for the health care
initiative, rather than letting the deficit balloon as the Republicans, along
with President George W. Bush, did when they created the Medicare prescription
drug benefit in 2003. That program will add a projected $803 billion to the
national debt in the decade through 2019, according to the White House budget
office.

''They charged theirs on the government's credit card,'' Rahm Emanuel, the White
House chief of staff, said of the Republicans.

Even so, Mr. Obama's vow to tax only the rich is a variation ''of Bush's policy
that nobody has to pay for anything,'' said Leonard Burman, a veteran of the
Clinton administration Treasury and director of the nonpartisan Tax Policy
Center.

''Democrats are more worried about the deficits,'' Mr. Burman added, but ''they
put the burden on a tiny fraction of the population that they figure doesn't
vote for them anyway.''

Mr. Burman and others recall that in the creation of Social Security and
Medicare, Presidents Franklin D. Roosevelt and Lyndon B. Johnson insisted that
beneficiaries contribute through payroll taxes, both to finance the programs and
to give all Americans a vested interest. The same philosophy should apply to
seeking universal health coverage, they say.

''This idea that everything new that government provides ought to be paid for by
the top 5 percent, that's a basically unstable way of governing,'' Mr. Burman
said.

Mr. Obama recently dismissed concerns that taxing the rich to pay for health
care would foreclose that option when he and Congress turn to deficit reduction.
''Health care reform is fiscal reform,'' he said.

''If we don't do anything on health care inflation, then we might as well close
up shop when it comes to dealing with our long-term debt and deficit problems,
because that's the driver of it -- Medicare and Medicaid,'' Mr. Obama said.

But his no-new-tax admonition for most Americans even now complicates the
behind-the-scenes work of the panel he established to recommend ways to simplify
the tax code and raise more revenue.

The panel, which is led by Paul A. Volcker, a former chairman of the Federal
Reserve, is to report by Dec. 4. Overhauling the code, as in 1986, generally
creates winners and losers across the board; leaving 95 percent of taxpayers
unscathed will not be easy.

That has already proved true in the health care deliberations. Proposals to
raise about $50 billion over 10 years by taxing sugared drinks foundered partly
because the levy would hit nearly everyone.

And when Congressional leaders opposed Mr. Obama's chief idea for raising
revenues -- limiting affluent taxpayers' deductions -- his campaign vow against
taxing the middle class made finding an acceptable alternative difficult.

While the president endorsed House Democrats' surtax idea, saying it ''meets my
principle that it's not being shouldered by families who are already having a
tough time,'' he could not embrace a bipartisan Senate proposal to tax
employer-provided health benefits above a certain amount. He had criticized a
similar idea as a middle-class tax during his presidential campaign.

Yet taxing at least the most generous employer-provided plans above a threshold
amount would meet two elusive goals for Mr. Obama: It would raise a lot of money
and, economists say, cut overall health spending by making consumers more
cost-conscious.

Administration officials recently began promoting a fallback. Rather than tax
individuals, it would single out insurance companies that sell ''Cadillac''
plans. David Axelrod, a White House strategist, has described the proposal in
populist terms, saying it would hit ''the $40,000 policies that the head of
Goldman Sachs has'' and ''not impact on the middle class.''

That position, analysts predict, cannot hold over time.

''There is no way we can pay for health care and the rest of the Obama agenda,
plus get our long-term deficits under control, simply by raising taxes on the
wealthy,'' said Isabel V. Sawhill, a former Clinton administration budget
official. ''The middle class is going to have to contribute as well.''

URL: http://www.nytimes.com

LOAD-DATE: August 1, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: President Obama has promised that 95 percent of Americans ''will
not see their taxes increase by a single dime'' in his term. (PHOTOGRAPH BY
STEPHEN CROWLEY/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


