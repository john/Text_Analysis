                   Copyright 2009 The New York Times Company


                             978 of 2021 DOCUMENTS


                               The New York Times

                          November 11, 2009 Wednesday
                              Late Edition - Final

A Message After the Vote

BYLINE: By KATHARINE Q. SEELYE

SECTION: Section A; Column 0; National Desk; ADVERTISING; Pg. 22

LENGTH: 226 words


Liberal interest groups in favor of overhauling the health care system are
taking different tacks in trying to advance their goals, at least as far as
advertising goes.

MoveOn.org is spending $500,000 on television commercials to criticize Democrats
who voted against the House bill on Saturday night. The targets include Jason
Altmire of Pennsylvania and Heath Shuler of North Carolina, both of whose
districts voted for John McCain for president in 2008. (MoveOn, though, is also
planning some ''thank you'' events this week for House Democrats who did support
the bill.)

At the same time, the umbrella group Health Care for America Now has teamed up
with the American Federation of State, County and Municipal Employees to spend
$650,000 on advertisements to thank 20 Democrats who voted for the bill.

The employees' union is also working with the advocacy group Americans United to
spend $350,000 on commercials thanking 11 members, including Representative Anh
Cao of Louisiana, the sole Republican who voted for the bill (even if he has
also taken his share of heat from opponents).

Whether the advertisements flatter members or chastise them, they are meant to
encourage them to vote in favor of health care legislation the second time
around, when a bill that is merged with one in the Senate comes back for final
passage.

KATHARINE Q. SEELYE

URL: http://www.nytimes.com

LOAD-DATE: November 11, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO

PUBLICATION-TYPE: Newspaper


