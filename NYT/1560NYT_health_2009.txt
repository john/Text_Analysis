                   Copyright 2010 The New York Times Company


                             1560 of 2021 DOCUMENTS


                               The New York Times

                          February 24, 2010 Wednesday
                              Late Edition - Final

G.O.P. Expects Little From Obama's Health Forum

BYLINE: By ROBERT PEAR and DAVID M. HERSZENHORN; Peter Baker contributed
reporting.

SECTION: Section A; Column 0; National Desk; Pg. 15

LENGTH: 924 words

DATELINE: WASHINGTON


Republican Congressional leaders on Tuesday rejected President Obama's challenge
to come up with a single comprehensive proposal to achieve his goal of
guaranteeing health insurance for nearly all Americans.

But they said they would attend a televised forum to discuss the issue with Mr.
Obama on Thursday, even as they voiced doubt that he and Congressional Democrats
were acting in good faith.

The Senate Republican leader, Mitch McConnell of Kentucky, and the House
Republican leader,  John A. Boehner of Ohio, made clear that they had extremely
low expectations for the six-hour session.

''We will be at the meeting on Thursday and anxious to participate in the
discussion,'' Mr. McConnell said. ''But it appears as if the administration has
already made up their mind.''

Mr. Boehner said Republicans would attend the meeting intent on defeating the
Democrats' plans. ''President Obama has basically crippled the health care
summit by coming in with a rerun'' of the bills passed by the House and the
Senate, he said.

''This so-called summit is a charade,'' Mr. Boehner said. ''The Democrats want
us to boycott so they can say Republicans walked away and they have no choice
but to plow ahead with their health care takeover. They want us to boycott so we
won't be a thorn in their sides on issues like jobs and abortion. We shouldn't
let the White House have a six-hour taxpayer-funded infomercial on Obama Care.''

Mr. Boehner said House Republicans would take their own proposal into the
meeting with Mr. Obama. Senate Republicans will carry a set of ideas for
step-by-step changes in health care, similar but not identical to the House
Republican package. None of the Republican approaches would seek to cover close
to the number of uninsured people -- 31 million -- that the White House said its
plan would cover.

Democrats generally welcomed the proposal unveiled Monday by Mr. Obama. The
House Democratic leader, Representative Steny H. Hoyer of Maryland, called it
''a very positive step forward.''

House Democratic leaders focused Tuesday on explaining the president's proposal
to members of their caucus and did not count votes to measure support for it.
But one potentially worrisome sign for the White House was the reaction of
Representative Bart Stupak, Democrat of Michigan,  a leading opponent of
abortion.

''I was pleased to see that President Obama's health care proposal did not
include several of the sweetheart deals provided to select states in the Senate
bill,'' Mr. Stupak said. ''Unfortunately, the president's proposal encompasses
the Senate language allowing public funding of abortion. The Senate language is
a significant departure from current law and is unacceptable.''

Mr. Stupak voted for the House bill in November after it was amended to prohibit
the use of federal subsidies to pay ''any part of the costs of any health plan
that includes coverage of abortion,'' with limited exceptions. Of the 219
Democrats who voted for the bill, 41 did so after backing Mr. Stupak's amendment
inserting the abortion restrictions.

Mr. Obama on Tuesday endorsed a bill that would eliminate the exemption from
federal antitrust law that health insurance companies have long enjoyed. He said
the bill would benefit consumers by promoting competition and outlawing
''practices like price-fixing, bid-rigging and market allocation that drive up
costs.''

The proposal that House Republicans will carry into the meeting with Mr. Obama
would encourage small businesses to band together to buy insurance; would give
federal money to states to run high-risk pools for people who cannot obtain
private insurance; and would limit damages in medical malpractice lawsuits.

The Congressional Budget Office said the proposal could lead to lower premiums,
in part by reducing the number of state-required benefits or increasing the
number of relatively healthy people with insurance.

The House Republican bill would cost $61 billion over 10 years, far less than
the $950 billion cost of Mr. Obama's plan, as estimated by the White House.

The Congressional Budget Office said the House Republican bill would provide
coverage to three million uninsured people, roughly one-tenth of the number who
would gain coverage under the president's proposal.

Sage Eastman, a spokesman for Republicans on the House Ways and Means Committee,
said: ''Why is coverage the dominant theme? The president and Democrats had a
year to make their case for full coverage, and by every poll -- and election --
it has been rejected. Cost has been and remains the No. 1 issue.''

The White House on Tuesday challenged Republicans to put forward a ''consensus
plan'' before Thursday's meeting. Dan Pfeiffer, the White House communications
director, said, ''A collection of piecemeal and sometimes conflicting ideas
won't do.''

Mr. McConnell said he and seven other Republican senators would go ''in good
faith'' to the meeting. But he called Mr. Obama's latest proposal a nonstarter.

''The American people thought the debate on this approach to reform was over,''
Mr. McConnell said. ''Yet here we are again, being told by the White House that
we have to consider the same health care bills that caused such a backlash
across the country in December.''

Mr. Hoyer said he preferred to pass a comprehensive bill to rein in health costs
and expand coverage. But he did not insist on all or nothing. ''You know me,''
Mr. Hoyer said. ''If you cannot do a whole, doing part is also good. There are a
number of things I think we can agree on.''

URL: http://www.nytimes.com

LOAD-DATE: February 24, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: Senator Mitch McConnell said Tuesday that ''it appears as if
the administration has already made up their mind'' on health care. (PHOTOGRAPH
BY STEPHEN CROWLEY/THE NEW YORK TIMES)
 Representative Bart Stupak said any public financing of abortion ''is
unacceptable.'' (PHOTOGRAPH BY ALEX BRANDON/ASSOCIATED PRESS)

PUBLICATION-TYPE: Newspaper


