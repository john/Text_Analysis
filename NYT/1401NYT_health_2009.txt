                   Copyright 2010 The New York Times Company


                             1401 of 2021 DOCUMENTS


                               The New York Times

                            January 22, 2010 Friday
                         The New York Times on the Web

Obama's Political Challenge

SECTION: Section ; Column 0; Editorial Desk; LETTERS; Pg.

LENGTH: 304 words


To the Editor:

In ''What Didn't Happen'' (column, Jan. 18), Paul Krugman argues that President
Obama's political problems do not stem from an overly ambitious agenda, but from
wrong judgments on policy and politics.

I would point to President Obama's choice of warmed-over ''inside the Beltway''
triangulators as advisers who counseled him to be modest, nonconfrontational and
compromising in his policies -- and thereby alienate his political base.

We should have had a more vibrant stimulus package, a more progressive (and
meaningful) health care proposal and far more pointed communication to the
public to rebut the Republicans. Vice President Joe Biden should have been set
loose to make the progressive case!

I agree that President Obama needed to do what President Ronald Reagan did:
continue his campaign into his presidency to argue more forcefully for the
changes he promised during his campaign.

Unless the message and tone out of this administration change very soon, we will
have wasted a tremendous opportunity to move the country forward.

James Ponsoldt  Athens, Ga., Jan.  18, 2010

To the Editor:

Paul Krugman tells us that much of President Obama's trouble arises from his
failure to spend adequate time blaming his predecessor, as he says President
Ronald Reagan did.

While it's true that the economy was in bad shape early in Mr. Reagan's
presidency, people were instinctively supportive of his policies of lower taxes,
less spending and smaller government, and were confident that these ideas would
ultimately prove successful.

President Obama's policies are the polar opposite: the government running car
companies, vastly expanding its involvement in health care and spending money we
clearly don't have. This instinctively makes people nervous.

Jeffrey Epstein   Charlotte, N.C., Jan.  18, 2010

URL: http://www.nytimes.com

LOAD-DATE: January 22, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


