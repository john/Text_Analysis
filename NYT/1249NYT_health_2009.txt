                   Copyright 2009 The New York Times Company


                             1249 of 2021 DOCUMENTS


                               The New York Times

                          December 23, 2009 Wednesday
                              Late Edition - Final

Delayed Vacation for President

BYLINE: By HELENE COOPER

SECTION: Section A; Column 0; National Desk; STAYING IN TOWN; Pg. 23

LENGTH: 135 words


It's official: No Hawaii for President Obama until the Senate finishes its
health care legislation this week.

''I will not leave until my friends in the Senate have completed their work,''
Mr. Obama told reporters Tuesday after meeting with a group of bankers. ''My
attitude is, if they are making these sacrifices to provide health care to all
Americans, the least I can do is be around and provide them any encouragement
and last-minute help.''

Mr. Obama was originally supposed to leave for Hawaii on Wednesday morning.

Though he may be stuck in Washington a bit longer, he does have one consolation.
When you have your own plane waiting at Andrews Air Force Base, you can delay a
trip and still know that you will not be held in confinement on the tarmac for
hours when it is time to go. HELENE COOPER

URL: http://www.nytimes.com

LOAD-DATE: December 23, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


