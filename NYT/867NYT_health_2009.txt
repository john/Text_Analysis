                   Copyright 2009 The New York Times Company


                             867 of 2021 DOCUMENTS


                               The New York Times

                           October 22, 2009 Thursday
                              Late Edition - Final

Historical Context, in 8 Pages

BYLINE: By REED ABELSON

SECTION: Section A; Column 0; National Desk; IN BRIEF; Pg. 25

LENGTH: 133 words


If you are interested in reading a just-the-facts analysis of the health care
legislation pending in Congress, here's your chance.

Health Affairs, the health policy journal, with support from the Robert Wood
Johnson Foundation, has just issued a new policy brief looking at the various
bills in the House and Senate, and their potential impact on the insurance
market. The report can be found at healthaffairs.org.

The brief is not exactly light reading. But what document can be breezy, if it
must parse insurance terms like ''adjusted community rating''?

It does, though, offer some useful historical context for the debate. And it
provides a straightforward look at the pros and cons of different aspects of the
legislation -- all in eight pages, even if you count the footnotes. REED ABELSON

URL: http://www.nytimes.com

LOAD-DATE: October 22, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


