                   Copyright 2009 The New York Times Company


                             428 of 2021 DOCUMENTS


                               The New York Times

                            August 27, 2009 Thursday
                         The New York Times on the Web

Obama Offers Tribute to 'a Defender of a Dream'

BYLINE: By JEFF ZELENY

SECTION: Section ; Column 0; National Desk; Pg.

LENGTH: 1117 words

DATELINE: OAK BLUFFS, Mass.


President Obama on Wednesday praised the life and legacy of Senator Edward M.
Kennedy,  calling him ''one of the most accomplished Americans ever to serve our
democracy.''

''His ideas and ideals are stamped on scores of laws, and reflected in millions
of lives,'' Mr. Obama said. ''In seniors who know new dignity, in families who
know new opportunity, in children who know education's promise -- and in all who
can pursue their dream in an America that is more equal and more just, including
myself.''

The president echoed the sentiment expressed by political figures of both
parties across America and around the world who paid tribute to the
Massachusetts senator.

An era of Democratic politics and 30 years separated Mr. Obama and Mr. Kennedy,
but they grew exceedingly close during the presidential race last year, when Mr.
Kennedy's endorsement provided a critical lift to Mr. Obama's candidacy. They
last saw each other three months ago, but aides said they spoke occasionally by
phone, largely about the president's challenge in remaking the nation's health
care system.

The president did not mention legislation during a brief televised statement. It
was premature, administration officials said, to know if the senator's death
would change the course of a bitter Congressional debate on health care.

''His extraordinary life on this earth has come to an end. His extraordinary
work lives on,'' Mr. Obama said, speaking from the Blue Heron Farm in the town
of Chilmark. ''For his family, he was a guardian. For America, he was a defender
of a dream.''

Word that Mr. Kennedy had succumbed to brain cancer reached Mr. Obama as he
vacationed on Martha's Vineyard, just across the Nantucket Sound from the
Kennedy compound in Hyannis Port. An aide woke up the president with the news
shortly after 2 a.m.  He conveyed his condolences in a telephone call to Mr.
Kennedy's wife, Vicki, at about 2:25 a.m., said Robert Gibbs, the White House
press secretary.

An American flag, which was lowered to half-staff shortly after sunrise in Oak
Bluffs, waved in the breeze against the backdrop of the rippling Atlantic.
Residents and tourists on this tiny Massachusetts island gathered around
televisions in cafes to watch coverage of the senator's death.

''His fight has given us the opportunity that was denied us when his brothers
John and Robert were taken from us,'' Mr. Obama said, ''the blessing of time to
say thank you and goodbye.''

Funeral arrangements have not been announced, but aides said Mr. Obama would
deliver a eulogy at the funeral Mass. It is a bookend to his first appearance
with Mr. Kennedy at the Democratic National Convention in 2004, when Mr. Obama
was preceded on stage in Boston for his national political debut by Mr. Kennedy
in a symbolic showcase of the party's dean and its new generation.

''I valued his wise counsel in the Senate, where, regardless of the swirl of
events, he always had time for a new colleague,'' Mr. Obama said earlier
Wednesday in a statement. ''I cherished his confidence and momentous support in
my race for the presidency.''

The decision by Mr. Kennedy, the patriarch of the Democratic Party, to support
Mr. Obama's candidacy served as a critical moment in the long primary fight with
Senator Hillary Rodham Clinton. For weeks, the Clintons had implored Mr. Kennedy
to stay neutral in the race, but on Jan. 28, 2008, he said he grew troubled by
the tone of the campaign and issued his endorsement before campaigning across
the country on Mr. Obama's behalf.

His decision to back Mr. Obama created a significant rift with former President
Bill Clinton, associates of both men have said, which forever changed their
relationship. Mr. Kennedy appeared with Mr. Obama at American University in
Washington, asking Democrats ''to turn the page on the old politics of
misrepresentation and distortion.''

''He will be a president who refuses to be trapped in the patterns of the
past,'' Mr. Kennedy said that day, interrupting his speech more than once to
embrace Mr. Obama. ''He is a leader who sees the world clearly without being
cynical.''

Mr. Obama told friends that appearing with Mr. Kennedy and other members of the
family at American University was among his favorite moments of the campaign.

As Mr. Kennedy's battle with brain cancer wore on in recent months, he would
occasionally speak by telephone to Mr. Obama. There was considerable speculation
that during Mr. Obama's vacation to Martha's Vineyard this week, he would visit
Mr. Kennedy and his family, but aides said the senator's condition was too grave
and a presidential visit would be too disruptive.

''For five decades, virtually every major piece of legislation to advance the
civil rights, health and economic well being of the American people bore his
name and resulted from his efforts,'' Mr. Obama said in his statement on
Wednesday morning. He added, ''The Kennedy family has lost their patriarch, a
tower of strength and support through good times and bad.''

Mr. Obama is scheduled to vacation on Martha's Vineyard through Sunday. Aides
said that there were no immediate plans for him to visit the Kennedy family, but
his schedule was pending until funeral arrangements for the senator were
announced.

Others across the political landscape, both in the United States and abroad,
echoed the president's sentiments early Wednesday.

Harry Reid, Democrat of Nevada and the Senate majority leader, said: ''The
Kennedy family and the Senate family have together lost our patriarch... . The
liberal lion's mighty roar may now fall silent, but his dream shall never die.''

But Mr. Kennedy's death also evoked impassioned expressions of sympathy and
respect from across the aisle.

A senior Senate Republican, Orrin Hatch of Utah, said, ''I have to say there are
very few people in my lifetime that I've had more respect, and now reverence,
for, than Ted Kennedy.'' He called the late lawmaker ''an iconic,
larger-than-life United States senator whose influence cannot be overstated.''

Former Prime Minister Tony Blair of Britain called Senator Kennedy ''a great and
good man'' who had inspired ''admiration, respect and devotion, not just in
America but around the world.''

He lauded Mr. Kennedy for his efforts to promote peace in Northern Ireland: ''I
saw his focus and determination firsthand in Northern Ireland, where his
passionate commitment was matched with a practical understanding of what needed
to be done to bring about peace and to sustain it. I was delighted he could join
us in Belfast the day devolved government was restored. My thoughts and prayers
today are with all his family and friends as they reflect on the loss of a great
and good man.''

URL: http://www.nytimes.com

LOAD-DATE: August 27, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


