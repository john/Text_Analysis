                   Copyright 2010 The New York Times Company


                             1570 of 2021 DOCUMENTS


                               The New York Times

                            February 26, 2010 Friday
                              Late Edition - Final

At Health Forum, Obama Finds Uniting Democrats Is Still His Major Challenge

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; NEWS ANALYSIS; Pg. 14

LENGTH: 887 words

DATELINE: WASHINGTON


After more than six hours of extraordinary debate on Thursday over health care
policy, President Obama had not won over any of the Republicans, and he seemed
to end the day largely where he started, with little choice but to try to rally
his Democrats to act on their own.

Their most viable path seemed to be an effort to attach revisions to the health
care bill to a budget reconciliation measure, which the Senate could adopt by a
simple majority. ''If nothing comes of this we're going to press forward,'' said
Senator Richard J. Durbin of Illinois, the No. 2 Democrat. ''We just can't quit.
This is a once-in-a-political-lifetime opportunity to deal with a health care
system that is really unsustainable.''

But doing so would require mustering the support of centrist Democrats in the
House and the Senate who have expressed apprehensions about both the health care
bill and the reconciliation process, which Republicans are portraying as an
unfair parliamentary tactic to skirt the normal rules.

It was unclear if the event had won over any of those votes, especially among
House Democrats who opposed the  bill in November, and whose support could be
critical to reviving it.

''It's interesting that they are having a discussion and we are continuing to
have the debate we have been having for a year now, but how are you going to get
it passed?'' said Representative Jason Altmire of Pennsylvania, who was among
the 39 Democrats to vote no. Mr. Altmire, who did not attend the session but
planned to watch it later on video, said, ''I don't see very many at all who
voted no who are going to switch their votes unless there are substantial
changes in the bill.''

While the forum was novel, Mr. Obama still seemed burdened with the challenges
of having pursued  a largely middle-of-the-road proposal that has hampered the
Democrats all along. It has disappointed some in the party's liberal base,
especially without a public option. It holds little or no appeal for
Republicans, and it confuses and scares many people in the middle.

And yet, just five weeks after the Republican victory in a special Senate
election in Massachusetts left the Democrats' health care legislation on the
edge of collapse, Mr. Obama's unusual forum, and his relentless effort to
portray Democrats and Republicans as agreeing on many points, restored the
health care issue to center stage, and reminded a skeptical public of the
gravity of the problems he is trying to fix.

Mr. Obama also seemed to widen the playing field, giving the Democrats some
additional options. They could try to win final approval of the legislation
using budget reconciliation, which would avert a Republican filibuster in the
Senate. The president suggested a decision on that could be made within six
weeks.

They could potentially devise further changes to the bill, adding Republican
ideas even without Republican cooperation. One area of common ground to emerge
at the forum was an idea put forward by Senator Tom Coburn, Republican of
Oklahoma, to use undercover regulators posing as patients to root out fraud by
doctors and hospitals. ''That's something that I'd be very interested in
exploring,'' Mr. Obama said. Senator Charles E. Schumer, Democrat of New York,
called it ''a great idea.''

Representative George Miller, Democrat of California, said he thought the forum
would lead to changes that would improve the bill and gain support. ''Today's
meeting was very helpful in that regard,'' Mr. Miller said. ''You have to go
hunt for the votes. But you have to have a product.''

The Democrats could also begin to break their proposal into pieces that have a
better chance of winning bipartisan support. The House on Wednesday passed a
stand-alone bill to repeal the insurance industry's exemption from federal
antitrust laws by a vote of 406 to 19. Thursday's summit meeting suggested that
other ideas, like extending dependent coverage for adult children, could pass by
a similarly big margin.

After Republicans said that they shared some of the Democrats' goals on tighter
insurance regulation, including ending annual and lifetime caps on benefits,
Vice President Joseph R. Biden Jr. said Republicans could not argue that
government should have no role.

''You're either in or you're out,'' he snapped at Representative Eric Cantor of
Virginia, the House Republican whip, who was highly critical at the session of
the Democrats' legislative plans.

The fundamental question facing Republicans was not whether they could persuade
Democrats to take a different approach, but whether continuing their  opposition
in the wake of Mr. Obama's grand gesture of bipartisanship could turn into a
liability in a tense midterm election year.

While opinion polls show deep public unease over the health care legislation,
they also show simmering frustration at the partisanship and gridlock in
Washington. For now, Republican leaders seem confident that they have the public
on their side.

''It is not irrelevant that the American people, if you average out all of the
polls, are opposed to this bill by 55 to 37,'' the Senate Republican leader,
Mitch McConnell of Kentucky, said at the forum, adding that the public also
opposes reconciliation. ''We know how the American people feel about this,'' he
said. ''This is not a close call.''

URL: http://www.nytimes.com

LOAD-DATE: February 26, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Congressional leaders joined President Obama on Thursday in six
hours of televised debate. (PHOTOGRAPH BY STEPHEN CROWLEY/THE NEW YORK TIMES)

DOCUMENT-TYPE: News Analysis

PUBLICATION-TYPE: Newspaper


