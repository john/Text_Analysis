                   Copyright 2009 The New York Times Company


                             550 of 2021 DOCUMENTS


                               The New York Times

                          September 10, 2009 Thursday
                              Late Edition - Final

Obama, Armed With Details, Challenges Congress

BYLINE: By SHERYL GAY STOLBERG and JEFF ZELENY

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1251 words

DATELINE: WASHINGTON


President Obama confronted a critical Congress and a skeptical nation on
Wednesday, decrying the ''scare tactics'' of his opponents and presenting his
most forceful case yet for a sweeping health care overhaul that has eluded
Washington for generations.

In blunt language before a rare joint session of Congress, Mr. Obama vowed that
he would ''not waste time'' with those who have made a political calculation to
oppose him, but left the door open to working with Republicans to cut health
costs and expand coverage to millions of Americans.

''The time for bickering is over,'' the president declared. ''The time for games
has passed. Now is the season for action.''

The president was greeted by booming applause from Democrats and polite
handshakes from Republicans. But the political challenge at hand soon became
clear as several Republican lawmakers heckled Mr. Obama when he dismissed the
notion that so-called death panels would deny care to the elderly.

''It is a lie, plain and simple,'' Mr. Obama declared.

''You lie!'' Representative Joe Wilson of South Carolina yelled back after Mr.
Obama said it was not true that the Democrats were proposing to provide health
coverage to illegal immigrants.

The 47-minute speech was an effort by Mr. Obama to regain his political footing
on health care, his highest legislative priority. He insisted throughout that he
had not closed the door on reaching a bipartisan compromise. He gave a nod to
Senator John McCain, Republican of Arizona, and embraced his proposal to create
a high-risk pool to help cover people with pre-existing conditions against
catastrophic expenses.

And, with the widow of Senator Edward M. Kennedy sitting in the House gallery,
the president appealed to the nation's conscience, reading a letter Mr. Kennedy
had written in May with instructions that it be delivered to the president upon
his death. In it, Mr. Kennedy wrote that health care was ''above all a moral
issue; at stake are not just the details of policy, but fundamental principles
of social justice and the character of our country.''

Presidents since Theodore Roosevelt have been advocating universal health care
without success, and Mr. Obama vowed to fare better. ''I am not the first
president to take up this cause,'' he said, ''but I am determined to be the
last.''

The speech came after a rocky August for the White House, in which many
lawmakers held public meetings that deteriorated into shouting matches over
health care.

After months of insisting he would leave the specifics to lawmakers, Mr. Obama
used the speech to present his most detailed outline yet of a plan he said would
provide ''security and stability'' to those who have insurance and cover those
who do not, all without adding to the federal deficit.

The president placed a price tag on the plan of about $900 billion over 10
years, which he said was ''less than we have spent on the Iraq and Afghanistan
wars.'' But he devoted much of his address to making the case for why such a
plan is necessary, and sought to reassure the elderly and the Americans who
already have insurance that they would not be worse off.

As expected, Mr. Obama repeated his support for a government insurance plan to
compete with the private sector, though he said he would consider alternatives
to the ''public option.''

He sketched out a vision for a plan in which it would be illegal for insurers to
drop sick people or deny them coverage for pre-existing conditions, and in which
every American would be required to carry health coverage, just as drivers must
carry auto insurance.

Mr. Obama did embrace some fresh proposals. He announced a new initiative to
create pilot projects intended to curb  medical malpractice lawsuits, a cause
important to physicians and Republicans.

He endorsed a plan, contained in a draft proposal being circulated by Senator
Max Baucus, the Montana Democrat who is chairman of the Senate Finance
Committee, to help pay for expanding coverage by taxing insurance companies that
offer expensive, so-called gold-plated insurance plans.

And, seeking to reassure those who worry he will run up the federal deficit, Mr.
Obama promised to include a provision that ''requires us to come forward with
more spending cuts'' if the savings he envisions do not materialize.

In embracing Mr. McCain and the malpractice projects, the White House appeared
to be seeking to lay the groundwork for an argument that the final bill would be
bipartisan not because it garners Republican votes but because it contains
Republican ideas. That is the same argument Mr. Obama used when the economic
recovery package passed with just three Republican votes.

Republicans seemed primed for a fight; many, like Senator Charles E. Grassley,
the Iowa Republican who has been deeply involved in health negotiations,
released statements about the speech even before it began. Mr. Grassley called
on Mr. Obama to ''start building the kind of legislation that could win the
support of 70 to 80 senators,''  a goal Mr. Grassley said could not be achieved
if the bill contained a new government plan.

In the Republican response, Representative Charles Boustany Jr. of Louisiana, a
heart surgeon, agreed that the health care system needed an overhaul. But he
urged the president to start anew, focusing on a ''common-sense, bipartisan
plan.''

An hour after the speech, Mr. Wilson, the heckler, issued an apology for his
outburst.

The speech was the president's second address before a joint session of
Congress. But the political backdrop on Wednesday was far different from his
appearance in the House chamber on the 36th day of his term, when an optimistic
wave of momentum was at his back and his Republican rivals were dispirited and
in disarray.

''What we have also seen in these last months is the same partisan spectacle
that only hardens the disdain many Americans have toward their own government.
Instead of honest debate, we have seen scare tactics,'' Mr. Obama said. ''Some
have dug into unyielding ideological camps that offer no hope of compromise. Too
many have used this as an opportunity to score short-term political points, even
if it robs the country of our opportunity to solve a long-term challenge.''

He added, ''And out of this blizzard of charges and countercharges, confusion
has reigned.''

While Mr. Obama was addressing lawmakers inside the ornate House chamber, the
much more important audience was outside Washington: the 180 million Americans
who already have health insurance and who remain skeptical that Mr. Obama's plan
will change things for the better. Inside the chamber, the president drew
laughter when he said, ''there remain some significant details to be ironed
out.''

For Mr. Obama, the speech was a go-for-broke moment; there is no more dramatic
venue for a president than an address to a joint session to Congress. For many
Democrats, the speech evoked memories of a similar health care address by
President Bill Clinton, 16 years ago this month. Mr. Clinton called for
''security, simplicity, savings, choice, quality and responsibility'' -- the
same broad themes Mr. Obama evoked Wednesday night.

The architect of the Clinton plan, of course, was Mr. Clinton's wife, Hillary
Rodham Clinton.

On Wednesday night, Mr. Obama's wife, Michelle, sat quietly in the House
gallery, holding the hand of Victoria Reggie Kennedy, Mr. Kennedy's widow. Mrs.
Clinton, now Mr. Obama's secretary of state, sat in the front row, smiling and
shaking hands.

URL: http://www.nytimes.com

LOAD-DATE: September 10, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: President Obama entered the House to booming applause from
Democrats and polite handshakes from Republicans.(PHOTOGRAPH BY STEPHEN
CROWLEY/THE NEW YORK TIMES)(pg. A1)
 Nancy Pelosi, the House speaker, led Democrats as they applauded Mr. Obama's
speech.(PHOTOGRAPH BY DOUG MILLS/THE NEW YORK TIMES)(pg. A26)

PUBLICATION-TYPE: Newspaper


