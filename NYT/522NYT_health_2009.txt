                   Copyright 2009 The New York Times Company


                             522 of 2021 DOCUMENTS


                               The New York Times

                           September 8, 2009 Tuesday
                              Late Edition - Final

When Obama Speaks on Health Care

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 24

LENGTH: 986 words


To the Editor:

Re ''Let's Get Fundamental,'' by David Brooks (column, Sept. 4):

Mr. Brooks has it exactly right. Fundamental change in our dysfunctional and
overpriced health care system is absolutely necessary, and it will be impossible
to achieve that transformation while protecting the huge profits of big
corporate contributors to Congress.

Allowing Congress, itself dysfunctional and unable to look beyond industry
lobbyists, to craft a mosaic that reinforces the most expensive and wasteful
processes within our present system was a near-fatal mistake.

For those of us who fervently supported President Obama as an agent of change,
settling for some Band-Aids on the present bloated systems, which focus on
profit rather than on health, is a missed opportunity of colossal proportions.
Not change we can believe in.

Ray Bellamy Tallahassee, Fla., Sept. 4, 2009

The writer is an orthopedic surgeon.

To the Editor:

If David Brooks is correct and ''the American health care system is
dysfunctional at the core,'' the market solutions he proposes will fix nothing.
The current system ''hides information, muddies choices, encourages more
treatment instead of better care, neglects cheap innovation, inflates costs and
unintentionally increases suffering'' precisely to make profits.

The only real solution -- one that replaces the profit motive with actually
keeping us healthy and operates in most civilized nations on the planet -- has
already been taken off the negotiating table by our government-corporate
plutocracy, and that is a single-payer system.

Tom Orange Cleveland, Sept. 4, 2009

To the Editor:

David Brooks is right to call attention to the urgent need to ''bend the curve''
in health care, and he highlights many excellent ideas to do so. But I don't
agree that President Obama has ignored these issues.

President Obama has been extremely forceful in his advocacy for serious delivery
system reform. The bills pending in Congress have many features that help get us
there. The impediment has come from extremists who decry mythical ''death
panels'' and make inherently untenable pledges never to reform Medicare.

Any serious commitment to health care reform across the political spectrum would
lead to the changes Mr. Brooks calls for and more.

David M. Cutler Cambridge, Mass., Sept. 4, 2009

The writer is a professor of applied economics at Harvard University.

To the Editor:

Re ''Obama to Speak Before Congress on Health Care'' (front page, Sept. 3):

Jettisoning the public option, as even some Democrats suggest, would kill the
cost-saving part of a health care bill. If insurers are going to be obliged to
cover pre-existing conditions and insure all who apply, they are going to raise
the cost of insurance substantially.

Democrats should not allow the public option to be tarred as socialist. Public
education, by comparison, is socialized.

The public option, which could make a reasonable profit that could help
Medicare, would insure an honest price for your health care needs.

Sebastian L. Muccilli Lake Park, Fla., Sept. 3, 2009

To the Editor:

Robert Gibbs, the White House spokesman, told the country that President Obama
was willing to be a one-term president in order to pass a comprehensive health
care reform measure.

In Wednesday's address, Mr. Obama should back up such bluster with a renewed
call for a strong public option.

I, along with many members of a fickle youthful electorate, will be watching.
Remember, Mr. President, you won big in November.

Kyle Thomas Manchester, Conn., Sept. 4, 2009

To the Editor:

Your article indicates that the White House may seek to undermine the
school-based health care provision in health care reform. If the aim of health
care reform is to reduce inequities in access to health care services and
provide more people with a medical home, then school health centers must remain
an integral part of the reform.

School-based health care is an essential component of the health care safety
net. Consider that about 70 percent of school health centers report that more
than half of their school population is eligible for free and reduced-price
school lunches.

School-based health centers have enjoyed bipartisan support because everyone
knows that a healthier child makes for a more successful student.

Congress and the president should be working to expand access to medical homes
by supporting school-based health care. Otherwise, the message to our children
is that the government is foreclosing on their future.

Dara Tom Oakland, Calif., Sept. 3, 2009

The writer is communications director for the California School Health Centers
Association.

To the Editor:

Re ''Roosevelt: The Great Divider,'' by Jean Edward Smith (Op-Ed, Sept. 3):

Professor Smith's article on Franklin D. Roosevelt as divider could not have
been more timely and should be a reminder that Roosevelt's lessons and
strategies are just as applicable this month for health care as they were
earlier in the year for saving the economy.

Roosevelt once compared himself to a quarterback who had to call different
signals from one play to the next as the game changed, and President Obama the
basketball player shouldn't find it too hard to see the analogies in his sport.
The opponents of health care reform have figured out how to stop the
well-advertised and predictable effort at bipartisanship, and it's time to go
with new plays and new tactics.

As Professor Smith suggests, one of those alternatives is called partisanship.
It seems that this generation of Republicans will not bend on health care, so
why not try the aggressive strategy? The president's speech on Wednesday should
be the start.

One might even say if you can't join 'em, beat 'em.

Peter Kovler Washington, Sept. 3, 2009

The writer served as president of the F.D.R. Centennial Committee in 1982 and
chairman of the Committee for Roosevelt History Month in 1996.

URL: http://www.nytimes.com

LOAD-DATE: September 8, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY JORDAN AWAN)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


