                   Copyright 2010 The New York Times Company


                             1814 of 2021 DOCUMENTS


                               The New York Times

                             March 30, 2010 Tuesday
                              Late Edition - Final

What You Need to Know in the First Year

BYLINE: By TARA PARKER-POPE

SECTION: Section D; Column 0; Science Desk; WELL; Pg. 1

LENGTH: 2125 words


More than a week after President Obama signed the sweeping new health care law,
which will eventually provide insurance coverage for 32 million uninsured
Americans, many of us are still scratching our heads. What just happened? And
how and when will we start feeling its effect?

In the long term, the legislation will require most Americans to obtain health
insurance. It will also offer federal subsidies to lower premiums and
significantly expand eligibility for Medicaid.

The changes will mean that 94 percent of legal residents not covered by Medicare
will have health insurance, up from  83 percent now, according to the
Congressional Budget Office.

While the biggest changes will not  take effect until 2014, some important
provisions will begin as early as June, while others will kick in by the end of
the year. These include significant new restrictions on the insurance industry
and new protections for consumers who already have health insurance. There are
also perks for Medicare recipients and help for young adults. And in just 90
days there will be new coverage for people who have lost health insurance and
can't qualify for an individual policy.

''The basic thrust of this law is that all of these nooks and crannies, all
these gaps where private insurance has left you without any option, those are
going to be taken away,'' said DeAnn Friedholm, the campaign director of health
reform for Consumers Union, the nonprofit publisher of Consumer Reports. ''It's
complicated, but it does establish a very key, important policy that you're
going to have options, regardless of your health situation or your employment
situation.''

Some of the specific details will be outlined in the coming weeks by the Health
and Human Services department. However, here are answers to some commonly asked
questions about the health care changes coming within the next year.

Q. I don't have health insurance. How soon will the new law help me?

The answer depends on your age and reasons for not having insurance. If you
haven't had insurance for six months, and you can't afford or don't qualify for
insurance because of a pre-existing medical problem, you may be eligible for a
new federal ''high risk'' pool to be offered by the end of June.

The cost of the monthly premiums hasn't been announced, but the rates are to be
based on a ''standard population,'' suggesting they will be based on a healthier
group than typically used to calculate premiums for high-risk plans. On average,
an enrollee won't pay more than 35 percent of covered benefits, and annual
out-of-pocket costs won't be more than $5,950 for individuals and $11,900 for
families. In addition, there are no lifetime limits -- meaning the policy won't
be canceled if someone requires expensive medical treatment

Q. How many people can sign up for the new plan?

Until national health officials specify the premium costs and exactly what will
or will not be covered, nobody knows how many people can sign up. The $5 billion
set aside by Congress must last until 2014, when other options become available.
By comparison, 35 states already spend a combined total of $2 billion annually
on high-risk insurance pools that cover 200,000 people.

Q. How is the new federal pool different from what is already offered by state
high-risk pools or Medicaid?

The federal plan is expected to offer more-affordable coverage than the existing
state plans and will not  impose the same income restrictions as Medicaid. State
plans also typically impose high deductibles and premiums (some charge as much
as $1,200 a month), and up to 12-month waiting periods before covering
pre-existing health problems.

The experience of April and Steve Kohrherr of Afton, Va., shows how existing
public plans fall short for many families. Their oldest son, Griffin, 6, has
hemophilia, a severe bleeding disorder. His care, which has included brain
surgery for a life-threatening bleed as well as twice-weekly infusions with a
clotting drug, totals $500,000 or more a year.

The high cost of Griffin's care would disqualify him from most state plans.
Adding Griffin to the small group plan at the restaurant where Mr. Kohrherr
works would have increased premiums for all the workers, making it unaffordable
for everyone. Griffin now is covered by Medicaid, but he will lose the benefit
if his family's income exceeds about $40,000. Ms. Kohrherr works part time, but
goes without insurance because the family of four cannot afford the $200 monthly
cost to add her to her husband's policy.

''If anybody was in my shoes and held their kid who was close to death, and if
they had to worry about insurance at that moment, then they would never have
been against this bill,''  Ms. Kohrherr said. ''All of the sudden I feel like I
can think about my child's future without worrying.''

Q. How will the law affect children with pre-existing conditions?

Beginning in September, the new law is expected to stop insurance companies from
rejecting children or excluding coverage because of pre-existing medical
problems. That's what happened to Diane Knight, 52, of Orem, Utah, when she
tried to get health insurance for her 17-year-old daughter.

Although Ms. Knight and her husband had family insurance in the past, they lost
it when they left their jobs to start a small business. When they discovered
that they were unable to get new insurance because both had a past cancer
diagnosis, they sought an individual policy just for their daughter. But she was
rejected, too, because she had used expensive prescription acne cream when she
was younger and the insurance company did not want to pay for that in the
future.

''To deny a perfectly healthy 17-year-old girl, and then for the rest of her
life she has to say, 'Yes I've been denied health insurance' -- that's
unacceptable,'' said Ms. Knight, who returned to teaching public school  to
obtain insurance for her family. ''I'm a conservative Republican, but I have
lived the nightmare of health insurance.''

Since the law passed, insurers have argued that it uses vague language and does
not require them to provide insurance to all children right away. Legislative
experts say that the intent of Congress was clear and that the federal
government will probably write the rules to reflect this, which may lead the
industry to take the battle to court.

Q. Will Medicare recipients receive any immediate benefits?

This year Medicare recipients with high drug costs will get a rebate of up to
$250. And in 2011, the plan will pick up a larger share of brand-name drug
costs. In addition, Medicare recipients won't be charged co-pays or deductibles
for preventive care like immunizations and cholesterol screening.

The drug rebate is the first step in a 10-year plan to close the ''doughnut
hole,'' the gap that occurs because Medicare Part D stops reimbursing for
prescriptions after the first $2,830 in costs a year. The retiree must then pay
all drug costs until they reach $6,440, at which point Part D pays again.

Rosale Bertrand, a 69-year-old Medicare recipient in Salt Lake City, says that
early in the year, her 10 prescriptions for diabetes, high blood pressure,
asthma, ulcers and chronic depression cost her about $200 a month because Part D
covers much of the cost. But the doughnut hole starts in late March or April,
and she must spend $600 a month until Part D kicks in again in the fall, she
says.

To maintain her prescriptions, Ms. Bertrand has twice borrowed against her home
and has maxed out her credit cards. Under the new law, she will save about $250
this year and about $540 next year. ''It's a start,'' she said. ''I was very
relieved anything good could come out of it.''

Q. Will young, healthy adults who don't have insurance be helped by the reforms?

Starting in September, adult children younger than 26 can be added to their
parent's health policy. Some plans already extend coverage to adult dependents
as long as they are full-time students. Although Health and Human Services still
must announce the exact eligibility requirements, Congress deleted a restriction
related to marital status.

''We may see a loosening of requirements around who qualifies as a dependent
child,'' said Jennifer Tolbert, associate director of the Commission on Medicaid
and the Uninsured for the Kaiser Family Foundation. ''When they removed the
requirement that a dependent child didn't have to be unmarried, that was a
signal to say, 'We want this to a apply to a larger group.' ''

The provision will offer some relief to Sarah Lynch, 25, of Austin, Tex., who
was kicked off her parents' plan at 23 when her course work dropped to just two
classes in her final semester. Ms. Lynch has paid about $460 a month to extend
coverage under a provision known as Cobra, but that plan expires in June. She
has been unable to find full-time work with benefits, and her application for
private insurance was rejected without explanation. The new law will give her
about six months of insurance coverage on her parents' plan before she turns 26.
''It gives me a little more time to find a job with benefits,'' she said.

Q. What are the immediate benefits for people who already have insurance?

Beginning in September, insurance companies will no longer be able to rescind a
policy once someone gets sick, nor can they impose lifetime limits on coverage.

Today, honest mistakes on a lengthy insurance application -- like forgetting to
disclose a parent's high blood pressure -- could be grounds for losing your
insurance.

Under the new rules, companies generally can't rescind a policy for a minor
application error. ''The law takes away the incentive for insurance companies to
look for application mistakes,'' said Marian Mulkey, senior program officer with
the California HealthCare Foundation. ''There have been some egregious examples
of someone getting cancer triggering a review of years of health history that
seems very targeted and punitive.''

Patricia Sevchuk of Ewing, N.J., said her daughter Laura scrupulously paid her
Cobra premiums while being treated for late-stage breast cancer. But more than a
month after she died in 2008, the insurance company notified her husband that as
much as $400,000 would no longer be covered because the medical bills had
exceeded a $1 million lifetime cap. Although one oncologist waived her fees
after hearing about the family's plight, other creditors have demanded payment,
and bankruptcy remains a possibility.

''There isn't a day that goes by that I don't think about her and what she went
through,''  Mrs. Sevchuk said. ''To know that as much as we tried to save her,
all it did was cause more anguish to the people who were left -- her husband and
her daughter. It's heartbreaking.''

Q. Won't all these changes increase my health care premiums?

How the changes  will affect existing insurance costs is a source of fierce
debate. Over all, the Congressional Budget Office has said that by 2016, the
provisions in the new law will result in little if any increase in premiums for
people with employer-sponsored plans. People with nongroup plans (those not
offered by employers) may see increases, but more than half the enrollees in
nongroup plans will qualify for federal subsidies, lowering costs for middle-
and moderate-income families on average by about 60 percent, the C.B.O. said.

Beginning in September, insurance firms will face new limits on administrative
costs and executive compensation. Violations will trigger rebates to consumers.
In addition, the overhaul  package includes additional money for states to
review unreasonable increases in insurance rates.

''Middle- and moderate-income families will have tremendous help in the
pocketbook as a result of federal subsidies that will significantly lower the
out-of-pocket burden,'' said Ron Pollack, executive director of the consumer
health group Families USA. ''And there is now a process where the federal
government as well as states will review premium increases to determine their
reasonableness.''

Q. Will small-business owners notice any immediate benefits?

This year tax credits as high as 35 percent of premiums will be available to
many small businesses that offer health coverage to employees. Dale B. Cole Jr.,
co-owner and chief financial officer of Consolidated Trailers in Baltimore,
said that he did not yet know how the legislation would  affect him but that he
hoped it would ease his costs and allow him to shop around for better coverage.

''We've used mirrors and smoke to try to get the premiums to where they're
bearable,'' he said. ''We went from a full plan with a small deductible and
great prescription coverage to a plan now that is basically a high-deductible
plan. It ain't much.''

URL: http://www.nytimes.com

LOAD-DATE: March 30, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: DALE B. COLE JR., BUSINESS OWNER (PHOTOGRAPH BY MONICA LOPOSSAY
FOR THE NEW YORK TIMES)
APRIL KOHRHERR WITH GRIFFIN, RIGHT (PHOTOGRAPH BY ANDREW SHURTLEFF FOR THE NEW
YORK TIMES)
 SARAH LYNCH, WHO WAS DROPPED FROM HER PARENTS' INSURANCE PLAN (PHOTOGRAPH BY
BEN SKLAR FOR THE NEW YORK TIMES)(D4) DRAWING (DRAWING BY STUART BRADFORD)(D1)

DOCUMENT-TYPE: Question

PUBLICATION-TYPE: Newspaper


