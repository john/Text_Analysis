                   Copyright 2009 The New York Times Company


                              45 of 2021 DOCUMENTS


                               The New York Times

                            June 17, 2009 Wednesday
                              Late Edition - Final

Democrats Work to Pare Cost of Health Care Bill

BYLINE: By DAVID M. HERSZENHORN and ROBERT PEAR; John Harwood contributed
reporting.

SECTION: Section A; Column 0; National Desk; Pg. 15

LENGTH: 750 words

DATELINE: WASHINGTON


Senate Democrats worked feverishly on Tuesday to pare the costs of legislation
to overhaul the health care system, and were considering a reduction in proposed
subsidies to help uninsured Americans buy coverage.

Alarmed by cost estimates from the Congressional Budget Office, leaders of the
Senate Finance Committee delayed releasing a detailed description of their bill
for several days, as they effectively went back to the drawing board to cut the
price tag.

''It's clear there have got to be changes made to make the whole package
affordable,'' said Senator Kent Conrad, Democrat of North Dakota, who is a
member of the finance panel and chairman of the Senate Budget Committee.

Mr. Conrad said he was particularly concerned about the possibility that people
who now have employer-provided insurance would drop it in favor of
government-subsidized coverage, raising the cost to taxpayers.

The rush to trim costs came as Senate Democrats and the White House struggled to
respond to an initial financial analysis by the budget office showing that an
alternate proposal, developed by the Senate health committee, would cost $1
trillion over 10 years but could still leave 36 million Americans uninsured.

The analysis by the nonpartisan budget office showed Democrats falling far short
of their goal, which is to provide insurance to all Americans and offset the
expense of doing so with new taxes or cost savings. Republicans quickly seized
on the figures to charge that the Democrats' efforts would fail.

In addressing the cost issue, Democrats voiced deep internal discord over how to
pay for the legislation, with some pushing to tax employer-provided health
benefits above a preset limit and others urging tax increases outside the health
arena.

Despite the high cost estimate, the health committee said it would proceed on
Wednesday with its first public work on the sweeping legislation.

''Obviously, we thought we'd get better numbers based on earlier work that was
done, but this can't stop you,'' said Senator Christopher J. Dodd, Democrat of
Connecticut, who is leading the health committee's effort.

The finance chairman, Senator Max Baucus of Montana, adopted a more cautious
approach by delaying the description of his bill, which  had been expected
Wednesday.

A Democratic aide said the finance panel's bill would cost less than $1 trillion
over 10 years and would be ''fully paid for.'' The budget office is still
analyzing that measure, but a prior projection put the cost at $1.6 trillion.

As Mr. Baucus worked to lower the cost, he huddled in his office on Tuesday
evening with Senator Charles E. Grassley of Iowa, the senior Republican on the
committee, and Senator Michael B. Enzi of Wyoming, the senior Republican on the
health panel.

The study of the health committee bill provided ammunition to critics, including
the Republican leader, Senator Mitch McConnell of Kentucky. ''The health care
proposal being put together is not only extremely defective, it will cost a
fortune,'' he said.

White House officials pointed out that the health committee bill was just one of
several proposals and sought to keep the focus on President Obama's larger
goals.

Mr. Obama, moving aggressively on the issue, sent out an e-mail message to
supporters to raise money for a grass-roots campaign in support of the health
care legislation. And in an interview with CNBC and The New York Times, Mr.
Obama expressed a willingness to compromise on his call for a new public
insurance plan to compete with private insurers.

Republicans strongly oppose that idea, and some Democrats are pushing an
alternative that would create nonprofit health cooperatives instead.

''We're open-minded,'' Mr. Obama said. ''If, for example, the cooperative idea
that Kent Conrad has put forward, if that is a better way to reduce costs and
help families and businesses with their health care, I'm more than happy to
accept those good ideas.''

But at the Capitol, Democrats openly grappled with the cost issue. Mr. Dodd and
other leaders of the health committee said at a news conference that the
spending analysis did not factor in major components of their bill that have yet
to be made public, including a proposed public insurance plan that they said
would save billions of dollars.

As a practical matter, however, Congress must abide by the budget office
numbers. Senate Democrats plan to expand Medicaid eligibility, which would cover
more people but could also further raise costs.

URL: http://www.nytimes.com

LOAD-DATE: June 17, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Senator Christopher J. Dodd met with health care providers on
Capitol Hill on Tuesday to discuss legislation on a health care overhaul. The
first public work on the bill will start Wednesday.(PHOTOGRAPH BY STEPHEN
CROWLEY/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


