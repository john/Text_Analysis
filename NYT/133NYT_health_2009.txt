                   Copyright 2009 The New York Times Company


                             133 of 2021 DOCUMENTS


                               The New York Times

                              July 12, 2009 Sunday
                         The New York Times on the Web

Health Care for the Elderly

SECTION: Section ; Column 0; Metropolitan Desk; LETTER; Pg.

LENGTH: 185 words


To the Editor:

Re ''The Patients Doctors Don't Know,'' by Rosanne M. Leipzig (Op-Ed, July 2):

A sign of a successful society is how it treats its elders. Given the terrible
shortage of doctors now trained to address the special needs of older people, we
are failing these patients, and ultimately ourselves.

In thinking about health care reform, ways must be sought to attract future
physicians to geriatrics by building incentives like parity of compensation and
decent reimbursement rates, which will make geriatrics a more attractive
professional option.

The Medical Student Training in Aging Research scholarship program, supported by
the John A. Hartford Foundation, the National Institute on Aging and several
other foundations, has attracted more than 1,300 medical students since 1994
because of its innovative approach to geriatric medicine. Students have an
opportunity to conduct research, interact with patients, network and gain
exposure to innovative ideas and insights in geriatrics.

Stephanie Lederman   Executive Director   American Federation    for Aging
Research   New York, July 2, 2009

URL: http://www.nytimes.com

LOAD-DATE: July 12, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


