                   Copyright 2009 The New York Times Company


                             300 of 2021 DOCUMENTS


                               The New York Times

                            August 13, 2009 Thursday
                              Late Edition - Final

10 Steps to Better Health Care

BYLINE: By Atul Gawande, Donald Berwick, Elliott Fisher and Mark McClellan.

Atul Gawande directs the Center for Surgery and Public Health at Brigham and
Women's Hospital in Boston and is a staff writer at The New Yorker; Donald
Berwick is the president of the Institute for Healthcare Improvement in
Cambridge, Mass.; Elliott Fisher directs policy-reform efforts at the Dartmouth
Institute for Health Policy and Clinical Practice; and Mark McClellan is the
director of health care reform policy at the Brookings Institution. All are
physicians.

SECTION: Section A; Column 0; Editorial Desk; OP-ED CONTRIBUTORS; Pg. 27

LENGTH: 1159 words


WE have reached a sobering point in our national health-reform debate. Americans
have recognized that our health system is bankrupting us and that we have dealt
with this by letting the system price more and more people out of health care.
So we are trying to decide if we are willing to change -- willing to ensure that
everyone can have coverage. That means banishing the phrase ''pre-existing
condition.'' It also means finding ways to pay for coverage for those who can't
afford it without help.

Both of these steps stir heated argument, not to mention lobbyists' hearts. But
what creates the deepest unease is considering what we will have to do about the
system's exploding costs if pushing more people out is no longer an option. We
have really discussed only two options: raising taxes or rationing care. The
public is understandably alarmed.

There is a far more desirable alternative: to change how care is delivered so
that it is both less expensive and more effective. But there is widespread
skepticism about whether that is possible.

Yes, many European health systems have done it, but we are not Europe. And
evidence that places like the Mayo Clinic in Minnesota or the Cleveland Clinic
are doing it is likewise dismissed because their unique structures (for example,
their physicians work on salary rather than being paid for each service) make
them seem as far from Middle America as Sweden is.

Yet in studying communities all over America, not just a few unusual corners, we
have found evidence that more effective, lower-cost care is possible.

To find models of success, we searched among our country's 306 Hospital Referral
Regions, as defined by the Dartmouth Atlas of Health Care, for ''positive
outliers.'' Our criteria were simple: find regions with per capita Medicare
costs that are low or markedly declining in rank and where federal measures of
quality are above average. In the end, 74 regions passed our test.

So we invited physicians, hospital executives and local leaders from 10 of these
regions to a meeting in Washington so they could explain how they do what they
do. They came from towns big and small, urban and rural, North and South, East
and West. Here's the list: Asheville, N.C.; Cedar Rapids, Iowa; Everett, Wash.;
La Crosse, Wis.; Portland, Me.; Richmond, Va.; Sacramento; Sayre, Pa.; Temple,
Tex.; and Tallahassee, Fla., which, despite not ranking above the 50th
percentile in terms of quality, has made such great recent strides in both costs
and quality that we thought it had something to teach us.

If the rest of America could achieve the performances of regions like these, our
health care cost crisis would be over. Their quality scores are well above
average. Yet they spend more than $1,500 (16 percent) less per Medicare patient
than the national average and have a slower real annual growth rate (3 percent
versus 3.5 percent nationwide).

Caveat: Because we relied on Medicare data for our selections, it is possible
that some of these regions are not so low-cost from the viewpoint of
non-Medicare patients. But overall data strongly suggest that most of these
regions are providing excellent care for all patients while being far more
successful than others at not overusing or misusing health care resources.

So how do they do that?  Some have followed the Mayo model, with salaried
doctors employed by a unified local system focused on quality of care: these
include Temple, where the Scott and White clinic dominates the market, and
Sayre, where the Guthrie Clinic does. Other regions, including Richmond and
Everett, look more like most American communities, with several medical groups
whose physicians are paid on a traditional fee-for-service basis. But they, too,
have found ways to protect patients against the damaging incentives of a system
that encourages fragmentation of care and the pursuit of revenues over patient
needs.

The physicians and hospital leaders from Cedar Rapids told us how they have
adopted electronic systems to improve communication among physicians and quality
of care. Last year, they decided to investigate the overuse of CAT scans. They
examined the data and found that in just one year 52,000 scans were done in a
community of 300,000 people. A large portion of them were almost certainly
unnecessary, not to mention possibly harmful, as CAT scans have about 1,000
times as much radiation exposure as a chest X-ray.

''I was embarrassed for us,'' said Jim Levett, a cardiac surgeon and the head of
a large physician group. More important, the area's doctors and clinics are
turning that embarrassment into change by seeking out solutions to reduce the
expense and harm of unnecessary scans.

That number of scans in Cedar Rapids may seem shocking, but there is nothing
surprising about it. Nationwide, we do 62 million CAT scans a year for 300
million people. So Cedar Rapids's rate was actually better than average. But all
medicine is local. And until a community confronts what goes on in its own
population -- to the point of actually seeking the data and engaging those who
can solve the problem -- nothing will change.

The team from Portland told us of a collaboration of doctors, state officials,
insurers and community leaders to improve care. For more than four years,
physicians have been tracking some 60 measures of quality, like medication error
rates for their patients, and meeting voluntary cost-reduction goals.

Asheville, after gaining state support to avoid antitrust concerns, merged two
underutilized hospitals. In Sacramento, a decade of fierce competition among
four rival health systems brought about elimination of unneeded beds, adoption
of new electronic systems for patient data and a race to raise quality.
Sacramento also went from being one of America's high-cost areas for health care
to being among the low-cost elite.

In their own ways, each of these successful communities tells the same simple
story: better, safer, lower-cost care is within reach. Many high-cost regions
are just a few hours' drive from a lower-cost, higher-quality  region. And in
the more efficient areas, neither the physicians nor the citizens reported
feeling that care is ''rationed.'' Indeed, it's rational.

Many in Congress and the Obama administration seem to recognize this. The
various reform bills making their way through the process have included
provisions to protect successful medical communities by incorporating payment
approaches that reward those that slow spending growth while improving patient
outcomes. This is the right direction for reform.

There is a lot of troubling rhetoric being thrown around in the health care
debate. But we don't need to be trapped between charges that reforms will ration
care and doing nothing about costs and coverage. We must instead look at the
communities that are already redesigning American health care for the better,
and pursue ways for the nation to follow their lead.

URL: http://www.nytimes.com

LOAD-DATE: August 13, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY JOEL HOLLAND)

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


