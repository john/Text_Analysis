                   Copyright 2009 The New York Times Company


                             895 of 2021 DOCUMENTS


                               The New York Times

                            October 27, 2009 Tuesday
                              Late Edition - Final

Obama's Fund-Raising, In His Best Interest

BYLINE: By JEFF ZELENY

SECTION: Section A; Column 0; National Desk; WHITE HOUSE MEMO; Pg. 18

LENGTH: 710 words

DATELINE: MIAMI BEACH


It has been nearly a year since President Obama logged so many miles on the
road, traveling from city to city and coast to coast. This time, he is not
chasing votes. He is working the fund-raising circuit for Democratic candidates.

When Mr. Obama arrived here  on Monday evening and stepped into a grand ballroom
of the Fontainebleau Hotel for a pair of events, he made his 26th fund-raising
stop since taking office nine months ago. The trip to Florida came after visits
to San Francisco, Boston and New York City -- all within the last 11 days.

Yes, Mr. Obama is embroiled in a health care debate. He is also moving closer to
saying whether he intends to send more troops to Afghanistan. But despite those
tasks, other challenges weigh on the White House: protecting Democrats in
Congress and fighting the curse of history, where the party in power
traditionally loses seats in the midterm elections.

The president urged his supporters to be resilient.

''When we started off,'' he said, ''we didn't think, Boy, this was going to be a
cakewalk, getting a guy named Barack Hussein Obama elected president.''

For politicians, raising money is as commonplace as shaking hands and delivering
speeches. But Mr. Obama has spent far less time on the fund-raising tour than
many others -- and not just because he is fairly new to the national stage.

In his own presidential race, particularly during the primary fight, money began
flowing in through his campaign Web site, sharply reducing the number of
audiences he had to appear before to ask for money. Yet in his new role as the
leader of the Democratic Party, Mr. Obama's advisers have come to the conclusion
that the benefits of raising money -- and keeping his Congressional majorities
-- outweigh any negative optics of attending so many fund-raisers.

''We could have the president here twice a month and we'd have great events,''
said Kirk Wagar, a Miami lawyer who served as a finance chairman of Mr. Obama's
campaign in Florida last year. ''People are very invested in him.''

Mr. Wagar was among the 450 people who gathered Monday evening to see Mr. Obama
in his first visit to Miami since taking office. The back-to-back events for the
party's senatorial and Congressional campaign committees raised $1.5 million,
aides said, and provided the president a chance to be energized by a friendly
audience.

''Just because I'm skinny doesn't mean I'm not tough,'' Mr. Obama said. ''I
don't rattle. I'm not going to shrink back because now is the time.''

The appearance at the Fontainebleau Hotel, a freshly renovated beachside spot
that once was a favorite of Frank Sinatra, Elvis Presley and Dean Martin, drew
far less notice than Mr. Obama's fund-raisers initially did. Back in March, when
he raised money for the first time in office, the White House was peppered with
questions about the propriety of such an event, given the country's unemployment
rate and bleak economy.

Now, it has just become part of Mr. Obama's routine, even though critics say it
stands in contrast to his campaign pledge to change the ways of Washington.
While he does not personally accept contributions from lobbyists or political
action committees, the party's House and Senate committees do, so some of his
rules have little practical effect.

Robert Gibbs, the White House press secretary, defended the robust fund-raising
schedule. He noted that even though George W. Bush had appeared at only six
fund-raising events at this point in his presidency, the campaign finance rules
have changed, and donors can no longer write unlimited checks to political
parties. Now, donations are capped at $30,400, with individual candidates able
to accept $2,400 per election cycle.

Even with the president's help, the Democratic National Committee is nearly at
parity with its Republican counterpart this year. But party officials say
Democrats hold an edge in a majority of the competitive Congressional races.
Still, only twice in the last 75 years has the president's party gained seats in
both the House and Senate in a midterm election.

''The reason you're here tonight is you have more to do,'' Mr. Obama said,
ticking through a list of accomplishments since taking office. ''Governing is
even harder than campaigning.''

URL: http://www.nytimes.com

LOAD-DATE: October 27, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: President Obama greeted supporters Monday at a fund-raising
reception for Democratic Congressional candidates in Miami.(PHOTOGRAPH BY DOUG
MILLS/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


