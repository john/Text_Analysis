                   Copyright 2009 The New York Times Company


                              67 of 2021 DOCUMENTS


                               The New York Times

                              June 22, 2009 Monday
                              Late Edition - Final

Who Pays for Medical Mistakes?

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 20

LENGTH: 1013 words


To the Editor:

Re ''Malpractice and Health Care Reform'' (editorial, June 17):

Medical malpractice lawsuits serve two functions. They compensate patients for
medical costs and lost income when they suffer bad outcomes because of medical
errors. They also encourage doctors and hospitals to avoid these errors. Society
would be better served if these two functions were separated.

Medical and disability insurance for all should cover the economic losses
resulting from unsuccessful procedures whether or not the results stem from
medical errors or other factors.

The cause of the disability has no effect on the needs of the patient. If
hospitals knew that the needs of the patient would be taken care of, they could
focus on how to deliver the best care. Medical review boards or courts could
find effective ways to deal with doctors and hospitals that provide substandard
care.

Until society finds a way to ensure that everyone's medical costs and lost
income are covered, we will not be able to address medical malpractice in a way
that is fair to both patients and doctors.

David Martin

Mount Holly, Vt., June 17, 2009

To the Editor:

Lawmakers should consider abolishing our out-of-date, fragmented medical
malpractice system and replacing it with a system similar to workers'
compensation.

Under such a system, a patient would file a claim with a medical malpractice
compensation agency that would evaluate it and determine the proper award to the
patient. The issue of a doctor's possible malfeasance would not be part of the
process; it would be taken up by a professional board.

Also, there would be no awards for pain and suffering, just compensation for
medical expenses and loss of earnings. The award would not come from the doctor
or a hospital but from a common fund to which all medical providers would
contribute.

Doctors could have the option of not belonging to such a system, provided
patients were made aware of this and of their right to sue for malpractice.

Philip M. Shaw

Mill Valley, Calif., June 17, 2009

To the Editor:

Your editorial says: ''Malpractice claims do drive up insurance premiums paid by
doctors in some high-risk specialties, such as obstetrics and neurosurgery.
Those costs are presumably passed on to patients.''

But most doctors today practice under the price control imposed by managed-care
companies and the government. The only way they can adjust their income in
accordance to their expenses is by increasing the volume of appointments,
working faster, using more shortcuts and potentially making errors -- which
creates a vicious cycle.

This is an unacceptable situation and needs to be confronted by all who desire a
true health care reform plan that will cut costs but preserve quality.

Michael Harel

New York, June 17, 2009

The writer is a doctor.

To the Editor:

The ''medical malpractice crisis'' is a myth ginned up by lobbyists who
represent insurance and health care special interests, with no regard for the
consumers who put their faith in medical providers.

Reforms to our liability system must make patients the priority. Patient safety
initiatives, as independent studies have shown, are the only proven way to
reduce premiums. Guidelines that dictated when a malpractice case could proceed,
on the other hand, would pose significant dangers, including reducing the
quality of care and imposing unfair obstacles to legitimate legal actions. Nor
are caps on damages the answer, as your editorial points out.

A more effective way to improve care, protect victims of malpractice and reduce
the burden on doctors would be to hold health maintenance organizations
accountable for their negligence and the negligence of their doctors. Americans
must have a fair way to get compensation when medical professionals make
mistakes, period. Nicholas Papain

New York, June 17, 2009

The writer is president of the New York State Trial Lawyers Association.

To the Editor:

The reason malpractice is expensive and burdensome is not any unfairness in the
system, but because too many patients are being hurt every day in a system that
has failed to use basic safety checklists that have made injuries rare in other
high-risk industries.

When the hospital industry's own two-year pilot project to improve safety is
called the Five Million Lives Campaign, we can see that the true urgency of
reform lies in safety and quality.

''When the right care is delivered to the right patient at the right time, every
time'' is how Elizabeth A. McGlynn, a RAND Corporation researcher, defined
quality care. And when we achieve that, malpractice lawsuits will no longer be
the ''burden'' that some doctors perceive. Patrick Malone

Chevy Chase, Md., June 17, 2009

The writer is a lawyer and the author of ''The Life You Save: Nine Steps to
Finding the Best Medical Care -- and Avoiding the Worst.''

To the Editor:

Cooperative self-insurance could cut health care costs drastically in two ways.
It could reduce doctors' premiums to the much lower level required to cover only
malpractice awards and not insurance company profits, salaries and reserves. And
it could make all jurors aware that extravagant awards would not come out of
insurance-company profits but from the pockets of their own doctors. Shane Mage

New York, June 17, 2009

To the Editor:

Your editorial concludes by saying that the only way to deter negligence is for
patients to retain the legal right to seek higher damages than have been
offered. A better way to protect patients and reduce negligence is to create
objective report cards for doctors and hospitals, taking into account the risk
profile of the patient population, and then give the public access to this
information.

The report cards should identify ''super docs'' and failing ones. The best
doctors can then serve as mentors. If failing doctors don't improve, their
privileges should be suspended.

We also need registries to establish and monitor the efficacy and complications
of all drugs and devices.

Jerry Frankel

Houston, June 17, 2009

The writer is a doctor.

URL: http://www.nytimes.com

LOAD-DATE: June 22, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY LENNY NAAR)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


