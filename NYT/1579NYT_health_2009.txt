                   Copyright 2010 The New York Times Company


                             1579 of 2021 DOCUMENTS


                               The New York Times

                            February 28, 2010 Sunday
                              Late Edition - Final

Six Hours of Hot Air!

BYLINE: By ROSS DOUTHAT.

Ross Douthat is a columnist for The Times.

SECTION: Section WK; Column 0; Editorial Desk; OP-ED CONTRIBUTOR THE HEALTH CARE
SHOW OF SHOWS; Pg. 12

LENGTH: 768 words

DATELINE: Washington


Maybe you feel a twinge of guilt about having missed President Obama's health
care forum on Thursday. Maybe you wonder if you should have called in sick,
stocked up on popcorn,  printed out a few Congressional Budget Office reports
and tuned in for six hours to do your civic duty.

Let me put your mind at ease. Not since Sarah Palin's ill-starred interview with
Katie Couric has a political event so perfectly anticipated a ''Saturday Night
Live'' satire.

The president himself set the tone of self-parody. Some of Mr.  Obama's critics
have suggested that he can be a  wee bit pedantic, a touch too professorial. Now
they have six hours of videotape to back them up. For the president, this was
less a conversation than a seminar: He lectured and interrupted (''Let me just
finish, Lamar''), started debates and then cut them off, ruled his opponents'
arguments out of order and always gave himself the final word.

His Republican opponents, meanwhile, were out to disprove the notion that they
have no ideas on health care reform. Not so, America, not so! They have two
ideas, malpractice reform and interstate purchasing, which they clung to all day
like Al Gore with his lockbox. Also, they had several piled-high copies of the
lengthy Senate health care bill, and a slogan to go with them: ''Let's start
over from a clean sheet of paper.'' What would end up on that paper? Why,
malpractice reform and interstate purchasing, of course!

Then there were the Congressional Democrats. So often stereotyped as a group of
ineffective bleeding hearts, for whom every political debate is a chance to
relive the civil rights era, they more than lived up to that cliche. There were
rambling remarks from Harry Reid and Nancy Pelosi, health care sob stories from
everyone else, and, courtesy of Tom Harkin of Iowa, an extended analogy between
high-risk insurance pools and racial segregation.

No comedy-sketch atmosphere would be complete, of course, without a dose of Joe
Biden's logorrhea: ''I think it requires a little bit of humility,'' he
ruminated at one point, ''to be able to know what the American people think, and
I don't. I can't. I can't swear I do. I know what I think. I think I know what
they think, but I'm not sure what they think.''

Nor am I, Mr. Vice President. But here's what they should think, based on
Thursday's forum: The Democrats have a health care plan that may turn out very,
very badly, and the Republicans, for all their protestations, don't really have
a plan at all.

The first five hours proved the first point. Even with Professor Obama keeping a
firm rein on the proceedings, the Republicans (especially Jon Kyl of Arizona,
Paul Ryan of Wisconsin and Dave Camp of Michigan, all exceptions to the
self-parodic norm) were able to demonstrate that you don't need to mention
''death panels'' to critique health care reform. You can talk about the bill's
budgetary gimmickry, the burdens of its mandates and the risks involved in
having Washington set prices, define benefits and (eventually) limit care.

In the last hour, though, President Obama finally invited the Republicans to
offer their own ideas -- beyond, yes, tort reform and interstate purchasing --
for covering the uninsured. And the Grand Old Party's representatives lapsed
swiftly into platitudes and filibustering.

This wasn't amusing; it was shameful. Ideas for expanding coverage not only
exist on the political right, they've also been championed by politicians who
were sitting at that table -- by John McCain in the last presidential campaign;
by Lamar Alexander and Chuck Grassley, past co-sponsors of the Wyden-Bennett
bill; by Representative Ryan and Tom Coburn in this year's Patients' Choice Act.
But the Republicans have clearly decided to offer nothing, absolutely nothing,
that could be construed as changing the existing system by more than an iota.

As the forum  wound down, the participants complimented one another on having
such a respectful and substantive conversation. (''Never have so many members of
the House and Senate behaved so well for so long before so many television
cameras,'' Joe Barton of Texas remarked.) Afterward, some commentators acted as
though our elected representatives were to be congratulated just for talking
publicly about policy without falling on their faces.

No congratulations are in order. The forum exemplified why Americans have every
reason to hate Washington right now. The first five hours revealed a majority
party whose health care bill probably deserves to be defeated. But the sixth one
exposed a minority party that deserves to lose as well.

URL: http://www.nytimes.com

LOAD-DATE: February 28, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


