                   Copyright 2010 The New York Times Company


                             1470 of 2021 DOCUMENTS


                               The New York Times

                           February 4, 2010 Thursday
                              Late Edition - Final

Health Official Can't Guarantee Openness in Talks

BYLINE: By ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 18

LENGTH: 690 words

DATELINE: WASHINGTON


Kathleen Sebelius, the secretary of health and human services, told Congress on
Wednesday that she could not guarantee greater openness in negotiations over
legislation to remake the nation's health care system.

''I am not a principal in the negotiations,'' Ms. Sebelius said in testimony
before the Senate Finance Committee. ''Nor is my staff.''

Ms. Sebelius said administration officials provided ''technical support'' to
Congress, but did not control the negotiations.

Her comments came about five hours after President Obama affirmed the need for
openness in efforts by the administration and Congress to finish work on a
health bill intended to expand coverage and rein in costs.

In the rush to pass legislation in December, Senate leaders and White House
officials did much of their work behind closed doors, and Mr. Obama said
Wednesday that the perception of secrecy may have undermined public trust.

''I take some fault for this,'' Mr. Obama said at a meeting with Senate
Democrats. ''At the end of the process, when we were fighting through all these
filibusters and trying to get it done quickly -- so that we could pivot and
start talking about other issues that were so important to the American people
-- some of that transparency got lost. And I think we paid a price for it.''

The chairman of the committee, Senator Max Baucus, Democrat of Montana, said he
was ''very confident'' that Congress would soon pass a comprehensive health
bill, even though the legislation was now stalled.

''We are on the brink of accomplishing real health care reform,'' Mr. Baucus
said. ''I urge my colleagues, on both sides of the aisle and both sides of the
Capitol, not to give up.''

Mr. Obama's budget assumes approval of the legislation. But he and Congressional
Democratic leaders have not said how they intend to achieve that goal, in the
face of continued opposition from Republicans who say the government cannot
afford its existing health care commitments.

Jobs and the economy have replaced health care as Mr. Obama's top priority, but
Ms. Sebelius said passage of the health bill would help create jobs and save
them.

The hearing came as the government issued a report estimating that national
health spending totaled $2.5 trillion last year, or 17.3 percent of the economy.
The health share of the gross domestic product increased 1.1 percentage points
from 2008, the largest one-year increase since the government started keeping
track of such data in 1960.

A major factor in the growth of health spending was the increase in Medicaid
enrollment and Medicaid spending as a result of rising unemployment. As people
lost jobs, they lost private insurance, and many turned to Medicaid.

Also contributing to the increase, federal officials said, was the fact that
many people sought treatment for the H1N1 flu virus.

The federal report, published on the Web site of the journal Health Affairs,
estimates that health spending will climb to $4.5 trillion, or nearly one-fifth
of the economy, by 2019.

Christopher J. Truffer, an actuary at the Centers for Medicare and Medicaid
Services and the main author of the report, predicted that public spending for
doctors' services, hospital care and prescription drugs would grow faster than
private spending in the coming decade.

As a result, Mr. Truffer  said, the public share of total health spending -- 47
percent in 2008 -- is expected to exceed 50 percent by 2012 and then reach 52
percent by 2019.

In the report, Mr. Truffer said, ''we do not consider how any health care reform
legislation might change the projections.''

The Congressional Budget Office has said the health bills passed by the House
and the Senate could reduce federal deficits in the next 10 years. But Richard
S. Foster, the chief Medicare actuary, has said the legislation could also speed
the growth of overall health spending, increasing the 10-year total by $220
billion to $290 billion.

The new report predicts that, under current law, health spending will grow in
the next decade at an average annual rate of 6.1 percent, which is 1.7
percentage points faster than the economy as a whole.

URL: http://www.nytimes.com

LOAD-DATE: February 4, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Kathleen Sebelius, the secretary of health and human services,
before Congress on Wednesday. (PHOTOGRAPH BY AMANDA LUCIDON FOR THE NEW YORK
TIMES)

PUBLICATION-TYPE: Newspaper


