                   Copyright 2010 The New York Times Company


                             1586 of 2021 DOCUMENTS


                               The New York Times

                              March 1, 2010 Monday
                              Late Edition - Final

Partisanship's Influence On Health Bill's Future

BYLINE: By JOHN HARWOOD

SECTION: Section A; Column 0; National Desk; Pg. 18

LENGTH: 862 words


In one crucial respect, Democratic and Republican Congressional leaders
understand each other better than their acrimonious debates would suggest.

Their meeting of the minds is not on health policy, as last week's bipartisan
session made clear. What they share, instead, is a dedication to party unity as
an overriding imperative -- and a relentlessly improving track record of
achieving it.

As they try to govern with President Obama, Democrats recognize in minority
Republicans the same obstructionism they practiced at the expense of President
George W. Bush and his party. ''To be negative is easy, I know that,'' Speaker
Nancy Pelosi told columnists recently in describing Republican tactics. ''That's
how we won the House.''

That is also the flaw in the argument that Democrats would exacerbate
polarization by enacting a comprehensive health care overhaul without Republican
support. Polarization on Capitol Hill has already reached near-perfect levels.

In fact, as Ms. Pelosi and Senator Harry Reid, the majority leader, strain for
enough ''yes'' votes on health care to win, polarization may be their best
friend.

A Growing Trend

When Congress passed President Franklin D. Roosevelt's Social Security plan
during the Great Depression in 1935, large majorities in both parties voted
''yes.''

Three decades later, when President Lyndon B. Johnson won passage of Medicare,
most of the Democratic majority in Congress and half of the Republicans backed
him. When Congress considered the Civil Rights Act of 1964, Senator Mike
Mansfield of Montana, the Democratic majority leader, worked alongside Senator
Everett M. Dirksen of Illinois, the minority leader, to overcome a filibuster by
Democratic Southerners.

Those Democratic and Republican parties no longer exist. The kinds of Southern
Democrats who resisted Johnson's agenda and Northern Republicans who supported
it have switched parties; longstanding differences between liberals and
conservatives are now reinforced by party affiliation, not blurred by it.

That is evident in CQ's tally of voting trends since Dwight D. Eisenhower's
presidency. As social and political turbulence crested in the late 1960s,
Democratic and Republican lawmakers voted with the majority of their party's
caucus slightly more than half the time.

Since then, ''the level of consistency between party identification and ideology
grew across the board,'' the political scientist Gary C. Jacobson observed in a
recent book. Last year, according to CQ, House members and senators sided with
their parties roughly 90 percent of the time.

Mr. Jacobson called that the most profound partisan polarization since before
World War I. The result is a deepening sense of tribal identification that makes
it easier for current Congressional leaders -- Ms. Pelosi, Mr. Reid and their
Republican counterparts, Representative John A. Boehner of Ohio and Senator
Mitch McConnell of Kentucky -- to marshal their troops than it was for their
recent predecessors.

The question for Mr. Obama and his party over the next month is whether that
cohesion is enough, in a dismal midterm election year, on an issue that has
thwarted every Democratic president since Franklin Roosevelt.

A Dangerous Prospect

''The speaker's got an enormous task ahead of her,'' cautions Howard Paster, the
top Congressional vote counter for President Bill Clinton. To succeed where Mr.
Clinton failed in 1994, Mr. Paster said, Mr. Obama, his cabinet and
Congressional allies must engage in ''some of the most crass horse-trading in
the world.''

That is a dangerous prospect. The horse-trading that won over Democrats earlier
in the debate -- most notably financial favors that Senator Ben Nelson of
Nebraska got for his home state -- provoked a backlash.

But the deepening partisan solidarity since even Mr. Clinton's time gives
Democratic leaders an added tool.

In the early 1990s, some conservative Democrats playing main roles on Capitol
Hill held their positions largely by virtue of seniority, said former
Representative Vic Fazio of California, who was once chairman of the House
Democratic Caucus. Today, advancement within both parties is tied more
explicitly to supporting floor votes and raising campaign money.

Thus, when Ms. Pelosi looks for votes among the 39 Democrats who opposed the
health care bill last year, the 14 first-term members must pit re-election
concerns against the impact on their future advancement within the House.
Veterans who lead committees and subcommittees, if blamed for the demise of Mr.
Obama's top domestic priority, could find those positions in jeopardy.

In the age of polarization, however, individual self-interest is only part of
the argument. So are shared objectives from decades of clashes between warring
partisan armies.

''There's an idealistic appeal: this is a party goal that we've been promoting
for 50 years,'' Mr. Fazio said. And ''Republican recalcitrance is really driving
the Democrats to a point where they're going to coalesce.''

In the end, Mr. Fazio predicted, Ms. Pelosi and Mr. Reid will find the
majorities they seek. He added, ''I also think that if they fail, it was worth
the try.''

URL: http://www.nytimes.com

LOAD-DATE: March 1, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Senator Harry Reid and Speaker Nancy Pelosi last week at
President Obama's health care meeting. The polarized Capitol may help in their
search for ''yes'' votes on the bill. (PHOTOGRAPH BY JASON REED/REUTERS)

PUBLICATION-TYPE: Newspaper


