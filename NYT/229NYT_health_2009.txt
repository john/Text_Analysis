                   Copyright 2009 The New York Times Company


                             229 of 2021 DOCUMENTS


                               The New York Times

                             July 28, 2009 Tuesday
                              Late Edition - Final

As Special Interests Aid Dodd, He Distances Himself From Them

BYLINE: By RAYMOND HERNANDEZ

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 18

LENGTH: 1026 words


As Senator Christopher J. Dodd of Connecticut assumes a central role in the
debate over health care, the pharmaceutical industry has helped finance efforts
to bolster his image back home as he braces for a potentially bruising
re-election contest.

The industry's campaign-style push for Mr. Dodd, part of a larger effort to
highlight the work of certain lawmakers around the country, portray him as a
defender of ordinary citizens in brochures sent to more than 100,000 homes in
Connecticut and in a 30-second television spot that ran for three weeks.

For Mr. Dodd, the support provided by the Pharmaceutical Research and
Manufacturers of America, or PhRMA, the industry's lobbying arm, comes at a
politically sensitive, if not awkward, time. He is trying to combat a perception
that he has become too close to powerful interest groups in Washington after 28
years in the Senate.

As part of that effort, Mr. Dodd's own campaign has produced two videos,
''Lobbyists Cry'' and ''The Blues,'' presenting him as a politician who has
caused grief for Washington lobbyists. He also sent a fund-raising solicitation
asserting that lobbyists do not have access to him.

''The lobbyists can't get meetings with Chris,'' the solicitation says. ''He
won't return their phone calls. He even yells at them during hearings.''

But even as Mr. Dodd attempts to distance himself from these special interests,
he is clearly relying on their help as he prepares for his re-election, a
reality seized upon by his Republican critics.

He has not only benefited from the hundreds of thousands of dollars in
advertisement courtesy of the pharmaceutical industry and Families U.S.A., a
health-care advocacy group the industry teamed up with. But a few weeks ago, Mr.
Dodd attended a $1,500-a-plate campaign fund-raiser sponsored by lobbyists
representing U.S. Oncology, a provider of cancer drugs and services.

Mr. Dodd's aides say he has often opposed legislation sought by the
pharmaceutical industry and other groups that have contributed to him. Jay
Howser, Mr. Dodd's campaign manager, also said the senator had nothing to do
with the decision to run the ads and described PhRMA's partner in the ad
campaign, Families U.S.A., as a liberal group.

''We have no control over what these groups do,'' he said.

The efforts drug companies have undertaken to promote -- and, presumably, to
curry favor with -- Mr. Dodd underscore the major stake they have in the debate
in Congress over how to pay for coverage of the uninsured.

PhRMA has pledged to provide $80 billion over 10 years to help cover the costs
of the health-care overhaul. But powerful House members do not think the drug
makers have given enough.

Other lawmakers also benefited from the ad campaign. The drug industry, along
with Families U.S.A., also ran commercials praising a few dozen other members of
Congress, largely from states where drug companies have a large presence,
according to PhRMA.

But Mr. Dodd, a senior Health Committee member, is no ordinary lawmaker. He has
assumed the leading role on the committee that his friend, Senator Edward M.
Kennedy, had until he was stricken by a brain tumor.

''Obviously, Senator Dodd is a player in the health-care reform debate,''
acknowledged Ken Johnson, a PhRMA spokesman. ''He also represents a state where
our companies have a large economic footprint.''

The organization would not say how much was spent on the television spot and
brochures promoting Mr. Dodd. But according to the research firm TNS Media
Intelligence, which tracks advertising spending, it cost about $187,000 to run
the television spots from May 30 to June 22. The brochure mailings, in turn,
cost at least $40,000, according to political consultants who are not involved
in the race.

The support Mr. Dodd has received from PhRMA comes at a crucial time politically
for him, with polls showing voters in his home state disapproving of his
performance. The group's campaign coincided with an advertising blitz Mr. Dodd
undertook from May 28 to July 2 at a cost of nearly $500,000, according to his
campaign.  Rob Simmons, a former Republican congressman, is expected to
challenge Mr. Dodd in the November 2010 election.

Mr. Dodd's problems stem in part from the view among some voters that he has
developed cozy ties with the corporations he is supposed to oversee in his
capacity as a senior member of several committees with jurisdiction over the
financial, health care and other industries.

In some ways, he is to blame for this perception.

In March he faced a firestorm over his support for a measure that would serve to
exempt American International Group, a big campaign contributor of his, from
Congressional efforts to limit some executive compensation packages to Wall
Street firms that received federal bailout money. After initially denying that
he was behind the measure, he acknowledged that his staff introduced it at the
urging of the Obama administration.

That came only months after he was accused of receiving preferential treatment
from Countrywide Financial Corporation, which assigned him to a V.I.P. program
in 2003 when he refinanced mortgages on his homes in Connecticut and Washington.
Mr. Dodd said that he did not believe that he received preferential rates,
however.

Mr. Dodd recently went to Martha's Vineyard for a retreat organized by the
Democratic Senatorial Campaign Committee, where 30 senators mingled with party
donors, including lobbyists.

Over his decades  in Congress Mr. Dodd has raised more than $550,000 from drug
company representatives, according to the Center for Responsive Politics.

In addition, Mr. Dodd's wife, Jackie Clegg, was paid nearly $80,000 as a member
of the board of Cardiome Pharma Corporation, according to the documents most
recently filed with the Securities and Exchange Commission. Ms. Clegg also holds
more than 200,000 shares in Javelin Pharmaceuticals, where she is also a board
member.

Colleen Flanagan, a Dodd spokeswoman, said Ms. Clegg consulted an ethics lawyer
to see if the board positions posed a conflict of interest given her husband's
Senate role. ''Her career is entirely her own,'' she added.

URL: http://www.nytimes.com

LOAD-DATE: July 28, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: A video for Senator Dodd that extols how he treats lobbyists.

PUBLICATION-TYPE: Newspaper


