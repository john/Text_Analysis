                   Copyright 2010 The New York Times Company


                             1371 of 2021 DOCUMENTS


                               The New York Times

                            January 18, 2010 Monday
                              Late Edition - Final

Reliable, Liberal Massachusetts? Think Again

BYLINE: By MICHAEL COOPER

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1209 words

DATELINE: MARLBOROUGH, Mass.


Angela Grenham, 50, was raised a Massachusetts Democrat -- ''We had the crucifix
and the J.F.K. sign,'' she recalled -- and voted for a Democrat for president as
recently as 2000, when she supported Al Gore.

But this weekend she stood on a street corner here in the heart of Boston's
politically unpredictable western suburbs holding up a sign for the Republican
running to fill the seat long held by Senator Edward M. Kennedy, Scott Brown,
who has vowed to stop the Democratic health care bill in its tracks.

Cars and pickup trucks honked their approval as they sped by. Ms. Grenham
proudly announced that a group of rivals holding up signs for the Democratic
candidate in Tuesday's special election, Martha Coakley, had given up and left
the other corners of the intersection.

''The response was not great for them,'' said Ms. Grenham, who changed her
registration to independent several years ago as she grew more conservative.

Voters like Ms. Grenham, who said her activism was driven by concerns about
taxes, immigration, national security and health care, have helped make this
crucial race too close to call, endangering the Democrats' 60-vote supermajority
in the Senate and President Obama's health care overhaul. Mr. Obama came to
Boston on Sunday in hopes of rescuing Ms. Coakley's faltering candidacy.
''Understand what's at stake here, Massachusetts,'' Mr. Obama said.

Marlborough is the kind of place where many Massachusetts elections are won and
lost these days. Democrats outnumber Republicans by three to one in the state,
but independent voters now outnumber them both: a majority of the state's voters
are no longer members of either party. And the independent vote is particularly
strong in places like Marlborough, a small city in the crescent-shaped swath of
suburbs surrounding Boston, whose swing voters have tipped the balance one way
or the other in several recent statewide elections. Those voters could prove
crucial again on Tuesday.

The lingering economic downturn and unease about the Democrats' muscular return
to government power have left many voters here in a sour mood.

''It's just tax, tax, tax, and I think the people are just getting sick of it,''
said Richard Gasparoni, 57, an independent who was holding up a Brown sign at
another intersection here along Main Street.

Mr. Gasparoni, who has lived here all his life and works as a tax manager for a
medical device company, said that he had never campaigned for anyone before, but
that he was moved to act because he was upset about the state's decision to
raise its sales tax, was leery of the health bill in Congress and was fed up
with the scandals involving several Democratic state lawmakers.

''I think people have had enough,'' Mr. Gasparoni said.

States do not get much more Democratic than Massachusetts. Democrats hold every
statewide office and control both houses of the legislature with lopsided
majorities. The state's entire Congressional delegation is Democratic.

But Massachusetts does not always live up to its national stereotype as a
bastion of liberalism. Yes, it was the only state to vote for George McGovern
for president in 1972, but it also voted twice for Ronald Reagan. Democratic
enrollment has fallen from 48 percent of the electorate in 1984 to 37 percent
last year. And thanks largely to votes from independent voters in the suburbs,
Massachusetts was led by Republican governors for 16 straight years, until Gov.
Deval Patrick, a Democrat, broke the streak with his 2006 landslide election.
Now Mr. Patrick is dealing with slipping approval ratings as he seeks
re-election.

In tough times like these, the political hegemony of the Democrats has its
risks.

''The Democrats are controlling both Washington and Beacon Hill,'' said Joseph
Malone, a Republican who served as the state treasurer during the 1990s. ''So if
I'm angry as hell about what's going on with our government, who am I going to
throw out? Who am I going to vote against? That's a big part of this race.''

Several political analysts said that it was still possible -- some said it was
even likely -- that Ms. Coakley, the state's attorney general, would win on
Tuesday, particularly if Mr. Obama's visit helps turn out the Democratic
faithful in strongholds like Boston and if Ms. Coakley is able to hold down Mr.
Brown's margins among independents.

But independent, suburban voters in several other parts of the Northeast voted
Republican in November after trending Democratic for years, ousting Gov. Jon
Corzine of New Jersey, a Democrat, and unexpectedly returning Nassau County, on
Long Island, to Republican rule.

The possibility of a voter revolt in Massachusetts took many Democrats by
surprise: local officials were overwhelmingly re-elected last year. Some worry
now that they were too complacent in this race. Special elections like this one
usually have small turnouts, but the January blizzard of campaign commercials --
it is not unusual to see three or four in a row during local newscasts -- has
whipped up interest in the race.

Many of the advertisements are negative, and a sizable portion of them are being
paid for by interest groups trying to either pass or block the health care bill.

Several independent voters said they wanted to elect Mr. Brown to block the
health care bill, which they denounced as full of deals for special interests --
though several said they thought Massachusetts' law extending near-universal
coverage, one of the models for the national bill, had been largely a success.

''It's not perfect, but why should we have to pay again when we have health
care?'' said Ms. Grenham, who works as a physical therapist.

Massachusetts was not hit quite as hard as other parts of the country during the
recession -- its unemployment rate in November was 8.8 percent, below the
national rate of 10 percent -- but there has been no shortage of pain and
economic anxiety. Voters have grown restive: a poll released last week by the
Suffolk University Political Research Center found that 55 percent of
respondents said the state was on the wrong track, up from 44 percent in April
2007.

Sitting over his coffee at the counter of Main Street Cafe, Michael Corbett, 64,
an independent, said he was supporting Mr. Brown based largely on national
security issues. Mr. Corbett, a veteran, was already upset by the Obama
administration's plans to try some terrorism suspects in civilian courts; he
said he was appalled when Ms. Coakley suggested in a debate last week that the
terrorists were gone from Afghanistan.

''Her answers on homeland security the other day were a joke,'' he said. ''She
had no idea. God almighty, there's no more terrorists in Afghanistan?''

At his wife's beauty parlor in Framingham, he said, a clientele that had largely
supported Mr. Obama last year was now uneasy. And he said that in towns where
Obama-Biden lawn signs sprouted just over a year ago, he now saw signs
supporting Brown.

''That doesn't mean that they're going to get out and vote,'' said Mr. Corbett,
who has a business selling industrial maintenance and repair supplies. ''But the
signs are out there. It's certainly a difference. The Obama-Biden signs are
gone, and I just think the people are sick of this.''

URL: http://www.nytimes.com

LOAD-DATE: January 18, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: President Obama headlined a Martha Coakley rally on Sunday.
(PHOTOGRAPH BY DOUG MILLS/THE NEW YORK TIMES) (A1)
 Angela Grenham is among the independent voters supporting a Republican for a
Senate seat. (PHOTOGRAPH BY GRETCHEN ERTL FOR THE NEW YORK TIMES) (A13)

PUBLICATION-TYPE: Newspaper


